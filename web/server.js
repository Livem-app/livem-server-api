
const Hapi = require('hapi');
const Server = new Hapi.Server();
const logger = require('winston');
const config = require('../config')
const db = require('../models/mongodb');
const middleware = require('./middleware');
const Auth = require('./middleware/authentication.js');
const amqpConn = require('../models/rabbitMq');


//cluster
const cluster = require('cluster');
const http = require('http');
const numCPUs = require('os').cpus().length;
var ipc = require('node-ipc');
var fork = require('child_process').fork;


if (cluster.isMaster) {
    const redisEvent =require('../models/redis/redisEventListner')
    logger.info(`Master ${process.pid} is running`);

    // Fork workers.
    for (let i = 0; i < numCPUs; i++) {
        cluster.fork();
        logger.info(`Forking process number ${i}...`);
    }

    // Listen for dying workers
    cluster.on('exit', function (worker) {
        // Replace the dead worker,
        // we're not sentimental
        logger.info(`worker ${worker.process.pid} died`);
        cluster.fork();

    });

} else {


    Server.connection({
        port: config.server.port, routes: {
            cors: true
        }
    }); 
    /* +_+_+_+_+_+_+_+_+_+_+ Plugins / Middlewares +_+_+_+_+_+_+_+_+_+_+ */
    Server.register(
        [
            middleware.good,
            middleware.swagger.inert,
            middleware.swagger.vision,
            middleware.swagger.swagger,
            middleware.auth.HAPI_AUTH_JWT,
        ], function (err) {
            if (err)
                Server.log(['error'], 'hapi-swagger load error: ' + err)

            else
                Server.log(['start'], 'hapi-swagger interface loaded')
        });

    Server.auth.strategy('providerJWT', 'jwt', middleware.auth.providerJwt);

    Server.auth.strategy('customerJWT', 'jwt', middleware.auth.customerJwt);

    Server.auth.strategy('dispatcherJWT', 'jwt', middleware.auth.dispatcherJWT);

    Server.auth.strategy('refJwt', 'jwt', middleware.auth.refreshJwt);

    Server.auth.strategy('auth', 'jwt', middleware.auth.providerJwt);

    Server.route(require('./router'));
    Server.route(require('../library/mailgun/webhook'));
    Server.route(require('../library/twilio/webhook'))
   


}
const turfLibrary = require('../library/turf');
const initialize = () => {
    Server.start(() => { 
        logger.info(`Server is listening on port `, config.server.port)
        db.connect(() => { });//create a connection to mongodb
        turfLibrary.readCityZone(() => { });
        amqpConn.connect(() => { });
    });// Add the route
}


module.exports = { initialize };