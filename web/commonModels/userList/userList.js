var userListCollection = require("../../../models/userList");
var ObjectId = require("mongodb").ObjectID;

class userList {

    /**
     * 
     * @param {*} _idInString 
     * @param {*} firstName 
     * @param {*} lastName 
     * @param {*} pushToken 
     * @param {*} profilePic 
     * @param {*} countrycode 
     * @param {*} userType 
     * @param {*} firebaseTopic 
     */
    createUser(_idInString, firstName, lastName, profilePic, countrycode, phone, userType, firebaseTopic, deviceType) {
        let dataToInsert = {
            _id: ObjectId(_idInString),
            firstName: firstName,
            lastName: lastName,
            profilePic: profilePic,
            countrycode: countrycode,
            phone: phone,
            userType: userType,
            firebaseTopic: firebaseTopic,
            deviceType: deviceType
        }; 
        userListCollection.Insert(dataToInsert, () => { });
    }

    /**
     * 
     * @param {*} _idInString 
     * @param {*} dataAsObject 
     */
    updateUser(_idInString, dataAsObject) {
        userListCollection.UpdateById(_idInString, dataAsObject, () => { })
    }


    /**
     * 
     * @param {*} _idInString 
     */
    deleteUser(_idInString) {
        userListCollection.Delete({ _id: ObjectId(_idInString) }, () => { })
    }

    getPushToken(_idInString, callback) {
        userListCollection.SelectOne({ _id: ObjectId(_idInString) }, (err, result) => {
            let pushToken = "No have Push || user in DB";
            if (result && result.pushToken != null) {
                pushToken = result.pushToken;
            }
            return callback({ pushToken: pushToken });
        })
    }

}


module.exports = new userList();