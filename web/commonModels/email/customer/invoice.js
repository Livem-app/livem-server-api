'use strict';

const email = require('../email');
const img = require('../imageUrl');

module.exports.invoice = (params) => {
    let bookingData = params.bookingData;
    let providerData = bookingData.providerData;
    let accounting = bookingData.accounting;
    let bookingType = bookingData.bookingType == 1 ? 'Now' : 'Schedule';
    let paymentMethod = bookingData.paymentMethod == 1 ? 'Cash' : 'Card';

    var myvarData = '<html lang="it">' +
    '<html>' +
    '' +
    '<body>' +
    '  <table style="background-color:#d6d6d5;border:none;border-collapse:collapse;border-spacing:0;width:100%" width="100%" cellspacing="0"' +
    '  cellpadding="0" border="0" bgcolor="#d6d6d5">' +
    '  <tbody>' +
    '    <tr>' +
    '      <td align="center">' +
    '        <table style="border:none;border-collapse:collapse;border-spacing:0;max-width:700px;width:100%" width="100%" cellspacing="0"' +
    '        cellpadding="0" border="0">' +
    '        <tbody>' +
    '          <tr>' +
    '            <td style="background-color:#ffffff" align="center">' +
    '              <table style="border:none;border-collapse:collapse;border-spacing:0;max-width:700px;width:100%" cellspacing="0" cellpadding="0"' +
    '              border="0">' +
    '              <tbody>' +
    '                <tr>' +
    '                  <td style="background-size:cover;" valign="bottom" bgcolor="" align="center">' +
    '                    <div>' +
    '                      <table style="border:none;border-collapse:collapse;border-spacing:0;width:100%" cellspacing="0" cellpadding="0" border="0">' +
    '                        <tbody>' +
    '                          <tr>' +
    '' +
    '                          </tr>' +
    '                        </tbody>' +
    '                      </table>' +
    '                    </div>' +
    '                  </td>' +
    '                </tr>' +
    '              </tbody>' +
    '            </table>' +
    '            <table style="border:none;border-collapse:collapse;border-spacing:0;margin:0 auto;max-width:700px;width:100%" cellspacing="0"' +
    '            cellpadding="0" border="0">' +
    '            <tbody>' +
    '              <tr>' +
    '                <td style="padding:0;margin:0;background-color:#ffffff" align="center">' +
    '                  <table style="border:none;border-collapse:collapse;border-spacing:0;width:100%" cellspacing="0" cellpadding="0" border="0">' +
    '                    <tbody>' +
    '                      <img style="" src="https://s3.amazonaws.com/livemapplication/email/Hero_mail_600x400.jpg">' +
    '                    </tbody>' +
    '                  </table>' +
    '                </td>' +
    '              </tr>' +
    '            </tbody>' +
    '          </table>' +

        // STARTING HERE - This is different, check
        '      <table border="0" cellpadding="0" cellspacing="0" width="100%" id="ko_twocolumnBlock_6">' +
        '         <tbody>' +
        '            <tr class="row-a">' +
        '               <td bgcolor="#f9f9f9" align="center" class="section-padding" style="padding-top: 0px; padding-left: 15px; padding-bottom: 0px; padding-right: 15px;">' +
        '                  <table border="0" cellpadding="0" cellspacing="0" width="700" class="responsive-table">' +
        '                     <tbody>' +
        '                        <tr>' +
        '                           <td>' +
        '                              <!-- TWO COLUMNS -->' +
        '							  <table cellpadding="0" cellspacing="0" border="0" width="700" align="left" class="responsive-table" style="background:#fff;padding:2% ;border: 1px solid #e6e3e3;">' +
        '								<tbody><tr><td style="font-size: 28px; letter-spacing:1px; padding: 10px;font-family: fantasy; font-weight: 400;width:47%">$' + accounting.amount + '</td>' +
        '								    <td class="mobile_disturb" style="float: right; color: #b2b2b2; margin-top: 18px; margin-right: 10px;font-family: sans-serif;letter-spacing: 1px;font-size: 13px;">' + params.bookingDate + '</td>' +
        '									' +
        '								</tr>' +
        '							  </tbody></table>' +
        '							' +
        '                              <table cellspacing="0" cellpadding="0" border="0" width="100%">' +
        '                                 <tbody>' +
        '                                    <tr>' +
        '                                       <td valign="top" style="padding: 2%;background:#fff; border: 1px solid #e6e3e3; border-top: 0px;" class="mobile-wrapper">' +
        '                                          <!-- LEFT COLUMN -->' +
        '                                          <table cellpadding="5" cellspacing="0" border="0" width="48%" align="left" class="responsive-table" style="background:#fff; border: 1px solid #e6e3e3;">' +
        '                                             <tbody>' +
        '                                               ' +
        '												<tr class="bottom_border">' +
        '												  <td colspan="3" id="app_det">' +
        '												    Booking  details' +
        '												  </td>' +
        '												</tr>' +
        '												<tr class="bottom_border">' +
        '												  <td  colspan="3">' +
        '												    <p class="left_col_head">BOOKING ID</p>' +
        '													<span class="left_col_field adjust">' + bookingData.bookingId + '</span>' +
        '												  </td>' +
        '												</tr><tr class="bottom_border">' +
        '' +
        '												  <td  colspan="3">' +
        '												    <p class="left_col_head">START TIME</p>' +
        '													<span class="left_col_field adjust">' + params.startTime + '</span>' +
        '												  </td>' +
        '                                                  </tr><tr class="bottom_border">' +
        '' +
        '												  <td  colspan="3">' +
        '												    <p class="left_col_head">END TIME</p>' +
        '													<span class="left_col_field adjust">' + params.endTime + '</span>' +
        '                                                  </td>' +
        '                                                  ' +
        '                                                </tr>' +
        '                                                <tr class="bottom_border">' +
        '                                                    <td colspan="3">' +
        '                                                      <p class="left_col_head">TYPE OF ARTIST</p>' +
        '                                                      <span class="left_col_field">' +
        '                                                                    ' + params.category +
        '                                                      </span>' +
        '                                                    </td>' +
        '                                                  </tr>' +
        '												<tr class="bottom_border">' +
        '												  <td colspan="3">' +
        '												    <p class="left_col_head">JOB LOCATION</p>' +
        '													<span class="left_col_field">' + bookingData.addLine1 +
        '                                                                                                        ' +
        '													</span>' +
        '												  </td>' +
        '												</tr>' +
        '												<tr class="bottom_border">' +
        '												  <td colspan="3">' +
        '												    <p class="left_col_head">PROVIDER DETAILS</p>' +
        '													<span><img src="' + providerData.profilePic + '" style="margin-bottom:15px;float:left;margin-right:5px;height:70px;width:70px;border-radius:50px"></span><span class="left_col_field name_section" style="margin-left:0px">' +
        '													<span class="class_name">' + providerData.firstName + ' ' + providerData.lastName + '  <br>' +
        '													<span class="class_name">' + providerData.phone +
        '													</span>' +
        '												  </span></span></td>' +
        '												</tr>' +
        '											    <tr>' +
        '												  <td colspan="3">' +
        '												    <p class="left_col_head" style="text-align:center">Issued on behalf of ' + providerData.firstName + ' ' + providerData.lastName + ' </p>' +
        '												  </td>' +
        '												</tr>' +
        '                                             </tbody>' +
        '                                          </table>' +
        '                                          <!-- RIGHT COLUMN -->' +
        '                                          <table cellpadding="5" cellspacing="0" border="0" width="47%" align="right" class="responsive-table">' +
        '                                             <tbody>' +
        '											  <tr>' +
        '											    <td class="rt_col_head" colspan="3">RECEIPT</td>' +
        '											  </tr>' +
        '                                                <tr>' +
        '												  <td class="rt_col_head2">' +
        '												   BOOKING TYPE' +
        '												  </td>' +
        '												  <td class="align-rt">' + bookingType + '' +
        '												  </td>' +
        '                                                </tr>' +
        '												<tr>' +
        '                                                                                                  </tr>' +
        '                                                <tr class="bottom_border">' +
        '                                                    <td class="bold_txt" style="font-size:17px">' +
        '                                                      <span> SERVICE NAME' +
        '                                                    </td>' +
        '                                                    <td class="bold_txt" style="font-size:15px;">' +
        '                                                       PRICE' +
        '                                                    </td>' +
        '                                                  </tr>' +
        '                                                <tr>' +
        '												  <td class="rt_col_head2" >' +
        '                                                   ' + params.serviceName +
        '												  </td>' +
        '												  <td class="align-rt"  >' +
        '                                                     ' + params.currencySymbol + ' ' + params.servicePrice + '' +
        '												  </td>' +
        '                                                </tr>' +
        '												<tr>' +
        '												  <td class="rt_col_head2">' +
        '												  ARTIST DISCOUNT' +
        '												  </td>' +
        '												  <td class="align-rt">' +
        '                                                    ' + params.currencySymbol + ' ' + params.accounting.discount +
        '												  </td>' +
        '                                                </tr>' +
        '												<tr>' +
        '												  <td class="rt_col_head2">' +
        '                                                   APP DISCOUNT' +
        '												  </td>' +
        '												  <td class="align-rt">' +
        '                                                     ' + params.currencySymbol + ' 00.00' +
        '												  </td>' +
        '                                                </tr>' +
        '												' +
        '												' +
        '											' +
        '												<tr style="">' +
        '												  <td class="rt_col_head2" style="font-size: 10px;line-height: 9px;margin-bottom: 0px;">CHARGED</td>' +
        '                                                </tr>' +
        '												<tr class="bottom_border">' +
        '												  <td class="bold_txt" style="font-size:17px">' +
        '												    <span><img src="http://iserve.ind.in/pics/pp/cash.png" style=" float: left;  padding-top: 2px; margin-right: 5px;"></span>' + paymentMethod + ' ' +
        '												  </td>' +
        '												  <td class="bold_txt" style="font-size:15px;">' +
        '												     ' + params.currencySymbol + ' ' + accounting.amount +
        '												  </td>' +
        '                                                </tr>' +
        '												<tr>' +
        '												   <td>' +
        '												    <p class="left_col_head"> Additional Notes:</p> ' +
        '												   </td>' +
        '												</tr>' +
        '                                             </tbody>' +
        '                                          </table>' +
        '                                       </td>' +
        '                                    </tr>' +
        '                                 </tbody>' +
        '                              </table>' +
        '                           </td>' +
        '                        </tr>' +
        '                     </tbody>' +
        '                  </table>' +
        '               </td>' +
        '            </tr>' +
        '         </tbody>   ' +
        '      </table>' +
        '      <table border="0" cellpadding="0" cellspacing="0" width="100%" id="ko_titleBlock_7">' +
        '         <tbody>' +
        '            <tr class="row-a">' +
        '               <td bgcolor="#f9f9f9" align="center" class="section-padding" style="padding: 30px 15px 15px 15px;">' +
        '                  <table border="0" cellpadding="0" cellspacing="0" width="700" style="padding: 0 0 20px 0;" class="responsive-table" id="end_block">' +
        '                     <!-- TITLE -->' +
        '                     <tbody>' +
        '                        <tr>' +
        '                           <td align="center" class="padding-copy" colspan="2" style="padding: 0 0 10px 0px; color:#000; font-size: 25px; font-family: Helvetica, Arial, sans-serif; font-weight: normal; color: #232323!important;">This is an electronically generated invoice.<br> All Taxes are included.  </td>' +
        '                        </tr>' +
        '                     </tbody>' +
        '                  </table>' +
        '               </td>' +
        '            </tr>' +
        '         </tbody>' +
        '      </table>' +
        '      <!-- FOOTER -->' +
        '' +
        '      <table style="border:none;border-collapse:collapse;border-spacing:0;width:100%;/* background: #f9f9f9; */" width="100%" cellspacing="0" cellpadding="0" border="0">' +
        '        <tbody>' +
        '            <tr>' +
        '                <td style="background-color: #f9f9f9;" align="center">' +
        '                    <table style="border:none;border-collapse:collapse;border-spacing:0;width:700;background: #000;" cellspacing="0" cellpadding="0" border="0">' +
        '                        <tbody>' +
        '                            <tr>' +
        '                                <td align="center">' +
        // STARTING FOOTER HERE - this is different - check
        '              <table style="background-color:#ffffff;border:none;border-collapse:collapse;border-spacing:0;width:100%" width="100%" cellspacing="0"' +
        '              cellpadding="0"' +
        '              border="0">' +
        '              <tbody id="footer-content">' +
        '                <tr>' +
        '                  <td style="padding-top:30px">' +
        '                    <img src="https://s3.amazonaws.com/livemapplication/email/Guitar_text_700x350.jpg" style="clear:both;display:block;height:auto;max-width:700px;outline:none;text-decoration:none;width:100%"' +
        '                    width="700"' +
        '                    border="0"' +
        '                    height=""' +
        '                    tabindex="0">' +
        '                    <div dir="ltr" style="opacity: 0.01; left: 701.5px; top: 1586px;">' +
        '                      <div id=":12b" role="button" tabindex="0" aria-label="Download attachment " data-tooltip-class="a1V" data-tooltip="Download">' +
        '                        <div></div>' +
        '                      </div>' +
        '                    </div>' +
        '                  </td>' +
        '                </tr>' +
        '                <tr>' +
        '                  <td style="padding:55px; text-align: center;"><img src="https://s3.amazonaws.com/livemapplication/email/LiveM_logo.jpg" alt="LiveM Logo"></td>' +
        '                </tr>' +
        '                <tr>' +
        '                  <td style="background: #f43545">' +
        '                    <img src="https://s3.amazonaws.com/livemapplication/email/Footer.png" alt="" style="margin-bottom: -70px;">' +
        '' +
        '                    <div style="padding-top: 16px;' +
        '                      padding-bottom: 15px;' +
        '                      font-size: 15px;' +
        '                      color: #fff;' +
        '                      font-family: \'Arial\';' +
        '                      text-align: center" class="">' +
        '' +
        '                      <p style="margin:5px">@ Libera la Musica Ltd. 2019</p>' +
        '                      <p style="margin:5px">Terms & Conditions  | Help Center </p>' +
        '' +
        '                    </div>' +
        '                  </td>' +
        '                </tr>' +
        '              </tbody>' +
        '            </table>' +
        '          </td>' +
        '        </tr>' +
        '      </tbody>' +
        '    </table>' +
        '  </td>' +
        '</tr>' +
        '</tbody>' +
        '</table>' +
        '</td>' +
        '</tr>' +
        '</tbody>' +
        '</table>' +
        '</td>' +
        '</tr>' +
        '</tbody>' +
        '</table>' +
        '</td>' +
        '</tr>' +
        '</tbody>' +
        '</table>' +
        '' +
        '' +
        '        </div>' +
        '      </td>' +
        '    </tr>' +
        '  </tbody>' +
        '</table>' +
        '</body>' +
        '' +
        '</html>';

    params.subject = 'Invoice for your booking on ' + params.bookingDate + 'on ' + img["AppName"];
    params.html = myvarData;
    params.trigger = img["TRIGGER"]["invoiceForCustomer"];

    email.sendMails(params);
}
