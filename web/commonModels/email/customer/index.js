'use strict';

const signup = require('./signup').signUp;

const invoice = require('./invoice').invoice;

const cancelBooking = require('./cancelBooking').cancelBooking;

const forgotPassword = require('./forgotPassword').forgotPassword;

const expiredBooking = require('./expiredBooking').expiredBooking;

const deActiveByAdmin = require('./deActiveByAdmin').deActiveByAdmin;

const rejectBookingByProvider = require('./rejectBookingByProvider').rejectBooking;

const acceptBookingByProvider = require('./acceptBookingByProvider').acceptBooking;

const passwordUpdatedForCustomer = require('./passwordChange').passwordUpdatedForCustomer;
module.exports = {
    signup,
    invoice,
    cancelBooking,
    forgotPassword,
    expiredBooking,
    deActiveByAdmin,
    rejectBookingByProvider,
    acceptBookingByProvider,
    passwordUpdatedForCustomer,
} 