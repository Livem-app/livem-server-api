'use strict';

const email = require('../email');
const logger = require('winston');
const img = require('../imageUrl');
const jwt = require('jsonwebtoken');
const google = require('googleapis');
const configuration = require('../../../../configuration');
const urlshortener = google.urlshortener({ version: 'v1', auth: configuration.API_KEY });

module.exports.forgotPassword = (params) => {
    const getUrl = (data) => {
        return new Promise((resolve, reject) => {
            let token = jwt.sign({ id: params.id, type: params.userType }, configuration.JWT_KEY, { expiresIn: '900000' });
            let urlLink = configuration.forgotpasswordLink + "??" + token;
            var GOOGLE = require('googleapis');
            var URLSHORTENER = GOOGLE.urlshortener({ version: 'v1', auth: configuration.API_KEY });
            URLSHORTENER.url.insert({ 'resource': { 'longUrl': urlLink } }, (err, result) => {
                if (err) {
                    logger.error("err", err);
                    resolve(urlLink);
                } else {
                    resolve(result.id);
                    logger.info("result", result.id);
                }
            });
        });
    }
    const sendMails = (data) => {
        return new Promise((resolve, reject) => {
          let myvar = '<!DOCTYPE html>' +
          '<html>' +
          '' +
          '<body>' +
          '  <table style="background-color:#d6d6d5;border:none;border-collapse:collapse;border-spacing:0;width:100%" width="100%" cellspacing="0"' +
          '  cellpadding="0" border="0" bgcolor="#d6d6d5">' +
          '  <tbody>' +
          '    <tr>' +
          '      <td align="center">' +
          '        <table style="border:none;border-collapse:collapse;border-spacing:0;max-width:700px;width:100%" width="100%" cellspacing="0"' +
          '        cellpadding="0" border="0">' +
          '        <tbody>' +
          '          <tr>' +
          '            <td style="background-color:#ffffff" align="center">' +
          '              <table style="border:none;border-collapse:collapse;border-spacing:0;max-width:700px;width:100%" cellspacing="0" cellpadding="0"' +
          '              border="0">' +
          '              <tbody>' +
          '                <tr>' +
          '                  <td style="background-size:cover;" valign="bottom" bgcolor="" align="center">' +
          '                    <div>' +
          '                      <table style="border:none;border-collapse:collapse;border-spacing:0;width:100%" cellspacing="0" cellpadding="0" border="0">' +
          '                        <tbody>' +
          '                          <tr>' +
          '' +
          '                          </tr>' +
          '                        </tbody>' +
          '                      </table>' +
          '                    </div>' +
          '                  </td>' +
          '                </tr>' +
          '              </tbody>' +
          '            </table>' +
          '            <table style="border:none;border-collapse:collapse;border-spacing:0;margin:0 auto;max-width:700px;width:100%" cellspacing="0"' +
          '            cellpadding="0" border="0">' +
          '            <tbody>' +
          '              <tr>' +
          '                <td style="padding:0;margin:0;background-color:#ffffff" align="center">' +
          '                  <table style="border:none;border-collapse:collapse;border-spacing:0;width:100%" cellspacing="0" cellpadding="0" border="0">' +
          '                    <tbody>' +
          '                      <img style="" src="https://s3.amazonaws.com/livemapplication/email/Hero_mail_600x400.jpg">' +
          '                    </tbody>' +
          '                  </table>' +
          '                </td>' +
          '              </tr>' +
          '            </tbody>' +
          '          </table>' +

          // STARTING HERE
              '                                        <table style="border:none;border-collapse:collapse;border-spacing:0;margin:auto;max-width:700px;width:100%" width="100%"' +
              '                                               cellspacing="0" cellpadding="0" border="0">' +
              '                                            <tbody>' +
              '                                                <tr>' +
              '                                                    <td align="center">' +
              '                                                        <table style="background-color:#ffffff;border:none;border-collapse:collapse;border-spacing:0;margin:auto;width:100%" width="100%"' +
              '                                                               cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff">' +
              '                                                            <tbody>' +
              '                                                                <tr>' +
              '                                                                    <td align="center">' +
              '                                                                        <table style="border:none;border-collapse:collapse;border-spacing:0;width:100%" width="100%" cellspacing="0" cellpadding="0"' +
              '                                                                               border="0">' +
              '                                                                            <tbody>' +
              '                                                                                <tr>' +
              '                                                                                    <td style="background-color:#ffffff" align="center">' +
              '                                                                                        <table style="border:none;border-collapse:collapse;border-spacing:0;width:100%" width="100%" cellspacing="0" cellpadding="0"' +
              '                                                                                               border="0">' +
              '                                                                                            <tbody>' +
              '                                                                                                <tr>' +
              '                                                                                                    <td>' +
              '                                                                                                        <table style="border:none;border-collapse:collapse;border-spacing:0;width:100%" width="100%" cellspacing="0" cellpadding="0"' +
              '                                                                                                               border="0">' +
              '                                                                                                            <tbody>' +
              '                                                                                                                <tr>' +
              '                                                                                                                    <td>' +
              '                                                                                                                        <table style="border:none;border-collapse:collapse;border-spacing:0;width:100%" width="100%" cellspacing="0" cellpadding="0"' +
              '                                                                                                                               border="0">' +
              '                                                                                                                            <tbody>' +
              '                                                                                                                                <tr>' +
              '                                                                                                                                    <td style="padding:0 14px 0 14px" align="left">' +
              '                                                                                                                                        <table style="border:none;border-collapse:collapse;border-spacing:0;width:100%" cellspacing="0" cellpadding="0" border="0">' +
              '                                                                                                                                            <tbody>' +
              '                                                                                                                                                <tr>' +
              '                                                                                                                                                    <td>' +
              '                                                                                                                                                        <table style="border:none;border-collapse:collapse;border-spacing:0;max-width:672px;width:100%" cellspacing="0" cellpadding="0"' +
              '                                                                                                                                                               border="0">' +
              '                                                                                                                                                            <tbody>' +
              '                                                                                                                                                                <tr>' +
              '                                                                                                                                                                    <td style="height:40px;padding-left:12px;padding-right:12px">' +
              '                                                                                                                                                                        <table style="border:none;border-collapse:collapse;border-spacing:0;table-layout:fixed;width:100%" width="100%" cellspacing="0"' +
              '                                                                                                                                                                               cellpadding="0"' +
              '                                                                                                                                                                               border="0"' +
              '                                                                                                                                                                               align="left">' +
              '                                                                                                                                                                            <tbody>' +
              '                                                                                                                                                                                <tr>' +
              '                                                                                                                                                                                    <td style="font-size:1px;line-height:1px" height="40">' +
              '                                                                                                                                                                                    </td>' +
              '                                                                                                                                                                                </tr>' +
              '                                                                                                                                                                            </tbody>' +
              '                                                                                                                                                                        </table>' +
              '                                                                                                                                                                    </td>' +
              '                                                                                                                                                                </tr>' +
              '                                                                                                                                                            </tbody>' +
              '                                                                                                                                                        </table>' +
              '                                                                                                                                                    </td>' +
              '                                                                                                                                                </tr>' +
              '                                                                                                                                            </tbody>' +
              '                                                                                                                                        </table>' +
              '                                                                                                                                    </td>' +
              '                                                                                                                                </tr>' +
              '                                                                                                                            </tbody>' +
              '                                                                                                                        </table>' +
              '                                                                                                                        <table style="border:none;border-collapse:collapse;border-spacing:0;width:100%" width="100%" cellspacing="0" cellpadding="0"' +
              '                                                                                                                               border="0">' +
              '                                                                                                                            <tbody>' +
              '                                                                                                                                <tr>' +
              '                                                                                                                                    <td style="padding:0 14px 0 14px" align="left">' +
              '                                                                                                                                        <table style="border:none;border-collapse:collapse;border-spacing:0;width:100%" cellspacing="0" cellpadding="0" border="0">' +
              '                                                                                                                                            <tbody>' +
              '                                                                                                                                                <tr>' +
              '                                                                                                                                                    <td valign="top">' +
              '                                                                                                                                                        <table style="border:none;border-collapse:collapse;border-spacing:0;width:100%" cellspacing="0" cellpadding="0" border="0"' +
              '                                                                                                                                                               align="left">' +
              '                                                                                                                                                            <tbody>' +
              '                                                                                                                                                                <tr>' +
              '                                                                                                                                                                    <td style="padding-left:12px;padding-right:12px">' +
              '                                                                                                                                                                        <table style="border:none;border-collapse:collapse;border-spacing:0;table-layout:fixed;width:100%" width="100%" cellspacing="0"' +
              '                                                                                                                                                                               cellpadding="0"' +
              '                                                                                                                                                                               border="0"' +
              '                                                                                                                                                                               align="left">' +
              '                                                                                                                                                                            <tbody>' +
              '                                                                                                                                                                                <tr>' +
              '                                                                                                                                                                                    <td style="color:#000000;font-family:\'ClanPro-Book\',\'HelveticaNeue-Light\',\'Helvetica Neue Light\',Helvetica,Arial,sans-serif;font-size:28px;line-height:36px">' +
              '                                                                                                                                                                                        Hello' +
              '                                                                                                                                                                            <username>' + params.toName + '</username>,' +
              '                                                                                                                                                                    </td>' +
              '                                                                                                                                                                </tr>' +
              '                                                                                                                                                                <tr>' +
              '                                                                                                                                                                    <td>' +
              '                                                                                                                                                                        <table style="border:none;border-collapse:collapse;border-spacing:0;width:100%" width="100%" cellspacing="0" cellpadding="0"' +
              '                                                                                                                                                                               border="0">' +
              '                                                                                                                                                                            <tbody>' +
              '                                                                                                                                                                                <tr>' +
              '                                                                                                                                                                                    <td style="font-size:0px;line-height:0px;padding-bottom:30px">' +
              '                                                                                                                                                                                    </td>' +
              '                                                                                                                                                                                </tr>' +
              '                                                                                                                                                                            </tbody>' +
              '                                                                                                                                                                        </table>' +
              '                                                                                                                                                                    </td>' +
              '                                                                                                                                                                </tr>' +
              '                                                                                                                                                                <tr>' +
              '                                                                                                                                                                    <td style="color:#717172;font-family:\'ClanPro-Book\',\'HelveticaNeue-Light\',\'Helvetica Neue Light\',Helvetica,Arial,sans-serif;font-size:16px;line-height:28px">' +
              '                                                                                                                                                                        Please click below to reset your password on '+img["AppName"]+' ' +
              '' +
              '                                                                                                                                                                        <br>' +
              '                                                                                                                                                                        <br>' +
              '                                                                                                                                                                        "' + data + '"' +
              '                                                                                                                                                                        <br>' +
              '                                                                                                                                                                        <br>                                                                                                                                                                                   ' +
              '                                                                                                                                                                        This link will be active for "15" minutes from the time of receipt of this email. So please make haste in changing your password' +
              '' +
              '                                                                                                                                                                        <br>' +
              '                                                                                                                                                                        <br>                                                                                                                                                                                    Cheers,' +
              '                                                                                                                                                                        <br>                                                                                                                                                                                    LiveM' +
              '                                                                                                                                                                        Team' +
              '                                                                                                                                                                    </td>' +
              '                                                                                                                                                                </tr>' +
              '                                                                                                                                                                <tr>' +
              '                                                                                                                                                                    <td>' +
              '                                                                                                                                                                        <table style="border:none;border-collapse:collapse;border-spacing:0;width:100%" width="100%" cellspacing="0" cellpadding="0"' +
              '                                                                                                                                                                               border="0">' +
              '                                                                                                                                                                            <tbody>' +
              '                                                                                                                                                                                <tr>' +
              '                                                                                                                                                                                    <td style="font-size:0px;line-height:0px;padding-bottom:34px">' +
              '                                                                                                                                                                                    </td>' +
              '                                                                                                                                                                                </tr>' +
              '                                                                                                                                                                            </tbody>' +
              '                                                                                                                                                                        </table>' +
              '                                                                                                                                                                    </td>' +
              '                                                                                                                                                                </tr>' +
              '                                                                                                                                                            </tbody>' +
              '                                                                                                                                                        </table>' +
              '                                                                                                                                                    </td>' +
              '                                                                                                                                                </tr>' +
              '                                                                                                                                            </tbody>' +
              '                                                                                                                                        </table>' +
              '                                                                                                                                    </td>' +
              '                                                                                                                                </tr>' +
              '                                                                                                                            </tbody>' +
              '                                                                                                                        </table>' +
              '                                                                                                                    </td>' +
              '                                                                                                                </tr>' +
              '                                                                                                            </tbody>' +
              '                                                                                                        </table>' +
              '                                                                                                        <table style="border:none;border-collapse:collapse;border-spacing:0;width:100%" width="100%" cellspacing="0" cellpadding="0"' +
              '                                                                                                               border="0">' +
              '                                                                                                            <tbody>' +
              '                                                                                                                <tr>' +
              '                                                                                                                    <td style="padding:0 14px 0 14px" align="left">' +
              '                                                                                                                        <table style="border:none;border-collapse:collapse;border-spacing:0;width:100%" cellspacing="0" cellpadding="0" border="0">' +
              '                                                                                                                            <tbody>' +
              '                                                                                                                                <tr>' +
              '                                                                                                                                    <td>' +
              '                                                                                                                                        <table style="border:none;border-collapse:collapse;border-spacing:0;max-width:672px;width:100%" cellspacing="0" cellpadding="0"' +
              '                                                                                                                                               border="0"' +
              '                                                                                                                                               align="left">' +
              '                                                                                                                                            <tbody>' +
              '                                                                                                                                                <tr>' +
              '                                                                                                                                                    <td style="padding-bottom:0px;padding-left:12px;padding-right:12px;padding-top:0px">' +
              '                                                                                                                                                        <table style="border:none;border-collapse:collapse;border-spacing:0;table-layout:fixed;width:100%" width="100%" cellspacing="0"' +
              '                                                                                                                                                               cellpadding="0"' +
              '                                                                                                                                                               border="0"' +
              '                                                                                                                                                               align="left">' +
              '                                                                                                                                                            <tbody>' +
              '                                                                                                                                                                <tr>' +
              '                                                                                                                                                                    <td height="1">' +
              '                                                                                                                                                                    </td>' +
              '                                                                                                                                                                </tr>' +
              '                                                                                                                                                            </tbody>' +
              '                                                                                                                                                        </table>' +
              '                                                                                                                                                    </td>' +
              '                                                                                                                                                </tr>' +
              '                                                                                                                                            </tbody>' +
              '                                                                                                                                        </table>' +
              '                                                                                                                                    </td>' +
              '                                                                                                                                </tr>' +
              '                                                                                                                            </tbody>' +
              '                                                                                                                        </table>' +
              '                                                                                                                    </td>' +
              '                                                                                                                </tr>' +
              '                                                                                                            </tbody>' +
              '                                                                                                        </table>' +
          // STARTING FOOTER HERE
          '              <table style="background-color:#ffffff;border:none;border-collapse:collapse;border-spacing:0;width:100%" width="100%" cellspacing="0"' +
          '              cellpadding="0"' +
          '              border="0">' +
          '              <tbody id="footer-content">' +
          '                <tr>' +
          '                  <td style="padding-top:30px">' +
          '                    <img src="https://s3.amazonaws.com/livemapplication/email/Guitar_text_700x350.jpg" style="clear:both;display:block;height:auto;max-width:700px;outline:none;text-decoration:none;width:100%"' +
          '                    width="700"' +
          '                    border="0"' +
          '                    height=""' +
          '                    tabindex="0">' +
          '                    <div dir="ltr" style="opacity: 0.01; left: 701.5px; top: 1586px;">' +
          '                      <div id=":12b" role="button" tabindex="0" aria-label="Download attachment " data-tooltip-class="a1V" data-tooltip="Download">' +
          '                        <div></div>' +
          '                      </div>' +
          '                    </div>' +
          '                  </td>' +
          '                </tr>' +
          '                <tr>' +
          '                  <td style="padding:55px; text-align: center;"><img src="https://s3.amazonaws.com/livemapplication/email/LiveM_logo.jpg" alt="LiveM Logo"></td>' +
          '                </tr>' +
          '                <tr>' +
          '                  <td style="background: #f43545">' +
          '                    <img src="https://s3.amazonaws.com/livemapplication/email/Footer.png" alt="" style="margin-bottom: -70px;">' +
          '' +
          '                    <div style="padding-top: 16px;' +
          '                      padding-bottom: 15px;' +
          '                      font-size: 15px;' +
          '                      color: #fff;' +
          '                      font-family: \'Arial\';' +
          '                      text-align: center" class="">' +
          '' +
          '                      <p style="margin:5px">@ Libera la Musica Ltd. 2019</p>' +
          '                      <p style="margin:5px">Terms & Conditions  | Help Center </p>' +
          '' +
          '                    </div>' +
          '                  </td>' +
          '                </tr>' +
          '              </tbody>' +
          '            </table>' +
          '          </td>' +
          '        </tr>' +
          '      </tbody>' +
          '    </table>' +
          '  </td>' +
          '</tr>' +
          '</tbody>' +
          '</table>' +
          '</td>' +
          '</tr>' +
          '</tbody>' +
          '</table>' +
          '</td>' +
          '</tr>' +
          '</tbody>' +
          '</table>' +
          '</td>' +
          '</tr>' +
          '</tbody>' +
          '</table>' +
          '' +
          '' +
          '        </div>' +
          '      </td>' +
          '    </tr>' +
          '  </tbody>' +
          '</table>' +
          '</body>' +
          '' +
          '</html>';

            params.subject = img["SUBJECT"]["forgotPasswordForCustomer"];
            params.html = myvar;
            params.trigger = img["TRIGGER"]["forgotPasswordForCustomer"];

            email.sendMails(params);
        });


    }
    getUrl()
        .then(sendMails)
        .catch(e => {
            logger.error("Send mails error for  forgot password", e)
        })
}
