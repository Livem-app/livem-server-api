'use strict';

const email = require('../email');
const img = require('../imageUrl');

module.exports.rejectBooking = (params) => {

  var myvar = '<!DOCTYPE html>' +
  '<html>' +
  '' +
  '<body>' +
  '  <table style="background-color:#d6d6d5;border:none;border-collapse:collapse;border-spacing:0;width:100%" width="100%" cellspacing="0"' +
  '  cellpadding="0" border="0" bgcolor="#d6d6d5">' +
  '  <tbody>' +
  '    <tr>' +
  '      <td align="center">' +
  '        <table style="border:none;border-collapse:collapse;border-spacing:0;max-width:700px;width:100%" width="100%" cellspacing="0"' +
  '        cellpadding="0" border="0">' +
  '        <tbody>' +
  '          <tr>' +
  '            <td style="background-color:#ffffff" align="center">' +
  '              <table style="border:none;border-collapse:collapse;border-spacing:0;max-width:700px;width:100%" cellspacing="0" cellpadding="0"' +
  '              border="0">' +
  '              <tbody>' +
  '                <tr>' +
  '                  <td style="background-size:cover;" valign="bottom" bgcolor="" align="center">' +
  '                    <div>' +
  '                      <table style="border:none;border-collapse:collapse;border-spacing:0;width:100%" cellspacing="0" cellpadding="0" border="0">' +
  '                        <tbody>' +
  '                          <tr>' +
  '' +
  '                          </tr>' +
  '                        </tbody>' +
  '                      </table>' +
  '                    </div>' +
  '                  </td>' +
  '                </tr>' +
  '              </tbody>' +
  '            </table>' +
  '            <table style="border:none;border-collapse:collapse;border-spacing:0;margin:0 auto;max-width:700px;width:100%" cellspacing="0"' +
  '            cellpadding="0" border="0">' +
  '            <tbody>' +
  '              <tr>' +
  '                <td style="padding:0;margin:0;background-color:#ffffff" align="center">' +
  '                  <table style="border:none;border-collapse:collapse;border-spacing:0;width:100%" cellspacing="0" cellpadding="0" border="0">' +
  '                    <tbody>' +
  '                      <img style="" src="https://s3.amazonaws.com/livemapplication/email/Hero_mail_600x400.jpg">' +
  '                    </tbody>' +
  '                  </table>' +
  '                </td>' +
  '              </tr>' +
  '            </tbody>' +
  '          </table>' +

      // STARTING HERE - check content
      '                                    <table style="border:none;border-collapse:collapse;border-spacing:0;margin:auto;max-width:700px;width:100%" width="100%"' +
      '                                        cellspacing="0" cellpadding="0" border="0">' +
      '                                        <tbody>' +
      '                                            <tr>' +
      '                                                <td align="center">' +
      '                                                    <table style="background-color:#ffffff;border:none;border-collapse:collapse;border-spacing:0;margin:auto;width:100%" width="100%"' +
      '                                                        cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff">' +
      '                                                        <tbody>' +
      '                                                            <tr>' +
      '                                                                <td align="center">' +
      '                                                                    <table style="border:none;border-collapse:collapse;border-spacing:0;width:100%" width="100%" cellspacing="0" cellpadding="0"' +
      '                                                                        border="0">' +
      '                                                                        <tbody>' +
      '                                                                            <tr>' +
      '                                                                                <td style="background-color:#ffffff" align="center">' +
      '                                                                                    <table style="border:none;border-collapse:collapse;border-spacing:0;width:100%" width="100%" cellspacing="0" cellpadding="0"' +
      '                                                                                        border="0">' +
      '                                                                                        <tbody>' +
      '                                                                                            <tr>' +
      '                                                                                                <td>' +
      '                                                                                                    <table style="border:none;border-collapse:collapse;border-spacing:0;width:100%" width="100%" cellspacing="0" cellpadding="0"' +
      '                                                                                                        border="0">' +
      '                                                                                                        <tbody>' +
      '                                                                                                            <tr>' +
      '                                                                                                                <td>' +
      '                                                                                                                    <table style="border:none;border-collapse:collapse;border-spacing:0;width:100%" width="100%" cellspacing="0" cellpadding="0"' +
      '                                                                                                                        border="0">' +
      '                                                                                                                        <tbody>' +
      '                                                                                                                            <tr>' +
      '                                                                                                                                <td style="padding:0 14px 0 14px" align="left">' +
      '                                                                                                                                    <table style="border:none;border-collapse:collapse;border-spacing:0;width:100%" cellspacing="0" cellpadding="0" border="0">' +
      '                                                                                                                                        <tbody>' +
      '                                                                                                                                            <tr>' +
      '                                                                                                                                                <td>' +
      '                                                                                                                                                    <table style="border:none;border-collapse:collapse;border-spacing:0;max-width:672px;width:100%" cellspacing="0" cellpadding="0"' +
      '                                                                                                                                                        border="0">' +
      '                                                                                                                                                        <tbody>' +
      '                                                                                                                                                            <tr>' +
      '                                                                                                                                                                <td style="height:40px;padding-left:12px;padding-right:12px">' +
      '                                                                                                                                                                    <table style="border:none;border-collapse:collapse;border-spacing:0;table-layout:fixed;width:100%" width="100%" cellspacing="0"' +
      '                                                                                                                                                                        cellpadding="0"' +
      '                                                                                                                                                                        border="0"' +
      '                                                                                                                                                                        align="left">' +
      '                                                                                                                                                                        <tbody>' +
      '                                                                                                                                                                            <tr>' +
      '                                                                                                                                                                                <td style="font-size:1px;line-height:1px" height="40">' +
      '                                                                                                                                                                                </td>' +
      '                                                                                                                                                                            </tr>' +
      '                                                                                                                                                                        </tbody>' +
      '                                                                                                                                                                    </table>' +
      '                                                                                                                                                                </td>' +
      '                                                                                                                                                            </tr>' +
      '                                                                                                                                                        </tbody>' +
      '                                                                                                                                                    </table>' +
      '                                                                                                                                                </td>' +
      '                                                                                                                                            </tr>' +
      '                                                                                                                                        </tbody>' +
      '                                                                                                                                    </table>' +
      '                                                                                                                                </td>' +
      '                                                                                                                            </tr>' +
      '                                                                                                                        </tbody>' +
      '                                                                                                                    </table>' +
      '                                                                                                                    <table style="border:none;border-collapse:collapse;border-spacing:0;width:100%" width="100%" cellspacing="0" cellpadding="0"' +
      '                                                                                                                        border="0">' +
      '                                                                                                                        <tbody>' +
      '                                                                                                                            <tr>' +
      '                                                                                                                                <td style="padding:0 14px 0 14px" align="left">' +
      '                                                                                                                                    <table style="border:none;border-collapse:collapse;border-spacing:0;width:100%" cellspacing="0" cellpadding="0" border="0">' +
      '                                                                                                                                        <tbody>' +
      '                                                                                                                                            <tr>' +
      '                                                                                                                                                <td valign="top">' +
      '                                                                                                                                                    <table style="border:none;border-collapse:collapse;border-spacing:0;width:100%" cellspacing="0" cellpadding="0" border="0"' +
      '                                                                                                                                                        align="left">' +
      '                                                                                                                                                        <tbody>' +
      '                                                                                                                                                            <tr>' +
      '                                                                                                                                                                <td style="padding-left:12px;padding-right:12px">' +
      '                                                                                                                                                                    <table style="border:none;border-collapse:collapse;border-spacing:0;table-layout:fixed;width:100%" width="100%" cellspacing="0"' +
      '                                                                                                                                                                        cellpadding="0"' +
      '                                                                                                                                                                        border="0"' +
      '                                                                                                                                                                        align="left">' +
      '                                                                                                                                                                        <tbody>' +
      '                                                                                                                                                                            <tr>' +
      '                                                                                                                                                                                <td style="color:#000000;font-family:\'ClanPro-Book\',\'HelveticaNeue-Light\',\'Helvetica Neue Light\',Helvetica,Arial,sans-serif;font-size:28px;line-height:36px">' +
      '                                                                                                                                                                                    Hello' +
      '                                                                                                                                                                                    <username>' + params.toName + '</username>,' +
      '                                                                                                                                                                                </td>' +
      '                                                                                                                                                                            </tr>' +
      '                                                                                                                                                                            <tr>' +
      '                                                                                                                                                                                <td>' +
      '                                                                                                                                                                                    <table style="border:none;border-collapse:collapse;border-spacing:0;width:100%" width="100%" cellspacing="0" cellpadding="0"' +
      '                                                                                                                                                                                        border="0">' +
      '                                                                                                                                                                                        <tbody>' +
      '                                                                                                                                                                                            <tr>' +
      '                                                                                                                                                                                                <td style="font-size:0px;line-height:0px;padding-bottom:30px">' +
      '                                                                                                                                                                                                </td>' +
      '                                                                                                                                                                                            </tr>' +
      '                                                                                                                                                                                        </tbody>' +
      '                                                                                                                                                                                    </table>' +
      '                                                                                                                                                                                </td>' +
      '                                                                                                                                                                            </tr>' +
      '                                                                                                                                                                            <tr>' +
      '                                                                                                                                                                                <td style="color:#717172;font-family:\'ClanPro-Book\',\'HelveticaNeue-Light\',\'Helvetica Neue Light\',Helvetica,Arial,sans-serif;font-size:16px;line-height:28px">' +
      '                                                                                                                                                                                        Thank you for booking ' + params.toName + ' for ' + params.bookingDate + '. ' + params.providerName + ' has <b>not accepted</b> your booking request' +
      '                                                                                                                                                                                        ' +
      '                                                                                                                                                                                    <br>' +
      '                                                                                                                                                                                    <br>                                                                                                                                                                                    ' +
      '                                                                                                                                                                                    We apologise for any inconvenience caused , please book another provider on the app again.  <br>    <br>' +
      '                                                                                                                                                                                                                                                                                                                                                                Cheers,' +
      '                                                                                                                                                                                    <br>  ' +
      '                                                                                                                                                                                                                                                                                                                                                                    LiveM' +
      '                                                                                                                                                                                    Team' +
      '                                                                                                                                                                                </td>' +
      '                                                                                                                                                                            </tr>' +
      '                                                                                                                                                                            <tr>' +
      '                                                                                                                                                                                <td>' +
      '                                                                                                                                                                                    <table style="border:none;border-collapse:collapse;border-spacing:0;width:100%" width="100%" cellspacing="0" cellpadding="0"' +
      '                                                                                                                                                                                        border="0">' +
      '                                                                                                                                                                                        <tbody>' +
      '                                                                                                                                                                                            <tr>' +
      '                                                                                                                                                                                                <td style="font-size:0px;line-height:0px;padding-bottom:34px">' +
      '                                                                                                                                                                                                </td>' +
      '                                                                                                                                                                                            </tr>' +
      '                                                                                                                                                                                        </tbody>' +
      '                                                                                                                                                                                    </table>' +
      '                                                                                                                                                                                </td>' +
      '                                                                                                                                                                            </tr>' +
      '                                                                                                                                                                        </tbody>' +
      '                                                                                                                                                                    </table>' +
      '                                                                                                                                                                </td>' +
      '                                                                                                                                                            </tr>' +
      '                                                                                                                                                        </tbody>' +
      '                                                                                                                                                    </table>' +
      '                                                                                                                                                </td>' +
      '                                                                                                                                            </tr>' +
      '                                                                                                                                        </tbody>' +
      '                                                                                                                                    </table>' +
      '                                                                                                                                </td>' +
      '                                                                                                                            </tr>' +
      '                                                                                                                        </tbody>' +
      '                                                                                                                    </table>' +
      '                                                                                                                    <table style="border:none;border-collapse:collapse;border-spacing:0;width:100%" width="100%" cellspacing="0" cellpadding="0"' +
      '                                                                                                                        border="0">' +
      '                                                                                                                        <tbody>' +
      '                                                                                                                            <tr>' +
      '                                                                                                                                <td style="padding:0 14px 0 14px" align="left">' +
      '                                                                                                                                    <table style="border:none;border-collapse:collapse;border-spacing:0;width:100%" cellspacing="0" cellpadding="0" border="0">' +
      '                                                                                                                                        <tbody>' +
      '                                                                                                                                            <tr>' +
      '                                                                                                                                                <td>' +
      '                                                                                                                                                    <table style="border:none;border-collapse:collapse;border-spacing:0;max-width:672px;width:100%" cellspacing="0" cellpadding="0"' +
      '                                                                                                                                                        border="0"' +
      '                                                                                                                                                        align="left">' +
      '                                                                                                                                                        <tbody>' +
      '                                                                                                                                                            <tr>' +
      '                                                                                                                                                                <td style="padding-bottom:0px;padding-left:12px;padding-right:12px;padding-top:0px">' +
      '                                                                                                                                                                    <table style="border:none;border-collapse:collapse;border-spacing:0;table-layout:fixed;width:100%" width="100%" cellspacing="0"' +
      '                                                                                                                                                                        cellpadding="0"' +
      '                                                                                                                                                                        border="0"' +
      '                                                                                                                                                                        align="left">' +
      '                                                                                                                                                                        <tbody>' +
      '                                                                                                                                                                            <tr>' +
      '                                                                                                                                                                                <td height="1">' +
      '                                                                                                                                                                                </td>' +
      '                                                                                                                                                                            </tr>' +
      '                                                                                                                                                                        </tbody>' +
      '                                                                                                                                                                    </table>' +
      '                                                                                                                                                                </td>' +
      '                                                                                                                                                            </tr>' +
      '                                                                                                                                                        </tbody>' +
      '                                                                                                                                                    </table>' +
      '                                                                                                                                                </td>' +
      '                                                                                                                                            </tr>' +
      '                                                                                                                                        </tbody>' +
      '                                                                                                                                    </table>' +
      '                                                                                                                                </td>' +
      '                                                                                                                            </tr>' +
      '                                                                                                                        </tbody>' +
      '                                                                                                                    </table>' +
      // STARTING FOOTER HERE
      '              <table style="background-color:#ffffff;border:none;border-collapse:collapse;border-spacing:0;width:100%" width="100%" cellspacing="0"' +
  '              cellpadding="0"' +
  '              border="0">' +
  '              <tbody id="footer-content">' +
  '                <tr>' +
  '                  <td style="padding-top:30px">' +
  '                    <img src="https://s3.amazonaws.com/livemapplication/email/Guitar_text_700x350.jpg" style="clear:both;display:block;height:auto;max-width:700px;outline:none;text-decoration:none;width:100%"' +
  '                    width="700"' +
  '                    border="0"' +
  '                    height=""' +
  '                    tabindex="0">' +
  '                    <div dir="ltr" style="opacity: 0.01; left: 701.5px; top: 1586px;">' +
  '                      <div id=":12b" role="button" tabindex="0" aria-label="Download attachment " data-tooltip-class="a1V" data-tooltip="Download">' +
  '                        <div></div>' +
  '                      </div>' +
  '                    </div>' +
  '                  </td>' +
  '                </tr>' +
  '                <tr>' +
  '                  <td style="padding:55px; text-align: center;"><img src="https://s3.amazonaws.com/livemapplication/email/LiveM_logo.jpg" alt="LiveM Logo"></td>' +
  '                </tr>' +
  '                <tr>' +
  '                  <td style="background: #f43545">' +
  '                    <img src="https://s3.amazonaws.com/livemapplication/email/Footer.png" alt="" style="margin-bottom: -70px;">' +
  '' +
  '                    <div style="padding-top: 16px;' +
  '                      padding-bottom: 15px;' +
  '                      font-size: 15px;' +
  '                      color: #fff;' +
  '                      font-family: \'Arial\';' +
  '                      text-align: center" class="">' +
  '' +
  '                      <p style="margin:5px">@ Libera la Musica Ltd. 2019</p>' +
  '                      <p style="margin:5px">Terms & Conditions  | Help Center </p>' +
  '' +
  '                    </div>' +
  '                  </td>' +
  '                </tr>' +
  '              </tbody>' +
  '            </table>' +
  '          </td>' +
  '        </tr>' +
  '      </tbody>' +
  '    </table>' +
  '  </td>' +
  '</tr>' +
  '</tbody>' +
  '</table>' +
  '</td>' +
  '</tr>' +
  '</tbody>' +
  '</table>' +
  '</td>' +
  '</tr>' +
  '</tbody>' +
  '</table>' +
  '</td>' +
  '</tr>' +
  '</tbody>' +
  '</table>' +
  '' +
  '' +
  '        </div>' +
  '      </td>' +
  '    </tr>' +
  '  </tbody>' +
  '</table>' +
  '</body>' +
  '' +
  '</html>';


    params.subject = img["SUBJECT"]["rejectBookingForCustomer"];
    params.html = myvar;
    params.trigger = img["TRIGGER"]["rejectBookingForCustomer"];

    email.sendMails(params);
}
