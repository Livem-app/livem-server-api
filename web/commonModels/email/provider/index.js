'use strict';

const signup = require('./signup').signupProvider;

const welcome = require('./welcome').providerWelcome;

const forgotPassword = require('./forgotPassword').forgotPassword;

const newBookingRecevied = require('./newBookingRecevied').newBookingRecevied;

const cancelBooking = require('./cancelBooking').bookingCancelByProvider; 

module.exports = {
    signup,
    welcome,
    cancelBooking,
    forgotPassword,
    newBookingRecevied, 
} 