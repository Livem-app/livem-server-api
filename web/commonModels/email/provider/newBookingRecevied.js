'use strict';

const email = require('../email');
const img = require('../imageUrl');
const jwt = require('jsonwebtoken');
const configuration = require('../../../../configuration');

module.exports.newBookingRecevied = (params) => {
    let tokenAccept = jwt.sign({ bookingId: params.bookingId, providerId: params.providerId, status: 3 }, configuration.JWT_KEY, { expiresIn: '6h' });
    let tokenReject = jwt.sign({ bookingId: params.bookingId, providerId: params.providerId, status: 4 }, configuration.JWT_KEY, { expiresIn: '6h' });
    let acceptBooking = configuration.acceptBookingLink + "??" + tokenAccept;
    let rejectBooking = configuration.rejectBookingLink + "??" + tokenReject;
    
    let myvar = '<!DOCTYPE html>' +
        '<html>' +
        '' +
        '<body>' +
        '  <table style="background-color:#d6d6d5;border:none;border-collapse:collapse;border-spacing:0;width:100%" width="100%" cellspacing="0"' +
        '  cellpadding="0" border="0" bgcolor="#d6d6d5">' +
        '  <tbody>' +
        '    <tr>' +
        '      <td align="center">' +
        '        <table style="border:none;border-collapse:collapse;border-spacing:0;max-width:700px;width:100%" width="100%" cellspacing="0"' +
        '        cellpadding="0" border="0">' +
        '        <tbody>' +
        '          <tr>' +
        '            <td style="background-color:#ffffff" align="center">' +
        '              <table style="border:none;border-collapse:collapse;border-spacing:0;max-width:700px;width:100%" cellspacing="0" cellpadding="0"' +
        '              border="0">' +
        '              <tbody>' +
        '                <tr>' +
        '                  <td style="background-size:cover;" valign="bottom" bgcolor="" align="center">' +
        '                    <div>' +
        '                      <table style="border:none;border-collapse:collapse;border-spacing:0;width:100%" cellspacing="0" cellpadding="0" border="0">' +
        '                        <tbody>' +
        '                          <tr>' +
        '' +
        '                          </tr>' +
        '                        </tbody>' +
        '                      </table>' +
        '                    </div>' +
        '                  </td>' +
        '                </tr>' +
        '              </tbody>' +
        '            </table>' +
        '            <table style="border:none;border-collapse:collapse;border-spacing:0;margin:0 auto;max-width:700px;width:100%" cellspacing="0"' +
        '            cellpadding="0" border="0">' +
        '            <tbody>' +
        '              <tr>' +
        '                <td style="padding:0;margin:0;background-color:#ffffff" align="center">' +
        '                  <table style="border:none;border-collapse:collapse;border-spacing:0;width:100%" cellspacing="0" cellpadding="0" border="0">' +
        '                    <tbody>' +
        '                      <img style="" src="https://s3.amazonaws.com/livemapplication/email/Hero_mail_600x400.jpg">' +
        '                    </tbody>' +
        '                  </table>' +
        '                </td>' +
        '              </tr>' +
        '            </tbody>' +
        '          </table>' +
        //Starting Here
        '                                    <table style="border:none;border-collapse:collapse;border-spacing:0;margin:auto;max-width:700px;width:100%" width="100%"' +
        '                                        cellspacing="0" cellpadding="0" border="0">' +
        '                                        <tbody>' +
        '                                            <tr>' +
        '                                                <td align="center">' +
        '                                                    <table style="background-color:#ffffff;border:none;border-collapse:collapse;border-spacing:0;margin:auto;width:100%" width="100%"' +
        '                                                        cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff">' +
        '                                                        <tbody>' +
        '                                                            <tr>' +
        '                                                                <td align="center">' +
        '                                                                    <table style="border:none;border-collapse:collapse;border-spacing:0;width:100%" width="100%" cellspacing="0" cellpadding="0"' +
        '                                                                        border="0">' +
        '                                                                        <tbody>' +
        '                                                                            <tr>' +
        '                                                                                <td style="background-color:#ffffff" align="center">' +
        '                                                                                    <table style="border:none;border-collapse:collapse;border-spacing:0;width:100%" width="100%" cellspacing="0" cellpadding="0"' +
        '                                                                                        border="0">' +
        '                                                                                        <tbody>' +
        '                                                                                            <tr>' +
        '                                                                                                <td>' +
        '                                                                                                    <table style="border:none;border-collapse:collapse;border-spacing:0;width:100%" width="100%" cellspacing="0" cellpadding="0"' +
        '                                                                                                        border="0">' +
        '                                                                                                        <tbody>' +
        '                                                                                                            <tr>' +
        '                                                                                                                <td>' +
        '                                                                                                                    <table style="border:none;border-collapse:collapse;border-spacing:0;width:100%" width="100%" cellspacing="0" cellpadding="0"' +
        '                                                                                                                        border="0">' +
        '                                                                                                                        <tbody>' +
        '                                                                                                                            <tr>' +
        '                                                                                                                                <td style="padding:0 14px 0 14px" align="left">' +
        '                                                                                                                                    <table style="border:none;border-collapse:collapse;border-spacing:0;width:100%" cellspacing="0" cellpadding="0" border="0">' +
        '                                                                                                                                        <tbody>' +
        '                                                                                                                                            <tr>' +
        '                                                                                                                                                <td>' +
        '                                                                                                                                                    <table style="border:none;border-collapse:collapse;border-spacing:0;max-width:672px;width:100%" cellspacing="0" cellpadding="0"' +
        '                                                                                                                                                        border="0">' +
        '                                                                                                                                                        <tbody>' +
        '                                                                                                                                                            <tr>' +
        '                                                                                                                                                                <td style="height:40px;padding-left:12px;padding-right:12px">' +
        '                                                                                                                                                                    <table style="border:none;border-collapse:collapse;border-spacing:0;table-layout:fixed;width:100%" width="100%" cellspacing="0"' +
        '                                                                                                                                                                        cellpadding="0"' +
        '                                                                                                                                                                        border="0"' +
        '                                                                                                                                                                        align="left">' +
        '                                                                                                                                                                        <tbody>' +
        '                                                                                                                                                                            <tr>' +
        '                                                                                                                                                                                <td style="font-size:1px;line-height:1px" height="40">' +
        '                                                                                                                                                                                </td>' +
        '                                                                                                                                                                            </tr>' +
        '                                                                                                                                                                        </tbody>' +
        '                                                                                                                                                                    </table>' +
        '                                                                                                                                                                </td>' +
        '                                                                                                                                                            </tr>' +
        '                                                                                                                                                        </tbody>' +
        '                                                                                                                                                    </table>' +
        '                                                                                                                                                </td>' +
        '                                                                                                                                            </tr>' +
        '                                                                                                                                        </tbody>' +
        '                                                                                                                                    </table>' +
        '                                                                                                                                </td>' +
        '                                                                                                                            </tr>' +
        '                                                                                                                        </tbody>' +
        '                                                                                                                    </table>' +
        '                                                                                                                    <table style="border:none;border-collapse:collapse;border-spacing:0;width:100%" width="100%" cellspacing="0" cellpadding="0"' +
        '                                                                                                                        border="0">' +
        '                                                                                                                        <tbody>' +
        '                                                                                                                            <tr>' +
        '                                                                                                                                <td style="padding:0 14px 0 14px" align="left">' +
        '                                                                                                                                    <table style="border:none;border-collapse:collapse;border-spacing:0;width:100%" cellspacing="0" cellpadding="0" border="0">' +
        '                                                                                                                                        <tbody>' +
        '                                                                                                                                            <tr>' +
        '                                                                                                                                                <td valign="top">' +
        '                                                                                                                                                    <table style="border:none;border-collapse:collapse;border-spacing:0;width:100%" cellspacing="0" cellpadding="0" border="0"' +
        '                                                                                                                                                        align="left">' +
        '                                                                                                                                                        <tbody>' +
        '                                                                                                                                                            <tr>' +
        '                                                                                                                                                                <td style="padding-left:12px;padding-right:12px">' +
        '                                                                                                                                                                    <table style="border:none;border-collapse:collapse;border-spacing:0;table-layout:fixed;width:100%" width="100%" cellspacing="0"' +
        '                                                                                                                                                                        cellpadding="0"' +
        '                                                                                                                                                                        border="0"' +
        '                                                                                                                                                                        align="left">' +
        '                                                                                                                                                                        <tbody>' +
        '                                                                                                                                                                            <tr>' +
        '                                                                                                                                                                                <td style="color:#000000;font-family:\'ClanPro-Book\',\'HelveticaNeue-Light\',\'Helvetica Neue Light\',Helvetica,Arial,sans-serif;font-size:28px;line-height:36px">' +
        '                                                                                                                                                                                    Hello' +
        '                                                                                                                                                                                    <username>' + params.toName + '</username>,' +
        '                                                                                                                                                                                </td>' +
        '                                                                                                                                                                            </tr>' +
        '                                                                                                                                                                            <tr>' +
        '                                                                                                                                                                                <td>' +
        '                                                                                                                                                                                    <table style="border:none;border-collapse:collapse;border-spacing:0;width:100%" width="100%" cellspacing="0" cellpadding="0"' +
        '                                                                                                                                                                                        border="0">' +
        '                                                                                                                                                                                        <tbody>' +
        '                                                                                                                                                                                            <tr>' +
        '                                                                                                                                                                                                <td style="font-size:0px;line-height:0px;padding-bottom:30px">' +
        '                                                                                                                                                                                                </td>' +
        '                                                                                                                                                                                            </tr>' +
        '                                                                                                                                                                                        </tbody>' +
        '                                                                                                                                                                                    </table>' +
        '                                                                                                                                                                                </td>' +
        '                                                                                                                                                                            </tr>' +
        '                                                                                                                                                                            <tr>' +
        '                                                                                                                                                                                <td style="color:#717172;font-family:\'ClanPro-Book\',\'HelveticaNeue-Light\',\'Helvetica Neue Light\',Helvetica,Arial,sans-serif;font-size:16px;line-height:28px">' +
        '                                                                                                                                                                                        You just got a new booking on ' + img["AppName"] + '. The details are as follows:' +
        '                                                                                                                                                                                    <br>' +
        '                                                                                                                                                                                    <br>                                                                                                                                                                                    ' +
        '                                                                                                                                                                                   <b>Booking Id : </b>' + params.bookingId + '<br>' +
        '                                                                                                                                                                                   <b>Booking Date:</b>' + params.bookingDate + ' <br>' +
        '                                                                                                                                                                                   <b>Booking Type :</b>' + params.bookingType + ' <br>' +
        '                                                                                                                                                                                   <b>Confirmed Address : </b>' + params.address + '' +
        '                                                                                                                                                                                   <div style="padding-top:12px;">' +
        '                                                                                                                                                                                        <b> Booking Category : ' + params.bookingCategory + '</b>' +
        '                                                                                                                                                                                         ' +
        '                                                                                                                                                                                    </div>' +
        '                                                                                                                                                                                    <div>' +
        '                                                                                                                                                                                            <b>Services:</b>' +
        '                                                                                                                                                                                            <br>' +
        '                                                                                                                                                                                            <div style="" class="ewe">' +
        '                                                                                                                                                                                            <div style="display: inline-block;width: 50%;"><b>Service Name </b> <br>' +
        '                                                                                                                                                                                             <p style="margin:0">' + params.service.name + ' ' + params.service.unit + '</p>                                                                                                                                                                                       ' +
        '                                                                                                                                                                                             <p style="margin:0">Discount applied</p>' +
        '                                                                                                                                                                                             <p style="margin:0">Total</p>' +
        '                                                                                                                                                                                         </div>	 ' +
        '                                                                                                                                                                                          <div style="float: right;width:42%"><b>Price</b>' +
        '     ' +
        '                                                                                                                                                                                             <p style="margin:0">' + params.currencySymbol + ' ' + params.service.price + '</p> ' +
        '                                                                                                                                                                                             <p style="margin:0">' + params.currencySymbol + ' ' + params.discount + '</p>   ' +
        '                                                                                                                                                                                             <p style="margin:0">' + params.currencySymbol + ' ' + params.amount + '</p>   ' +
        '                                                                                                                                                                                         </div>' +
        '                                                                                                                                                                                         </div>' +
        '                                                                                                                                                                                        </div>' +
        '' +
        '                                                                                                                                                                                        <div>' +
        '                                                                                                                                                                                            <p>To accepted the booking click here : <span><a target="_blank	" href="' + acceptBooking + '" style="background: #00aade;color: #fff;border: transparent;padding: 10px;border-radius: 10px;cursor:pointer">Accept Booking</a></span></p>' +
        '                                                                                                                                                                                        </div>' +
        '                                                                                                                                                                                        <div>' +
        '                                                                                                                                                                                            <p>To deny the booking click here : <span><a  href="' + rejectBooking + '" target="_blank" style="background: red;' +
        '                                                                                                                                                                                                color: #fff;' +
        '                                                                                                                                                                                                border: transparent;' +
        '                                                                                                                                                                                                padding: 10px;' +
        '                                                                                                                                                                                                border-radius: 10px;cursor:pointer">Reject Booking</a></span></p>' +
        '                                                                                                                                                                                        </div>' +
        '                                                                                                                                                                                   ' +
        '                                                                                                                                                                                    <br>' +
        '                                                                                                                                                                                    <br>                                                                                                                                                                                    Cheers,' +
        '                                                                                                                                                                                    <br>                                                                                                                                                                                    LiveM' +
        '                                                                                                                                                                                    Team' +
        '                                                                                                                                                                                </td>' +
        '                                                                                                                                                                            </tr>' +
        '                                                                                                                                                                            <tr>' +
        '                                                                                                                                                                                <td>' +
        '                                                                                                                                                                                    <table style="border:none;border-collapse:collapse;border-spacing:0;width:100%" width="100%" cellspacing="0" cellpadding="0"' +
        '                                                                                                                                                                                        border="0">' +
        '                                                                                                                                                                                        <tbody>' +
        '                                                                                                                                                                                            <tr>' +
        '                                                                                                                                                                                                <td style="font-size:0px;line-height:0px;padding-bottom:34px">' +
        '                                                                                                                                                                                                </td>' +
        '                                                                                                                                                                                            </tr>' +
        '                                                                                                                                                                                        </tbody>' +
        '                                                                                                                                                                                    </table>' +
        '                                                                                                                                                                                </td>' +
        '                                                                                                                                                                            </tr>' +
        '                                                                                                                                                                        </tbody>' +
        '                                                                                                                                                                    </table>' +
        '                                                                                                                                                                </td>' +
        '                                                                                                                                                            </tr>' +
        '                                                                                                                                                        </tbody>' +
        '                                                                                                                                                    </table>' +
        '                                                                                                                                                </td>' +
        '                                                                                                                                            </tr>' +
        '                                                                                                                                        </tbody>' +
        '                                                                                                                                    </table>' +
        '                                                                                                                                </td>' +
        '                                                                                                                            </tr>' +
        '                                                                                                                        </tbody>' +
        '                                                                                                                    </table>' +
        // Footer Here
        '              <table style="background-color:#ffffff;border:none;border-collapse:collapse;border-spacing:0;width:100%" width="100%" cellspacing="0"' +
        '              cellpadding="0"' +
        '              border="0">' +
        '              <tbody id="footer-content">' +
        '                <tr>' +
        '                  <td style="padding-top:30px">' +
        '                    <img src="https://s3.amazonaws.com/livemapplication/email/Guitar_text_700x350.jpg" style="clear:both;display:block;height:auto;max-width:700px;outline:none;text-decoration:none;width:100%"' +
        '                    width="700"' +
        '                    border="0"' +
        '                    height=""' +
        '                    tabindex="0">' +
        '                    <div dir="ltr" style="opacity: 0.01; left: 701.5px; top: 1586px;">' +
        '                      <div id=":12b" role="button" tabindex="0" aria-label="Download attachment " data-tooltip-class="a1V" data-tooltip="Download">' +
        '                        <div></div>' +
        '                      </div>' +
        '                    </div>' +
        '                  </td>' +
        '                </tr>' +
        '                <tr>' +
        '                  <td style="padding:55px; text-align: center;"><img src="https://s3.amazonaws.com/livemapplication/email/LiveM_logo.jpg" alt="LiveM Logo"></td>' +
        '                </tr>' +
        '                <tr>' +
        '                  <td style="background: #f43545">' +
        '                    <img src="https://s3.amazonaws.com/livemapplication/email/Footer.png" alt="" style="margin-bottom: -70px;">' +
        '' +
        '                    <div style="padding-top: 16px;' +
        '                      padding-bottom: 15px;' +
        '                      font-size: 15px;' +
        '                      color: #fff;' +
        '                      font-family: \'Arial\';' +
        '                      text-align: center" class="">' +
        '' +
        '                      <p style="margin:5px">@ Libera la Musica Ltd. 2019</p>' +
        '                      <p style="margin:5px">Terms & Conditions  | Help Center </p>' +
        '' +
        '                    </div>' +
        '                  </td>' +
        '                </tr>' +
        '              </tbody>' +
        '            </table>' +
        '          </td>' +
        '        </tr>' +
        '      </tbody>' +
        '    </table>' +
        '  </td>' +
        '</tr>' +
        '</tbody>' +
        '</table>' +
        '</td>' +
        '</tr>' +
        '</tbody>' +
        '</table>' +
        '</td>' +
        '</tr>' +
        '</tbody>' +
        '</table>' +
        '</td>' +
        '</tr>' +
        '</tbody>' +
        '</table>' +
        '' +
        '' +
        '        </div>' +
        '      </td>' +
        '    </tr>' +
        '  </tbody>' +
        '</table>' +
        '</body>' +
        '' +
        '</html>';

    params.subject = " You got a new booking for " + params.bookingDate;
    params.html = myvar;
    params.trigger = img["TRIGGER"]["newBookingRecevied"];

    email.sendMails(params);
}
