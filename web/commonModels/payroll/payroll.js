'use strict'

const async = require('async');
const Moment = require('moment');
const ObjectID = require('mongodb').ObjectID;
const joi = require('joi');

const bookings = require('../../../models/bookings');
const provider = require('../../../models/provider');
const providerDailyOnlineLogs = require('../../../models/providerDailyOnlineLogs');
const payroll = require('../../../models/payroll');
const payrollWeekly = require('../../../models/payrollWeekly');
const appConfig = require('../../../models/appConfig');


/**
 * Method to update the payroll data of a master (daily and weekly)
 * @param {*} params - id of the master
 * @param {*} callback
 */
const updatePayrollData = (params, callback) => {

    aggregateAndUpdatePayrollData({
        id: params.id,
        from: Moment().startOf('day').unix(),
        to: Moment().endOf('day').unix(),
        type: 'daily',
        cycleName: 'PC-D-' + Moment().format('DD-MMM-YY')
    }, () => { return 1; });
    appConfig.readOne({ 'payrollSettings': { $exists: true } }, (err, doc) => {
        if (err)
            return 0;

        let day = 'Sunday';

        if (doc != null)
            day = (typeof doc.payrollSettings.weekly != 'undefined'
                && typeof doc.payrollSettings.weekly.triggerDay != 'undefined')
                ? doc.payrollSettings.weekly.triggerDay
                : 'Sunday';

        let condition = {
            id: params.id,
            from: Moment().day(day).subtract({ days: 7 }).startOf('day').unix(),
            to: Moment().day(day).endOf('day').unix(),
            type: 'weekly',
            cycleName: 'PC-W-' + Moment().day(day).format('DD-MMM-YY')
        }
        aggregateAndUpdatePayrollData(condition, () => { return 1; });

        return 1;
    });

    return callback(null, 'done');
}

/**
 * Method to aggregate and update the payroll data
 * @param {*} params
 * @param {*} callback
 */
const aggregateAndUpdatePayrollData = (params, callback) => {


    let data = {
        type: params.type,
        did: params.id,
        name: '',
        phone: '',
        email: '',
        payrollType: 1,
        earnings: 0,
        dues: 0,
        rating: 0,
        deadHead: 0,
        bookings: 0,
        onlineTime: 0,
        waitingTime: 0,
        tripDistance: 0,
        completedBookings: 0,
        received: 0,
        accepted: 0,
        ignored: 0,
        rejected: 0,
        cancelled: 0,
        cancelledByCustomer: 0,
        acceptanceRate: 0,

        from: params.from,
        to: params.to,
        cycleName: params.cycleName
    }

    let condition = {
        id: params.id,
        from: params.from,
        to: params.to
    }
    async.parallel([
        function (cb) {
            avgRating(condition, (err, rate) => {
                if (err)
                    return cb(err);

                data.rating = rate;

                return cb(null, 'done');
            });
        },//compute the average rating 
        function (cb) {
            onlineTime(condition, (err, time) => {
                if (err)
                    return cb(err);
                data.onlineTime = time;

                return cb(null, 'done');
            });
        },//compute the total onlineTime 
        function (cb) {
            totalJobTime(condition, (err, time) => {
                if (err)
                    return cb(err);
                data.waitingTime = time;

                return cb(null, 'done');
            });
        },//compute the total jobtime 
        function (cb) {
            completedAndCancelledBookings(condition, (err, bookings) => {
                if (err)
                    return cb(err);

                data.completedBookings = bookings.completed;
                data.cancelled = bookings.cancelled;
                data.cancelledByCustomer = bookings.cancelledByCustomer;

                data.bookings = bookings.completed + bookings.cancelled + bookings.cancelledByCustomer;

                return cb(null, 'done');
            });
        },//compute the total completed bookings
        function (cb) {
            earnings(condition, (err, total) => {
                if (err)
                    return cb(err);

                data.earnings = parseFloat(total || 0);
                return cb(null, 'done');
            });
        },//compute the total earnings 
        function (cb) {
            earningsDues(condition, (err, total) => {
                if (err)
                    return cb(err);
                data.dues = parseFloat(total || 0);
                return cb(null, 'done');
            });
        },//compute the total earnings 
        function (cb) {
            acceptanceRate(condition, (err, result) => {
                if (err)
                    return cb(err);
                data.received = result.received || 0;
                data.accepted = result.accepted || 0;
                data.ignored = result.ignored || 0;
                data.rejected = result.rejected || 0;
                // data.acceptanceRate = result.acceptanceRate || 0;

                return cb(null, 'done');
            });
        },//compute the acceptance rate 
        function (cb) {
            provider.getProviderById(params.id, (err, doc) => {
                if (err)
                    return cb(err);

                if (doc === null)
                    return cb({ errNum: 400, errMsg: 'Could not find provider', errFlag: 1 });

                data.name = doc.firstName + ' ' + doc.lastName;
                data.phone = doc.phone[0].phone || '';
                data.email = doc.email || '';
                // data.dues = doc.walletAmount || 0;
                data.payrollType = doc.payrollType || 1;
                data.image = doc.profilePic || '';

                return cb(null, 'done');
            })
        }//get the wallet amount
    ], (err, results) => {
        if (err)
            return callback(err);
        let totalBookings = parseInt(data.ignored || 0) + parseInt(data.rejected) + parseInt(data.bookings);

        let totalBookingsConsidered = parseInt(data.completedBookings || 0) + parseInt(data.cancelledByCustomer || 0);

        let acceptanceRate = (totalBookingsConsidered / totalBookings) * 100;

        data.acceptanceRate = parseFloat(isNaN(acceptanceRate) ? 0 : acceptanceRate.toFixed(2));

        update(data, (err, status) => {
            if (err)
                return callback(err);

            // updateAverageAcceptanceRate(params.id, () => { return 1 });//update the overall acceptance rate

            return callback(null, 'done');
        })
    })
}




/**
 * Method to calculate the average rating of the master
 * @param {*} params -
 * @param {*} id - id of the master
 * @param {*} from - from datetime
 * @param {*} to - to datetime
 * @param {*} cb - callback function
 * @return avgRating || 0
 */
const avgRating = (params, cb) => {

    let aggregationQuery = [
        {
            $match: {
                providerId: params.id,
                status: 10,
                bookingCompletedAt: {
                    $gte: params.from || 0,
                    $lte: params.to || moment().endOf('day').unix()
                }

            }
        },
        { $group: { _id: null, count: { $sum: 1 }, total: { $sum: '$reviewByCustomer.rating' } } }
    ];
    bookings.aggregate(aggregationQuery, (err, docs) => {
        let avgRating = (docs.length === 0) ? 0 : (docs[0].total / docs[0].count).toFixed(1);
        return cb(null, avgRating);
    });

}

const onlineTime = (params, cb) => {

    let id = new ObjectID(params.id);//convert string id to mongodId

    let aggregationQuery = [
        {
            $match: {
                providerId: new ObjectID(id),
                timestamp: {
                    $gte: params.from || 0,
                    $lte: params.to || moment().endOf('day').unix()
                }
            }
        },
        { $project: { totalOnline: 1 } },
        { $group: { _id: null, total: { $sum: '$totalOnline' } } }
    ];
    providerDailyOnlineLogs.aggregate(aggregationQuery, (err, docs) => {
        if (err)
            return cb(err);

        return cb(null, (docs.length === 0) ? 0 : parseInt(docs[0].total || 0));
    });

}
const totalJobTime = (params, cb) => {

    let id = new ObjectID(params.id);//convert string id to mongodId

    let aggregationQuery = [
        {
            $match: {
                providerId: params.id,
                status: { $in: [10] },
                bookingCompletedAt: {
                    $gte: params.from || 0,
                    $lte: params.to || moment().endOf('day').unix()
                }
            }
        },
        { $project: { totalJobTime: '$accounting.totalJobTime' } },
        { $group: { _id: null, total: { $sum: '$totalJobTime' } } }
    ];
    bookings.aggregate(aggregationQuery, (err, docs) => {
        if (err)
            return cb(err);
        return cb(null, (docs.length > 0) ? docs[0].total : 0);
    });

}

const completedAndCancelledBookings = (params, cb) => {

    let id = new ObjectID(params.id);//convert string id to mongodId

    let aggregationQuery = [
        {
            $match: {
                providerId: params.id,
                status: { $in: [10, 11, 12] },
                'bookingRequestedFor': {
                    $gte: params.from || 0,
                    $lte: params.to || moment().endOf('day').unix()
                }
            }
        },
        { $project: { _id: 1, status: 1 } },
        { $group: { _id: '$status', count: { $sum: 1 } } }
    ];
    bookings.aggregate(aggregationQuery, (err, docs) => {
        if (err)
            return cb(err);

        let data = {
            completed: 0,
            cancelled: 0,
            cancelledByCustomer: 0
        }

        docs.forEach(item => {
            switch (item._id) {
                case 10: data.completed = item.count || 0; break;
                case 12: data.cancelledByCustomer = item.count || 0; break;
                case 11: data.cancelled = item.count || 0; break;
            }
        })

        return cb(null, data);
    });
}

const earnings = (params, cb) => {

    let id = new ObjectID(params.id);//convert string id to mongodId

    let aggregationQuery = [
        {
            $match: {
                providerId: params.id,
                status: { $in: [11,12, 10] },
                bookingCompletedAt: {
                    $gte: params.from || 0,
                    $lte: params.to || moment().endOf('day').unix()
                }
            }
        },
        { $project: { earnings: '$accounting.providerEarning' } },
        { $group: { _id: null, total: { $sum: '$earnings' } } }
    ];
    bookings.aggregate(aggregationQuery, (err, docs) => {
        if (err)
            return cb(err);
        return cb(null, (docs.length > 0) ? parseFloat((docs[0].total).toFixed(2)) : 0);
    });
}

const earningsDues = (params, cb) => {

    let id = new ObjectID(params.id);//convert string id to mongodId

    let aggregationQuery = [
        {
            $match: {
                providerId: params.id,
                status: { $in: [11,12, 10] },
                bookingCompletedAt: {
                    $gte: params.from || 0,
                    $lte: params.to || moment().endOf('day').unix()
                }
            }
        },
        { $project: { earnings: '$accounting.dues' } },
        { $group: { _id: null, total: { $sum: '$earnings' } } }
    ];
    bookings.aggregate(aggregationQuery, (err, docs) => {
        if (err)
            return cb(err);
        return cb(null, (docs.length > 0) ? parseFloat((docs[0].total).toFixed(2)) : 0);
    });
}
/**
 * Method to calculate the acceptance rate
 * @param {*} params -
 * @param {*} id - id of the master
 * @param {*} from - from datetime
 * @param {*} to - to datetime
 * @param {*} cb - callback function
 * @return received, accepted, ignored, rejected, cancelled, cancelledByCustomer, acceptanceRate
 */
const acceptanceRate = (params, cb) => {

    let id = new ObjectID(params.id);//convert string id to mongodId

    let aggregationQuery = [
        {
            $match: {
                providerId: params.id,
                bookingCompletedAt: {
                    $gte: params.from || 0,
                    $lte: params.to || moment().endOf('day').unix()
                }
            }
        },
        { $unwind: '$jobStatusLogs' },
        { $project: { _id: 0, 'id': '$providerId', 'status': '$jobStatusLogs.status' } },
        { $group: { _id: '$status', count: { $sum: 1 } } }
    ];
    bookings.aggregate(aggregationQuery, (err, docs) => {
        if (err)
            return cb(status.status(3));
        let data = {
            received: 0,
            accepted: 0,
            ignored: 0,
            rejected: 0,
            acceptanceRate: 0
        }
        let cancelbyProvider = 0;
        if (docs.length > 0) {

            let rate = 0,
                total = 0;

            docs.forEach(item => {

                if (item._id != 5)
                    total += parseInt(item.count);
                // if (item._id != 1)
                //     data.received += item.count;
                switch (item._id) {
                    case 1: data.received = parseInt(item.count); break;
                    case 3: data.accepted = parseInt(item.count); break;
                    case 4: data.rejected = parseInt(item.count); break;
                    case 5: data.ignored = parseInt(item.count); break;
                    case 11: cancelbyProvider = parseInt(item.count); break;
                    default: break;
                }
            });

            rate = parseFloat(((data.accepted / total) * 100).toFixed(2));
            data.accepted = data.accepted - cancelbyProvider;
            data.acceptanceRate = isNaN(rate) ? 0 : rate;

        }

        return cb(null, data);
    });
}
const driverDailyPayrollSchema = joi.object({
    did: joi.string().required().description('id of the driver'),
    name: joi.string().required().description('name of the driver'),
    phone: joi.string().required().description('phone number of the driver'),
    image: joi.string().allow('').description('profile pic'),
    email: joi.string().required().description('email of the driver'),
    payrollType: joi.number().required().description('payroll type, daily, weekly'),
    bookings: joi.number().default(1).description('number of bookings'),
    rating: joi.number().required().description('average rating'),
    onlineTime: joi.number().required().description('total online time'),
    tripDistance: joi.number().required().description('trip distance travelled'),
    deadHead: joi.number().required().description('dead head, distance between booking accepted & pickup location'),
    dues: joi.number().required().description('total dues of the driver'),
    earnings: joi.number().required().description('total earnings of the driver'),
    received: joi.number().required().description('number of bookings received'),
    accepted: joi.number().required().description('number of bookings accepted'),
    ignored: joi.number().required().description('number of bookings ignored'),
    rejected: joi.number().required().description('number of bookings rejected'),
    cancelled: joi.number().required().description('number of bookings cancelled'),
    cancelledByCustomer: joi.number().required().description('number of bookings cancelledByCustomer'),
    acceptanceRate: joi.number().required().description('average acceptance rate'),
    waitingTime: joi.number().required().description('waiting time'),
    completedBookings: joi.number().required().description('completed bookings'),

    type: joi.string().required().description('daily, weekly'),
    from: joi.number().required().description('from date'),
    to: joi.number().required().description('to date'),
    cycleName: joi.string().required().description('payment cycle name')
}).required()
const update = (params, callback) => {

    let data = joi.attempt(params, driverDailyPayrollSchema);
    data.did = new ObjectID(data.did);//convert the string id to mongodId

    let collectionName = (data.type === 'daily') ? 'payroll' : 'payrollWeekly';

    let updateQuery = {
        query: {
            did: data.did,
            fromDate: data.from,
            toDate: data.to
        },
        data: {
            $set: {
                name: data.name,
                phone: data.phone,
                email: data.email,
                image: data.image,
                payrollType: data.payrollType,
                bookings: data.bookings,
                rating: parseFloat((data.rating || 0).toFixed(2)),
                onlineTime: parseInt(data.onlineTime || 0),
                tripDistance: parseFloat((data.tripDistance || 0).toFixed(2)),
                deadHead: parseFloat((data.deadHead || 0).toFixed(2)),
                received: parseInt(data.received),
                accepted: parseInt(data.accepted),
                ignored: parseInt(data.ignored),
                rejected: parseInt(data.rejected),
                cancelled: parseInt(data.cancelled),
                cancelledByCustomer: parseInt(data.cancelledByCustomer),
                acceptanceRate: parseFloat((data.acceptanceRate || 0).toFixed(2)),
                dues: parseFloat((data.dues || 0).toFixed(2)),
                earnings: parseFloat((data.earnings || 0).toFixed(2)),
                waitingTime: parseInt(data.waitingTime || 0),
                completedBookings: parseInt(data.completedBookings || 0),
                paymentStatus: 'Not Settled',
                cycleName: data.cycleName
            }
        }
    }
    if (data.type === 'daily') {
        payroll.findUpdate(updateQuery, (err, doc) => { return callback(err, doc) });
    } else {
        payrollWeekly.findUpdate(updateQuery, (err, doc) => { return callback(err, doc) });
    }

}
/** ==================================================== **/

module.exports = {
    updatePayrollData
}