'use strict';

module.exports.notifyiTitle = function (successCode, language, data) {
    var return_;
    switch (successCode) {
        case 1:
            return_ = 'New Booking Received';
            break;
        case 2:
            return_ = 'Booking ACK';
            break;
        case 3:
            return_ = 'Booking accepted by the Artist';
            break;
        case 4:
            return_ = 'Booking denied by the Artist';
            break;
        case 5:
            return_ = 'Booking expired';
            break;
        case 6:
            return_ = 'Artist On the Way';
            break;

        case 7:
            return_ = 'Artist arrived';
            break;
        case 8:
            return_ = 'Artist job started';
            break;

        case 9:
            return_ = 'Artist raised invoice';
            break;
        case 10:
            return_ = 'Artist completed job';
            break;
        case 11:
            return_ = 'Booking cancelled by the Artist';
            break;
        case 12:
            return_ = 'Booking cancelled by customer';
            break;
        case 14:
            return_ = 'Artist paused job';
            break;



        default:
            return_ = 'Unassigned.';

    }
    return return_;

}
module.exports.notifyiMsg = function (successCode, language, data) {
    var return_;
    switch (successCode) {
        case 1:
            return_ = 'You have received a new booking from ' + data.customerName;
            break;
        case 2:
            return_ = 'Booking ACK';
            break;
        case 3:
            return_ = data.providerName + ' has accepted your booking request for ' + data.bookingDate;
            break;
        case 4:
            return_ = data.providerName + ' has rejected your booking request for ' + data.bookingDate;
            break;
        case 5:
            return_ = 'Your requested Artist ' + data.providerName + ' did not respond to the booking request. Please try booking another one of our Artists on LiveM';
            break;

        case 6:
            return_ = data.providerName + ' is on the way to your location';
            break;

        case 7:
            return_ = data.providerName + '  is at the requested location , please be ready.';
            break;
        case 8:
            return_ = data.providerName + '  has started the job , please note the time';
            break;

        case 9:
            return_ = data.providerName + ' has completed the job and will soon raise the invoice';
            break;
        case 10:
            return_ = data.providerName + ' Please review the receipt for your booking on' + data.bookingDate;
            break;

        case 11:
            return_ = data.providerName + ' has cancelled the booking for ' + data.bookingDate;
            break;
        case 12:
            return_ = data.customerName + ' has cancelled the booking.';
            break;
        case 14:
            return_ = data.providerName + '  has paused the job';
            break;


        default:
            return_ = 'Unassigned.';


    }
    return return_;

}