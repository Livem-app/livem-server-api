'use strict';

const joi = require('joi');
const Async = require('async');
const error = require('../../router/customer/error');
const moment = require('moment');
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;
const mqtt_module = require('../../../models/MQTT');
const provider = require('../../../models/provider');
const schedule = require('../../../models/schedule');
const category = require('../../../models/category');
const cities = require('../../../models/cities');
const dailySchedules = require('../../../models/dailySchedules');
const providerProfileViews = require('../../../models/providerProfileViews');
const configuration = require('../../../configuration');
const turf = require('../../../library/turf');

const getProviderhandler = (req, reply) => {
    logger.info("Locatio Post APi", req);
    const dbError = { message: error['genericErrMsg']['500']['1'], data: 500 };
    const getAllProvider = () => {
        return new Promise((resolve, reject) => {
            if (req.bookingType == 1) {
                provider.readBylatLong(req.lat, req.long, cityId, (err, res) => {
                    return err ? reject({ message: error['genericErrMsg']['500']['1'], code: 500 })
                        : resolve(res);
                });
            } else if (req.bookingType == 2) {
                let scheduleIdArr = [];
                let providerIdArr = [];
                let providerData = [];
                let serverTime = (moment().unix());
                let deviceTime = moment(req.deviceTime, 'YYYY-MM-DD HH:mm:ss').unix();
                let timeZone = serverTime - deviceTime - 200;
                let scheduledate = req.scheduleDate;//moment(req.scheduleDate).unix();
                let strtDateTime = scheduledate;//+ timeZone;
                let endDate = strtDateTime + req.scheduleTime;
                let dateTime = moment.unix(strtDateTime).format('YYYY-MM-DD hh:mm');
                let startOfDay = moment(dateTime).startOf('day').unix();
                let query = {
                    'start': { '$lte': strtDateTime },
                    'end': { '$gte': endDate },
                }
                schedule.readByAggregate(req.lat, req.long, query, (err, res) => {
                    if (res === null) {
                        return reject({ message: error['postLocation']['409']['1'], code: 409 })
                    } else if (res) {

                        res.forEach(e => {
                            scheduleIdArr.push(new ObjectID(e._id));
                        });
                        let condition = [
                            { '$match': { "date": startOfDay } },
                            { '$unwind': '$schedule' },
                            {
                                '$match': {
                                    "schedule.scheduleId": { "$in": scheduleIdArr },
                                }
                            }

                        ];
                        dailySchedules.readByAggregate(condition, (err, res) => {
                            if (res) {
                                res.forEach(pro => {
                                    providerIdArr.push(new ObjectID(pro.schedule.providerId));
                                });
                                provider.readByAggregate(req.lat, req.long, { "_id": { "$in": providerIdArr }, cityId: cityId, profileStatus: 1 }, (err, res) => {
                                    Async.forEach(res, (pro, callbackloopv) => {
                                        category.read({ _id: new ObjectID(pro.catlist[0].cid) }, (err, res) => {
                                            let strtDateTimeSer = scheduledate - (parseInt(res.prepTime) * 60);
                                            let endTimeSer = (parseInt(res.prepTime) * 60) + strtDateTime + (req.scheduleTime * 60) + (parseInt(res.packupTime) * 60);
                                            let condition = [
                                                { '$unwind': '$schedule' },
                                                {
                                                    '$match': {
                                                        "schedule.providerId": new ObjectID(pro._id),
                                                        'schedule.startTime': { '$lte': strtDateTimeSer },
                                                        'schedule.endTime': { '$gte': endTimeSer }
                                                    }
                                                }

                                            ];

                                            dailySchedules.readByAggregate(condition, (err, res) => {
                                                if (err) {
                                                    // reject(dbError);
                                                } else if (res === null) {
                                                    callbackloopv(null);
                                                } else {
                                                    // console.log("--", res);
                                                    Async.forEach(res, function (sche, callbackloop2) {
                                                        let scheduleData = sche.schedule;
                                                        if (scheduleData.location) {
                                                            pro.location = scheduleData.location;
                                                        }

                                                        // pro.location = scheduleData.location;
                                                        let booked = scheduleData.booked
                                                        if (scheduleData.startTime <= strtDateTimeSer && scheduleData.endTime >= endTimeSer) {
                                                            if (booked.length != 0) {
                                                                Async.forEach(booked, function (bookSche, callbackloopp) {
                                                                    if (
                                                                        ((strtDateTimeSer + 600) <= bookSche.start && endTimeSer <= bookSche.start) ||
                                                                        ((strtDateTimeSer + 600) >= bookSche.end && endTimeSer >= bookSche.end)
                                                                    ) {
                                                                        logger.debug("-----not----booked-----");
                                                                        callbackloopp(null);
                                                                    }
                                                                    else {
                                                                        logger.debug("---------booked-----");
                                                                        callbackloopp(dbError);
                                                                    }
                                                                    // callbackloopp(null)
                                                                }, (err, res) => {
                                                                    if (err) {
                                                                        logger.error("err in Async For loop", err)
                                                                    } else {
                                                                        // console.log(pro);
                                                                        providerData.push(pro);
                                                                    }

                                                                    callbackloop2(null);
                                                                });
                                                            } else {
                                                                // console.log(pro);
                                                                providerData.push(pro);
                                                                callbackloop2('false');
                                                            }
                                                        } else {
                                                            callbackloop2('false');
                                                        }
                                                    }, function (loopErr, res) {
                                                        callbackloopv(null);
                                                    });
                                                }
                                            });


                                        });
                                    }, (loopErr, res) => {
                                        return err ? reject({ message: error['genericErrMsg']['500']['1'], code: 500 })
                                            : resolve(providerData);
                                    });
                                });

                            }
                        });

                    }
                });
            }
        });
    }
    let currencySymbol = '$';
    let currency = 'USD';
    let cityId = "";
    let distanceMatrix = 0;
    const getCity = (data) => {
        return new Promise((resolve, reject) => {
            // turf.isWithinPolygons(req.lat, req.long, (err, res) => {
            //     if (res == null) { 
            //         resolve(data);

            //     } else {
            //         currencySymbol = res ? res.currencySymbol : '$' || '$';
            //         currency = res ? res.currency : 'USD' || 'USD';
            //         resolve(data);
            //     }
            // });
            let con = {
                polygons: {
                    $geoIntersects: {
                        $geometry: {
                            type: 'Point',
                            coordinates: [parseFloat(req.long || 0.0), parseFloat(req.lat || 0.0)]
                        }
                    }
                }
            };
            cities.read(con, (err, res) => {
                if (err) {
                    return reject({ message: error['genericErrMsg']['500']['1'], code: 500 });
                }
                if (res === null) {
                    logger.error("city not operational")
                    var msg = {
                        errNum: 404, errFlag: 1,
                        errMsg: 'Currently we are not operational in your region , please contact liveM support.', data: []
                    };
                    mqtt_module.publish('provider/' + req.customerId, JSON.stringify(msg), { qos: 0 }, function (mqttErr, mqttRes) {
                        if (mqttErr)
                            return reject({ message: error['genericErrMsg']['500']['1'], code: 500 });
                        else {
                            return reject({ message: error['genericErrMsg']['500']['1'], code: 500 });
                        }
                    })
                } else {
                    currencySymbol = res ? res.currencySymbol : '$' || '$';
                    currency = res ? res.currency : 'USD' || 'USD';
                    distanceMatrix = res ? res.distanceMatrix : 0;
                    cityId = (res._id).toString();
                    resolve(data);
                }

            });
        });
    }
    const responseData = (data) => {
        return new Promise((resolve, reject) => {
            let dataArr = [];
            if (data.length == 0) {
                return resolve(dataArr);
            }
            Async.forEach(data, (e, callbackloop) => {
                console.log("ee", e.firstName + 'Location ' + e.location.longitude + 'lat' + e.location.latitude + ' e.distance ' + e.distance);
                let distanceUnit = distanceMatrix == 0 ? 1000 : 1608;
                let proDistance = e.distance / distanceUnit;
                if (proDistance < e.radius) {
                    let price = e.services[0] ? e.services[0].price : 0
                    Async.forEach(e.services, (ser, callbackloo1) => {
                        price = ser.price < price ? ser.price : price;
                        callbackloo1(null, price);
                    }, (loopErr) => {
                        let dt = {
                            'id': (e._id).toString(),
                            'firstName': e.firstName ? e.firstName : '',
                            'lastName': e.lastName ? e.lastName : '',
                            'image': e.profilePic ? e.profilePic : '',
                            'location': e.location ? e.location : '',
                            'bannerImage': e.image ? e.image : '',
                            'status': e.status == 3 ? 1 : 0,
                            'distance': proDistance > 0 ? parseFloat((proDistance).toFixed(2)) : 0.00 || 0.00,
                            'youtubeUrlLink': e.link || '',
                            'averageRating': e.averageRating,
                            'amount': price || 0,
                            'providerId': parseFloat(e._id),
                            "currencySymbol": currencySymbol || "$",
                            "currency": currency || "USD",
                            "distanceMatrix": distanceMatrix || 0
                        };
                        dataArr.push(dt);
                        callbackloop(null, dataArr);
                    });
                } else {
                    callbackloop(null, dataArr);
                }

            }, (loopErr) => {
                if (dataArr.length != 0) {
                    dataArr.sort((a, b) => {
                        return ((a.id > b.id) ? 1 : -1);
                    });
                    resolve(dataArr);
                }

            });
        });
    }
    const sendMqtt = (data) => {
        return new Promise((resolve, reject) => {
            var msg;
            if (data.length == 0) {
                msg = {
                    errNum: 404, errFlag: 1,
                    errMsg: 'Currently no providers are available at this location.', data: []
                };
            } else {
                msg = { errNum: 200, errFlag: 0, errMsg: 'Got Details.', data: data };
            }
            mqtt_module.publish('provider/' + req.customerId, JSON.stringify(msg), { qos: 0 }, function (mqttErr, mqttRes) {
                if (mqttErr)
                    return reject({ message: error['genericErrMsg']['500']['1'], code: 500 });
                else {
                    return resolve(data)
                }
            })
        });
    }
    getCity()
        .then(getAllProvider)
        .then(responseData)
        .then(sendMqtt)
        .then(data => {
            return reply({ message: error['postLocation']['200']['1'], data: data });
        }).catch(e => {
            logger.error("customer getProvider API error =>", e)
            return reply({ message: e.message });
        })
};


module.exports = {
    getProviderhandler
};