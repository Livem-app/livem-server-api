

'use strict';
const logger = require('winston');
const provider = require('../../../models/provider');
const providerDailyOnlineLogs = require('../../../models/providerDailyOnlineLogs');
const providerOnlineLogs = require('../../../models/providerOnlineLogs');
const dispatcher = require('../dispatcher');
const fcm = require('../../../library/fcm');
const moment = require('moment');
const ObjectID = require('mongodb').ObjectID;
const Timestamp = require('mongodb').Timestamp;


const timeOut = (providerId) => {

    var st = 'timeout'
    var updateQuery = {
        providerId: providerId,
        status: st,
        latitude: 0.0,
        longitude: 0.0
    };
    providerOnlineLogs.updateByProviderId(updateQuery, (err, res) => {
    });//update in online log
    let timestamp = moment().unix();
    providerDailyOnlineLogs.read({ providerId: new ObjectID(providerId) }, (err, doc) => {
        let lastOnline = doc ? doc.lastOnline || timestamp : timestamp;
        let totalOnline = doc ? doc.totalOnline || 0 : 0;
        totalOnline += (timestamp - lastOnline);
        let queryObj = {
            query: { providerId: new ObjectID(providerId), date: moment().format('DD-MMM-YY'), timestamp: moment().startOf('day').unix() },
            data: {
                $set: { totalOnline: totalOnline },
                $push: {
                    logs: { timeStamp: timestamp, status: parseInt(4), interval: timestamp - lastOnline },
                    shifts: { startTs: lastOnline, endTs: timestamp, interval: timestamp - lastOnline }
                }
            }
        };
        providerDailyOnlineLogs.updateByProviderId(queryObj, (err, res) => { });
    })//update in daaily log


    let updateData = { $set: { "status": 7, "statusMsg": "Timeout", statusUpdateTime: new Timestamp(1, moment().unix()) } };
    provider.findOneAndUpdate(providerId, updateData, (err, cbData) => {
        dispatcher.providerStatus({ _id: providerId }, (err, res) => { });
    });//send dispatcher

    let con = { _id: ObjectID(providerId) };
    provider.read(con, (err, res) => {

        try {
            let requestforProvider = {
                fcmTopic: res.fcmTopic,
                action: 5,
                pushType: 1,
                title: "provider location update",
                msg: "provider location update",
                deviceType: res.mobileDevices.deviceType || 1
            }
            if (res.mobileDevices.deviceType == 1) {
                fcm.notifyForTimeOut(requestforProvider, (e, r) => { });
            }

        } catch (err) {
            logger.error("Provider send push for time out", err);
        }

    });
}

module.exports = {
    timeOut
};