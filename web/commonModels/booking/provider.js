//update booking status in provider collection

'use strict';
const logger = require('winston');
const moment = require('moment-timezone');
const email = require('../email/customer');
const ObjectID = require('mongodb').ObjectID;
const provider = require('../../../models/provider');

const updateBookingStatus = (providerId, bookingId, status) => {
    status = parseInt(status);
    switch (status) {
        case 1:
            let updateQuery1 = {
                query: { _id: new ObjectID(providerId) },
                data: { $push: { unAssignedBooking: { bookingId: bookingId, status: status } } }
            };
            provider.updateOne(updateQuery1, (err, result) => {
            });
            break;
        case 2:
            let updateQuery2 = {
                query: { _id: new ObjectID(providerId), "unAssignedBooking.bookingId": bookingId },
                data: { $set: { 'unAssignedBooking.$.status': status } }
            };
            provider.updateOne(updateQuery2, (err, result) => {
            });
            break;

        case 3: 
            let updateQuery23 = {
                query: { _id: new ObjectID(providerId) },
                data: { $pull: { unAssignedBooking: { "bookingId": bookingId } } }
            };
            provider.updateOne(updateQuery23, (err, result) => {
            });
            let updateQuery22 = {
                query: { _id: new ObjectID(providerId) },
                data: { $push: { assignedBooking: { bookingId: bookingId, status: status } } }
            };
            provider.updateOne(updateQuery22, (err, result) => {
            });
            break;


        case 6:
        case 7:
        case 8:
        case 9: 
            let updateQuery9 = {
                query: { _id: new ObjectID(providerId), "assignedBooking.bookingId": bookingId },
                data: { $set: { 'assignedBooking.$.status': status } }
            };
            provider.updateOne(updateQuery9, (err, result) => {
            });
            break;

        case 4:
        case 5: 
            let updateQuery4 = {
                query: { _id: new ObjectID(providerId) },
                data: { $pull: { unAssignedBooking: { "bookingId": bookingId } } }
            };
            provider.updateOne(updateQuery4, (err, result) => {
            });
            let updateQuery5 = {
                query: { _id: new ObjectID(providerId) },
                data: { $push: { booking: { bookingId: bookingId, status: status } } }
            };
            provider.updateOne(updateQuery5, (err, result) => {
            });
            break;
        case 10:
        case 11:
        case 12: 
            let updateQuery10 = {
                query: { _id: new ObjectID(providerId) },
                data: { $pull: { assignedBooking: { "bookingId": bookingId } } }
            };
            provider.updateOne(updateQuery10, (err, result) => {
            });
            let updateQuery11 = {
                query: { _id: new ObjectID(providerId) },
                data: { $push: { booking: { bookingId: bookingId, status: status } } }
            };
            provider.updateOne(updateQuery11, (err, result) => {
            });
            break;

    }
}
module.exports = {
    updateBookingStatus
};