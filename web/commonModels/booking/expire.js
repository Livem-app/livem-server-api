'use strict';
const logger = require('winston');
const moment = require('moment-timezone');
const email = require('../email/customer');
const ObjectID = require('mongodb').ObjectID;
const bookingMsg = require('../notificationMsg/bookingMsg');
const fcm = require('../../../library/fcm');
const paymentMethod = require('../paymentMethod');
const mqtt_module = require('../../../models/MQTT');
const bookings = require('../../../models/bookings');
const customer = require('../../../models/customer');
const provider = require('../../../models/provider');
const dailySchedules = require('../../../models/dailySchedules');
const webSocket = require('../../../models/webSocket');
const assignedBookings = require('../../../models/assignedBookings');
const unassignedBookings = require('../../../models/unassignedBookings');
const providerUpdateStatus = require('./provider');
const stripe = require('../stripe');

const campaignAndreferral = require('../../router/campaignAndreferral/promoCode/post');
const expired = (bookingId) => {
    let bookingData = '';
    let customerData = '';
    let providerData = '';
    const getBooking = (data) => {
        return new Promise((resolve, reject) => {
            let condition = { bookingId: parseFloat(bookingId) };
            unassignedBookings.read(condition, (err, res) => {
                if (err) {
                    return reject(err);
                } else {
                    bookingData = res;
                    return resolve(res);
                }
            });
        });
    }
    const readCustomer = (data) => {
        return new Promise((resolve, reject) => {
            let con = { _id: ObjectID(bookingData.slaveId) };
            customer.read(con, (err, res) => {
                customerData = res;
                return err ? reject(err) : resolve(bookingData);
            });
        });
    }
    const readProvider = (data) => {
        return new Promise((resolve, reject) => {
            let con = { _id: ObjectID(bookingData.providerId) };
            provider.read(con, (err, res) => {
                providerData = res;
                return err ? reject(err) : resolve(bookingData);
            });
        });
    }
    const removeSchedule = (data) => {
        return new Promise((resolve, reject) => {
            if (bookingData.bookingType == 2) {
                let scheduleData;
                dailySchedules.readByBookingId(parseFloat(bookingId), (err, res) => {
                    if (err)
                        return resolve(data);
                    if (res) {
                        dailySchedules.cancelSchedule(parseFloat(bookingId), (err, result) => {
                            let schedule = res.schedule;
                            schedule.forEach(element => {
                                if (element["providerId"] == bookingData.providerId) {
                                    let scheArr = "";
                                    let bookArr = [];
                                    let book = element.booked;
                                    if (book.length != 0) {
                                        book.forEach(booked => {
                                            if (booked.bookingId != parseFloat(bookingId)) {
                                                bookArr.push(booked);
                                            } else {
                                                scheArr = element;
                                            }
                                        });
                                        scheArr.booked = bookArr;
                                        dailySchedules.addDailyScheduleInBook(res._id, scheArr, (err, res) => {
                                            return resolve(data);
                                        });
                                    }

                                }
                            });
                        });
                    } else {
                        return resolve(data);
                    }
                });
            } else {
                return resolve(data);
            }
        });

    }
    const sendPush = (data) => {
        return new Promise((resolve, reject) => {
            if (bookingData.claimData && Object.keys(bookingData.claimData).length !== 0 && bookingData.claimData.claimId != "") {
                campaignAndreferral.unlockCouponHandler({ claimId: bookingData.claimData.claimId, bookingId: bookingData.bookingId }, (err, res) => {
                 
                })
            }
            var mqttRes = {
                bookingId: bookingData.bookingId,
                status: 5,
                statusMsg: "Expired",
                statusUpdateTime: moment().unix(),

            };

            let requestforCustomer = {
                fcmTopic: customerData.fcmTopic,
                action: 5,
                pushType: 1,
                title: bookingMsg.notifyiTitle(5, 1),
                msg: bookingMsg.notifyiMsg(5, 1, {
                    providerName: providerData.firstName + ' ' + providerData.lastName
                }),
                data: mqttRes,
                deviceType: customerData.mobileDevices.deviceType || 1
            }
            let requestforProvider = {
                fcmTopic: providerData.fcmTopic,
                action: 5,
                pushType: 1,
                title: "Booking Expired",
                msg: "Booking Expired",
                data: mqttRes,
                deviceType: providerData.mobileDevices.deviceType || 1
            }
            fcm.notifyFcmTpic(requestforCustomer, (e, r) => { });
            fcm.notifyFcmTpic(requestforProvider, (e, r) => { });
            var msg = { errNum: 200, errFlag: 0, errMsg: 'Got Details.', data: mqttRes };
            mqtt_module.publish('jobStatus/' + bookingData.slaveId, JSON.stringify(msg), { qos: 2 }, function (mqttErr, mqttRes) {
            });
            mqtt_module.publish('jobStatus/' + bookingData.providerId, JSON.stringify(msg), { qos: 2 }, function (mqttErr, mqttRes) {
            });
            webSocket.publish('admin/dispatchLog', JSON.stringify(msg), {}, (mqttErr, mqttRes) => {
            });
            return resolve(data);
        });
    }
    const sendMailToCustomer = (data) => {
        return new Promise((resolve, reject) => {
            let params = {
                toName: customerData.firstName + " " + customerData.lastName,
                to: customerData.email,
                bookingDate: moment.unix(bookingData.bookingRequestedFor).tz("Asia/Kolkata").format('MMMM Do YYYY, h:mm:ss a'),
                providerName: providerData.firstName + ' ' + providerData.lastName

            };
            email.expiredBooking(params);
            // paymentMethod.refund({ bookingId: bookingData.bookingId }, (err, res) => { });//refund reject booking
            return resolve(data);
        });
    }
    const checkStripeAndUpdate = (data) => {
        return new Promise((resolve, reject) => {
            if (bookingData.paymentMethod == 2) {
                stripe.stripeTransaction.refundCharge(
                    bookingData.accounting.chargeId,
                    ''
                ).then(res => {
                    logger.info("Expire booking refund sucess");
                }).catch(e => {
                    logger.error("cancel booking by provider stripe error", e)
                });
            }

            bookingData.status = 5;
            bookingData.statusMsg = "Artist didn't respond to this booking.";
            let jobStatusLogs = {
                status: 5,
                statusMsg: 'Expired',
                stausUpdatedBy: '',
                userId: bookingData.providerId || '',
                timeStamp: moment().unix(),
                latitude: '',
                longitude: ''
            }
            let jobStatusArr = bookingData.jobStatusLogs;
            jobStatusArr.push(jobStatusLogs);
            bookingData.jobStatusLogs = jobStatusArr;
            bookingData.accounting.amount = 0;
            bookingData.accounting.cancellationFee = 0;
            bookingData.accounting.discount = 0;
            bookingData.accounting.total = 0;
            bookingData.accounting.dues = 0;
            bookingData.bookingCompletedAt = moment().unix();
            providerUpdateStatus.updateBookingStatus(bookingData.providerId, parseFloat(bookingId), 5);
            unassignedBookings.findOneDelete({ bookingId: parseFloat(bookingId) }, (err, res) => {
                if (err) {
                    return reject(dbErrResponse);
                } else if (res === null) {
                    return reject({ message: "Not found", code: 404 });
                } else {
                    bookings.post(bookingData, (err, result) => {
                        return err ? reject(err) : resolve(result);
                    });
                }
            });

        });

    }
    getBooking()
        .then(readProvider)
        .then(readCustomer)
        .then(removeSchedule)
        .then(checkStripeAndUpdate)
        .then(sendPush)
        .then(sendMailToCustomer)
        .then(data => {
            logger.info("push send for expired booking");
        }).catch(e => {
            logger.error("Exxpire Booking error", e);
        });


}

module.exports = {
    expired
};