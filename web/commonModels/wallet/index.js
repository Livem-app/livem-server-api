'use strict';

const user = require('./user');

const wallet = require('./wallet');

const transcation = require('./transcation');

module.exports = {
    user,
    wallet,
    transcation
};