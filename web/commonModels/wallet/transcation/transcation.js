'use strict';
const logger = require('winston');
const moment = require('moment');
const ObjectID = require('mongodb').ObjectID;
const rabbitmqUtil = require('../../../../models/rabbitMq/wallet');
const rabbitMq = require('../../../../models/rabbitMq');
const wallet = require('../../../../worker/wallet/wallet')

const cashTransction = (params, callback) => {
    const recivedFromCustomer = (data) => {
        return new Promise((resolve, reject) => {
            let dataArr = {
                userId: params.userId.toString(),
                trigger: 'TRIP',
                comment: 'Invoice payment - Cash Collected',
                currency: params.currency || 'usd',
                currencySymbol: params.currencySymbol || '$',
                txnType: 1,
                amount: parseFloat(params.amount || 0),
                paymentType: 'CASH',
                paymentTxtId: '',
                tripId: params.bookingId,
                bookingType: 'Service',
                userType: 1
            };
            wallet.walletTransction(dataArr, (err, res) => {
            });
            return resolve(data);
        });
    }
    const debitInApp = (data) => {
        return new Promise((resolve, reject) => {
            let dataArr = {
                userId: 1,
                trigger: 'TRIP',
                comment: 'Invoice payment - Cash Collected By Provider',
                currency: params.currency || 'usd',
                currencySymbol: params.currencySymbol || '$',
                txnType: 2,
                amount: parseFloat(0),
                paymentType: 'CASH',
                tripId: params.bookingId,
                bookingType: 'Service',
                userType: 4,
                initiatedBy: 'Customer'
            };
            wallet.walletTransction(dataArr, (err, res) => {
            });
            return resolve(data);
        });
    } //debit from apps wallet & credit it into masters wallet

    const creditInProvider = (data) => {

        return new Promise((resolve, reject) => {
            let dataArr = {
                userId: params.providerId,
                trigger: 'TRIP',
                comment: 'Invoice payment - Cash Collected By Provider',
                currency: params.currency || 'usd',
                currencySymbol: params.currencySymbol || '$',
                txnType: 1,
                amount: parseFloat(params.appEarning || 0),
                paymentType: 'CASH',
                tripId: params.bookingId,
                bookingType: 'Service',
                userType: 2,
                initiatedBy: 'Customer'
            };
            wallet.walletTransction(dataArr, (err, res) => {
            });
            return resolve(data);
        });
    } //debit from apps wallet & credit it into masters wallet
    recivedFromCustomer()
        .then(debitInApp)
        .then(creditInProvider)
        .then(data => {
            callback(null, true)
        }).catch(e => {
            callback(e)
        });
}


const cardTransction = (params, callback) => {
    console.log("---", params)
    const recivedFromCustomer = (data) => {
        return new Promise((resolve, reject) => {
            let dataArr = {
                userId: params.userId.toString(),
                trigger: 'TRIP',
                comment: 'Invoice payment by customer',
                currency: params.currency || 'usd',
                currencySymbol: params.currencySymbol || '$',
                txnType: 2,
                amount: parseFloat(params.amount || 0),
                paymentType: 'CARD',
                paymentTxtId: params.chargeId,
                tripId: params.bookingId,
                bookingType: 'Service',
                userType: 1
            };

            wallet.walletTransction(dataArr, (err, res) => {
                return err ? reject(err) : resolve(params);
            });
        });
    }//debit from customers card & log into wallet transaction

    const creditInApp = (data) => {
        return new Promise((resolve, reject) => {
            let dataArr = {
                userId: 1,
                trigger: 'TRIP',
                comment: 'Invoice payment by customer',
                currency: params.currency || 'usd',
                currencySymbol: params.currencySymbol || '$',
                txnType: 1,
                amount: parseFloat(params.amount || 0),
                paymentType: 'CARD',
                paymentTxtId: params.chargeId,
                tripId: params.bookingId,
                bookingType: 'Service',
                userType: 4,
                initiatedBy: 'Customer'
            };

            wallet.walletTransction(dataArr, (err, res) => {
                return err ? reject(err) : resolve(params);
            });
        });
    } //credit in app
    const debitInProvider = (data) => {

        return new Promise((resolve, reject) => {
            let dataArr = {
                userId: params.providerId,
                trigger: 'TRIP',
                comment: 'Invoice payment - Cash Collected By Provider',
                currency: params.currency || 'usd',
                currencySymbol: params.currencySymbol || '$',
                txnType: 2,
                amount: parseFloat(params.providerEarning || 0),
                paymentType: 'CARD',
                paymentTxtId: params.chargeId,
                tripId: params.bookingId,
                bookingType: 'Service',
                userType: 2,
                initiatedBy: 'Customer'
            };

            wallet.walletTransction(dataArr, (err, res) => {
                return err ? reject(err) : resolve(params);
            });
        });
    } //debit from masters wallet
    const debitPaymentGatwayCommission = (data) => {

        return new Promise((resolve, reject) => {
            let dataArr = {
                userId: 1,
                trigger: 'TRIP',
                comment: 'Invoice payment - PG commission',
                currency: params.currency || 'usd',
                currencySymbol: params.currencySymbol || '$',
                txnType: 2,
                amount: parseFloat(params.pgComm || 0),
                paymentType: 'CARD',
                paymentTxtId: params.chargeId,
                tripId: params.bookingId,
                bookingType: 'Service',
                userType: 4,
                initiatedBy: 'Customer'
            };

            wallet.walletTransction(dataArr, (err, res) => {
                return err ? reject(err) : resolve(params);
            });
        });
    }//debit from apps wallet(payment gateway fee)



    const creditInPGWallet = (data) => {
        return new Promise((resolve, reject) => {
            let dataArr = {
                userId: 1,
                trigger: 'TRIP',
                comment: 'Invoice payment - PG commission',
                currency: params.currency || 'usd',
                currencySymbol: params.currencySymbol || '$',
                txnType: 1,
                amount: parseFloat(params.pgComm || 0),
                paymentType: 'CARD',
                paymentTxtId: params.chargeId,
                tripId: params.bookingId,
                bookingType: 'Service',
                userType: 5,
                initiatedBy: 'Customer'
            };

            wallet.walletTransction(dataArr, (err, res) => {
                return err ? reject(err) : resolve(params);
            });
        });
    } //credit into pg wallet

    recivedFromCustomer()
        .then(creditInApp)
        .then(debitPaymentGatwayCommission)
        .then(debitInProvider)
        .then(creditInPGWallet)
        .then(data => {
            callback(null, true)
        }).catch(e => {
            callback(e)
        });
}


/**
 * Method to update the masters wallet on bank payout
 * @param {*} params - masterId, amount, txnId
 */
const bankPayout = (params, callback) => {
    const creditInProvider = (data) => {
        return new Promise((resolve, reject) => {
            let dataArr = {
                userId: params.providerId,
                trigger: 'PAYOUT',
                comment: 'Payout by app',
                currency: 'eur',
                currencySymbol: '€',
                txnType: 1,
                amount: parseFloat(params.amount || 0),
                paymentType: 'BANK PAYOUT',
                paymentTxtId: params.txnId,
                userType: 2,
            };
            wallet.walletTransction(dataArr, (err, res) => {
            });
            return resolve(data);
        });
    }//update the masters wallet i.e, credit into the masters wallet
    const debitInApp = (data) => {
        return new Promise((resolve, reject) => {
            let dataArr = {
                userId: 1,
                trigger: 'PAYOUT',
                comment: 'Payout by app',
                currency: 'eur',
                currencySymbol: '€',
                txnType: 2,
                amount: parseFloat(params.amount || 0),
                paymentType: 'BANK PAYOUT',
                paymentTxtId: params.txnId,
                userType: 4,
            };
            wallet.walletTransction(dataArr, (err, res) => {
            });
            return resolve(data);
        });
    }//debit from apps wallet

    creditInProvider()
        .then(debitInApp)
        .then(data => {
            callback(null, true)
        }).catch(e => {
            callback(e)
        });

}
module.exports = {
    bankPayout,
    cashTransction,
    cardTransction
};