const stripeErrorMsg = require('../../router/stripe/stripeErrorMsg'); 

let errorMessage = (err) => {
    return new Promise((resolve, reject) => {
        let message = '';
        console.log("mmmmmmmmmmmmmmmmmmmmmmmmmmm", err.type )
        switch (err.type) {
            case 'StripeCardError':
                // A declined card error
                message = err.message; // => e.g. "Your card's expiration year is invalid."
                break;
            case 'RateLimitError':
                // Too many requests made to the API too quickly
                message = stripeErrorMsg['stripeErr']['rateLimit'];
                break;
            case 'StripeInvalidRequestError':
                // Invalid parameters were supplied to Stripe's API
                message = stripeErrorMsg['stripeErr']['invalidParams'];
                break;
            case 'StripeAPIError':
                // An error occurred internally with Stripe's API
                message = stripeErrorMsg['stripeErr']['apiErr'];
                break;
            case 'StripeConnectionError':
                // Some kind of error occurred during the HTTPS communication
                message = stripeErrorMsg['stripeErr']['connectionErr'];
                break;
            case 'StripeAuthenticationError':
                // You probably used an incorrect API key
                message = stripeErrorMsg['stripeErr']['authErr'];
                break;
            default:
                // Handle any other types of unexpected errors
                message = stripeErrorMsg['stripeErr']['defaultErr'];
                break;
        }
        return resolve(err.type + ' : ' + (err.message || message));
    });
}

module.exports = {
    errorMessage
};