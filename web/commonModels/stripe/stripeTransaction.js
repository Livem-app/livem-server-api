'use strict';

const Joi = require('joi');
const logger = require('winston');
const moment = require('moment');
const async = require('async');
const ObjectID = require('mongodb').ObjectID;

const config = require('../../../config');
const stripeMode = config.stripe.STRIPE_MODE;
const stripeConnectAccount = require('../../../models/stripeConnectAccount');
const stripeCustomer = require('../../../models/stripeCustomer');
//const stripeProvider = require('../../../models/stripeProvider');

const stripeLib = require('../../../library/stripe');

const stripeModel = require('../stripe/stripeError');
const stripeErrorMsg = require('../../router/stripe/stripeErrorMsg');

/**
 * Authorised Amount (Create Charge) //create booking
 * @param {*} user  Mongo ID for stripe customer
 * @param {*} source source to charge
 * @param {*} amount amount
 * @param {*} currency currency(USD)
 * @param {*} description 
 */
let createCharge = (user, source, amount, currency, description, metadata) => {
    const dbErrResponse = { message: stripeErrorMsg['genericErrMsg']['500']['0'] };

    let getCustomer = () => {
        return new Promise((resolve, reject) => {
            stripeCustomer.getCustomer(user, stripeMode)
                .then((data) => {
                    if (data) {
                        console.log("------------data---", data)
                        return resolve(data);
                    } else {
                        console.log("---------------")
                        return reject({ message: stripeErrorMsg['stripeTransaction']['customeNotFound'] });
                    }
                }).catch((err) => {
                    console.log("-------err--------".err)
                    return reject(dbErrResponse);
                });
        });
    };

    let chargeCard = (data) => {
        return new Promise((resolve, reject) => {
            let chargeObj = {
                amount: Math.round(parseFloat(amount) * 100),
                currency: currency || 'usd',
                description: description,
                customer: data.stripeId,
                // card: source,
                metadata: metadata,
                capture: false
            };
            if (source != '')
                chargeObj.card = source;
            stripeLib.createCharge(chargeObj, (err, charge) => {
                if (err) {
                    console.log("------------eeeeeeeeeeeeeeeee--", err)
                    stripeModel.errorMessage(err)
                        .then((message) => {
                            return reject({ message: message });
                        });
                } else {
                    console.log("-0-00---0-0")
                    return resolve(charge);
                }
            });
        });
    };

    return new Promise((resolve, reject) => {
        getCustomer()
            .then(chargeCard)
            .then((chargeData) => {
                console.log("-0-00---0-cdddd0")
                return resolve({ message: stripeErrorMsg['stripeTransaction']['authoriseSuccess'], data: chargeData })
            }).catch((err) => {
                console.log("-0-00---0weeeee-0", err)
                logger.error("Stripe create charge error : ", err);
                return reject({ message: err.message });
            });
    });
};

/**
 * Cature uncaptured Charge  // booking complete , booking canceled with charge amount
 * @param {*} chargeId - Charge ID
 * @param {*} amount - amount to charge (send 0 or blank to capture entire amount)
 */
let captureCharge = (chargeId, amount) => {
    let refundCharge = () => {
        return new Promise((resolve, reject) => {
            let captureObj = {};
            if (amount != '' && amount != 0) {
                captureObj.amount = Math.round(parseFloat(amount) * 100);
            }
            stripeLib.capture(chargeId, captureObj, (err, charge) => {
                if (err) {
                    stripeModel.errorMessage(err)
                        .then((message) => {
                            return reject({ message: message });
                        });
                } else {
                    return resolve(charge);
                }
            });
        });
    };
    return new Promise((resolve, reject) => {
        refundCharge()
            .then((chargeData) => {
                return resolve({ message: stripeErrorMsg['stripeTransaction']['captureChargeSuccess'], data: chargeData })
            }).catch((err) => {
                logger.error("Stripe cancel charge error : ", err);
                return reject({ message: err.message });
            });
    });
};

/**
 * Create and Captured Amount
 * @param {*} user  Mongo ID for stripe customer
 * @param {*} source source to charge
 * @param {*} amount amount
 * @param {*} currency currency(USD)
 * @param {*} description 
 */
let createAndCaptureCharge = (user, source, amount, currency, description) => {
    const dbErrResponse = { message: stripeErrorMsg['genericErrMsg']['500']['0'] };

    let getCustomer = () => {
        return new Promise((resolve, reject) => {
            stripeCustomer.getCustomer(user, stripeMode)
                .then((data) => {
                    if (data) {
                        return resolve(data);
                    } else {
                        return reject({ message: stripeErrorMsg['stripeTransaction']['customeNotFound'] });
                    }
                }).catch((err) => {
                    return reject(dbErrResponse);
                });
        });
    };

    let chargeCard = (data) => {
        return new Promise((resolve, reject) => {
            let chargeObj = {
                amount: Math.round(parseFloat(amount) * 100),
                currency: currency || 'usd',
                description: description,
                customer: data.stripeId,
                // card: source,
                capture: true
            };
            if (source != '')
                chargeObj.card = source;
            stripeLib.createCharge(chargeObj, (err, charge) => {
                if (err) {
                    stripeModel.errorMessage(err)
                        .then((message) => {
                            return reject({ message: message });
                        });
                } else {
                    return resolve(charge);
                }
            });
        });
    };

    return new Promise((resolve, reject) => {
        getCustomer()
            .then(chargeCard)
            .then((chargeData) => {
                return resolve({ message: stripeErrorMsg['stripeTransaction']['chargeSuccess'], data: chargeData })
            }).catch((err) => {
                logger.error("Stripe Charge Customer error : ", err);
                return reject({ message: err.message });
            });
    });
};


/**
 * Refund Charge cancel without charge
 * @param {*} chargeId - Charge ID
 * @param {*} amount - amount to refund (send 0 or blank to refund entire amount)
 */
let refundCharge = (chargeId, amount) => {
    let refundCharge = () => {
        return new Promise((resolve, reject) => {
            let refundObj = {
                charge: chargeId
            };
            if (amount != '' || amount != 0)
                refundObj.amount = Math.round(parseFloat(amount) * 100);
            stripeLib.refundCharge(refundObj, (err, charge) => {
                if (err) {
                    stripeModel.errorMessage(err)
                        .then((message) => {
                            return reject({ message: message });
                        });
                } else {
                    return resolve(charge);
                }
            });
        });
    };
    return new Promise((resolve, reject) => {
        refundCharge()
            .then((chargeData) => {
                return resolve({ message: stripeErrorMsg['stripeTransaction']['refundChargeSuccess'], data: chargeData })
            }).catch((err) => {
                logger.error("Stripe cancel charge error : ", err);
                return reject({ message: err.message });
            });
    });
};

/**
 * Transfer to connect account //
 * @param {*} user  Mongo ID for stripe connect account user
 * @param {*} amount amount
 * @param {*} currency currency(USD)
 */
let transferToConnectAccount = (user, amount, currency) => {
    const dbErrResponse = { message: stripeErrorMsg['genericErrMsg']['500']['0'] };

    let getAccount = () => {
        return new Promise((resolve, reject) => {
            console.log("user", user);
            stripeConnectAccount.getAccount(user, stripeMode)
                .then((data) => {
                    if (data) {
                        console.log("strip data", data);
                        return resolve(data);
                    } else {
                        console.log("stripe not found data");
                        return reject({ message: stripeErrorMsg['stripeTransaction']['accountNotFound'] });
                    }
                }).catch((err) => {
                    return reject(dbErrResponse);
                });
        });
    };

    let transferData = (data) => {
        return new Promise((resolve, reject) => {
            let transferObj = {
                amount: Math.round(parseFloat(amount) * 100),
                currency: 'eur',
                destination: data.stripeId,
            };
            stripeLib.transferToConnectAccount(transferObj, (err, transfer) => {
                if (err) {
                    stripeModel.errorMessage(err)
                        .then((message) => {
                            return reject({ message: message });
                        });
                } else {
                    return resolve(transfer);
                }
            });
        });
    };

    return new Promise((resolve, reject) => {
        getAccount()
            .then(transferData)
            .then(transfer => {
                console.log("transfer --- suc", transfer)
                return resolve(transfer);
            }).catch((err) => {
                console.log("--------------------------------------------------", err.message)
                logger.error("Stripe Transfer error : ", err.message);
                return reject({ message: err.message });
            });
    });
};


module.exports = {
    createCharge,
    captureCharge,
    createAndCaptureCharge,
    refundCharge,
    transferToConnectAccount,
};
