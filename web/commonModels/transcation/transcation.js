'use strict';
const logger = require('winston');
const provider = require('../../../models/provider');
const dispatcher = require('../dispatcher');
const fcm = require('../../../library/fcm');
const moment = require('moment');
const ObjectID = require('mongodb').ObjectID;
const rabbitmqUtil = require('../../../models/rabbitMq/emailUtils');
const rabbitMq = require('../../../models/rabbitMq');

const cashTransction = (params, callback) => {
    const recivedFromCustomer = (data) => {
        return new Promise((resolve, reject) => {
            let dataArr = {
                userId: params.userId.toString(),
                trigger: 'TRIP',
                comment: 'Invoice payment - Cash Collected',
                currency: 'usd',
                txnType: 1,
                amount: parseFloat(params.amount || 0),
                paymentType: 'CASH',
                tripId: params.bookingId,
                bookingType: 'Service',
                userType: 2
            };

            rabbitmqUtil.InsertQueue(
                rabbitMq.getChannelWallet(),
                rabbitMq.queueWallet,
                dataArr,
                (err, doc) => {

                });
        });
    }
}

module.exports = {
    cashTransction
};