'use strict'

const async = require('async');
const Moment = require('moment');
const ObjectID = require('mongodb').ObjectID;
const joi = require('joi');

const paymentCycles = require('../../../models/paymentCycles');
const provider = require('../../../models/provider');
const providerDailyOnlineLogs = require('../../../models/providerDailyOnlineLogs');
const payroll = require('../../../models/payroll');
 

const runDailyCycle = (req, reply) => {

    let params = {
        name: 'PC-D-' + Moment().format('DD-MMM-YY'),
        startDate: Moment().startOf('day').unix(),
        endDate: Moment().endOf('day').unix()
    }

    runCycle(params)
        .then(data => { 

            let d = {
                name: params.name,
                startDate: params.startDate,
                endDate: params.endDate,
                operators: 0,
                drivers: 0,
                dues: 0,
                status: 'Not Settled',
                type: 1,
                pendingDrivers: []
            }

            if (data.length > 0) {
                d.drivers = data[0].drivers || 0;
                d.dues = data[0].dues || 0;
                d.pendingDrivers = data[0].pendingDrivers || []
            }

            return updateCycleData(params, d)
        })
        .then(data => { 
        })
        .catch(e => { console.log(error) })

    return reply({ errMsg: 'Started' });
}

module.exports = {
    runDailyCycle
}

const runCycle = (params) => {

    return new Promise((resolve, reject) => {

        let query = [
            { $match: { fromDate: params.startDate, toDate: params.endDate } },
            {
                $group: {
                    _id: null,
                    startDate: { $first: '$fromDate' },
                    endDate: { $first: '$toDate' },
                    drivers: { $sum: 1 },
                    dues: { $sum: '$dues' },
                    pendingDrivers: {
                        $push: {
                            id: '$did',
                            dues: '$dues',
                            name: '$name',
                            email: '$email',
                            phone: '$phone'
                        }
                    }
                }
            }
        ]
        payroll.aggregate(query, (err, data) => {
            if (err)
                return reject(err)

            return resolve(data)
        })
    })
}

const updateCycleData = (condition, data) => {

    return new Promise((resolve, reject) => {

        let query = {
            query: condition,
            data: { $set: data }
        }
        paymentCycles.findUpdate(query, (err, doc) => {
            if (err)
                return reject(err)

            return resolve(doc)
        })
    })
}