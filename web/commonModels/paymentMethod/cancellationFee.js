'use strict';

const joi = require('joi');
const Async = require('async');
const moment = require('moment');
const logger = require('winston');
const stripe = require('../../../library/stripe');
const stripeCharges = require('../../../models/stripeCharges');

const refundCancellaionFee = (params, cb) => {

    let stripeData;
    const getStripCharge = (data) => {
        return new Promise((resolve, reject) => {
            stripeCharges.read({ bookingId: params.bookingId }, (err, res) => {
                if (err) {
                    return reject(err);
                } else {
                    stripeData = res;
                    return resolve(res);
                }
            });
        });
    }
    const refundCharge = (data) => {
        return new Promise((resolve, reject) => {
            stripe.refundCharge(data.chargeId, (err, res) => {
                return err ? reject(err) : resolve(res);
            });
        });
    }
    const insertRefundData = (data) => {
        return new Promise((resolve, reject) => {
            let data = {
                bookingId: params.bookingId,
                customerId: stripeData.customerId,
                cardId: stripeData.cardId,
                chargeId: data.id,
                captured: false,
                slaveId: (stripeData.slaveId),
                amount: parseFloat(stripeData.amount),
                stripeAmount: stripeData.stripeAmount,
                currency: 'usd',
                chargeDate: moment().unix(),
                refund: true,
                refundDate: moment().unix(),
                name: stripeData.name,
                status: 'Refund'
            };
            stripeCharges.post(data, (err, res) => {
                return err ? reject(err) : resolve(res);
            });
        });
    }

    const chargeCancellationFee = (data) => {
        return new Promise((resolve, reject) => {
            if (params.fee != 0) {
                stripe.createCharge({
                    amount: Math.round(parseFloat(params.fee) * 100),
                    currency: 'usd',
                    description: 'bookingId - ' + params.bookingId,
                    customerId: stripeData.customerId,
                    cardId: stripeData.cardId,
                    capture: true
                }, (err, charge) => {
                    if (err) {
                        return reject(err);
                    } else {
                        let inserData = {
                            bookingId: params.bookingId,
                            cardId: stripeData.paymentCardId,
                            chargeId: charge.id,
                            captured: charge.captured,
                            slaveId: stripeData.slaveId,
                            amount: parseFloat(params.fee),
                            stripeAmount: Math.round(parseFloat(params.fee) * 100),
                            currency: 'usd',
                            chargeDate: moment().unix(),
                            chargeDate: moment().unix(),
                            status: (charge.captured) ? 'Capture' : 'Uncapture',
                            name: stripeData.firstName
                        }
                        stripeCharges.post(inserData, (e, r) => {
                            return err ? reject(dbError) : resolve(data);
                        });
                        return resolve(data);
                    }
                });
            } else {
                return resolve(data);
            }


        });
    }

    getStripCharge()
        .then(refundCharge)
        .then(insertRefundData)
        .then(chargeCancellationFee)
        .then(data => {
            return cb(true);
        }).catch(e => {
            return cb(false);
            logger.error("Charge Refund error");
        });

}

module.exports.refundCancellaionFee;