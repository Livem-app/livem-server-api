'use strict';

const joi = require('joi');
const Async = require('async');
const moment = require('moment');
const logger = require('winston');
const stripe = require('../../../library/stripe');
const stripeCharges = require('../../../models/stripeCharges');

const refund = (params, cb) => {
    let stripeData;
    const getStripCharge = (data) => {
        return new Promise((resolve, reject) => {
            stripeCharges.read({ bookingId: params.bookingId }, (err, res) => {
                if (err) {
                    return reject(err);
                } else {
                    stripeData = res;
                    return resolve(res);
                }
            });
        });
    }
    const refundCharge = (data) => {
        return new Promise((resolve, reject) => {
            stripe.refundCharge(data.chargeId, (err, res) => {
                return err ? reject(err) : resolve(res);
            });
        });
    }
    const insertRefundData = (data) => {
        return new Promise((resolve, reject) => {
            let data = {
                bookingId: params.bookingId,
                customerId: stripeData.customerId,
                cardId: stripeData.cardId,
                chargeId: data.id,
                captured: false,
                slaveId: (stripeData.slaveId),
                amount: parseFloat(stripeData.amount),
                stripeAmount: stripeData.stripeAmount,
                currency: 'usd',
                chargeDate: moment().unix(),
                refund: true,
                refundDate: moment().unix(),
                name: stripeData.name,
                status: 'Refund'
            };
            stripeCharges.post(data, (err, res) => {
                return err ? reject(err) : resolve(res);
            });
        });
    }
    getStripCharge()
        .then(refundCharge)
        .then(insertRefundData)
        .then(data => {
            return cb(true);
        }).catch(e => {
            logger.error("Charge Refund error");
            return cb(false);
        });

}

module.exports.refund;