'use strict';

const entity = '/server';
const get = require('./get'); 
const headerValidator = require('../../../middleware/validator');


module.exports = [

    /**
     * @name GET /app/config
     */
    {
        method: 'GET',
        path: entity + '/serverTime',
        handler: get.handler,
        config: {
            tags: ['api', entity],
            description: "This API tp get current server time",
            notes: 'This API tp get current server time.',
            auth: false,
            response: get.responseCode,
            validate: {
                headers: headerValidator.headerLanValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },

];