
'use strict';
const joi = require('joi');
const moment = require('moment-timezone'); 

/**
 * @method GET /customer/serverTime
 * @param {*} req 
 * @param {*} reply  
 */


const handler = (req, reply) => {
    return reply({ message: "success", data: moment().unix() }).code(200);


};//API handler

const responseCode = {
    // status: {

    //     200: { message: joi.any().default(errorMsg['genericErrMsg']['200']), data: joi.any() },
    // }

}//swagger response code

module.exports = {
    handler,
    responseCode
};