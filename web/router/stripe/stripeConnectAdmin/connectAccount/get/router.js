
'use strict';

const headerValidator = require('../../../../../middleware/validator');

const entity = "admin/connectAccount";

const getConnectAccount = require('./get');
const stripeErrorMsg = require('../../../stripeErrorMsg');

module.exports = [
    /**
    * api to post ack bookings
    */
    {
        method: 'GET',
        path: '/' + entity + '/{id}',
        handler: getConnectAccount.APIHandler,
        config: {
            tags: ['api', 'admin'],
            description: stripeErrorMsg['apiDescription']['getConnectAccount'],
            notes: stripeErrorMsg['apiDescription']['getConnectAccount'],
            auth: false,
            response: getConnectAccount.responseCode,
            validate: {
                params: getConnectAccount.payload,
                headers: headerValidator.headerLanValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    }
]