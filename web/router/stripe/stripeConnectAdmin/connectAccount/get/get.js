'use strict';

const Joi = require('joi');
const logger = require('winston');
const moment = require('moment');
const async = require('async');
const ObjectID = require('mongodb').ObjectID;

const config = require('../../../../../../config')
const stripeMode = config.stripe.STRIPE_MODE;
const stripeConnectAccount = require('../../../../../../models/stripeConnectAccount');

const stripeLib = require('../../../../../../library/stripe');

const stripeErrorMsg = require('../../../stripeErrorMsg');
const stripeModel = require('../../../../../commonModels/stripe');

const payload = Joi.object({
    id: Joi.any().required().default('59de3501cb1a0d242003901a').description('_id'), // Require only for testing purpose
}).required();

const APIHandler = (req, reply) => {
    const dbErrResponse = { message: stripeErrorMsg['genericErrMsg']['500'][req.headers.lan], code: 500 };

    let getAccount = () => {
        return new Promise((resolve, reject) => {
            stripeConnectAccount.getAccount(req.params.id, stripeMode)
                .then((data) => {
                    if (data) {
                        stripeLib.retrieveAccount(data.stripeId, (err, account) => {
                            if (err) {
                                stripeModel.stripeError.errorMessage(err)
                                    .then((message) => {
                                        return reject({ message: message, code: 500 });
                                    });
                            } else {
                                return resolve(account);
                            }
                        });
                    } else {
                        return reject({ message: stripeErrorMsg['getConnectAccount']['400'][req.headers.lan], code: 400 });
                    }
                }).catch((err) => {
                    return reject(dbErrResponse);
                });
        });
    };

    getAccount()
        .then((accData) => {
            return reply({ message: stripeErrorMsg['genericErrMsg']['200'][req.headers.lan], data: accData }).code(200);
        }).catch((err) => {
            logger.error("Stripe Get Card error : ", err);
            return reply({ errNum: 400, errMsg: 'Invalid user', errFlag: 1 });
        });
};

let lang = stripeErrorMsg['langaugeId'];

const responseCode = {
    status: {
        500: { message: Joi.any().default(stripeErrorMsg['genericErrMsg']['500'][lang]) },
        400: { message: stripeErrorMsg['getConnectAccount']['400'][lang] },
        200: { message: stripeErrorMsg['genericErrMsg']['200'][lang], data: Joi.any() }
    }
}//swagger response code

module.exports = { payload, APIHandler, responseCode };