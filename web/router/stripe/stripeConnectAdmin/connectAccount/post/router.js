
'use strict';

const headerValidator = require('../../../../../middleware/validator');

const entity = "admin/connectAccount";

const postConnectAccount = require('./post');
const stripeErrorMsg = require('../../../stripeErrorMsg');

module.exports = [
    /**
    * api to add or update account details
    */
    {
        method: 'POST',
        path: '/' + entity,
        handler: postConnectAccount.APIHandler,
        config: {
            tags: ['api', 'admin'],
            description: stripeErrorMsg['apiDescription']['postConnectAccount'],
            notes: stripeErrorMsg['apiDescription']['postConnectAccount'],
            auth: false,
            // response: postConnectAccount.responseCode,
            validate: {
                payload: postConnectAccount.payload,
                headers: headerValidator.headerLanValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    }
]