
'use strict';

const headerValidator = require('../../../../../middleware/validator');

const entity = "admin/externalAccount";

const deleteExternalAccount = require('./delete');
const stripeErrorMsg = require('../../../stripeErrorMsg');

module.exports = [
    /**
    * Api to Delete Card from stripe
    */
    {
        method: 'DELETE',
        path: '/' + entity,
        handler: deleteExternalAccount.APIHandler,
        config: {
            tags: ['api', 'admin'],
            description: stripeErrorMsg['apiDescription']['deleteExternalAccount'],
            notes: stripeErrorMsg['apiDescription']['deleteExternalAccount'],
            auth: false,
            response: deleteExternalAccount.responseCode,
            validate: {
                payload: deleteExternalAccount.payload,
                headers: headerValidator.headerLanValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    }
]