'use strict';

const Joi = require('joi');
const logger = require('winston');
const moment = require('moment');
const async = require('async');
const ObjectID = require('mongodb').ObjectID;

const config = require('../../../../../../config')
const stripeMode = config.stripe.STRIPE_MODE;
const stripeConnectAccount = require('../../../../../../models/stripeConnectAccount');

const stripeLib = require('../../../../../../library/stripe');

const stripeErrorMsg = require('../../../stripeErrorMsg');
const stripeModel = require('../../../../../commonModels/stripe');

const payload = Joi.object({
    // Testing Purpose Only
    id: Joi.any().required().default('59de3501cb1a0d242003901a').description('_id'),
    accountId: Joi.any().required().description('Account ID')
}).required();

const APIHandler = (req, reply) => {
    const dbErrResponse = { message: stripeErrorMsg['genericErrMsg']['500'][req.headers.lan], code: 500 };

    // Testing Purpose Only
    // req.auth = {
    //     credentials: { _id: req.payload.id }
    // };
    // req.headers.lan = 0;

    let getAccount = () => {
        return new Promise((resolve, reject) => {
            stripeConnectAccount.getAccount(req.payload.id, stripeMode)
                .then((data) => {
                    if (data) {
                        return resolve(data);
                    } else {
                        return reject({ message: stripeErrorMsg['deleteExternalAccount']['400'][req.headers.lan], code: 400 });
                    }
                }).catch((err) => {
                    return reject(dbErrResponse);
                });
        });
    };

    let deleteExternalAccount = (data) => {
        return new Promise((resolve, reject) => {
            stripeLib.deleteBankAccount(data.stripeId, req.payload.accountId, (err, confirmation) => {
                if (err) {
                    stripeModel.stripeError.errorMessage(err)
                        .then((message) => {
                            return reject({ message: message, code: 500 });
                        });
                } else {
                    return resolve(true);
                }
            });
        });
    };

    getAccount()
        .then(deleteExternalAccount)
        .then((data) => {
            return reply({ message: stripeErrorMsg['deleteExternalAccount']['200'][req.headers.lan] }).code(200);
        }).catch((err) => {
            logger.error("Stripe Delete Card error : ", err);
            return reply({ message: err.message }).code(err.code);
        });
};

let lang = stripeErrorMsg['langaugeId'];

const responseCode = {
    status: {
        500: { message: Joi.any().default(stripeErrorMsg['genericErrMsg']['500'][lang]) },
        200: { message: stripeErrorMsg['deleteExternalAccount']['200'][lang] },
        400: { message: stripeErrorMsg['deleteExternalAccount']['400'][lang] }
    }
}//swagger response code

module.exports = { payload, APIHandler, responseCode };