
'use strict';

const headerValidator = require('../../../../../middleware/validator');

const entity = "admin/externalAccount";

const patchExternalAccount = require('./patch');
const stripeErrorMsg = require('../../../stripeErrorMsg');

module.exports = [
    /**
    * API to make card as a default
    */
    {
        method: 'PATCH',
        path: '/' + entity,
        handler: patchExternalAccount.APIHandler,
        config: {
            tags: ['api', 'admin'],
            description: stripeErrorMsg['apiDescription']['patchExternalAccount'],
            notes: stripeErrorMsg['apiDescription']['patchExternalAccount'],
            auth: false,
            response: patchExternalAccount.responseCode,
            validate: {
                payload: patchExternalAccount.payload,
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    }
]