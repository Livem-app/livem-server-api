'use strict';

const Joi = require('joi');
const logger = require('winston');
const moment = require('moment');
const async = require('async');
const ObjectID = require('mongodb').ObjectID;

const config = require('../../../../../../config')
const stripeMode = config.stripe.STRIPE_MODE;
const stripeConnectAccount = require('../../../../../../models/stripeConnectAccount');

const stripeLib = require('../../../../../../library/stripe');

const stripeErrorMsg = require('../../../stripeErrorMsg');
const stripeModel = require('../../../../../commonModels/stripe');

const payload = Joi.object({
    // Testing Purpose Only
    id: Joi.any().required().default('59de3501cb1a0d242003901a').description('_id'),
    email: Joi.any().required().default('shailesh@mobifyi.com').description('Email'),

    account_number: Joi.string().required().description('000123456789').default('000123456789'),
    routing_number: Joi.string().required().description('110000').default('110000000'),
    account_holder_name: Joi.string().required().description('account holder name').default('test'),
    country: Joi.string().required().description('country').default('US'),
}).required();

const APIHandler = (req, reply) => {
    const dbErrResponse = { message: stripeErrorMsg['genericErrMsg']['500'][req.headers.lan], code: 500 }; 
    // Testing Purpose Only
    // req.auth = {
    //     credentials: { _id: req.payload.id }
    // };
    // req.headers.lan = 0;

    let getAccount = () => {
        return new Promise((resolve, reject) => {
            stripeConnectAccount.getAccount(req.payload.id, stripeMode)
                .then((data) => {
                    if (data) {
                        stripeLib.retrieveAccount(data.stripeId, (err, account) => {
                            if (err) {
                                stripeModel.stripeError.errorMessage(err)
                                    .then((message) => {
                                        return reject({ message: message, code: 500 });
                                    });
                            } else {
                                return resolve(account);
                            }
                        });
                    } else {
                        return reject({ message: stripeErrorMsg['postExternalAccount']['400'][req.headers.lan], code: 400 });
                    }
                }).catch((err) => {
                    return reject(dbErrResponse);
                });
        });
    };

    let addExternalAccount = (account) => {
        return new Promise((resolve, reject) => {
            let data = {
                account_number: req.payload.account_number,
                routing_number: req.payload.routing_number,
                account_holder_name: req.payload.account_holder_name,
                country: req.payload.country
            };
            stripeLib.addBankAccount(account.id, data, (err, banckAccount) => {
                if (err) {
                    stripeModel.stripeError.errorMessage(err)
                        .then((message) => {
                            return reject({ message: message, code: 500 });
                        });
                } else {
                    return resolve(banckAccount);
                }
            });
        });
    };

    getAccount()
        .then(addExternalAccount)
        .then((data) => {
            return reply({ errNum: 200, errFlag: 0, message: stripeErrorMsg['genericErrMsg']['200'][req.headers.lan] }).code(200);
        }).catch((err) => {
            logger.error("Stripe Post Card error : ", err);
            return reply({ message: err.message }).code(err.code);
        });
};

let lang = stripeErrorMsg['langaugeId'];

const responseCode = {
    status: {
        500: { message: Joi.any().default(stripeErrorMsg['genericErrMsg']['500'][lang]) },
        400: { message: stripeErrorMsg['postExternalAccount']['400'][lang] },
        200: { message: stripeErrorMsg['genericErrMsg']['200'][lang] },
    }
}//swagger response code

module.exports = { payload, APIHandler, responseCode };