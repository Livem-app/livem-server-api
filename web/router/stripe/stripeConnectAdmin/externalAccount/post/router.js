
'use strict';

const headerValidator = require('../../../../../middleware/validator');

const entity = "admin/externalAccount";

const postExternalAccount = require('./post');
const stripeErrorMsg = require('../../../stripeErrorMsg');

module.exports = [
    /**
    * api to add new bank
    */
    {
        method: 'POST',
        path: '/' + entity,
        handler: postExternalAccount.APIHandler,
        config: {
            tags: ['api', 'admin'],
            description: stripeErrorMsg['apiDescription']['postExternalAccount'],
            notes: stripeErrorMsg['apiDescription']['postExternalAccount'],
            auth: false,
            // response: postExternalAccount.responseCode,
            validate: {
                payload: postExternalAccount.payload,
                headers: headerValidator.headerLanValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    }
]