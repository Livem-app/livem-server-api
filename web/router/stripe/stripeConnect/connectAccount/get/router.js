
'use strict';

const headerValidator = require('../../../../../middleware/validator');

const entity = "connectAccount";

const getConnectAccount = require('./get');
const stripeErrorMsg = require('../../../stripeErrorMsg');

module.exports = [
    /**
    * api to post ack bookings
    */
    {
        method: 'GET',
        path: '/' + entity,
        handler: getConnectAccount.APIHandler,
        config: {
            tags: ['api', entity],
            description: stripeErrorMsg['apiDescription']['getConnectAccount'],
            notes: stripeErrorMsg['apiDescription']['getConnectAccount'],
            auth: 'providerJWT',
            response: getConnectAccount.responseCode,
            validate: {
                // params: getConnectAccount.payload,
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    }
]