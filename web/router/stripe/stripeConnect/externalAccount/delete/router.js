
'use strict';

const headerValidator = require('../../../../../middleware/validator');

const entity = "externalAccount";

const deleteExternalAccount = require('./delete');
const stripeErrorMsg = require('../../../stripeErrorMsg');

module.exports = [
    /**
    * Api to Delete Card from stripe
    */
    {
        method: 'DELETE',
        path: '/' + entity,
        handler: deleteExternalAccount.APIHandler,
        config: {
            tags: ['api', entity],
            description: stripeErrorMsg['apiDescription']['deleteExternalAccount'],
            notes: stripeErrorMsg['apiDescription']['deleteExternalAccount'],
            auth: 'providerJWT',
            response: deleteExternalAccount.responseCode,
            validate: {
                payload: deleteExternalAccount.payload,
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    }
]