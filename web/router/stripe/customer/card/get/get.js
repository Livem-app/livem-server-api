'use strict';

const Joi = require('joi');
const logger = require('winston');
const moment = require('moment');
const async = require('async');
const ObjectID = require('mongodb').ObjectID;

const config = require('../../../../../../config')
const stripeMode = config.stripe.STRIPE_MODE;
const stripeCustomer = require('../../../../../../models/stripeCustomer');

const stripeLib = require('../../../../../../library/stripe');

const stripeErrorMsg = require('../../../stripeErrorMsg');
const stripeModel = require('../../../../../commonModels/stripe');

const payload = Joi.object({
}).required();

const APIHandler = (req, reply) => {
    const dbErrResponse = { message: stripeErrorMsg['genericErrMsg']['500'][req.headers.lan], code: 500 };

    let stripeId = '';

    let getCustomer = () => {
        return new Promise((resolve, reject) => {
            stripeCustomer.getCustomer(req.auth.credentials._id, stripeMode)
                .then((data) => {
                    if (data) {
                        stripeId = data.stripeId;
                        stripeLib.retrieveCustomer(data.stripeId, (err, customer) => {
                            if (err) {
                                stripeModel.stripeError.errorMessage(err)
                                    .then((message) => {
                                        return reject({ message: message, code: 500 });
                                    });
                            } else {
                                return resolve(customer.default_source);
                            }
                        });
                    } else {
                        return resolve(true);
                    }
                }).catch((err) => {
                    return reject(dbErrResponse);
                });
        });
    };

    let getCard = (defaultCard) => {
        return new Promise((resolve, reject) => {
            if (stripeId == '')
                return resolve([]);
            else {
                stripeLib.getCards(stripeId, (err, data) => {
                    if (err) {
                        stripeModel.stripeError.errorMessage(err)
                            .then((message) => {
                                return reject({ message: message, code: 500 });
                            });
                    } else {
                        let cardData = data['data'] || [];
                        let cardArr = cardData.map(item => {
                            return {
                                'name': item.name || '',
                                'last4': item.last4,
                                'expYear': item.exp_year,
                                'expMonth': item.exp_month,
                                'id': item.id,
                                'brand': item.brand,
                                'funding': item.funding,
                                'isDefault': (item.id === defaultCard) ? true : false//set the default flag
                            }
                        });
                        return resolve(cardArr);
                    }
                });
            }
        });
    };

    getCustomer()
        .then(getCard)
        .then((cardArr) => {
            return reply({ message: stripeErrorMsg['genericErrMsg']['200'][req.headers.lan], data: cardArr }).code(200);
        }).catch((err) => {
            logger.error("Stripe Get Card error : ", err);
            return reply({ message: err.message }).code(err.code);
        });
};

let lang = stripeErrorMsg['langaugeId'];

const responseCode = {
    status: {
        500: { message: Joi.any().default(stripeErrorMsg['genericErrMsg']['500'][lang]) },
        200: { message: stripeErrorMsg['genericErrMsg']['200'][lang], data: Joi.any() }
    }
}//swagger response code

module.exports = { payload, APIHandler, responseCode };