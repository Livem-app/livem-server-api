
'use strict';

const headerValidator = require('../../../../../middleware/validator');

const entity = "card";

const getCard = require('./get');
const stripeErrorMsg = require('../../../stripeErrorMsg');

module.exports = [
    /**
    * api to get stored cards
    */
    {
        method: 'GET',
        path: '/' + entity,
        handler: getCard.APIHandler,
        config: {
            tags: ['api', entity],
            description: stripeErrorMsg['apiDescription']['getCard'],
            notes: stripeErrorMsg['apiDescription']['getCard'],
            auth: 'customerJWT',
            response: getCard.responseCode,
            validate: {
                // params: getCard.payload,
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    }
]