'use strict';

const Joi = require('joi');
const logger = require('winston');
const moment = require('moment');
const async = require('async');
const ObjectID = require('mongodb').ObjectID;

const config = require('../../../../../../config')
const stripeMode = config.stripe.STRIPE_MODE;
const stripeCustomer = require('../../../../../../models/stripeCustomer');

const stripeLib = require('../../../../../../library/stripe');

const stripeErrorMsg = require('../../../stripeErrorMsg');
const stripeModel = require('../../../../../commonModels/stripe');

const payload = Joi.object({
    email: Joi.any().required().default('shailesh@mobifyi.com').description('Email'),
    cardToken: Joi.any().required().description('Card Token')
}).required();

const APIHandler = (req, reply) => {
    const dbErrResponse = { message: stripeErrorMsg['genericErrMsg']['500'][req.headers.lan], code: 500 };

    let stripeId = '';

    let getCustomer = () => {
        return new Promise((resolve, reject) => {
            stripeCustomer.getCustomer(req.auth.credentials._id, stripeMode)
                .then((data) => {
                    if (data) {
                        stripeId = data.stripeId;
                        return resolve(data);
                    } else {
                        stripeLib.createCustomer({ email: req.payload.email }, (err, data) => {
                            if (err) {
                                stripeModel.stripeError.errorMessage(err)
                                    .then((message) => {
                                        return reject({ message: err.message, code: 500 });
                                    });
                            } else {
                                stripeId = data.id;
                                let insObj = {
                                    user: new ObjectID(req.auth.credentials._id),
                                    mode: stripeMode,
                                    stripeId: data.id
                                };
                                stripeCustomer.createCustomer(insObj)
                                    .then((data) => {
                                        return resolve(data);
                                    }).catch((err) => {
                                        return reject(dbErrResponse);
                                    });
                            }
                        });
                    }
                }).catch((err) => {
                    return reject(dbErrResponse);
                });
        });
    };

    let addCard = () => {
        return new Promise((resolve, reject) => {
            stripeLib.addCard({ id: stripeId, cardToken: req.payload.cardToken }, (err, data) => {
                if (err) {
                    stripeModel.stripeError.errorMessage(err)
                        .then((message) => {
                            return reject({ message: message, code: 500 });
                        });
                } else {
                    return resolve(data);
                }
            });
        });
    };

    getCustomer()
        .then(addCard)
        .then((data) => {
            return reply({ message: stripeErrorMsg['postCard']['200'][req.headers.lan]}).code(200);
        }).catch((err) => {
            logger.error("Stripe Post Card error : ", err);
            return reply({ message: err.message }).code(err.code);
        });
};

let lang = stripeErrorMsg['langaugeId'];

const responseCode = {
    status: {
        500: { message: Joi.any().default(stripeErrorMsg['genericErrMsg']['500'][lang]) },
        200: { message: stripeErrorMsg['postCard']['200'][lang] },
    }
}//swagger response code

module.exports = { payload, APIHandler, responseCode };