
'use strict';

const headerValidator = require('../../../../../middleware/validator');

const entity = "card";

const postCard = require('./post');
const stripeErrorMsg = require('../../../stripeErrorMsg');

module.exports = [
    /**
    * api to add new card
    */
    {
        method: 'POST',
        path: '/' + entity,
        handler: postCard.APIHandler,
        config: {
            tags: ['api', entity],
            description: stripeErrorMsg['apiDescription']['postCard'],
            notes: stripeErrorMsg['apiDescription']['postCard'],
            auth: 'customerJWT',
            response: postCard.responseCode,
            validate: {
                payload: postCard.payload,
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    }
]