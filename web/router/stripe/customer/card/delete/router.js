
'use strict';

const headerValidator = require('../../../../../middleware/validator');

const entity = "card";

const deleteCard = require('./delete');
const stripeErrorMsg = require('../../../stripeErrorMsg');

module.exports = [
    /**
    * Api to Delete Card from stripe
    */
    {
        method: 'DELETE',
        path: '/' + entity,
        handler: deleteCard.APIHandler,
        config: {
            tags: ['api', entity],
            description: stripeErrorMsg['apiDescription']['deleteCard'],
            notes: stripeErrorMsg['apiDescription']['deleteCard'],
            auth: 'customerJWT',
            response: deleteCard.responseCode,
            validate: {
                payload: deleteCard.payload,
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    }
]