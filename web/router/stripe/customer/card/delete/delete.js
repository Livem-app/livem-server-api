'use strict';

const Joi = require('joi');
const logger = require('winston');
const moment = require('moment');
const async = require('async');
const ObjectID = require('mongodb').ObjectID;

const config = require('../../../../../../config')
const stripeMode = config.stripe.STRIPE_MODE;
const stripeCustomer = require('../../../../../../models/stripeCustomer');

const stripeLib = require('../../../../../../library/stripe');

const stripeErrorMsg = require('../../../stripeErrorMsg');
const stripeModel = require('../../../../../commonModels/stripe');

const payload = Joi.object({
    cardId: Joi.any().required().description('Card ID')
}).required();

const APIHandler = (req, reply) => {
    const dbErrResponse = { message: stripeErrorMsg['genericErrMsg']['500'][req.headers.lan], code: 500 };

    let getCustomer = () => {
        return new Promise((resolve, reject) => {
            stripeCustomer.getCustomer(req.auth.credentials._id, stripeMode)
                .then((data) => {
                    if (data) {
                        return resolve(data);
                    } else {
                        reject({ message: stripeErrorMsg['deleteCard']['400'][req.headers.lan], code: 400 });
                    }
                }).catch((err) => {
                    return reject(dbErrResponse);
                });
        });
    };

    let deleteCard = (data) => {
        return new Promise((resolve, reject) => {
            stripeLib.deleteCard(data.stripeId, req.payload.cardId, (err, cards) => {
                if (err) {
                    stripeModel.stripeError.errorMessage(err)
                        .then((message) => {
                            return reject({ message: message, code: 500 });
                        });
                } else {
                    return resolve(true);
                }
            });
        });
    };

    getCustomer()
        .then(deleteCard)
        .then((data) => {
            return reply({ message: stripeErrorMsg['deleteCard']['200'][req.headers.lan] }).code(200);
        }).catch((err) => {
            logger.error("Stripe Delete Card error : ", err);
            return reply({ message: err.message }).code(err.code);
        });
};

let lang = stripeErrorMsg['langaugeId'];

const responseCode = {
    status: {
        500: { message: Joi.any().default(stripeErrorMsg['genericErrMsg']['500'][lang]) },
        200: { message: stripeErrorMsg['deleteCard']['200'][lang] },
        400: { message: stripeErrorMsg['deleteCard']['400'][lang] }
    }
}//swagger response code

module.exports = { payload, APIHandler, responseCode };