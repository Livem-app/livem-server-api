'use strict';

const customer = require('./customer');
const stripeConnect = require('./stripeConnect');
const stripeConnectAdmin = require('./stripeConnectAdmin');

module.exports = [].concat(
    customer,
    stripeConnect,
    stripeConnectAdmin
);