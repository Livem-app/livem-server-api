
'use strict';

const joi = require('joi');
const logger = require('winston');
const error = require('../../error');
const ObjectID = require('mongodb').ObjectID;
const appConfig = require('../../../../../models/appConfig');
const appVersions = require('../../../../../models/appVersions');
const customer = require('../../../../../models/customer');
const cities = require('../../../../../models/cities');

/**
 * @method GET /customer/config
 * @param {*} req 
 * @param {*} reply  
 */
const handler = (req, reply) => {
    const dbErrResponse = { message: error['genericErrMsg']['500'][req.headers.lan], code: 500 };
    let customerData;
    let cityData;
    const getProvider = (data) => {
        return new Promise((resolve, reject) => {
            customer.read({ _id: new ObjectID(req.auth.credentials._id) }, (err, res) => {
                if (err) {
                    return reject(dbErrResponse)
                } else {
                    customerData = res;
                    return resolve(data)
                }
            });
        });
    }
    const getCity = (data) => {
        return new Promise((resolve, reject) => {
            if (customerData.cityId) {
                cities.read({ _id: new ObjectID(customerData.cityId) }, (err, res) => {
                    if (err) {
                        return reject(dbErrResponse);
                    } else {
                        cityData = res;
                        return resolve(data);
                    }
                });
            } else {
                return resolve(data);
            }

        });
    }
    const getAppVersion = (data) => {
        return new Promise((resolve, reject) => {
            appVersions.read({ type: parseInt(customerData.mobileDevices.deviceType + '2') }, (err, doc) => {
                if (err)
                    return reject(dbErrResponse);

                if (doc === null) {
                    return reject({ message: error['getConfig']['404'][req.headers.lan], code: 404 });
                } else {
                    return resolve(doc);
                }

            });
        });
    }
    const getAppConfig = (data) => {
        return new Promise((resolve, reject) => {
            appConfig.read((err, conf) => {
                if (err) {
                    return reject(dbErrResponse);
                }

                if (conf === null) {
                    return reject({ message: error['getConfig']['404'][req.headers.lan], code: 404 });
                }
                let pushTopics = {
                    city: cityData ? cityData.customerPushTopic : '' || '',
                    allCustomers: conf.pushTopics.allCustomers,
                    allCitiesCustomers: conf.pushTopics.allCitiesCustomers,
                    outZoneCustomers: conf.pushTopics.outZoneCustomers,

                }
                let custFreq = {
                    customerHomePageInterval: conf.pubnubSettings.customerHomePageInterval,
                }//customer frequeency

                let proFreq = {
                    locationPublishInterval: conf.locationPublishInterval,
                    liveTrackInterval: conf.liveTrackInterval,
                    proTimeOut: conf.proTimeOut,
                }//provider frequency

                let responseData = {
                    currencySymbol: conf.currencySymbol || '',
                    currency: conf.currency || '',
                    mileage_metric: conf.mileage_metric,
                    stripeTestKeys: conf.stripeTestKeys || [],
                    stripeLiveKeys: conf.stripeLiveKeys || [],
                    providerGoogleMapKey: conf.DriverGoogleMapKeys || [],
                    custGoogleMapKeys: conf.custGoogleMapKeys || [],
                    paymentMethods: conf.paymentMethods,
                    customerFrequency: custFreq,
                    providerFrequency: proFreq,
                    latLongDisplacement: conf.latLongDisplacement,
                    pushTopics: pushTopics,
                    appVersion: data.latestVersion || '',
                    mandatory: data.mandatory || false,

                }//response data for API
                return resolve(responseData);
            });
        });
    }

    getProvider()
        .then(getCity)
        .then(getAppVersion)
        .then(getAppConfig)
        .then(data => {

            return reply({ message: error['getConfig']['200'][req.headers.lan], data: data }).code(200);
        })
        .catch(e => {
            logger.error("AppConfig postBooking API error =>", e)
            return reply({ message: e.message }).code(e.code);
        });


};//API handler

const responseCode = {
    status: {
        500: { message: error['genericErrMsg']['500'][error['lang']] },
        404: { message: error['getConfig']['404'][error['lang']] },
        200: { message: error['getConfig']['200'][error['lang']], data: joi.any() },
    }

}//swagger response code

module.exports = {
    handler,
    responseCode
};