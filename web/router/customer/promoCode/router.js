'use strict';

const entity = '/customer';  
const post = require('./post'); 
const error = require('../error');
const headerValidator = require('../../../middleware/validator'); 

module.exports = [
     
    /**
    * @name POST /app/referralCodeValidation
    */
    {
        method: 'POST',
        path: entity + '/promoCodeValidation',
        handler: post.handler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['getReferralCode'],
            notes: 'This API ensures that a referal code is used only once by one user',
            auth: 'customerJWT',
            // response: post.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: post.payload,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
     
];