'use strict';
const joi = require('joi');
const logger = require('winston');
const error = require('../error');
const ObjectID = require('mongodb').ObjectID;
const customer = require('../../../../models/customer');
const cities = require('../../../../models/cities');
const services = require('../../../../models/services');
const campaignAndreferral = require('../../campaignAndreferral/promoCode/post');

const payload = joi.object({
    latitude: joi.number().required().description('latitude').error(new Error('Latitude must be number')),
    longitude: joi.number().required().description('longitude').error(new Error('Longitude must be number')),
    gigTimeId: joi.any().required().description('Gig Time id'),
    paymentMethod: joi.number().required().integer().min(1).max(3).description('1- cash , 2- card,3-wallet').error(new Error("Payment method is missing")),
    couponCode: joi.string().required().min(4).description('Code required'),
}).required();//payload validator

/**
 * @method POST /customer/referralCodeValidation
 * @param {*} req 
 * @param {*} reply 
 */
const handler = (req, reply) => {
    const dbErrResponse = { message: error['genericErrMsg']['500'][req.headers.lan], code: 500 };
    let cityData = {};
    let serviceData = {};
    const getGigTime = (data) => {
        return new Promise((resolve, reject) => {
            services.read({ _id: new ObjectID(req.payload.gigTimeId) }, (err, res) => {
                if (err) {
                    return reject(dbErrResponse)
                } else {
                    serviceData = res;
                    return resolve(res);
                }
            })
        });

    }
    const getCity = (data) => {
        return new Promise((resolve, reject) => {
            let con = {
                polygons: {
                    $geoIntersects: {
                        $geometry: {
                            type: 'Point',
                            coordinates: [parseFloat(req.payload.longitude || 0.0), parseFloat(req.payload.latitude || 0.0)]
                        }
                    }
                }
            };
            cities.read(con, (err, res) => {
                if (err) {
                    return reject({ message: error['genericErrMsg']['500']['1'], code: 500 });
                }
                if (res === null) {
                    return reject(dbErrResponse)
                } else {
                    cityData = res;
                    resolve(data);
                }
            });
        });
    }
    const checkCode = (data) => {
        return new Promise((resolve, reject) => {
            console.log("req.auth.credentials._id", req.auth.credentials._id)
            console.log(cityData._id)
            let dataReq = {
                userId: req.auth.credentials._id,
                couponCode: req.payload.couponCode,
                cityId: (cityData._id).toString(),
                zoneId: '',
                paymentMethod: (req.payload.paymentMethod == 1) ? 2 : 1,
                vehicleType: 0,
                deliveryFee: 0,
                categoryId: serviceData.cat_id || "",
                cartValue: parseFloat(serviceData.is_unit),
                finalPayableAmount: parseFloat(serviceData.is_unit)
            }
            console.log(dataReq)
            campaignAndreferral.postRequestHandler(dataReq, (err, res) => {
                console.log("errrrrrr", err);
                console.log("ressssssss", res);
                if (err)
                    return reject(err)
                else
                    return resolve(res);
            })
        });
    }
    getGigTime()
        .then(getCity)
        .then(checkCode)
        .then(data => {
            console.log('ddddddddd', data);
            return reply({ message: error['getConfig']['200'][req.headers.lan], data: data.data }).code(200);
        })
        .catch(e => {
            logger.error("AppConfig postBooking API error =>", e)
            return reply({ message: e.message }).code(e.code);
        });
}
const responseCode = {
    // status: {
    //     200: { message: error['postReferralCodeValidation']['200'][error['lang']], data: joi.any() },
    //     400: { message: error['postReferralCodeValidation']['400'][error['lang']] },
    //     401: { message: error['postReferralCodeValidation']['401'][error['lang']] },
    //     500: { message: error['genericErrMsg']['500'][error['lang']] }
    // }

}//swagger response code

module.exports = {
    payload,
    handler,
    responseCode
};