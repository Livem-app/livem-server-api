'use strict';

const entity = '/customer'; 
const get = require('./get');  
const error = require('../../error');
const headerValidator = require('../../../../middleware/validator'); 

module.exports = [
    {
        method: 'GET',
        path: entity + '/support/{userType}',
        handler: get.handler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['getSupport'], 
            notes: "This api to list all FAQ text",
            auth: false,
            // response: get.responseCode,
            validate: {
                headers: headerValidator.headerLanValidator,
                params: get.params,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    } 
    
];