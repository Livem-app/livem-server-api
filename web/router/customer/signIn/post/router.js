'use strict';

const entity = '/customer';
const error = require('../../error');
const postSignIn = require('./post');
const headerValidator = require('../../../../middleware/validator');

module.exports = [

    /**
     * @name POST /customer/signIn
     */
    {
        method: 'POST',
        path: entity + '/signIn',
        handler: postSignIn.handler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['postSignIn'],
            notes: "This API allows user to sign in to the application, It allows you to sign in with facebook and googleas well, if the registration is processed with the same ID.",
            auth: false,
            // response: postSignIn.responseCode,
            validate: {
                headers: headerValidator.headerLanValidator,
                payload: postSignIn.payload,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }

    },





];