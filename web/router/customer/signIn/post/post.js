'use strict';
const joi = require('joi');
const error = require('../../error');
const Bcrypt = require('bcrypt');
const logger = require('winston');
const customer = require('../../../../../models/customer');
const appConfig = require('../../../../../models/appConfig');
const Auth = require('../../../../middleware/authentication');
const configuration = require('../../../../../configuration');
const mobileDevices = require('../../../../../models/mobileDevices');
const userList = require('../../../../commonModels/userList');
const stripeMethods = require('../../../../../library/stripe');


const payload = joi.object({
    emailOrPhone: joi.any().required().description('email or phone'),
    password: joi.string().allow('').description('password'),
    deviceId: joi.string().required().description('device id'),
    pushToken: joi.string().allow('').description('push token'),
    appVersion: joi.string().description('app version'),
    devMake: joi.string().description('Device Make'),
    devModel: joi.string().description('Device model'),
    devType: joi.number().required().integer().min(1).max(3).description('1- IOS , 2- Android, 3- Web').error(new Error('Device type is incorrect Please enter valid type')),
    deviceTime: joi.string().description('formate : YYYY-MM-DD HH:MM:SS'),
    loginType: joi.number().required().integer().min(1).max(3).description('1- normal login, 2- Fb , 3-google').error(new Error('Login type is incorrect Please enter valid type')),
    deviceOsVersion: joi.string().description('device os version'),
    facebookId: joi.any().allow().description(' Fb id'),
    googleId: joi.any().description(' google id'),
    latitude: joi.number().description(' latitude').error(new Error('Latitude must be number')),
    longitude: joi.number().description('longitude ').error(new Error('Longitude must be number')),
}).required();//payload of provider signin api

/**
 * @method customer/postPhoneValidation
 * @param {*} req 
 * @param {*} reply 
 */
const handler = (req, reply) => {
    const dbErrResponse = { message: error['genericErrMsg']['500'][req.headers.lan], code: 500 };
    let cardDetail = {};
    const getUser = () => {
        return new Promise((resolve, reject) => {
            let condition = {
                $or: [
                    { email: req.payload.emailOrPhone },
                    { phone: { $elemMatch: { phone: req.payload.emailOrPhone, isCurrentlyActive: true } } }
                ]
            }
            customer.read(condition, (err, res) => {
		console.log("errr in db",err);
                return err ? reject(dbErrResponse) : resolve(res);
            })
        });
    }//get customer by phone or email

    const verifyUsers = (data) => {
        return new Promise((resolve, reject) => {
            if (data === null)//user not found
                reject({ message: error['postSignIn']['404'][req.headers.lan], code: 404 })
            else if (data && data.status == 1)//not verify
                reject({ message: error['postSignIn']['411'][req.headers.lan], code: 411 })
            else if (data && data.status == 4)//in-active
                reject({ message: error['postSignIn']['403'][req.headers.lan], code: 403 })
            else { 
                    resolve(data);
            }
        });
    }//customer verify like acitve or verifyied or not

    const checkUsers = (data) => {
        return new Promise((resolve, reject) => {
            switch (req.payload.loginType) {
                case 1:
                    if (typeof req.payload.password != 'undefined' || req.payload.password != null) {
                        let isValidUser = Bcrypt.compareSync(req.payload.password, data.password);
                        return (!isValidUser) ? reject({ message: error['postSignIn']['401'][req.headers.lan], code: 401 }) :
                            resolve(data);
                    }
                    else {
                        reject({ message: error['postSignIn']['401'][req.headers.lan], code: 401 })
                    }
                    break;
                case 2:
                    return (req.payload.facebookId != data.facebookId) ?
                        reject({ message: error['postSignIn']['410'][req.headers.lan], code: 410 }) :
                        resolve(data);
                    break;
                case 3:
                    return (req.payload.googleId != data.googleId) ?
                        reject({ message: error['postSignIn']['410'][req.headers.lan], code: 410 }) :
                        resolve(data);
                    break;
                default:
                    return reject({ message: error['postSignIn']['401'][req.headers.lan], code: 401 });
                    break;
            }
        });
    }///check user by login type
    const getCard = (data) => {
        let defaultCard;
        return new Promise((resolve, reject) => { 
            if (data && data.paymentId == undefined) {
                return resolve(data);
            } else {
                stripeMethods.retrieveCustomer(data.paymentId, (err, customer) => {
                    if (err) {
                        return resolve(data);
                    } else { 
                        defaultCard = customer.default_source;//the default card
                        stripeMethods.getCards(data.paymentId, (err, cards) => {
                            if (err) {
                                return resolve(data);
                            } else { 
                                cards.data.forEach(item => {
                                    if (item.id === defaultCard) {
                                        cardDetail = {
                                            'name': item.name || '',
                                            'last4': item.last4,
                                            'expYear': item.exp_year,
                                            'expMonth': item.exp_month,
                                            'id': item.id,
                                            'brand': item.brand,
                                            'funding': item.funding,
                                            'isDefault': (item.id === defaultCard) ? true : false//set the default flag
                                        }
                                        return resolve(data);
                                    }
                                })
                            }
                        });
                    }
                });
            }
        });
    }//retrive customer

    const responseData = (data) => {
        return new Promise((resolve, reject) => {
            appConfig.read((err, res) => {
                let authToken = Auth.SignJWT({ _id: data._id.toString(), key: 'acc', deviceId: req.payload.deviceId }, 'customer', res.accessToken);//sign a new JWT
                req.payload.userId = (data._id).toString();
                req.payload.userType = 1;//customer
                req.payload.status = 3;//online 
                req.payload.deviceType = req.payload.devType;
                req.payload.deviceMake = req.payload.devMake;
                req.payload.deviceModel = req.payload.devModel;
                req.payload.fcmTopic = data._id + '00' + Math.floor(1000 + Math.random() * 9000);
                updateLoginStatus(req.payload, (err, res) => { });//update login  status 
                userList.updateUser(req.payload.userId,
                    {
                        deviceType: req.payload.deviceType,
                        firebaseTopic: req.payload.fcmTopic
                    }

                )//insert userlist table use for simple chat-module
                var phone, countryCode;
                data.phone.forEach(function (e) {
                    if (e.isCurrentlyActive == true) {
                        phone = e.phone;
                        countryCode = e.countryCode;
                    }
                }, this);

                let responseArr = {
                    token: authToken,
                    sid: data._id,
                    email: data.email ? data.email.toString() : "",
                    firstName: data.firstName ? data.firstName : "",
                    lastName: data.lastName ? data.lastName : "",
                    phone: phone ? phone : "",
                    countryCode: countryCode,
                    referralCode: data.referralCode || '',
                    profilePic: data.profilePic,
                    paymentId: data.paymentId ? data.paymentId : "",
                    fcmTopic: req.payload.fcmTopic || "",
                    currencyCode: "$",
                    PublishableKey: "pk_test_L130fWgicI73UClINTF8YvIF",
                    cardDetail: cardDetail,
                    requester_id: data.zendeskId || ""
                }
                resolve(responseArr);
            });
        });
    }//response data to the api

    getUser()
        .then(verifyUsers)
        .then(checkUsers)
        .then(getCard)
        .then(responseData)
        .then(data => {
            return reply({ message: error['postSignIn']['200'][req.headers.lan], data: data }).code(200);
        }).catch(e => {
            logger.error("customer postSignIn API error =>", e)
            return reply({ message: e.message }).code(e.code);
        });

};

const responseCode = {
    status: {
        200: {
            message: error['postSignIn']['200'][error['lang']], data: joi.any().example({
                "token": "string",
                "sid": "string",
                "email": "string",
                "firstName": "string",
                "lastName": "string",
                "phone": "string",
                "countryCode": "string",
                "referralCode": "string",
                "profilePic": "string",
                "paymentId": "string",
                "fcmTopic": "string",
                "currencyCode": "string",
                "PublishableKey": "string"
            })
        },
        400: { message: error['postSignIn']['400'][error['lang']] },
        403: { message: error['postSignIn']['403'][error['lang']] },
        401: { message: error['postSignIn']['401'][error['lang']] },
        404: { message: error['postSignIn']['404'][error['lang']] },
        411: { message: error['postSignIn']['411'][error['lang']] },
        410: { message: error['postSignIn']['410'][error['lang']] },
        409: { message: error['postSignIn']['409'][error['lang']] },
        500: { message: error['genericErrMsg']['500'][error['lang']] }
    }
}//swagger response code

const loginSchema = joi.object({
    userId: joi.string().required(),
    fcmTopic: joi.string(),
    status: joi.number().integer().required(),
    userType: joi.number().integer().required(),
    deviceId: joi.string().required(),
    deviceType: joi.number().integer().required(),
    appversion: joi.string(),
    deviceOsVersion: joi.any(),
    devMake: joi.string(),
    locationHeading: joi.string(),
    devModel: joi.string(),
    deviceTime: joi.any(),
    loggedIn: joi.any().default("")


}).unknown().required();//params
const updateLoginStatus = (params, cb) => {
    let data = joi.attempt(params, loginSchema);
    mobileDevices.updateByUserId(data.userId, (err, res) => {
        mobileDevices.updateByDeviceId(data, (err, res) => {
            customer.updateByUserId(data, (err, res) => {
                cb(null, res);
            });
        });
    });
}
 

module.exports = {
    payload,
    handler,
    responseCode
};
