'use strict'

const get = require('./get')
const post = require('./post');
const patch = require('./patch');

module.exports = [].concat(
    get,
    post,
    patch
);