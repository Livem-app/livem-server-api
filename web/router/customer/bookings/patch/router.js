'use strict';

const entity = '/customer';
const cancel = require('./cancel');
const error = require('../../error');
const headerValidator = require('../../../../middleware/validator');

module.exports = [

    /**
     * @name POST /customer/booking
     */
    {
        method: 'PATCH',
        path: entity + '/cancelBooking',
        handler: cancel.APIHandler,
        config: {
            tags: ['api', 'booking'],
            description: 'This Api is used to cancel booking with cancel resons.',
            notes: 'This Api is used to cancel booking with cancel resons.',
            auth: 'customerJWT',
            // response: cancel.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: cancel.payloadValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
];