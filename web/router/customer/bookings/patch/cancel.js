'use strict';
const joi = require('joi');
const error = require('../../error');
const ObjectID = require('mongodb').ObjectID;
const logger = require('winston');
const moment = require('moment-timezone');
const bookingMsg = require('../../../../commonModels/notificationMsg/bookingMsg');
const fcm = require('../../../../../library/fcm');
const mqtt_module = require('../../../../../models/MQTT');
const bookings = require('../../../../../models/bookings');
const provider = require('../../../../../models/provider');
const customer = require('../../../../../models/customer');
const webSocket = require('../../../../../models/webSocket');
const appConfig = require('../../../../../models/appConfig');
const email = require('../../../../commonModels/email/customer');
const paymentMethod = require('../../../../commonModels/paymentMethod');
const assignedBookings = require('../../../../../models/assignedBookings');
const unassignedBookings = require('../../../../../models/unassignedBookings');
const dispatchedBookings = require('../../../../../models/dispatchedBookings');
const dailySchedules = require('../../../../../models/dailySchedules');
const cancellationReasons = require('../../../../../models/cancellationReasons');
const stripe = require('../../../../commonModels/stripe');
const transcation = require('../../../../commonModels/wallet/transcation');
const rabbitmqUtil = require('../../../../../models/rabbitMq/wallet');
const rabbitMq = require('../../../../../models/rabbitMq');
const rabbitmqUtilPayroll = require("../../../../../models/rabbitMq/payroll");
const providerUpdateStatus = require('../../../../commonModels/booking/provider');
const campaignAndreferral = require('../../../campaignAndreferral/promoCode/post');

const payloadValidator = joi.object({
    bookingId: joi.number().description('bookingId').error(new Error('booking id is must be integer')),
    resonId: joi.number().description('cancel resons id ').error(new Error('reson id is missing')),
}).required();//payload of provider signin api -


let APIHandler = (req, reply) => {
    let providerData;
    let cancellationFee = 0;
    let dataToUpdate = {};
    let cancellationReason = '';
    const dbErrResponse = { message: error['genericErrMsg']['500'][req.headers.lan], code: 500 };
    const getAssignedBooking = (data) => {
        return new Promise((resolve, reject) => {
            let condition = { slaveId: req.auth.credentials._id, bookingId: req.payload.bookingId };
            unassignedBookings.findOneDelete(condition, (err, res) => {
                if (err) {
                    return reject(dbErrResponse);
                }
                else if (res.value == null) {
                    assignedBookings.findOneDelete(condition, (err, res) => {
                        if (err) {
                            return reject(dbErrResponse);
                        }
                        else if (res.value == null) {
                            return reject({ message: error['pacthCancelBooking']['404'][req.headers.lan], code: 404 });
                        }
                        else {
                            bookings.post(res.value, (err, result) => {
                                return err ? reject(dbErrResponse) : resolve(res.value);

                            });
                        }
                    });
                }
                else {
                    bookings.post(res.value, (err, result) => {
                        return err ? reject(dbErrResponse) : resolve(res.value);
                    });
                }
            });
        });
    }//find booking and post booking in booking collection
    const cancellationAccounting = (bookingData) => {
        return new Promise((resolve, reject) => {
            if (bookingData.status >= 3 && bookingData.bookingType == 1) {
                let updateQuery22 = {
                    query: { _id: new ObjectID(bookingData.providerId) },
                    data: { $set: { availableForNow: false } }
                };
                provider.updateOne(updateQuery22, (err, result) => {
                });
            }
            appConfig.read((err, appConfig) => {
                if (bookingData.claimData && Object.keys(bookingData.claimData).length !== 0 && bookingData.claimData.claimId != "") {
                    campaignAndreferral.unlockCouponHandler({ claimId: bookingData.claimData.claimId }, (err, res) => {
                        console.log("errr for unlock", err)
                        console.log("res for unlock", res)
                    })
                }

                if (appConfig.cancellationFee == true && (appConfig.cancelModeValue == 0 || (appConfig.cancelModeValue == 1 && bookingData.status >= 6))) {
                    let providerEarning = bookingData.accounting.providerEarning;
                    let appEarning = bookingData.accounting.appEarning;
                    let pgCommissionApp = bookingData.accounting.pgCommissionApp;
                    let pgCommissionProvider = bookingData.accounting.pgCommissionProvider;
                    let totalPgCommission = bookingData.accounting.totalPgCommission;
                    cancellationFee = bookingData.accounting.cancellationFee;
                    let dues = 0;
                    if (bookingData.paymentMethod == 2) {
                        pgCommissionApp = (cancellationFee * (2.9 / 100)) + 0.3;
                        totalPgCommission = (cancellationFee * (2.9 / 100)) + 0.3;
                    }


                    providerEarning = (cancellationFee) * (100 - bookingData.accounting.appCommission) / 100;
                    appEarning = cancellationFee - providerEarning - totalPgCommission;
                    if (bookingData.paymentMethod == 2) {
                        dues = -parseFloat(providerEarning);
                    } else {
                        dues = parseFloat(appEarning);
                    }
                    dataToUpdate = {
                        "accounting.amount": parseFloat(cancellationFee),
                        "accounting.total": parseFloat(cancellationFee),
                        "accounting.cancellationFee": parseFloat(cancellationFee),
                        'accounting.appEarning': parseFloat(appEarning),
                        'accounting.providerEarning': parseFloat(providerEarning),
                        'accounting.pgCommissionApp': parseFloat(pgCommissionApp),
                        'accounting.pgCommissionProvider': parseFloat(pgCommissionProvider),
                        'accounting.totalPgCommission': parseFloat(totalPgCommission),
                        'accounting.appEarningPgComm': parseFloat(appEarning + totalPgCommission),
                        'accounting.dues': dues,
                        status: 12,
                        statusMsg: error['bookingStatus']["12"],
                        resonId: req.payload.resonId || 0,
                        cancelBy: 1,
                        cancellationReason: cancellationReason || '',
                        bookingCompletedAt: moment().unix()
                    }
                    if (bookingData.paymentMethod == 2) {
                        let dataArr = {
                            bookingId: bookingData.bookingId,
                            userId: bookingData.slaveId,
                            providerId: bookingData.providerId,
                            amount: cancellationFee,
                            appEarning: appEarning,
                            chargeId: bookingData.accounting.chargeId,
                            pgComm: totalPgCommission,
                            providerEarning: providerEarning,
                            transcationType: 2,
                            currency: bookingData.currency,
                            currencySymbol: bookingData.currencySymbol
                        }
                        // transcation.cardTransction({
                        //     bookingId: bookingData.bookingId,
                        //     userId: bookingData.slaveId,
                        //     providerId: bookingData.providerId,
                        //     amount: cancellationFee,
                        //     appEarning: appEarning,
                        //     chargeId: bookingData.accounting.chargeId,
                        //     pgComm: totalPgCommission,
                        //     providerEarning: providerEarning
                        // }, (err, res) => { });
                        rabbitmqUtil.InsertQueue(
                            rabbitMq.getChannelWallet(),
                            rabbitMq.queueWallet,
                            dataArr,
                            (err, doc) => {

                            });
                    } else {
                        let dataArr = {
                            bookingId: bookingData.bookingId,
                            userId: bookingData.slaveId,
                            providerId: bookingData.providerId,
                            amount: cancellationFee,
                            appEarning: appEarning,
                            transcationType: 1,
                            currency: bookingData.currency,
                            currencySymbol: bookingData.currencySymbol
                        }
                        // transcation.cashTransction({

                        // }, (err, res) => { });
                        rabbitmqUtil.InsertQueue(
                            rabbitMq.getChannelWallet(),
                            rabbitMq.queueWallet,
                            dataArr,
                            (err, doc) => {

                            });
                    }
                    return resolve(bookingData);
                } else {
                    cancellationFee = 0;
                    dataToUpdate = {
                        "accounting.amount": parseFloat(cancellationFee),
                        "accounting.total": parseFloat(cancellationFee),
                        "accounting.cancellationFee": parseFloat(cancellationFee),
                        status: 12,
                        statusMsg: error['bookingStatus'][12],
                        resonId: req.payload.resonId || 0,
                        cancelBy: 1,
                        cancellationReason: cancellationReason || '',
                        bookingCompletedAt: moment().unix()
                    }
                    return resolve(bookingData);
                }
            })

        });
    }
    const stripeCheckOut = (data) => {
        return new Promise((resolve, reject) => {
            if (data.paymentMethod == 2) {
                if (cancellationFee == 0) {
                    stripe.stripeTransaction.refundCharge(
                        data.accounting.chargeId,
                        ''
                    ).then(res => {
                        return resolve(data);
                    }).catch(e => {
                        logger.error("cancel booking by provider stripe error", e)
                        return resolve(data);
                    });
                } else {
                    stripe.stripeTransaction.captureCharge(
                        data.accounting.chargeId,
                        cancellationFee
                    ).then(res => {
                        return resolve(data);
                    }).catch(e => {
                        logger.error("cancel booking by provider stripe error", e)
                        return resolve(data);
                    });
                }


            } else {
                return resolve(data);
            }
        });
    }
    const removeSchedule = (data) => {
        return new Promise((resolve, reject) => {
            let scheduleData;
            dailySchedules.readByBookingId(req.payload.bookingId, (err, res) => {
                if (err)
                    return resolve(data);
                if (res) {
                    dailySchedules.cancelSchedule(req.payload.bookingId, (err, result) => {
                        let schedule = res.schedule;
                        schedule.forEach(element => {
                            if (element["providerId"] == data.providerId) {
                                let scheArr = "";
                                let bookArr = [];
                                let book = element.booked;
                                if (book.length != 0) {
                                    book.forEach(booked => {
                                        if (booked.bookingId != req.payload.bookingId) {
                                            bookArr.push(booked);
                                        } else {
                                            scheArr = element;
                                        }
                                    });
                                    scheArr.booked = bookArr;
                                    dailySchedules.addDailyScheduleInBook(res._id, scheArr, (err, res) => {
                                        return resolve(data);
                                    });
                                }

                            }
                        });
                    });
                } else {
                    return resolve(data);
                }
            });
        });

    }
    const getCancellationReson = (data) => {
        return new Promise((resolve, reject) => {
            cancellationReasons.read(
                {
                    res_id: parseInt(req.payload.resonId),
                    "res_for": "Passenger"
                }, (err, res) => {
                    if (err) {
                        return reject(dbErrResponse);
                    } else {
                        cancellationReason = res.reasons[0]
                        return resolve(data);
                    }

                });
        });
    }
    const jobStatusUpdate = (data) => {
        return new Promise((resolve, reject) => {
            providerUpdateStatus.updateBookingStatus(data.providerId, req.payload.bookingId, 12);
            let updateQuery = {
                query: { bookingId: req.payload.bookingId },
                data: {
                    $set: dataToUpdate,
                    $push: {
                        jobStatusLogs: {
                            status: 12,
                            statusMsg: error['bookingStatus']["12"],
                            stausUpdatedBy: 'Customer',
                            userId: req.auth.credentials._id || '',
                            timeStamp: moment().unix(),
                            latitude: '',
                            longitude: ''
                        }
                    }
                }
            };
            bookings.findUpdate(updateQuery, (err, res) => {
                return err ? reject(dbErrResponse) : resolve(data);
            });

        });
    }//update job status

    const sendBookingResponse = (data) => {
        return new Promise((resolve, reject) => {
            provider.getProviderById(data.providerId, (err, pro) => {
                providerData = pro;
                var mqttRes = {
                    bookingId: req.payload.bookingId,
                    bookingType: data.bookingType,
                    reminderId: data.reminderId || "",
                    status: 12,//customer cancel
                    statusMsg: error['bookingStatus']["12"],
                    statusUpdateTime: moment().unix(),
                    proProfilePic: pro.profilePic || '',
                    bookingRequestedFor: data.bookingRequestedFor || "",
                    bookingEndtime: data.bookingEndtime || "",

                };
                var msg = { errNum: 200, errFlag: 0, errMsg: 'Got Details.', data: mqttRes };
                mqtt_module.publish('jobStatus/' + pro._id, JSON.stringify(msg), { qos: 2 }, function (mqttErr, mqttRes) {
                    return mqttErr ? reject(dbErrResponse) : resolve(data);
                });
            });
        });
    }//send booking response 

    const sendPushForProvider = (data) => {
        return new Promise((resolve, reject) => {
            let dataRes = {
                bookingId: req.payload.bookingId,
                bookingType: data.bookingType,
                reminderId: data.reminderId || "",
                status: 12,//customer cancel
                statusMsg: error['bookingStatus']["12"],
                statusUpdateTime: moment().unix(),
                proProfilePic: '',
                bookingRequestedFor: data.bookingRequestedFor || "",
                bookingEndtime: data.bookingEndtime || "",

            };
            let request = {
                fcmTopic: providerData.fcmTopic,
                action: 12,
                pushType: 1,
                title: bookingMsg.notifyiTitle(12, req.headers.lan),
                msg: bookingMsg.notifyiMsg(12, req.headers.lan, {
                    customerName: data.customerData.firstName + ' ' + data.customerData.lastName
                }),
                data: dataRes,
                deviceType: providerData.mobileDevices.deviceType
            }
            fcm.notifyFcmTpic(request, (e, r) => { });


            resolve(data);
        });
    }

    const sendMail = (data) => {
        return new Promise((resolve, reject) => {
            cancellationReasons.read(
                {
                    res_id: parseInt(req.payload.resonId),
                    "res_for": "Passenger"
                }, (err, res) => {
                    let params = {
                        toName: providerData.firstName + " " + providerData.lastName,
                        to: providerData.email,
                        bookingId: data.bookingId,
                        bookingDate: moment.unix(data.bookingRequestedFor).tz("Asia/Kolkata").format('MMMM Do YYYY, h:mm:ss a'),
                        resons: res.reasons[0],
                        customerName: data.customerData.firstName + ' ' + data.customerData.lastName

                    };
                    email.cancelBooking(params);
                    return resolve(data);
                });

        });
    }

    getAssignedBooking()
        .then(getCancellationReson)
        .then(cancellationAccounting)
        .then(stripeCheckOut)
        .then(removeSchedule)
        .then(jobStatusUpdate)
        .then(sendBookingResponse)
        .then(sendPushForProvider)
        .then(sendMail)
        .then(data => {
            rabbitmqUtilPayroll.InsertQueuePayroll(rabbitMq.getChannelPayroll(), rabbitMq.QueuePayroll, { id: req.auth.credentials._id }, () => { });
            // paymentMethod.cancellationFee({ bookingId: req.payload.bookingId, fee: 0 }, (err, res) => { });//cancellation fee refund
            return reply({ message: error['pacthCancelBooking']['200'][req.headers.lan] }).code(200);
        }).catch(e => {
            logger.error("provider statusBooking API error =>", e)
            return reply({ message: e.message }).code(e.code);
        });

};

const responseCode = {
    status: {
        500: { message: error['genericErrMsg']['500'][error['lang']] },
        404: { message: error['pacthCancelBooking']['404'][error['lang']] },
        200: { message: error['pacthCancelBooking']['200'][error['lang']] },
    }

}//swagger response code


module.exports = {
    payloadValidator,
    APIHandler,
    responseCode
};