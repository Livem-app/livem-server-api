'use strict';
const joi = require('joi');
const Async = require('async');
const error = require('../../error');
const logger = require('winston');
const moment = require('moment');
const ObjectID = require('mongodb').ObjectID;
const events = require('../../../../../models/events');
const mqtt_module = require('../../../../../models/MQTT');
const provider = require('../../../../../models/provider');
const services = require('../../../../../models/services');
const category = require('../../../../../models/category');
const customer = require('../../../../../models/customer');
const bookings = require('../../../../../models/bookings');
const webSocket = require('../../../../../models/webSocket');
const assignedBookings = require('../../../../../models/assignedBookings')
const unassignedBookings = require('../../../../../models/unassignedBookings');

const configuration = require('../../../../../configuration');


const paramsValidator = joi.object({
    bookingId: joi.number().description('bookingId').error(new Error('booking id is must be integer')),
}).required();

/**
 * @method GET /customer/booking/{bookingId}
 * @description get booking details by bookingId
 * @param {*} req 
 * @param {*} reply 
 */
const APIHandler = (req, reply) => {
    let providerData;
    let distanceValue = 0;
    const getBooking = (data) => {
        return new Promise((resolve, reject) => {
            let con = { slaveId: req.auth.credentials._id, bookingId: req.params.bookingId };
            unassignedBookings.read(con, (err, res) => {
                if (err) {
                    return reject({ message: error['genericErrMsg']['500'][req.headers.lan], data: 500 });
                } else if (res == null) {
                    assignedBookings.read(con, (err, res) => {
                        if (err) {
                            return reject({ message: error['genericErrMsg']['500'][req.headers.lan], data: 500 });
                        } else if (res == null) {
                            bookings.read(con, (err, res) => {
                                if (err) {
                                    return reject({ message: error['genericErrMsg']['500'][req.headers.lan], data: 500 });
                                } else if (res == null) {
                                    return reject({ message: error['getBookingDetails']['404'][req.headers.lan], data: 404 });
                                } else {
                                    resolve(res);
                                }
                            });
                        } else {
                            resolve(res);
                        }
                    });
                } else {
                    resolve(res);
                }
            });
        });
    }//get booking

    const jobStatusLog = (data) => {
        return new Promise((resolve, reject) => {
            let jobStatus = {};
            Async.forEach(data.jobStatusLogs, (e, cb) => {
                switch (e.status) {
                    case 1:
                        jobStatus.requestedTime = e.timeStamp;
                        break;
                    case 2:
                        jobStatus.receivedTime = e.timeStamp;
                        break;
                    case 3:
                        jobStatus.acceptedTime = e.timeStamp;
                        break;
                    case 4:
                        jobStatus.rejectedTime = e.timeStamp;
                        break;
                    case 5:
                        jobStatus.expiredime = e.timeStamp;
                        break;
                    case 6:
                        jobStatus.onthewayTime = e.timeStamp;
                        break;
                    case 7:
                        jobStatus.arrivedTime = e.timeStamp;
                        break;
                    case 8:
                        jobStatus.startedTime = e.timeStamp;
                        break;
                    case 9:
                        jobStatus.completedTime = e.timeStamp;
                        break;
                    case 10:
                        jobStatus.raiseInvoiceTime = e.timeStamp;
                        break;

                    default:
                        break;

                }
                cb(null);
            }, (err, re) => {
                data.jobStatusLogs = jobStatus;
                resolve(data);
            });
        });
    }//job status date log

    const getProvider = (data) => {
        return new Promise((resolve, reject) => {
            try {
                provider.readByAggregate(data.latitude, data.longitude,
                    { _id: new ObjectID(data.providerId) },
                    (err, pro) => {
                        if (err) {
                            distanceValue = 0;
                            return reject({ message: error['genericErrMsg']['500'][req.headers.lan], data: 500 });
                        } else if (pro[0]) {
                            providerData = pro[0];
                            distanceValue = pro[0].distance;
                            return resolve(data);
                        } else {
                            return resolve(data);
                        }

                    });
            } catch (err) {
                return reject({ message: error['genericErrMsg']['500'][req.headers.lan], data: 500 });
            }


        });

    }
    const responseData = (data) => {
        return new Promise((resolve, reject) => {
            let distanceUnit = data.distanceMatrix == 0 ? 1000 : 1608;
            let proDistance = distanceValue / distanceUnit;
            let providerDetail = {};
            if (providerData) {
                providerDetail = {
                    firstName: providerData.firstName || '',
                    lastName: providerData.lastName || '',
                    phone: data.providerData.phone || "",
                    providerId: data.providerId,
                    proStatus: providerData.status == 3 ? 1 : 0,
                    profilePic: providerData.profilePic || '',
                    proLocation: providerData.location,
                    distance: proDistance > 0 ? parseFloat((proDistance).toFixed(2)) : 0.00 || 0,
                    distanceMatrix: data.distanceMatrix || 0,
                    averageRating: providerData.averageRating || 0,
                    reviewCount: providerData.reviewCountNum || 0,
                }
            } else {
                providerDetail = {
                    firstName: data.providerData.firstName || '',
                    lastName: data.providerData.lastName || '',
                    phone: data.providerData.phone || "",
                    providerId: data.providerId,
                    proStatus: 0,
                    profilePic: data.providerData.profilePic || '',
                    proLocation: data.providerData.location,
                    distance: proDistance > 0 ? parseFloat((proDistance).toFixed(2)) : 0.00 || 0,
                    distanceMatrix: data.distanceMatrix || 0,
                    averageRating: 0,
                    reviewCount: 0,
                }
            }

            let dt = {
                bookingId: data.bookingId,
                bookingRequestedFor: data.bookingRequestedFor || '',
                bookingRequestedAt: data.bookingRequestedAt || '',
                bookingExpireTime: data.expiryTimestamp || '',
                currencySymbol: data.currencySymbol || '$',
                currency: data.currency || 'USD',
                serverTime: moment().unix(),
                bookingType: data.bookingType || 1,
                status: data.status,
                statusMsg: error["bookingStatus"][data.status],
                addLine1: data.addLine1 || '',
                addLine2: data.addLine2 || '',
                city: data.city || '',
                state: data.state || '',
                country: data.country || '',
                placeId: data.placeId || '',
                pincode: data.pincode || '',
                latitude: data.latitude || '',
                longitude: data.longitude || '',
                typeofEvent: data.event || '',
                gigTime: data.service || [],
                paymentMethod: data.paymentMethod == 1 ? 'cash' : 'card',
                providerDetail: providerDetail,
                jobStatusLogs: data.jobStatusLogs || [],
                bookingTimer: data.bookingTimer || [],
                signURL: data.signatureUrl || "",
                reviewByProvider: data.reviewByProvider || {},
                reviewByCustomer: data.reviewByCustomer || {},
                accounting: data.accounting || {},
                cancellationReason: data.cancellationReason || '',
                jobDiscription: data.jobDiscription || ""
            };
            return resolve(dt);
        });
    }//response booking data

    getBooking()
        .then(jobStatusLog)
        .then(getProvider)
        .then(responseData)
        .then(data => {
            return reply({ message: error['getBookingDetails']['200'][req.headers.lan], data: data }).code(200);
        })
        .catch(e => {
            logger.error("Customer getBookingDetails API error =>", e)
            return reply({ message: e.message }).code(e.code);
        });
};

const responseCode = {
    status: {
        200: { message: error['getBookingDetails']['200'][error['lang']], data: joi.any() },
        404: { message: error['getBookingDetails']['404'][error['lang']] },
        500: { message: error['genericErrMsg']['500'][error['lang']] }
    }
}//swagger response code


module.exports = {
    APIHandler,
    responseCode,
    paramsValidator
};