'use strict';

const joi = require('joi');
const Async = require('async');
const error = require('../../error');
const moment = require('moment');
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;
const events = require('../../../../../models/events');
const mqtt_module = require('../../../../../models/MQTT');
const customer = require('../../../../../models/customer');
const provider = require('../../../../../models/provider');
const services = require('../../../../../models/services');
const category = require('../../../../../models/category');
const bookings = require('../../../../../models/bookings');
const webSocket = require('../../../../../models/webSocket');
const assignedBookings = require('../../../../../models/assignedBookings')
const unassignedBookings = require('../../../../../models/unassignedBookings');
const configuration = require('../../../../../configuration');

/**
 * @method GET /customer/bookings
 * @description Get all bookings
 * @param {*} req 
 * @param {*} reply 
 */
let APIHandler = (req, reply) => {
    let pending = [], upcoming = [], past = [];

    const pendingBooking = (data) => {
        return new Promise(function (resolve, reject) {
            unassignedBookings.readAll({ slaveId: req.auth.credentials._id }, (err, result) => {
                if (err) {
                    return reject({ message: error['genericErrMsg']['500'][req.headers.lan], data: 500 });
                }
                else {
                    try {
                        Async.forEach(result, (res, callbackloopv) => {
                            return new Promise((resolve, reject) => {
                                try {
                                    provider.readByAggregate(res.latitude, res.longitude,
                                        { _id: new ObjectID(res.providerId) },
                                        (err, pro) => {
                                            if (err) {
                                                reject(err);
                                            } else {
                                                resolve(pro[0]);
                                            }
                                        });
                                } catch (err) {
                                    reject(err);
                                }

                            }).then(pro => {
                                let distanceUnit = res.distanceMatrix == 0 ? 1000 : 1608;
                                let proDistance = pro.distance / distanceUnit;
                                let bookingDetail = {
                                    bookingId: res.bookingId,
                                    bookingRequestedFor: res.bookingRequestedFor || '',
                                    bookingRequestedAt: res.bookingRequestedAt || '',
                                    eventStartTime: res.eventStartTime || '',
                                    bookingEndtime: res.bookingEndtime || "",
                                    bookingExpireTime: res.expiryTimestamp || '',
                                    currencySymbol: res.currencySymbol || '$',
                                    currency: res.currency || 'USD',
                                    distanceMatrix: res.distanceMatrix || 0,
                                    serverTime: moment().unix(),
                                    bookingType: res.bookingType || 1,
                                    distance: proDistance > 0 ? parseFloat((proDistance).toFixed(2)) : 0.00 || 0,
                                    status: res.status,
                                    statusMsg: error['bookingStatus'][res.status],
                                    firstName: pro.firstName || '',
                                    lastName: pro.lastName || '',
                                    profilePic: pro.profilePic || '',
                                    proLocation: pro.location,
                                    averageRating: pro.averageRating || 0,
                                    addLine1: res.addLine1 || '',
                                    addLine2: res.addLine2 || '',
                                    city: res.city || '',
                                    state: res.state || '',
                                    country: res.country || '',
                                    placeId: res.placeId || '',
                                    pincode: res.pincode || '',
                                    latitude: res.latitude || '',
                                    longitude: res.longitude || '',
                                    typeofEvent: res.event || '',
                                    gigTime: res.service || [],
                                    paymentMethod: res.paymentMethod == 1 ? 'cash' : 'card',
                                    jobDiscription: res.jobDiscription || ""
                                }
                                pending.push(bookingDetail);
                                callbackloopv(null, pending);
                            }).catch(e => {
                                let bookingDetail = {
                                    bookingId: res.bookingId,
                                    bookingRequestedFor: res.bookingRequestedFor || '',
                                    bookingRequestedAt: res.bookingRequestedAt || "",
                                    eventStartTime: res.eventStartTime || '',
                                    bookingExpireTime: res.expiryTimestamp || '',
                                    bookingEndtime: res.bookingEndtime || "",
                                    currencySymbol: res.currencySymbol || '$',
                                    currency: res.currency || 'USD',
                                    distanceMatrix: res.distanceMatrix || 0,
                                    serverTime: moment().unix(),
                                    bookingType: res.bookingType || 1,
                                    distance: 0,
                                    status: res.status,
                                    statusMsg: error['bookingStatus'][res.status],
                                    firstName: res.providerData.firstName || '',
                                    lastName: res.providerData.lastName || '',
                                    profilePic: res.providerData.profilePic || '',
                                    proLocation: res.providerData.location,
                                    addLine1: res.addLine1 || '',
                                    addLine2: res.addLine2 || '',
                                    city: res.city || '',
                                    state: res.state || '',
                                    country: res.country || '',
                                    placeId: res.placeId || '',
                                    pincode: res.pincode || '',
                                    latitude: res.latitude || '',
                                    longitude: res.longitude || '',
                                    averageRating: res.averageRating || 0,
                                    typeofEvent: res.event || '',
                                    gigTime: res.service || [],
                                    paymentMethod: res.paymentMethod == 1 ? 'cash' : 'card',
                                    jobDiscription: res.jobDiscription || ""
                                }
                                pending.push(bookingDetail);
                                callbackloopv(null, pending);
                            });
                        }, (loopErr, res) => {
                            return loopErr ? reject({ message: error['genericErrMsg']['500'][req.headers.lan], data: 500 })
                                : resolve(pending);
                        });
                    } catch (err) {
                        logger.error("get booking for customer error in catch block :", err);
                        return resolve(pending);
                    }
                }
            });

        });
    }//all panding booking

    const upcomingBooking = (data) => {
        return new Promise(function (resolve, reject) {
            assignedBookings.readAll({ slaveId: req.auth.credentials._id }, (err, result) => {
                if (err) {
                    return reject({ message: error['genericErrMsg']['500'][req.headers.lan], data: 500 });
                }
                else {
                    try {
                        Async.forEach(result, (res, callbackloopv) => {
                            return new Promise((resolve, reject) => {
                                try {
                                    provider.readByAggregate(res.latitude, res.longitude,
                                        { _id: new ObjectID(res.providerId) },
                                        (err, pro) => {
                                            if (err) {
                                                reject(err);
                                            } else {
                                                resolve(pro[0]);
                                            }
                                        });
                                } catch (err) {
                                    reject(err);
                                }

                            }).then(pro => {
                                let distanceUnit = res.distanceMatrix == 0 ? 1000 : 1608;
                                let proDistance = pro.distance / distanceUnit;
                                let bookingDetail = {
                                    bookingId: res.bookingId,
                                    bookingRequestedFor: res.bookingRequestedFor || '',
                                    bookingRequestedAt: res.bookingRequestedAt || '',
                                    bookingExpireTime: res.expiryTimestamp || '',
                                    bookingEndtime: res.bookingEndtime || "",
                                    eventStartTime: res.eventStartTime || '',
                                    currencySymbol: res.currencySymbol || '$',
                                    currency: res.currency || 'USD',
                                    distanceMatrix: res.distanceMatrix || 0,
                                    serverTime: moment().unix(),
                                    bookingType: res.bookingType || 1,
                                    distance: proDistance > 0 ? parseFloat((proDistance).toFixed(2)) : 0.00 || 0,
                                    status: res.status,
                                    statusMsg: error['bookingStatus'][res.status],
                                    firstName: pro.firstName || '',
                                    lastName: pro.lastName || '',
                                    profilePic: pro.profilePic || '',
                                    proLocation: pro.location,
                                    averageRating: pro.averageRating || 0,
                                    addLine1: res.addLine1 || '',
                                    addLine2: res.addLine2 || '',
                                    city: res.city || '',
                                    state: res.state || '',
                                    country: res.country || '',
                                    placeId: res.placeId || '',
                                    pincode: res.pincode || '',
                                    latitude: res.latitude || '',
                                    longitude: res.longitude || '',
                                    typeofEvent: res.event || '',
                                    gigTime: res.service || [],
                                    paymentMethod: res.paymentMethod == 1 ? 'cash' : 'card',
                                    jobDiscription: res.jobDiscription || ""
                                }
                                upcoming.push(bookingDetail);
                                callbackloopv(null, upcoming);
                            }).catch(e => {
                                let bookingDetail = {
                                    bookingId: res.bookingId,
                                    bookingRequestedFor: res.bookingRequestedFor || '',
                                    bookingRequestedAt: res.bookingRequestedAt || '',
                                    bookingExpireTime: res.expiryTimestamp || '',
                                    eventStartTime: res.eventStartTime || '',
                                    bookingEndtime: res.bookingEndtime || "",
                                    currencySymbol: res.currencySymbol || '$',
                                    currency: res.currency || 'USD',
                                    distanceMatrix: res.distanceMatrix || 0,
                                    serverTime: moment().unix(),
                                    bookingType: res.bookingType || 1,
                                    distance: 0,
                                    status: res.status,
                                    statusMsg: error['bookingStatus'][res.status],
                                    firstName: res.providerData.firstName || '',
                                    lastName: res.providerData.lastName || '',
                                    profilePic: res.providerData.profilePic || '',
                                    proLocation: res.providerData.location,
                                    addLine1: res.addLine1 || '',
                                    addLine2: res.addLine2 || '',
                                    city: res.city || '',
                                    state: res.state || '',
                                    country: res.country || '',
                                    placeId: res.placeId || '',
                                    pincode: res.pincode || '',
                                    latitude: res.latitude || '',
                                    longitude: res.longitude || '',
                                    averageRating: res.averageRating || 0,
                                    typeofEvent: res.event || '',
                                    gigTime: res.service || [],
                                    paymentMethod: res.paymentMethod == 1 ? 'cash' : 'card',
                                    jobDiscription: res.jobDiscription || ""
                                }
                                upcoming.push(bookingDetail);
                                callbackloopv(null, upcoming);
                            });
                        }, (loopErr, res) => {
                            return loopErr ? reject({ message: error['genericErrMsg']['500'][req.headers.lan], data: 500 })
                                : resolve(upcoming);
                        });
                    } catch (err) {
                        logger.error("get booking for customer error in catch block :", err);
                        return resolve(upcoming);
                    }

                }
            });

        });
    }//all current booking

    const pastBooking = (data) => {
        return new Promise(function (resolve, reject) {
            bookings.readAll({ slaveId: req.auth.credentials._id }, (err, result) => {
                if (err) {
                    return reject(err);
                }
                else {
                    try {
                        Async.forEach(result, (res, callbackloopv) => {
                            return new Promise((resolve, reject) => {
                                try {
                                    provider.readByAggregate(res.latitude, res.longitude,
                                        { _id: new ObjectID(res.providerId) },
                                        (err, pro) => {
                                            if (err) {
                                                reject(err);
                                            } else {
                                                resolve(pro[0]);
                                            }
                                        });
                                } catch (err) {
                                    reject(err);
                                }

                            }).then(pro => {
                                let distanceUnit = res.distanceMatrix == 0 ? 1000 : 1608;
                                let proDistance = pro.distance / distanceUnit;
                                let bookingDetail = {
                                    bookingId: res.bookingId,
                                    bookingRequestedFor: res.bookingRequestedFor || '',
                                    bookingRequestedAt: res.bookingRequestedAt || '',
                                    eventStartTime: res.eventStartTime || '',
                                    bookingExpireTime: res.expiryTimestamp || '',
                                    bookingEndtime: res.bookingEndtime || "",
                                    currencySymbol: res.currencySymbol || '$',
                                    currency: res.currency || 'USD',
                                    distanceMatrix: res.distanceMatrix || 0,
                                    bookingType: res.bookingType || 1,
                                    serverTime: moment().unix(),
                                    distance: proDistance > 0 ? parseFloat((proDistance).toFixed(2)) : 0.00 || 0,
                                    status: res.status,
                                    statusMsg: error['bookingStatus'][res.status],
                                    firstName: pro.firstName || '',
                                    lastName: pro.lastName || '',
                                    profilePic: pro.profilePic || '',
                                    proLocation: pro.location,
                                    averageRating: pro.averageRating || 0,
                                    addLine1: res.addLine1 || '',
                                    addLine2: res.addLine2 || '',
                                    city: res.city || '',
                                    state: res.state || '',
                                    country: res.country || '',
                                    placeId: res.placeId || '',
                                    pincode: res.pincode || '',
                                    latitude: res.latitude || '',
                                    longitude: res.longitude || '',
                                    typeofEvent: res.event || '',
                                    gigTime: res.service || [],
                                    paymentMethod: res.paymentMethod == 1 ? 'cash' : 'card',
                                    reviewByProvider: res.reviewByProvider || {},
                                    reviewByCustomer: res.reviewByCustomer || {},
                                    totalAmount: res.accounting.total || 0,
                                    accounting: res.accounting || {},
                                    cancellationReason: res.cancellationReason || '',
                                    jobDiscription: res.jobDiscription || ""
                                }
                                past.push(bookingDetail);
                                callbackloopv(null, past);
                            }).catch(e => {
                                console.log("-------------- past booking")
                                let bookingDetail = {
                                    bookingId: res.bookingId,
                                    bookingRequestedFor: res.bookingRequestedFor || '',
                                    bookingRequestedAt: res.bookingRequestedAt || '',
                                    bookingExpireTime: res.expiryTimestamp || '',
                                    eventStartTime: res.eventStartTime || '',
                                    bookingEndtime: res.bookingEndtime || "",
                                    currencySymbol: res.currencySymbol || '$',
                                    currency: res.currency || 'USD',
                                    distanceMatrix: res.distanceMatrix || 0,
                                    bookingType: res.bookingType || 1,
                                    serverTime: moment().unix(),
                                    distance: 0,
                                    status: res.status,
                                    statusMsg: error['bookingStatus'][res.status],
                                    firstName: res.providerData.firstName || '',
                                    lastName: res.providerData.lastName || '',
                                    profilePic: res.providerData.profilePic || '',
                                    proLocation: res.providerData.location,
                                    addLine1: res.addLine1 || '',
                                    addLine2: res.addLine2 || '',
                                    city: res.city || '',
                                    state: res.state || '',
                                    country: res.country || '',
                                    placeId: res.placeId || '',
                                    pincode: res.pincode || '',
                                    latitude: res.latitude || '',
                                    longitude: res.longitude || '',
                                    averageRating: res.averageRating || 0,
                                    typeofEvent: res.event || '',
                                    gigTime: res.service || [],
                                    paymentMethod: res.paymentMethod == 1 ? 'cash' : 'card',
                                    accounting: res.accounting,
                                    reviewByProvider: res.reviewByProvider,
                                    reviewByCustomer: res.reviewByCustomer || {},
                                    totalAmount: res.accounting.total || 0,
                                    cancellationReason: res.cancellationReason || '',
                                    jobDiscription: res.jobDiscription || ""
                                }
                                past.push(bookingDetail);
                                callbackloopv(null, past);
                            });
                        }, (loopErr, res) => {
                            return loopErr ? reject({ message: error['genericErrMsg']['500'][req.headers.lan], data: 500 })
                                : resolve(past);
                        });
                    } catch (err) {
                        logger.error("get booking for customer error in catch block :", err);
                        return resolve(past);
                    }
                }
            });

        });
    }//all past booking

    pendingBooking()
        .then(upcomingBooking)
        .then(pastBooking)
        .then(data => {
            if (pending.length != 0) {
                pending.sort((a, b) => {
                    return (b.bookingId) - (a.bookingId);
                });
            }
            if (past.length != 0) {
                past.sort((a, b) => {
                    return (b.bookingId) - (a.bookingId);
                });
            }
            if (upcoming.length != 0) {
                upcoming.sort((a, b) => {
                    return (b.bookingId) - (a.bookingId);
                });
            }
            let dataArr = {
                "pending": pending,
                "past": past,
                "upcoming": upcoming
            };
            return reply({ message: error['getBooking']['200'][req.headers.lan], data: dataArr }).code(200);

        })
        .catch(e => {
            logger.error("Customer getBooking API error =>", e)
            return reply({ message: e.message }).code(e.code);
        });
};

const responseCode = {
    status: {
        200: { message: error['getBooking']['200'][error['lang']], data: joi.any() },
        500: { message: error['genericErrMsg']['500'][error['lang']] }
    }
}//swagger response code


module.exports = {
    APIHandler,
    responseCode
};