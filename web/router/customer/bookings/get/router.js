'use strict';

const entity = '/customer'; 
const get = require('./get'); 
const getById = require('./getById');
const error = require('../../error'); 
const getInvoice = require('./getInvoice');
const headerValidator = require('../../../../middleware/validator');

module.exports = [

     /**
    * @name GET /customer/booking
    */
    {
        method: 'GET',
        path: entity + '/bookings',
        handler: get.APIHandler,
        config: {
            tags: ['api', 'booking'],
            description: error['apiDescription']['getBooking'],
            notes: 'Get all booking',
            auth: 'customerJWT',
            response: get.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },

    /**
    * @name GET /customer/booking/{bookingId}
    */
    {
        method: 'GET',
        path: entity + '/booking/{bookingId}',
        handler: getById.APIHandler,
        config: {
            tags: ['api', 'booking'],
            description: error['apiDescription']['getBookingDetails'],
            notes: 'This Api is used to get a perticuler booking details.',
            auth: 'customerJWT',
            response: getById.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                params: getById.paramsValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
    /**
    * @name GET /customer/booking/invoice/{bookingId}
    */
    {
        method: 'GET',
        path: entity + '/booking/invoice/{bookingId}',
        handler: getInvoice.APIHandler,
        config: {
            tags: ['api', 'booking'],
            description: error['apiDescription']['getBookingInvoice'],
            auth: 'customerJWT',
            // response: getInvoice.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                params: getInvoice.params,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
];