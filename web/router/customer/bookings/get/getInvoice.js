'use strict';

const joi = require('joi');
const Async = require('async');
const logger = require('winston');
const moment = require('moment');
const error = require('../../error');
const ObjectID = require('mongodb').ObjectID;
const events = require('../../../../../models/events');
const mqtt_module = require('../../../../../models/MQTT');
const provider = require('../../../../../models/provider');
const services = require('../../../../../models/services');
const category = require('../../../../../models/category');
const customer = require('../../../../../models/customer');
const bookings = require('../../../../../models/bookings');
const webSocket = require('../../../../../models/webSocket');
const assignedBookings = require('../../../../../models/assignedBookings')
const unassignedBookings = require('../../../../../models/unassignedBookings');

const params = joi.object({
    bookingId: joi.number().description('bookingId').error(new Error('booking id is must be integer')),
}).required();//params

/**
 * @method GET /customer/booking/{bookingId}
 * @description get booking details by bookingId
 * @param {*} req 
 * @param {*} reply 
 */
const APIHandler = (req, reply) => {

    const getBooking = (data) => {
        return new Promise((resolve, reject) => {
            let con = { slaveId: req.auth.credentials._id, bookingId: req.params.bookingId };
            bookings.read(con, (err, res) => {
                if (err) {
                    return reject({ message: error['genericErrMsg']['500'][req.headers.lan], data: 500 });
                } else if (res == null) {
                    return reject({ message: error['getBookingDetails']['404'][req.headers.lan], data: 404 });
                } else {
                    resolve(res);
                }
            });
        });
    }//get booking

    const responseData = (data) => {
        return new Promise((resolve, reject) => {
            let dt = {
                bookingId: data.bookingId,
                bookingRequestedFor: data.bookingRequestedFor || '',
                bookingRequestedAt: data.bookingRequestedAt || '',
                status: data.status,
                statusMsg: error["bookingStatus"][data.status],
                addLine1: data.addLine1 || '',
                addLine2: data.addLine2 || '',
                city: data.city || '',
                state: data.state || '',
                country: data.country || '',
                placeId: data.placeId || '',
                pincode: data.pincode || '',
                latitude: data.latitude || '',
                longitude: data.longitude || '',
                providerData: data.providerData,
                accounting: data.accounting,
                service: data.service,
                typeofEvent: data.event || '',
                signURL: data.signatureUrl || "",
                bookingType: data.bookingType || 1,
                currencySymbol: data.currencySymbol || '$',
                currency: data.currency || 'USD',
                distanceMatrix: data.distanceMatrix || 0,
                reviewByProvider: data.reviewByProvider,
                reviewByCustomer: data.reviewByCustomer,
                jobDiscription: data.jobDiscription || ""
            };
            return resolve(dt);
        });
    }//response booking data

    getBooking()
        .then(responseData)
        .then(data => {
            return reply({ message: error['getBookingDetails']['200'][req.headers.lan], data: data }).code(200);
        })
        .catch(e => {
            logger.error("Customer getBookingDetails API error =>", e)
            return reply({ message: e.message }).code(e.code);
        });
};

const responseCode = {
    status: {
        200: { message: error['getBookingDetails']['200'][error['lang']], data: joi.any() },
        404: { message: error['getBookingDetails']['404'][error['lang']] },
        500: { message: error['genericErrMsg']['500'][error['lang']] }
    }
}//swagger response code


module.exports = {
    params,
    APIHandler,
    responseCode
};