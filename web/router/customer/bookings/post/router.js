'use strict';

const entity = '/customer';
const post = require('./post');
const error = require('../../error'); 
const headerValidator = require('../../../../middleware/validator');

module.exports = [

    /**
     * @name POST /customer/booking
     */
    {
        method: 'POST',
        path: entity + '/booking',
        handler: post.APIHandler,
        config: {
            tags: ['api', 'booking'],
            description: error['apiDescription']['postBooking'],
            notes: 'Create booking',
            auth: 'customerJWT',
            // response: post.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: post.payloadValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    }
];