'use strict';

const joi = require('joi');
const Async = require('async');
const error = require('../../error');
const logger = require('winston');
const moment = require('moment-timezone');
const bookingMsg = require('../../../../commonModels/notificationMsg/bookingMsg');
const ObjectID = require('mongodb').ObjectID;
const Timestamp = require('mongodb').Timestamp;
const fcm = require('../../../../../library/fcm');
const redis = require('../../../../../models/redis');
const cities = require('../../../../../models/cities');
const events = require('../../../../../models/events');
const stripe = require('../../../../commonModels/stripe');
const mqtt_module = require('../../../../../models/MQTT');
const schedule = require('../../../../../models/schedule');
const provider = require('../../../../../models/provider');
const customer = require('../../../../../models/customer');
const services = require('../../../../../models/services');
const category = require('../../../../../models/category');
const appConfig = require('../../../../../models/appConfig');
const webSocket = require('../../../../../models/webSocket');
const configuration = require('../../../../../configuration');
const email = require('../../../../commonModels/email/provider');
const providerUpdateStatus = require('../../../../commonModels/booking/provider');
const providerPlans = require('../../../../../models/providerPlans');
const dispatchedBookings = require('../../../../../models/dispatchedBookings');
const unassignedBookings = require('../../../../../models/unassignedBookings');
const dailySchedules = require('../../../../../models/dailySchedules');
const campaignAndreferral = require('../../../campaignAndreferral/promoCode/post');

const distance = require('google-distance');
distance.apiKey = configuration["API_KEY"];



const payloadValidator = joi.object({
    bookingModel: joi.number().required().integer().min(1).max(3).description('1- Market Place, 2- on-demand , 3-bid').error(new Error('booking model missing')),
    bookingType: joi.number().required().integer().min(1).max(2).description('1- now , 2- scheduled').error(new Error('bookingType is missing')),
    paymentMethod: joi.number().required().integer().min(1).max(3).description('1- cash , 2- card,3-wallet').error(new Error("Payment method is missing")),
    jobDiscription: joi.string().allow('').description('jobDiscription'),
    addLine1: joi.string().required().description('address line 1'),
    addLine2: joi.any().description('address line 2'),
    city: joi.any().description('city'),
    state: joi.any().description('state'),
    country: joi.any().description('country'),
    placeId: joi.any().description('placeId'),
    pincode: joi.any().description('pincode').error(new Error('Pincode must be number')),
    latitude: joi.number().required().description('latitude').error(new Error('Latitude must be number')),
    longitude: joi.number().required().description('longitude').error(new Error('Longitude must be number')),
    jobImage: joi.array().description('Array of job images'),
    providerId: joi.any().required().description('provider id'),
    gigTimeId: joi.any().required().description('Gig Time id'),
    eventId: joi.any().required().description('event id'),
    promoCode: joi.any().description('Promo Code'),
    paymentCardId: joi.any().description('Select payment Card id'),
    eventStartTime: joi.number().integer().default(0).allow('').description('Event start in for Minit like 30'),
    bookingDate: joi.string().allow('').description("Booking date YYYY-MM-DD HH:MM:SS for schedule booking"),
    scheduleTime: joi.number().integer().allow('').description('Schedule booking for Minit like 30'),
    deviceTime: joi.string().optional().allow('').description('end date format : YYYY-MM-DD HH:MM:SS').error(new Error('current devidce  date & time is missing')),
}).required();//validator

Array.prototype.myPhone = function (obj) {
    return this.filter(function (item) {
        for (var prop in obj)
            if (!(prop in item) || obj[prop] !== item[prop])
                return false;
        return true;
    });
};

/**
 * @method POST /customer/booking
 * @description -create booking
 * @param {*} req 
 * @param {*} reply 
 */
const APIHandler = (req, reply) => {
    console.log("rrrrrrrrrrrrrrr", req.payload);
    let dataPayload = req.payload;
    dataPayload.slaveId = req.auth.credentials._id;
    let providerData;
    let customerData;
    let amount = 0;
    let distanceValue = 0;
    let cancelationFee = 0;
    let prepTime = 0;
    let packupTime = 0;
    let bookingId = parseInt(moment().valueOf());
    let strtDateTime = moment().unix();
    let endTime;
    let currencySymbol = '$';
    let currency = 'USD';
    let distanceMatrix = 0;
    let chargeId = "";
    let last4 = "";
    let appCommission = 20;
    let pgName = "";
    let scheduleTime = req.payload.scheduleTime;
    let claimData = {};
    let discount = 0;
    let totalPayAmount = 0;
    const dbError = { message: error['genericErrMsg']['500'][req.headers.lan], code: 500 };
    const selectEvents = (data) => {
        return new Promise((resolve, reject) => {
            try {
                events.read({ _id: new ObjectID(dataPayload.eventId) }, (error, event) => {
                    if (error)
                        return reject(dbError);
                    if (event) {
                        dataPayload.event = event || '';
                        resolve(dataPayload);
                    }
                });
            } catch (e) {
                return reject(dbError);
            }
        });
    }//get events

    const selectGigTime = (data) => {
        return new Promise((resolve, reject) => {
            provider.read({ _id: new ObjectID(data.providerId) }, (err, pro) => {
                if (err)
                    reject(dbError);
                if (pro) {
                    Async.forEach(pro.services, (ser, callbackloopv) => {
                        if ((ser.id).toString() == data.gigTimeId) {
                            var serviceArr = {
                                id: ser.id,
                                name: ser.name || '',
                                unit: ser.unit || '',
                                price: ser.price || '',
                                second: (ser.name * 60) ? (ser.name * 60) : 0
                            };
                            scheduleTime = parseInt(ser.name);
                            amount = ser.price;
                            totalPayAmount = ser.price;
                            dataPayload.services = serviceArr;
                        }
                        callbackloopv(null);
                    }, (loopErr) => {
                        if (req.payload.bookingType == 1) {
                            if (pro.availableForNow) {
                                return reject({ message: error['postBooking']['417'][req.headers.lan], code: 417 });
                            } else {
                                let condition = [
                                    { '$unwind': '$schedule' },
                                    {
                                        '$match': {
                                            "schedule.providerId": new ObjectID(req.payload.providerId),
                                            'schedule.startTime': { '$lte': moment().unix() },
                                            'schedule.endTime': { '$gte': moment().unix() }
                                        }
                                    }
                                ];
                                dailySchedules.readByAggregate(condition, (err, res) => {
                                    if (err) {
                                        return reject(dbError);
                                    } else if (res === null) {
                                        return resolve(dataPayload);
                                    } else {
                                        Async.forEach(res, function (sche, callbackloop) {
                                            let scheduleData = sche.schedule;
                                            let booked = scheduleData.booked
                                            if (scheduleData.startTime <= moment().unix() && scheduleData.endTime >= moment().unix()) {
                                                let dailySchedulesId = sche._id;
                                                Async.forEach(booked, function (bookSche, callbackloopp) {
                                                    if (
                                                        ((moment().unix() + 600) <= bookSche.start && moment().unix() + scheduleTime <= bookSche.start) ||
                                                        ((moment().unix() + 600) >= bookSche.end && moment().unix() + scheduleTime >= bookSche.end)
                                                    ) {
                                                        logger.info("schedule is free")
                                                    }
                                                    else {
                                                        logger.info("schedule not is free")
                                                        return reject({ message: error['postBooking']['417'][req.headers.lan], code: 417 });
                                                    }

                                                    callbackloopp(null)
                                                }, (err, res) => {
                                                    callbackloop(null);
                                                });

                                            } else {
                                                callbackloopp(null)
                                            }
                                        }, function (loopErr, res) {
                                            console.log("loopErr", loopErr)
                                            if (loopErr) {
                                                return reject({ message: error['postBooking']['417'][req.headers.lan], code: 417 });
                                            } else {
                                                return resolve(dataPayload);
                                            }

                                        });
                                    }
                                });
                            }
                        } else {
                            return resolve(dataPayload);
                        }

                    });
                }
            });
        });
    }//select gig time

    const selectCategory = (data) => {
        return new Promise((resolve, reject) => {
            services.read({ _id: new ObjectID(data.gigTimeId) }, (err, service) => {
                if (err) {
                    return reject(dbError);
                }
                else if (service === null) {
                    return reject(dbError);
                } else {
                    req.payload.categoryId = service.cat_id;
                    category.read({ _id: new ObjectID(service.cat_id) }, (error, cat) => {
                        if (error) {
                            return reject(dbError);
                        }

                        if (cat !== null) {
                            let catArr = {
                                id: cat._id,
                                name: cat.cat_name || '',
                            };
                            let pct = parseInt(cat.prepTime ? cat.prepTime > 0 ? cat.prepTime : 0 : 0) || 0;
                            let put = parseInt(cat.packupTime ? cat.packupTime > 0 ? cat.packupTime : 0 : 0) || 0;
                            cities.read({ _id: new ObjectID(cat.city_id) }, (err, res) => {
                                currencySymbol = res.currencySymbol;
                                currency = res.currency;
                                distanceMatrix = res.distanceMatrix || 0;
                                prepTime = (parseInt(pct)) * 60;
                                packupTime = (parseInt(put)) * 60;
                                cancelationFee = cat.can_fees;
                                dataPayload.category = catArr;
                                return resolve(dataPayload);
                            });

                        } else {
                            return resolve(dataPayload);
                        }
                    });
                }
            });
        });
    }//get ctegory

    const checkPaymentMethod = (data) => {
        return new Promise((resolve, reject) => {
            if (req.payload.paymentMethod == 2 && totalPayAmount > 0) {

                if (typeof req.payload.paymentCardId == 'undefined' ||
                    req.payload.paymentCardId == null ||
                    req.payload.paymentCardId == '') {
                    return reject({ message: error['postBooking']['410'][req.headers.lan], code: 410 });
                } else {
                    stripe.stripeTransaction.createCharge(
                        req.auth.credentials._id,
                        req.payload.paymentCardId,
                        totalPayAmount,
                        currency, 'bookingId - ' + bookingId,
                        {
                            customerId: req.auth.credentials._id,
                            bookingId: bookingId,
                            customerName: customerData.firstName,
                            customerPhone: customerData.phone[0].phone

                        }
                    ).then(res => {
                        chargeId = res.data.id;
                        last4 = res.data.source.last4;
                        pgName = 'stripe';
                        resolve(data);
                    }).catch(e => {
                        logger.error("e", e)
                        return reject({ message: error['postBooking']['411'][req.headers.lan], data: 411 });
                    }); //create a charge on the customers card(uncaptured) and log the chargeId
                }
            } else {
                if (providerData.walletHardLimitHit == true)
                    return reject({ message: error['postBooking']['416'][req.headers.lan], code: 416 });
                else
                    resolve(data);
            }
        });

    }//check payment type 
    const providerPlan = (data) => {
        return new Promise((resolve, reject) => {
            try {
                if (providerData.planId != null || providerData.planId != '') {
                    providerPlans.read({ _id: new ObjectID(providerData.planId) }, (err, res) => {
                        if (res === null) {
                            appCommission = 20;
                            return resolve(data);
                        } else {
                            appCommission = res ? res.appCommission : 20;
                            return resolve(data);
                        }

                    })
                }
            } catch (e) {
                appCommission = 20;
                return resolve(data);
            }

        });
    }
    const insertBooking = (data) => {
        return new Promise((resolve, reject) => {
            let eventStartIn = (req.payload.eventStartTime != "") ? (req.payload.eventStartTime * 60) : 0 || 0;
            let obj = { items: providerData.phone };
            let providerPhone = obj.items.myPhone({ "isCurrentlyActive": true });
            let objPhone = { items: customerData.phone };
            let customerPhone = objPhone.items.myPhone({ "isCurrentlyActive": true });
            appConfig.read((err, config) => {
                let updateTime = {
                    status: 0,
                    second: 0,
                    startTimeStamp: 0

                }
                let jobStatusLogs = [{
                    status: 1,
                    statusMsg: error['bookingStatus']["1"],
                    stausUpdatedBy: 'Customer',
                    userId: req.auth.credentials._id || '',
                    timeStamp: moment().unix(),
                    latitude: parseFloat(req.payload.latitude) || '',
                    longitude: parseFloat(req.payload.longitude) || ''
                }];
                let userData = {
                    bookingId: bookingId,
                    slaveId: dataPayload.slaveId,
                    customerId: dataPayload.slaveId,
                    bookingModel: dataPayload.bookingModel,
                    bookingType: dataPayload.bookingType,
                    paymentMethod: dataPayload.paymentMethod,
                    addLine1: dataPayload.addLine1,
                    addLine2: dataPayload.addLine2 || '',
                    city: dataPayload.city || '',
                    state: dataPayload.state || '',
                    country: dataPayload.country || '',
                    placeId: dataPayload.placeId || '',
                    pincode: dataPayload.pincode,
                    latitude: dataPayload.latitude,
                    longitude: dataPayload.longitude,
                    providerId: dataPayload.providerId,
                    gigTimeId: dataPayload.gigTimeId,
                    service: dataPayload.services,
                    jobImage: dataPayload.jobImage,
                    eventId: dataPayload.eventId,
                    event: dataPayload.event,
                    category: dataPayload.category,
                    promoCode: dataPayload.promoCode || '',
                    bookingRequestedFor: strtDateTime,
                    bookingRequestedAt: moment().unix(),
                    eventStartTime: strtDateTime,
                    bookingEndtime: endTime,
                    expiryTimestamp: moment().unix() + (config.dispatch_settings.dispatchDuration * 60),
                    status: 1,
                    statusMsg: error['bookingStatus']["1"], //Status.bookingStatus(1),
                    bookingTimer: updateTime,
                    jobStatusLogs: jobStatusLogs,
                    cityId: providerData.cityId || "",
                    currencySymbol: currencySymbol,
                    currency: currency,
                    distanceMatrix: distanceMatrix || 0,
                    reviewByProviderStatus: 0,
                    reviewByCustomerStatus: 0,
                    createBookingTs: new Timestamp(),
                    createBookingDate: new Date(),
                    jobDiscription: req.payload.jobDiscription || "",
                    // createBookingIsoDate: ISODate(),
                    providerData: {
                        firstName: providerData.firstName || '',
                        lastName: providerData.lastName || '',
                        profilePic: providerData.profilePic || '',
                        phone: providerPhone[0].countryCode + providerPhone[0].phone || "",
                        averageRating: providerData.averageRating || "",
                        location: providerData.location
                    },
                    customerData: {
                        firstName: customerData.firstName || '',
                        lastName: customerData.lastName || '',
                        profilePic: customerData.profilePic || '',
                        phone: customerPhone[0].countryCode + customerPhone[0].phone || "",
                        averageRating: customerData.averageRating || ""
                    },
                    accounting: {
                        amount: amount,
                        cancellationFee: cancelationFee,
                        discount: discount || 0,
                        total: totalPayAmount,
                        appEarning: 0,
                        providerEarning: 0,
                        pgCommissionApp: 0,
                        pgCommissionProvider: 0,
                        totalPgCommission: 0,
                        appEarningPgComm: 0,
                        paymentMethod: dataPayload.paymentMethod,
                        paymentMethodText: (dataPayload.paymentMethod == 1) ? 'cash' : 'card',
                        pgName: pgName,
                        chargeId: chargeId || 0,
                        last4: last4 || "",
                        appCommission: appCommission || 20
                    },
                    claimData: claimData
                };

                unassignedBookings.post(userData, (err, response) => {
                    redis.setex('bid_' + response.ops[0].bookingId, config.dispatch_settings.dispatchDuration * 60, response.ops[0].bookingId);
                    return err ? reject(dbError) : resolve(response.ops[0]);
                });

            });
        });
    }//insert booking

    const readProvider = (data) => {
        return new Promise((resolve, reject) => {
            provider.getProviderById(req.payload.providerId, (err, pro) => {
                providerData = pro;
                return err ? reject(dbError) : resolve(data);
            })
        });
    }//get provider
    const checkCode = (data) => {
        return new Promise((resolve, reject) => {
            if (req.payload.promoCode && req.payload.promoCode != "") {
                console.log("req.auth.credentials._id", req.auth.credentials._id)
                let dataReq = {
                    customerName: customerData.firstName,
                    customerEmail: customerData.email,
                    customerContactNumber: '',
                    couponCode: req.payload.promoCode,
                    userId: req.auth.credentials._id,
                    cityId: (providerData.cityId).toString(),
                    categoryId: req.payload.categoryId || "",
                    paymentMethod: (req.payload.paymentMethod == 1) ? 2 : 1,
                    cartValue: parseFloat(totalPayAmount),
                    deliveryFee: 0,
                    vehicleType: 0,
                    currency: 'inr',
                    bookingId: bookingId
                }
                console.log(dataReq)
                campaignAndreferral.lockCouponCodeHandler(dataReq, (err, res) => {
                    console.log("errrrrrr", err);
                    console.log("ressssssss", res);
                    if (err) {
                        return reject(err)
                    } else {
                        claimData = res;
                        discount = claimData ? claimData.statusCode == 200 ? claimData.finalAmount : 0 : 0
                        totalPayAmount -= discount;
                        return resolve(res);

                    }
                })
            } else {
                return resolve(data);
            }

        });
    }
    const checkSchedule = (data) => {
        let dailySchedulesId;
        return new Promise((resolve, reject) => {
            if (req.payload.bookingType == 2) {
                if (req.payload.deviceTime == 'undefined') {
                    return reject({ message: error['postBooking']['412'][req.headers.lan], data: 412 });
                }
                if (req.payload.bookingDate == 'undefined') {
                    return reject({ message: error['postBooking']['413'][req.headers.lan], data: 413 });
                }
                let serverTime = (moment().unix());
                let deviceTime = moment(req.payload.deviceTime, 'YYYY-MM-DD HH:mm:ss').unix();
                let timeZone = serverTime - deviceTime;

                let scheduledate = parseFloat(req.payload.bookingDate);
                strtDateTime = scheduledate - prepTime;
                endTime = scheduledate + (scheduleTime * 60) + packupTime;
                const getSchedule = (data) => {
                    let scheduleData;
                    return new Promise((resolve, reject) => {
                        let condition = [
                            { '$unwind': '$schedule' },
                            {
                                '$match': {
                                    "schedule.providerId": new ObjectID(req.payload.providerId),
                                    'schedule.startTime': { '$lte': strtDateTime },
                                    'schedule.endTime': { '$gte': strtDateTime }
                                }
                            }

                        ];
                        dailySchedules.readByAggregate(condition, (err, res) => {
                            if (err) {
                                return reject(dbError);
                            } else if (res === null) {
                                return reply({ message: "Unfortunately the artist " + providerData.firstName || "" + " is not available at that date and time, please try to another date/time or artist." }).code(414);
                            } else {
                                Async.forEach(res, function (sche, callbackloop) {
                                    scheduleData = sche.schedule;
                                    let booked = scheduleData.booked
                                    if (scheduleData.startTime <= strtDateTime && scheduleData.endTime >= endTime) {
                                        dailySchedulesId = sche._id;
                                        Async.forEach(booked, function (bookSche, callbackloopp) {
                                            if (
                                                ((strtDateTime + 600) <= bookSche.start && endTime <= bookSche.start) ||
                                                ((strtDateTime + 600) >= bookSche.end && endTime >= bookSche.end)
                                            ) {
                                                logger.info("schedule is free")
                                            }
                                            else {
                                                logger.info("schedule not is free")
                                                return reply({ message: error['postBooking']['415'][req.headers.lan] }).code(415);
                                            }

                                            callbackloopp(null)
                                        }, (err, res) => {
                                            callbackloop(null);
                                        });

                                    } else {
                                        return reply({ message: "Unfortunately the artist " + providerData.firstName || "" + " is not available at that date and time, please try to another date/time or artist." }).code(414);
                                    }
                                }, function (loopErr, res) {
                                    console.log("loopErr", loopErr)
                                    if (loopErr) {
                                        return reply({ message: error['postBooking']['413'][req.headers.lan] }).code(413);
                                    } else {
                                        return resolve(scheduleData);
                                    }

                                });
                            }
                        });
                    });
                }



                const insertBookSchedule = (response) => {
                    return new Promise((resolve, reject) => {
                        let insertData = {
                            status: 1,
                            bookingId: bookingId,
                            customerId: dataPayload.slaveId,
                            start: strtDateTime,
                            end: endTime,
                            firstName: customerData.firstName || '',
                            lastName: customerData.lastName || '',
                            profilePic: customerData.profilePic || '',
                            event: dataPayload.category ? dataPayload.category.name : ''

                        };
                        dailySchedules.updateSchedule(dailySchedulesId, response.scheduleId, insertData, (err, res) => {
                            resolve(data);
                        });
                    });
                }
                getSchedule(data)
                    .then(insertBookSchedule)
                    .then(response => {
                        return resolve(data);
                    })
                    .catch(err => {
                        logger.error("calling schedule function", err);
                        return reply({ message: "Unfortunately the artist " + providerData.firstName || "" + " is not available at that date and time, please try to another date/time or artist." }).code(414);
                    });
            } else {
                logger.error("Now Booking");
                return resolve(data);
            }
        });
    }
    const readCustomer = (data) => {
        return new Promise((resolve, reject) => {
            customer.read({ _id: new ObjectID(dataPayload.slaveId) }, (err, res) => {
                customerData = res;
                return err ? reject(dbError) : resolve(res);
            })
        });
    }//get customer

    const getProviderDistance = (data) => {
        return new Promise((resolve, reject) => {
            distance.get(
                {
                    index: 1,
                    origin: req.payload.latitude + ',' + req.payload.longitude,
                    destination: providerData.location.latitude + ',' + providerData.location.longitude
                }, (err, dis) => {
                    if (err) {
                        distanceValue = 0;
                    } else {
                        distanceValue = dis.distanceValue;
                    }
                    return resolve(data);
                });
        });
    }//get distance

    const postDispatchedBooking = (data) => {
        return new Promise((resolve, reject) => {
            let userData = {
                bookingId: data.bookingId,
                bookingType: data.bookingType,
                dispatchedBy: 1, //1:manual,2:auto
                dispatcherId: data.slaveId,
                slaveName: customerData.firstName || '',
                providerName: providerData.firstName,
                providerId: data.providerId,
                dispatchedByServerAt: moment().unix(),
                expiryTimestamp: data.expiryTimestamp, //add 30 second expire time
                status: 1,
                statusMsg: error['bookingStatus']["1"], //Status.bookingStatus(1),
                category: data.category,
                service: dataPayload.services,
                cityId: providerData.cityId || "",
            };
            dispatchedBookings.post(userData, (err, result) => {
                let updateQuery = {
                    query: { bookingId: data.bookingId },
                    data: {
                        $push: {
                            jobDispatchLogs: {
                                dispatchId: result.insertedIds[0]
                            }
                        }
                    }
                };
                unassignedBookings.findOneAndUpdate(updateQuery, (err, result) => {
                    return err ? reject(dbError) : resolve(data);
                });
            });
        });
    }//dispatch booking

    const sendBookingToPro = (data) => {
        return new Promise((resolve, reject) => {
            let responseData = {
                bookingId: data.bookingId,
                customerId: data.slaveId,
                firstName: data.providerData.firstName || '',
                lastName: data.providerData.lastName || '',
                phone: data.providerData.phone || "", //
                profilePic: data.providerData.profilePic || '',
                bookingRequestedFor: data.bookingRequestedFor || '',
                bookingExpireTime: data.expiryTimestamp || '',
                bookingType: data.bookingType || 1,
                addLine1: data.addLine1 || '',
                addLine2: data.addLine2 || '',
                city: data.city || '',
                state: data.state || '',
                country: data.country || "",
                placeId: data.placeId || "",
                pincode: data.pincode || "",
                latitude: data.latitude || "",
                longitude: data.longitude || "",
                averageRating: providerData.averageRating || 0,
                typeofEvent: data.event.name || '',
                gigTime: data.service,
                distance: distanceValue || 0,
                paymentMethod: data.paymentMethod == 1 ? 'cash' : 'card',
                status: data.status,
                bookingTimer: data.bookingTimer || []
            }
            webSocket.publish('admin/dispatchLog', responseData, {}, (mqttErr, mqttRes) => {
            });
            var msg = { errNum: 200, errFlag: 0, errMsg: 'Got Details.', data: responseData };
            mqtt_module.publish('booking/' + data.providerId, JSON.stringify(msg), { qos: 2 }, (mqttErr, mqttRes) => {
                return mqttErr ? reject(dbError) : resolve(responseData);
            });
        });
    }//send booking to Provider

    const sendPush = (data) => {
        return new Promise((resolve, reject) => {
            let request = {
                fcmTopic: providerData.fcmTopic,
                action: 1,
                pushType: 1,
                title: bookingMsg.notifyiTitle(1, req.headers.lan),
                msg: bookingMsg.notifyiMsg(1, req.headers.lan, {
                    customerName: customerData.firstName
                }),
                data: data,
                deviceType: providerData.mobileDevices.deviceType
            }
            fcm.notifyFcmTpic(request, (e, r) => { });
            resolve(data);
        });
    }

    const sendMail = (data) => {
        return new Promise((resolve, reject) => {
            let params = {
                toName: providerData.firstName + " " + providerData.lastName,
                to: providerData.email,
                providerId: providerData._id,
                bookingId: bookingId,
                bookingDate: moment().tz("Asia/Kolkata").format('MMMM Do YYYY, h:mm:ss a'),
                bookingType: req.payload.bookingType == 1 ? "Now" : 'scheduled',
                address: req.payload.addLine1,
                bookingCategory: dataPayload.category.name,
                service: dataPayload.services,
                discount: discount,
                amount: totalPayAmount,
                currencySymbol: currencySymbol
            };
            email.newBookingRecevied(params);
            resolve(data);
        });
    }

    selectEvents()
        .then(selectGigTime)
        .then(selectCategory)
        .then(readCustomer)
        .then(readProvider)
        .then(checkCode)
        .then(providerPlan)
        .then(checkSchedule)
        .then(getProviderDistance)
        .then(checkPaymentMethod)
        .then(insertBooking)
        .then(postDispatchedBooking)
        .then(sendBookingToPro)
        .then(sendPush)
        .then(sendMail)
        .then(data => {
            providerUpdateStatus.updateBookingStatus(providerData._id, data.bookingId, 1);//update booking status in provider collection
            let retData = {
                bookingId: data.bookingId
            }
            return reply({ message: error['postBooking']['200'][req.headers.lan], data: retData }).code(200);
        })
        .catch(e => {
            logger.error("Customer postBooking API error =>", e)
            return reply({ message: e.message }).code(e.code);
        });
};//API handler

const responseCode = {
    // status: {
    //     200: { message: error['postBooking']['200'][error['lang']], data: joi.any() },
    //     410: { message: error['postBooking']['410'][error['lang']] },
    //     411: { message: error['postBooking']['411'][error['lang']] },
    //     412: { message: error['postBooking']['412'][error['lang']] },
    //     413: { message: error['postBooking']['413'][error['lang']] },
    //     415: { message: error['postBooking']['415'][error['lang']] },
    //     416: { message: error['postBooking']['416'][error['lang']] },
    //     500: { message: error['genericErrMsg']['500'][error['lang']] }
    // }
}//swagger response code


module.exports = {
    payloadValidator,
    APIHandler,
    responseCode
};