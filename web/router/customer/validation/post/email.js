'use strict';

const joi = require('joi'); 
const logger = require('winston');
const error = require('../../error');
const customer = require('../../../../../models/customer'); 
var phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();//phone number validate


const payload = joi.object({
    email: joi.string().email().description('email id').error(new Error('Email is missing or incorrect'))
}).required(); 

/**
 * @method customer/postEmailValidation
 * @param {*} req 
 * @param {*} reply 
 */
const handler = (req, reply) => {
    let condition = { email: req.payload.email, status: { $ne: 1 } };
    customer.read(condition, (err, result) => {
        if (err)
            return reply({ message: error['genericErrMsg']['500'][req.headers.lan] }).code(500);

        if (result === null)
            return reply({ message: error['postEmailValidation']['200'][req.headers.lan] }).code(200);

        return reply({ message: error['postEmailValidation']['412'][req.headers.lan] }).code(412);
    });
};

const responseCode = {
    status: {
        200: { message: error['postEmailValidation']['200'][error['lang']] },
        412: { message: error['postEmailValidation']['412'][error['lang']] },
        500: { message: error['genericErrMsg']['500'][error['lang']] }
    }
}//swagger response code


module.exports = {
    payload,
    handler,
    responseCode
};