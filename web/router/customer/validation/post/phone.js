'use strict';

const joi = require('joi');
const error = require('../../error');
const logger = require('winston');
const customer = require('../../../../../models/customer');
var phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();//phone number validate


const payload = joi.object({
    countryCode: joi.string().required().description('country code'),
    phone: joi.string().required().description('enter phone number').error(new Error('Phone number must be integer')),
}).required();//payload of provider signin api

/**
 * @method POST /customer/postPhoneValidation
 * @param {*} req 
 * @param {*} reply 
 */
const handler = (req, reply) => {
    const verifyNumber = () => {
        return new Promise((resolve, reject) => {
            let phoneNumber = req.payload.countryCode + req.payload.phone;
            try {
                var isValid = phoneUtil.parse(phoneNumber, null);
                if (phoneUtil.isValidNumber(isValid)) {
                    resolve(phoneNumber);
                } else {
                    reject({ message: error['postPhoneValidation']['406'][req.headers.lan], code: 406 });
                }
            } catch (e) {
                reject({ message: error['postPhoneValidation']['406'][req.headers.lan], code: 406 }); S
            }
        });
    }
    const checkNumber = (data) => {
        return new Promise((resolve, reject) => {
            let condition = { 'phone.phone': req.payload.phone, 'phone.countryCode': req.payload.countryCode, status: { $ne: 1 } };
            customer.read(condition, (err, result) => {
                if (err)
                    reject({ message: error['genericErrMsg']['500'][req.headers.lan], code: 500 });
                if (result === null) {
                    resolve(true);
                } else {
                    reject({ message: error['postPhoneValidation']['412'][req.headers.lan], code: 412 });
                }
            })
        });
    }
    verifyNumber()
        .then(checkNumber)
        .then(data => {
            return reply({ message: error['postPhoneValidation']['200'][req.headers.lan] }).code(200);
        }).catch(e => {
            logger.error("Provider postPhoneValidation API error =>", e)
            return reply({ message: e.message }).code(e.code);
        })

};

const responseCode = {
    status: {
        200: { message: error['postPhoneValidation']['200'][error['lang']] },
        412: { message: error['postPhoneValidation']['412'][error['lang']] },
        406: { message: error['postPhoneValidation']['406'][error['lang']] },
        500: { message: error['genericErrMsg']['500'][error['lang']] }
    }
}//swagger response code


module.exports = {
    payload,
    handler,
    responseCode
};