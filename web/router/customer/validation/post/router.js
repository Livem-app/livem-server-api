'use strict';

const entity = '/customer';
const error = require('../../error');
const postPhone = require('./phone');
const postEmail = require('./email');
const headerValidator = require('../../../../middleware/validator');

module.exports = [


    /**
     * @name POST /customer/phoneNumberValidation
     */
    {
        method: 'POST',
        path: entity + '/phoneNumberValidation',
        handler: postPhone.handler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['postPhoneValidation'],
            notes: "verify phone number API Error code ", // We use Joi plugin to validate request
            auth: false,
            response: postPhone.responseCode,
            validate: {
                headers: headerValidator.headerLanValidator,
                payload: postPhone.payload,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }

        }
    },

    /**
     * @name POST /customer/emailValidation
     */
    {
        method: 'POST',
        path: entity + '/emailValidation',
        handler: postEmail.handler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['postPhoneValidation'],
            auth: false,
            response: postEmail.responseCode,
            validate: {
                headers: headerValidator.headerLanValidator,
                payload: postEmail.payload,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },



];