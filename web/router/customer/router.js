'use strict';

const signIn = require('./signIn');

const signUp = require('./signUp');

const logout = require('./logout');

const profile = require('./profile');

const support = require('./support');

const address = require('./address');

const provider = require('./provider');

const bookings = require('./bookings');

const category = require('./category');

const appConfig = require('./appConfig');

const validation = require('./validation');

const accessToken = require('./accessToken');

const referralCode = require('./referralCode')

const cancelReasons = require('./cancelReasons');

const reviewAndRating = require('./reviewAndRating');

const verificationCode = require('./verificationCode');

const promoCode = require('./promoCode');

const schedule = require('./schedule');

module.exports = [].concat(
    signIn,
    signUp,
    logout,
    profile,
    support,
    address,
    category,
    bookings,
    provider,
    appConfig,
    validation,
    accessToken,
    referralCode,
    cancelReasons,
    reviewAndRating,
    verificationCode,
    promoCode,
    schedule
);