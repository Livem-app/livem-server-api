'use strict';

const entity = '/customer';
const get = require('./get');
const getCatByBussinessGroup = require('./getCatByBussinessGroup');
const error = require('../../error');
const headerValidator = require('../../../../middleware/validator');

module.exports = [
    /**
     * @name GET /customer/serviceCateogries
     */
    {
        method: 'GET',
        path: entity + '/serviceCateogries',
        handler: get.handler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['getServiceCateogries'],
            notes: "This API is to get  the Genre/Category created  on the Admin Panel. This displays in the user app on the home screen ",
            auth: false,
            response: get.responseCode,
            validate: {
                headers: headerValidator.headerLanValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
    {
        method: 'GET',
        path: entity + '/categories',
        handler: getCatByBussinessGroup.handler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['getServiceCateogries'],
            notes: "This API is to get  the Genre/Category created  on the Admin Panel. This displays in the user app on the home screen ",
            auth: false,
            response: getCatByBussinessGroup.responseCode,
            validate: {
                headers: headerValidator.headerLanValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
    
];