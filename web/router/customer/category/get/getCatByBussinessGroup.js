
'use strict';

const joi = require('joi');
const error = require('../../error');
const logger = require('winston');
const category = require('../../../../../models/category');
const subCategory = require('../../../../../models/subCategory');
const Async = require('async');
/**
 * @method GET /customer/gerSerivceCategories
 * @description get all category
 * @param {*} req 
 * @property {string} req.lan
 * @param {*} reply 
 * 
 */
const handler = (req, reply) => {
    let catArr = [];
    let getGroup = function () {
        return new Promise((resolve, reject) => {
            category.readAllBG((err, res) => {
                return err ? reject(err) : resolve(res);
            });
        });
    }
    const getCategory = (data) => {
        return new Promise((resolve, reject) => {
            Async.forEach(data, function (bg, callbackloopv) {
                category.readByBG((bg._id).toString(), (err, res) => {
                    if(res.length > 0){
                        getSubCategory(res)
                            .then((catdata) => {
                                catArr.push({ groupId: bg._id, groupName: bg.name, selectImage: bg.selectImage, category: catdata });
                                callbackloopv(null, catArr);
                            })
                            .catch((err) => {
                                callbackloopv(null, catArr);
                            });
                        } else {
                            callbackloopv(null, catArr);
                        }
                });
            }, (err, res) => {
                resolve(res);
            });
        });
    }
    const getSubCategory = (data) => {
        return new Promise((resolve, reject) => {
            let category = []; 
            Async.forEach(data, function (catData, callbackloopv) {
                subCategory.readByCategory((catData._id).toString(), (err, res) => {
                    catData.subCategory = res;
                    category.push(catData);
                    callbackloopv(null, category);
                });
            }, (err, res) => {
                resolve(category);
            });
        });
    }
    getGroup()
        .then(getCategory)
        .then(data => {
            return reply({ message: error['getServiceCateogries']['200'][req.headers.lan], data: catArr }).code(200);
        }).catch(e => {
            logger.error("Provider getServiceCateogries API error =>", e)
            return reply({ message: e.message }).code(e.code);
        })
};

const responseCode = {
    // status: {
    //     500: { message: error['genericErrMsg']['500'][error['lang']] },
    //     200: {
    //         message: error['getServiceCateogries']['200'][error['lang']],
    //         data: joi.any()
    //     },
    // }

}//swagger response code

module.exports = {
    handler,
    responseCode
};