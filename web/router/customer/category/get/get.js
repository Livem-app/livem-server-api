
'use strict';

const joi = require('joi');
const logger = require('winston');
const error = require('../../error');
const category = require('../../../../../models/category'); 

/**
 * @method GET /customer/gerSerivceCategories
 * @description get all category
 * @param {*} req 
 * @property {string} req.lan
 * @param {*} reply 
 * 
 */
const handler = (req, reply) => {
    let getCategory = function () {
        return new Promise((resolve, reject) => {
            category.readAll((err, res) => {
                if (err)
                    reject({ message: error['genericErrMsg']['500'][req.headers.lan], code: 500 });
                let responseData = [];
                res.forEach(function (e) {
                    responseData.push({
                        "id": e._id,
                        "catName": e.cat_name,
                    });
                }, resolve(responseData));
            });
        });
    }
    getCategory()
        .then(data => {
            return reply({ message: error['getServiceCateogries']['200'][req.headers.lan], data: data }).code(200);
        }).catch(e => {
            logger.error("Provider getServiceCateogries API error =>", e)
            return reply({ message: e.message }).code(e.code);
        })
};

const responseCode = {
    status: {
        500: { message: error['genericErrMsg']['500'][error['lang']] },
        200: {
            message: error['getServiceCateogries']['200'][error['lang']],
            data: joi.any()
        },
    }

}//swagger response code

module.exports = {
    handler,
    responseCode
};