'use strict';
const joi = require('joi');
const error = require('../../error'); 
const moment = require('moment');
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;
const provider = require('../../../../../models/provider');
const customer = require('../../../../../models/customer'); 
const appConfig = require('../../../../../models/appConfig');
const verificationCode = require('../../../../../models/verificationCode');


const payload= joi.object({
    code: joi.number().required().description('provide code'),
    userId: joi.string().required().description('user id'),
    trigger: joi.number().integer().min(2).max(3).description('2- forgotpassword  ,  3- change number').error(new Error('trigger type is incorrect Please enter valid type')),
    userType: joi.number().integer().min(1).max(2).description(' 1- slave ,  2-  Master').error(new Error('User type is incorrect Please enter valid type'))
}).required();

/**
 * @method POST /customer/VerifyVerificationCode
 * @param {*} req 
 * @param {*} reply 
 */
const handler = (req, reply) => {
    var condition = {
        triggeredBy: req.payload.trigger,
        userId: req.payload.userId,
        status: true
    };

    verificationCode.read(condition, (err, d) => {
        if (err)
            return reply({ message: error['genericErrMsg']['500'][req.headers.lan] }).code(500); 
        if (d == null)
            return reply({ message: error['postVerifyVerificationCode']['402'][req.headers.lan] }).code(402);
        if (d.verified == true)
            return reply({ message: error['postVerifyVerificationCode']['401'][req.headers.lan] }).code(401);
        appConfig.read((err, res) => {
            var time = moment(d.generatedTime).add(res.expireOtp, 'seconds').valueOf();
            if (time < moment().valueOf())
                return reply({ message: error['postVerifyVerificationCode']['410'][req.headers.lan] }).code(410);
            let maxAttempt = d.maxAttempt ;
            if (maxAttempt < res.maxAttempOtp) {
                if (d.verificationCode == req.payload.code) {
                    var updateQuery = {
                        query: { userId: req.payload.userId, status: true },
                        data: {
                            $set: { maxAttempt: maxAttempt + 1, verified: true },
                            $push: {
                                attempts: {
                                    enteredValue: req.payload.code,
                                    verifiedOn: moment().valueOf(),
                                    success: true
                                }
                            }
                        },
                    }
                    verificationCode.updateAfterVerify(updateQuery, (err, result) => {
                    });
                    if (req.payload.trigger == 3) {
                        let condition = { _id: new ObjectID(req.payload.userId) };
                        customer.read(condition, (err, result) => {
                            if (err)
                                cb(err, null);
                            if (result != null) {
                                var phoneObj = [];
                                result.phone.forEach(e => {
                                    if (e.phone != req.payload.phone) {
                                        e.isCurrentlyActive = false;
                                        phoneObj.push(e);
                                    }
                                });
                                var data = {
                                    'countryCode': result.changeCountryCode,
                                    'phone': result.changePhone,
                                    'isCurrentlyActive': true
                                };
                                phoneObj.push(data);
                                var updateQuery = { $set: { phone: phoneObj } };
                                customer.findOneAndUpdate(req.payload.userId, updateQuery, (err, res) => {
                                    if (err)
                                        cb(err, null);
                                    return reply({ message: error['postVerifyVerificationCode']['200'][req.headers.lan] }).code(200);
                                })
                            }
                        });
                    } else {
                        return reply({ message: error['postVerifyVerificationCode']['200'][req.headers.lan] }).code(200);
                    }

                } else {
                    var updateQuery = {
                        query: { userId: req.payload.userId, status: true },
                        data: {
                            $set: { maxAttempt: maxAttempt + 1, verified: false },
                            $push: {
                                attempts: {
                                    enteredValue: req.payload.code,
                                    verifiedOn: moment().valueOf(),
                                    success: false
                                }
                            }
                        },
                    };
                    verificationCode.updateAfterVerify(updateQuery, (err, res) => {
                        if (err)
                            return reply({ message: error['genericErrMsg']['500'][req.headers.lan] }).code(500);
                        if (res)
                            return reply({ message: error['postVerifyVerificationCode']['403'][req.headers.lan] }).code(403);
                    });
                }
            } else {
                return reply({ message: error['postVerifyVerificationCode']['429'][req.headers.lan] }).code(429);
            }
        });
    });
};

const responseCode = {
    status: {
        200: { message: error['postVerifyVerificationCode']['200'][error['lang']], data: joi.any() },
        400: { message: error['postVerifyVerificationCode']['400'][error['lang']] },
        401: { message: error['postVerifyVerificationCode']['401'][error['lang']] },
        402: { message: error['postVerifyVerificationCode']['402'][error['lang']] },
        403: { message: error['postVerifyVerificationCode']['403'][error['lang']] },
        429: { message: error['postVerifyVerificationCode']['429'][error['lang']] },
        410: { message: error['postVerifyVerificationCode']['410'][error['lang']] },
        500: { message: error['genericErrMsg']['500'][error['lang']] }
    }
}//swagger response code


module.exports = {
    payload,
    handler,
    responseCode
};