'use strict';

const joi = require('joi');
const Async = require('async');
const error = require('../../error');
const moment = require('moment');
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;
const email = require('../../../../commonModels/email/customer');
const customer = require('../../../../../models/customer');
const appConfig = require('../../../../../models/appConfig');
const Auth = require('../../../../middleware/authentication');
const verificationCode = require('../../../../../models/verificationCode');
const userList = require('../../../../commonModels/userList');
const zendesk = require('./../../../../../models/zendesk');
const wallet = require('../../../../commonModels/wallet');

const payload = joi.object({
    code: joi.number().required().description('provide code'),
    userId: joi.string().required().description('user id'),
}).required();

/**
 * @method customer/verifyPhoneNumber
 * @param {*} req 
 * @param {*} reply 
 * @property {number} code
 * @property {string} userId
 */
const handler = (req, reply) => {
    const dbErrResponse = { message: error['genericErrMsg']['500'][req.headers.lan], code: 500 };
    let zendeskId = "";
    const checkCustomer = function (data) {
        return new Promise((resolve, reject) => {
            customer.read({ _id: new ObjectID(req.payload.userId) }, (e, r) => {
                return e ? reject(dbErrResponse) : (r == null) ?
                    reject({ message: error['postVerifyPhoneNumber']['404'][req.headers.lan], code: 404 }) : resolve(r);
            });
        });
    }//check phone  number

    const verifyOtp = function (data) {
        return new Promise((resolve, reject) => {
            verificationCode.getByProviderId(req.payload.userId, (err, d) => {
                if (err)
                    reject(dbErrResponse);
                if (d == null)
                    reject(dbErrResponse);
                if (d.verified == true)
                    reject({ message: error['postVerifyPhoneNumber']['413'][req.headers.lan], code: 413 });
                appConfig.read((err, res) => {
                    data.accessTokenTime = res.accessToken;
                    var time = moment(d.generatedTime).add(res.expireOtp, 'seconds').valueOf();
                    if (time < moment().valueOf()) {
                        reject({ message: error['postVerifyPhoneNumber']['410'][req.headers.lan], code: 410 });
                    }
                    else if (d.maxAttempt < res.maxAttempOtp) {
                        if (d.verificationCode == req.payload.code) {
                            var updateQuery = {
                                query: { userId: req.payload.userId, status: true },
                                data: {
                                    $set: { maxAttempt: d.maxAttempt + 1, verified: true },
                                    $push: {
                                        attempts: {
                                            enteredValue: req.payload.code,
                                            verifiedOn: moment().valueOf(),
                                            success: true
                                        }
                                    }
                                },
                            }
                            verificationCode.updateAfterVerify(updateQuery, (err, res) => { })
                            customer.updateVerifyPhone(req.payload.userId, (e, r) => {
                                return e ? reject(e) : resolve(data)
                            })
                        } else {
                            var updateQuery = {
                                query: { userId: req.payload.userId, status: true },
                                data: {
                                    $set: { maxAttempt: d.maxAttempt + 1, verified: false },
                                    $push: {
                                        attempts: {
                                            enteredValue: req.payload.code,
                                            verifiedOn: moment().valueOf(),
                                            success: false
                                        }
                                    }
                                },
                            };
                            verificationCode.updateAfterVerify(updateQuery, (err, res) => {
                                return err ? reject(dbErrResponse) :
                                    reject({ message: error['postVerifyPhoneNumber']['400'][req.headers.lan], code: 400 })
                            })
                        }
                    } else {
                        reject({ message: error['postVerifyPhoneNumber']['429'][req.headers.lan], code: 429 })
                    }
                });
            })
        });
    }//verify verification code

    const zendeskCreateUsaer = (data) => {
        return new Promise((resolve, reject) => {
            var url = zendesk.config.zd_api_url + '/users.json';
            var dataArr = { "user": { "name": data.firstName, "email": data.firstName+"-"+data.email, "role": 'end-user' } };
            zendesk.users.post(dataArr, url, function (err, result) {
                if (err) {
                    return reject(dbErrResponse);
                } else {
                    zendeskId = result.user ? result.user.id : "";
                    return resolve(data);
                }
            });
        });
    }//creat zendesk user
    const createUserForWallet = (data) => {
        return new Promise((resolve, reject) => {
            userList.createUser(
                req.payload.userId,
                data.firstName,
                data.lastName,
                data.profilePic,
                data.phone[0].countryCode,
                data.phone[0].phone,
                1,
                data.fcmTopic,
                data.mobileDevices.deviceType
            )//insert userlist table use for simple chat-module
            let dataArr = {
                userType: 1,
                userId: req.payload.userId,
            };
            wallet.user.users.userCreate(dataArr, (err, walletId) => {
                if (err)
                    return reject(dbErrResponse);

                if (walletId == '')
                    return reject({ message: error['postSignUp']['504'][req.headers.lan], code: 504 });

                customer.findOneAndUpdate(req.payload.userId,
                    {
                        $set: {
                            walletId: new ObjectID(walletId),
                            zendeskId: zendeskId
                        }
                    },
                    (err, res) => {
                        return err ? reject(dbErrResponse) : resolve(data);
                    });
            });
        });
    }

    checkCustomer()
        .then(verifyOtp)
        .then(zendeskCreateUsaer)
        .then(createUserForWallet)
        .then(d => {
            let params = {
                toName: d.firstName,
                to: d.email
            };
            email.signup(params);//send mail to custmer
            let responseData = {
                token: Auth.SignJWT({ _id: d._id.toString(), key: 'acc', deviceId: d.mobileDevices.deviceId }, 'customer', d.accessTokenTime), //sign a new JWT
                sid: d._id.toString(),
                email: d.email ? d.email : "",
                phone: d.phone[0].phone ? d.phone[0].phone.toString() : "",
                countryCode: d.phone[0].countryCode ? d.phone[0].countryCode.toString() : "",
                firstName: d.firstName || "",
                lastName: d.lastName || "",
                profilePic: d.profilePic || "",
                referralCode: d.referralCode || '',
                fcmTopic: d.fcmTopic || "",
                currencyCode: '$',
                PublishableKey: "pk_test_uJq2O46c1QWxAXQJnQe1dgI4",
                requester_id: zendeskId || '',
                loginType: d.loginType || '',

            };
            return reply({ message: error['postVerifyPhoneNumber']['200'][req.headers.lan], data: responseData }).code(200);
        }).catch(e => {
            logger.error("customer post VerifyPhoneNumber API error =>", e)
            return reply({ message: e.message }).code(e.code);
        })
};

const responseCode = {
    status: {
        200: { message: error['postVerifyPhoneNumber']['200'][error['lang']], data: joi.any() },
        410: { message: error['postVerifyPhoneNumber']['410'][error['lang']] },
        400: { message: error['postVerifyPhoneNumber']['400'][error['lang']] },
        429: { message: error['postVerifyPhoneNumber']['429'][error['lang']] },
        413: { message: error['postVerifyPhoneNumber']['413'][error['lang']] },
        404: { message: error['postVerifyPhoneNumber']['404'][error['lang']] },
        500: { message: error['genericErrMsg']['500'][error['lang']] }
    }
}//swagger response code


module.exports = {
    payload,
    handler,
    responseCode
};
