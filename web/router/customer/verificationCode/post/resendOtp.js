'use strict';
const joi = require('joi');
const moment = require('moment');
const logger = require('winston');
const error = require('../../error'); 
const ObjectID = require('mongodb').ObjectID;
const configuration = require('../../../../../configuration')
const provider = require('../../../../../models/provider');
const customer = require('../../../../../models/customer');
const verificationCode = require('../../../../../models/verificationCode');
const appConfig = require('../../../../../models/appConfig'); 
const rabbitmqUtil = require('../../../../../models/rabbitMq/twilio');
const rabbitMq = require('../../../../../models/rabbitMq');

const payloadValidator = joi.object({
    userId: joi.string().required().description('provider id'),
    userType: joi.number().integer().min(1).max(2).description('1 - customer,2 - provider').error(new Error('Please enter valid user type')),
    trigger: joi.number().integer().min(1).max(3).description('1 - Register,2 - Forgot Password,3-change number').error(new Error('Please enter valid trigger type')),
}).required();

let APIHandler = (req, reply) => {
    const dbErrResponse = { message: error['genericErrMsg']['500'][req.headers.lan], code: 500 };
    const getProvider = (data) => {
        return new Promise((resolve, reject) => {
            const condition = { _id: new ObjectID(req.payload.userId) };
            customer.read(condition, (err, res) => {
                if (err) {
                    return reject(dbErrResponse);
                }
                else if (res != null) {
                    return resolve(res);
                } else {
                    return reject({ message: error['postResendOtp']['404'][req.headers.lan], code: 404 });
                }
            });
        });
    }
    const sendOtp = (data) => {
        return new Promise((resolve, reject) => {
            var countryCode;
            var phoneNumber;

            if (req.payload.trigger == 3) {
                phoneNumber = data.changePhone;
                countryCode = data.changecountryCode;
            } else {
                data.phone.forEach(function (e) {
                    if (e.isCurrentlyActive == true) {
                        phoneNumber = e.phone;
                        countryCode = e.countryCode;
                    }
                }, this);
            }

            var randomnumber = (configuration.OTP == true) ? Math.floor(1000 + Math.random() * 9000) : 1111;
            var updateQuery = {
                query: { userId: req.payload.userId },
                data: {
                    $set: { status: false }
                },
                options: { multi: true }
            };
            verificationCode.updateAfterVerify(updateQuery, (err, result) => {
                if (err)
                    return reject(dbErrResponse);
                var condition = { userId: req.payload.userId, 'expiryTime': { '$gt': moment().valueOf() }, triggeredBy: req.payload.trigger };
                verificationCode.readAllData(condition, (e, d) => {
                    if (e)
                        return reject(dbErrResponse);
                    appConfig.read((err, res) => {
                        if (d.length < res.maxAttempForgotPassword || d === null) {
                            var condition = {
                                type: 1, // 1-mobile ,2- email
                                verificationCode: randomnumber,
                                generatedTime: moment().valueOf(),
                                expiryTime: moment().add(1, 'd').startOf('day').valueOf(),
                                triggeredBy: req.payload.trigger, // 1- forgot password ,2 - registration
                                maxAttempt: 0,
                                maxCount: 1,
                                userId: req.payload.userId.toString(),
                                userType: 1, // 1-slave, 2-master
                                givenInput: phoneNumber,
                                attempts: [],
                                status: true,
                                verified: false,
                            };
                            var trigger = "";
                            switch (req.payload.trigger) {
                                case 1:
                                    trigger = "Register Resend OTP";
                                    break;
                                case 2:
                                    trigger = "Forgot Password Resend OTP";
                                    break;
                                case 3:
                                    trigger = "Change Number Resend OTP";
                                    break;
                            }
                            verificationCode.updateById(req.payload.userId, (err, res) => {
                                verificationCode.post(condition, (er, response) => {
                                    if (er)
                                        return reject(dbErrResponse);
                                    resolve(response);
                                    let sms = {
                                        to: countryCode + phoneNumber,
                                        body: "Resend verification code" + randomnumber,
                                        from: configuration.MAILGUN_FROM_NAME,
                                        trigger: configuration.RESEND_OTP,
                                    } 
                                    rabbitmqUtil.InsertQueue(rabbitMq.getChannelSms(), rabbitMq.queueSms, sms, (err, doc) => {
                
                                    });
                                    // smsGatway.sendSms(countryCode + phoneNumber, "Verificaion code " + randomnumber, trigger, 'log', (rs) => {
                                    //     // smsGatway.sendSms({ mobile: ("+" + countryCode + phoneNumber), msg: Status.bookingStatus_notifyi(15) + " " + randomnumber }, function (rs) {

                                    //     return reply({ message: error['postResendOtp']['200'][req.headers.lan] }).code(200);
                                    // });
                                });
                            });
                        } else {
                            return reject({ message: error['postResendOtp']['429'][req.headers.lan], code: 429 });
                        }
                    });
                });
            });
        });
    }
    getProvider()
        .then(sendOtp)
        .then(data => {
            return reply({ message: error['postResendOtp']['200'][req.headers.lan] }).code(200);
        }).catch(e => {
            logger.error("Provider postForgotPassword error =>", e)
            return reply({ message: e.message }).code(e.code);
        });
};

const responseCode = {
    status: {
        200: { message: error['postResendOtp']['200'][error['lang']] },
        404: { message: error['postResendOtp']['404'][error['lang']] },
        429: { message: error['postResendOtp']['429'][error['lang']] },
        500: { message: error['genericErrMsg']['500'][error['lang']] }
    }
}//swagger response code


module.exports = {
    payloadValidator,
    APIHandler,
    responseCode
};