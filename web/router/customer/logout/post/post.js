'use strict';

const joi = require('joi');
const error = require('../../error');
const customer = require('../../../../../models/customer'); 


/**
 * @method POST /customer/logout
 * @description customer can logout
 * @param {*} req 
 * @param {*} reply 
 */
const handler = (req, reply) => {
    const updateProvider = (data) => {
        let dataArr = { $set: { loggedIn: false, accessCode: '' } };
        return new Promise((resolve, reject) => {
            customer.findOneAndUpdate(req.auth.credentials._id, dataArr, (err, item) => {
                return err ? reject({ message: error['genericErrMsg']['500'][req.headers.lan], code: 500 }) :
                    resolve(item);
            });
        });
    }

    updateProvider()
        .then(data => {
            return reply({ message: error['patchLogOut']['200'][req.headers.lan] }).code(200);
        }).catch(e => {
            logger.error("customer logout API error =>", e)
            return reply({ message: e.message }).code(e.code);
        });
};

const responseCode = {
    status: {
        200: { message: error['patchLogOut']['200'][error['lang']] },
        500: { message: error['genericErrMsg']['500'][error['lang']] }
    }

}//swagger response code


module.exports = {
    handler,
    responseCode
};