'use strict';

const entity = '/customer';
const error = require('../../error');
const post = require('./post');
const headerValidator = require('../../../../middleware/validator');

module.exports = [
    {
        method: 'POST',
        path: entity + '/logout',
        handler: post.handler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['postLogout'],
            notes: 'This API allows the user to log out from the app from his Profile Page',
            auth: 'customerJWT',
            response: post.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
];