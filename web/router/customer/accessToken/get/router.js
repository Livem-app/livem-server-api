'use strict';

const entity = '/customer';

const joi = require('joi');
const logger = require('winston');
const error = require('../../error');

const get = require('./get'); 
 
const headerValidator = require('../../../../middleware/validator');

module.exports = [

    /**
    * @name GET /app/accessToken
    */
    {
        method: 'GET',
        path: entity + '/accessToken',
        handler: get.handler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['getAccessToken'],
            notes: 'Get Accesss token',
            auth: 'refJwt',
            response: get.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
   

];