
'use strict';
const joi = require('joi');
const Async = require('async');
const logger = require('winston');
const error = require('../../error');
const ObjectID = require('mongodb').ObjectID;
const bookings = require('../../../../../models/bookings');
const customer = require('../../../../../models/customer');

const params = joi.object({
    // pageNo: joi.number().integer().required().description('0 defult, ').error(new Error('User type is incorrect Please enter valid type')),
}).required();
/**
 * @method GET /customer/reviewAndRating
 * @param {*} req 
 * @param {*} reply 
 */
const handler = (req, reply) => {
    const readBooking = (data) => {
        return new Promise((resolve, reject) => {
            bookings.readAll({ slaveId: req.auth.credentials._id, reviewByCustomerStatus: 0, status: 10 }, (err, res) => {
                return resolve(res);
            });
        });
    }
    const responseData = (data) => {
        var bookingId = [];
        return new Promise((resolve, reject) => {
            if (data === null)
                return resolve(data);
            Async.forEach(data, (e, callback) => {
                bookingId.push(e.bookingId);
                return callback(null, bookingId);
            }, (err, data) => {

                return resolve(bookingId);
            });
        });
    }//get customer review

    readBooking()
        .then(responseData)
        .then(data => {
            return reply({ message: error['getReviewAndRatingPending']['200'][req.headers.lan], data: data }).code(200);
        }).catch(e => {
            logger.error("customer update password API error =>", e)
            return reply({ message: e.message }).code(e.code);
        });
};

const responseCode = {
    // status: {
    //     200: { message: error['getReviewAndRating']['200'][error['lang']], data: joi.any() },
    //     500: { message: error['genericErrMsg']['500'][error['lang']] }
    // }

}//swagger response code

module.exports = {
    params,
    handler,
    responseCode
};