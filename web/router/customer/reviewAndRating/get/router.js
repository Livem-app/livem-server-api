'use strict';

const entity = '/customer'; 
const get =require('./get'); 
const error = require('../../error');
const getNotRatiing =require('./getNotRatiing');
const headerValidator = require('../../../../middleware/validator');

module.exports = [
    /**
     * @name GET /customer/reviewAndRating/{pageNo}
     */
    {
        method: 'GET',
        path: entity + '/reviewAndRating/{pageNo}',
        handler: get.handler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['getReviewAndRating'],
            notes: "This API is get all the review and rating.",
            auth: 'customerJWT',
            response: get.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                params: get.params,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
   
     /**
     * @name GET /customer/reviewAndRating
     */
    {
        method: 'GET',
        path: entity + '/reviewAndRatingPending',
        handler: getNotRatiing.handler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['getReviewAndRatingPaddingPending'],
            notes: "This API is  for the user to add review and rating perticuler booking.",
            auth: 'customerJWT',
            response: getNotRatiing.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                // payload: post.payload,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
];