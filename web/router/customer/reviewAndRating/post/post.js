'use strict';
const joi = require('joi');
const logger = require('winston');
const moment = require('moment');
const error = require('../../error');
const ObjectID = require('mongodb').ObjectID;
const customer = require('../../../../../models/customer');
const bookings = require('../../../../../models/bookings');
const provider = require('../../../../../models/provider');


const payload = joi.object({
    bookingId: joi.number().integer().required().description('Booking id'),
    rating: joi.number().max(5).required().description('rating is 1 to 5 (1 is nagative 5 is positive'),
    review: joi.string().allow('').description('The review of the customer'),
}).required();

/**
 * @method POST /customer/reviewAndRating
 * @param {*} req 
 * @param {*} reply 
 * @property {number} bookingId
 * @property {number} rating
 * @property {string} review
 */
const handler = (req, reply) => {
    let customerData;
    let providerData;
    const dbErrResponse = { message: error['genericErrMsg']['500'][req.headers.lan], code: 500 };

    const getBooking = () => {
        return new Promise((resolve, reject) => {
            bookings.read({ bookingId: req.payload.bookingId }, (err, data) => {
                if (err)
                    return reject(dbErrResponse);
                else if (data === null)
                    return reject({ message: error['postReviewAndRating']['404'][req.headers.lan], code: 404 });
                else {
                    if (data.reviewByCustomer == undefined) {
                        return resolve(data);
                    } else {
                        return reject({ message: error['postReviewAndRating']['200'][req.headers.lan], code: 200 });
                    }
                }
            });
        });
    }//get booking

    const getProvider = (data) => {
        return new Promise((resolve, reject) => {
            provider.read({ _id: new ObjectID(data.providerId) }, (err, item) => {
                providerData = item;
                return err ? reject(dbErrResponse) : resolve(item);
            });
        });
    }//get provider details

    const getCustomer = (data) => {
        return new Promise((resolve, reject) => {
            customer.read({ _id: new ObjectID(req.auth.credentials._id) }, (err, item) => {
                customerData = item;
                return err ? reject(dbErrResponse) : resolve(item);
            });
        });
    }//get customer details

    const postReview = (data) => {
        return new Promise((resolve, reject) => {
            if (providerData) {
                let totalRating = (providerData.totalRating == undefined || providerData.totalRating == "") ? 0 :
                    providerData.totalRating;
                let reviewCount = (providerData.reviewCount == undefined || providerData.reviewCount == "") ? 0 :
                    providerData.reviewCount;
                let reviewCountNum = (providerData.reviewCountNum == undefined || providerData.reviewCountNum == "") ? 0 :
                    providerData.reviewCountNum;

                let RevCount = reviewCount + 1;
                let revCountNum = reviewCountNum;
                if (req.payload.review != "") {
                    revCountNum = reviewCountNum + 1;
                }
                var totlaRat = totalRating + req.payload.rating;
                var avgRating = ((totlaRat) / (RevCount)).toPrecision(2);
                let queryObject = {
                    $set: { averageRating: parseFloat(avgRating), reviewCount: RevCount, reviewCountNum: revCountNum, totalRating: totlaRat },
                    $push: {
                        reviews: {
                            bookingId: req.payload.bookingId,
                            rating: req.payload.rating,
                            review: req.payload.review,
                            userId: new ObjectID(providerData._id),
                            reviewAt: moment().unix(),
                            firstName: customerData.firstName,
                            lastName: customerData.lastName,
                            profilePic: customerData.profilePic
                        }
                    }
                };
                provider.findOneAndUpdate(providerData._id, queryObject, (err, res) => {
                    return err ? reject(dbErrResponse) : resolve(data);
                });
            } else {
                return resolve(data);
            }
        });
    }//post review in customer

    const postRevieInBooking = (data) => {
        return new Promise((resolve, reject) => {
            let queryObject1 = {
                query: { bookingId: req.payload.bookingId },
                data: {
                    $set: {
                        reviewByCustomerStatus: 1,
                        ["reviewByCustomer"]: {
                            rating: req.payload.rating,
                            review: req.payload.review,
                            userId: new ObjectID(req.auth.credentials._id),
                            reviewAt: moment().unix(),
                        }
                    }
                }
            };
            bookings.findUpdate(queryObject1, (err, result) => {
                return err ? reject(dbErrResponse) : resolve(result);
            });
        });
    }//post review in booking

    getBooking()
        .then(getProvider)
        .then(getCustomer)
        .then(postReview)
        .then(postRevieInBooking)
        .then(data => {
            return reply({ message: error['postReviewAndRating']['200'][req.headers.lan] }).code(200);
        }).catch(e => {
            logger.error("Provider post review and rating API error =>", e)
            return reply({ message: e.message }).code(e.code);
        });

};

const responseCode = {
    status: {
        200: { message: error['postReviewAndRating']['200'][error['lang']] },
        500: { message: error['genericErrMsg']['500'][error['lang']] }
    }
}//swagger response code


module.exports = {
    payload,
    handler,
    responseCode
};