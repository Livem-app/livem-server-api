'use strict';

const entity = '/customer';  
const post =require('./post'); 
const error = require('../../error');
const headerValidator = require('../../../../middleware/validator');

module.exports = [
    
    /**
     * @name POST /customer/reviewAndRating
     */
    {
        method: 'POST',
        path: entity + '/reviewAndRating',
        handler: post.handler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['postReviewAndRating'],
            notes: "This API is  for the user to add review and rating perticuler booking.",
            auth: 'customerJWT',
            response: post.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: post.payload,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
    
];