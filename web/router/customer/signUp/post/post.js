'use strict';
const joi = require('joi');
var CRYPTO = require('crypto');
const Async = require('async');
const error = require('../../error');
const Bcrypt = require('bcrypt');
const moment = require('moment');
const logger = require('winston');
const geoip = require('geoip-lite');
const ObjectID = require('mongodb').ObjectID;
const Timestamp = require('mongodb').Timestamp;
const cities = require('../../../../../models/cities');
const category = require('../../../../../models/category');
const customer = require('../../../../../models/customer');
const campaigns = require('../../../../../models/campaigns');
const configuration = require('../../../../../configuration');
const mobileDevices = require('../../../../../models/mobileDevices');
const verificationCode = require('../../../../../models/verificationCode');
const phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();//phone number validate
const turf = require('../../../../../library/turf');
const rabbitmqUtil = require('../../../../../models/rabbitMq/twilio');
const rabbitMq = require('../../../../../models/rabbitMq');
const referralCampaigns = require('../../../campaignAndreferral/referralCampaigns/post')


const payload = joi.object({
    firstName: joi.string().required().description('first name'),
    lastName: joi.string().allow('').description('last name'),
    email: joi.string().email().required().description('email').error(new Error('Email is missing or incorrect')),
    password: joi.string().required().description('password'),
    countryCode: joi.string().required().description('country code'),
    phone: joi.string().required().description('mobile'),
    dateOfBirth: joi.string().required().description('Date of birth format : YYYY-MM-DD'),
    profilePic: joi.any().description('profile pic'),
    loginType: joi.number().required().integer().min(1).max(3).description('1- normal login, 2- Fb , 3-google').error(new Error('login type invalid Please Enter Valid Type')),
    facebookId: joi.any().description(' Fb id'),
    googleId: joi.any().description(' google id'),
    latitude: joi.number().description('latitude').error(new Error('Latitude must be number')),
    longitude: joi.number().description('longitude').error(new Error('Longitude must be number')),
    preferredGenres: joi.any().description(' Preferred Genres'),
    termsAndCond: joi.number().required().integer().min(0).max(1).description('0 - false,1 - true').error(new Error('Invalid Terms and Condition')),
    devType: joi.number().required().integer().min(1).max(3).description('1- IOS , 2- Android, 3- Web').error(new Error('Device Type Is Invalid')),
    deviceId: joi.string().required().description('device id'),
    pushToken: joi.string().allow('').description('push token'),
    appVersion: joi.string().description('app version'),
    deviceOsVersion: joi.string().description('device os version'),
    devMake: joi.string().description('Device Make'),
    devModel: joi.string().description('Device model'),
    deviceTime: joi.string().description('formate : YYYY-MM-DD HH:MM:SS'),
    referralCode: joi.any().description('referral code optional'),
    ipAddress: joi.any().allow('').description('ip address optional')
}).required();


const loginSchema = joi.object({
    userId: joi.string().required(),
    fcmTopic: joi.string(),
    status: joi.number().integer().required(),
    userType: joi.number().integer().required(),
    deviceId: joi.string().required(),
    deviceType: joi.number().integer().required(),
    appversion: joi.string(),
    deviceOsVersion: joi.any(),
    devMake: joi.string(),
    locationHeading: joi.string(),
    devModel: joi.string(),
    deviceTime: joi.any(),
    loggedIn: joi.any().default("")


}).unknown().required();//params

const updateLoginStatus = (params, cb) => {
    let data = joi.attempt(params, loginSchema);
    mobileDevices.updateByUserId(data.userId, (err, res) => {
        mobileDevices.updateByDeviceId(data, (err, res) => {
            customer.updateByUserId(data, (err, res) => {
                cb(null, res);
            });
        });
    });
}

/**
 * @method customer/registerUser
 * @param {*} req 
 * @param {*} reply 
 */
const handler = (req, reply) => {

    let cityId = 0;
    let cityName = "";
    let genresData = [];
    let cityData = {};
    const getCity = (code) => {
        return new Promise((resolve, reject) => {
            // turf.isWithinPolygons(req.payload.latitude, req.payload.longitude, (err, res) => {
            //     if (res == null) {
            //         var geo = geoip.lookup(req.payload.ipAddress);
            //         turf.isWithinPolygons(geo.ll[0], geo.ll[1], (err, res) => {
            //             if (res == null) {
            //                 console.log("gep", geo.city)
            //                 cityName = geo.city;
            //                 cities.inActiveCities(geo, (err, res) => { });
            //                 resolve(code);
            //             } else {
            //                 cityId = res._id;
            //                 cityName = res.city;
            //                 resolve(code);
            //             }
            //         });
            //     } else {
            //         cityId = res._id;
            //         cityName = res.city;
            //         resolve(code);
            //     }
            // });
            let con = {
                polygons: {
                    $geoIntersects: {
                        $geometry: {
                            type: 'Point',
                            coordinates: [parseFloat(req.payload.longitude || 0.0), parseFloat(req.payload.latitude || 0.0)]
                        }
                    }
                }
            };
            cities.read(con, (err, res) => {
                if (err) {
                    return reject({ message: error['genericErrMsg']['500']['1'], code: 500 });
                }
                if (res === null) {
                    var geo = geoip.lookup(req.payload.ipAddress);
                    let con2 = {
                        polygons: {
                            $geoIntersects: {
                                $geometry: {
                                    type: 'Point',
                                    coordinates: [parseFloat(geo.ll[1] || 0.0), parseFloat(geo.ll[0] || 0.0)]
                                }
                            }
                        }
                    };
                    cities.read(con2, (err, res2) => {
                        if (res2 == null) {
                            console.log("gep", geo.city)
                            cityName = geo.city;
                            cities.inActiveCities(geo, (err, res) => { });
                            resolve(code);
                        } else {
                            cityId = res2._id;
                            cityName = res2.city;
                            cityData = res2;
                            resolve(code);
                        }
                    });
                } else {
                    cityId = res._id;
                    cityName = res.city;
                    cityData = res;
                    resolve(code);
                }

            });
        });
    }//get city by lat long

    const getCategory = (data) => {
        return new Promise((resolve, reject) => {

            if (typeof req.payload.preferredGenres != 'undefined' && req.payload.preferredGenres != '' && req.payload.preferredGenres != null) {
                try {
                    let preferred_genres = (req.payload.preferredGenres).split(',');
                    Async.forEach(preferred_genres, (e, callback) => {
                        let condition = { _id: new ObjectID(e) };
                        category.read(condition, (err, res) => {
                            if (err) {
                                return callback(err);
                            }
                            else {
                                genresData.push({ 'id': e, 'catName': res.cat_name });
                                callback(null, genresData);
                            }
                        })

                    }, (err, data) => {
                        if (err)
                            return reject({ message: error['genericErrMsg']['500'][req.headers.lan], code: 500 });
                        else
                            return resolve(genresData);
                    });
                } catch (e) {
                    return reject({ message: error['genericErrMsg']['500'][req.headers.lan], code: 500 });
                }
            } else {
                return resolve(data);
            }
        })
    }//get caategory

    const insert = (data) => {
        return new Promise((resolve, reject) => {
            var geo = geoip.lookup(req.payload.ipAddress);

            var facebook, google;
            req.payload.loginType == 2 ? (google = 0, facebook = 1) : (req.payload.loginType == 3 ? (google = 1, facebook = 0) : (google = 0, facebook = 0));

            let query = {
                $or: [
                    { email: req.payload.email },
                    { 'phone.phone': req.payload.phone, 'phone.countryCode': req.payload.countryCode }
                ]
            };
            let insertData = {
                firstName: req.payload.firstName ? req.payload.firstName : "",
                lastName: req.payload.lastName ? req.payload.lastName : "",
                email: req.payload.email ? req.payload.email : "",
                password: Bcrypt.hashSync(req.payload.password, parseInt(configuration.SALT_ROUND)), //hash the password and store in db
                phone: [{
                    countryCode: req.payload.countryCode,
                    phone: req.payload.phone ? req.payload.phone : "",
                    isCurrentlyActive: true
                }],
                dateOfBirth: req.payload.dateOfBirth || '',
                preferredGenres: '',
                termsAndCondition: req.payload.termsAndCond,
                status: 1,
                loginType: req.payload.loginType,
                loginVerifiredBy: {
                    facebookId: facebook,
                    googleId: google,
                    emailId: 0,
                    phone: 0
                },
                facebookId: req.payload.facebookId || 0,
                googleId: req.payload.googleId || 0,
                latitude: req.payload.latitude || 0.0,
                longitude: req.payload.longitude || 0.0,
                profilePic: req.payload.profilePic || '',
                createdDt: moment().unix(),
                lastActive: moment().unix(),
                lastLogin: moment().unix(),
                deviceId: req.payload.deviceId || '',
                cityId: cityId || '',
                cityName: cityName || "",
                ipAddress: req.payload.ipAddress || "",
                createTs: new Timestamp(),
                createDate: new Date(),
            }



            customer.postSignUp(query, insertData, (err, response) => {
                if (err)
                    return reject({ message: error['genericErrMsg']['500'][req.headers.lan], code: 500 });
                let customerId = (response.value == null) ? response.lastErrorObject.upserted : response.value._id;
                req.payload.userId = (customerId).toString();
                req.payload.userType = 1;//customer
                req.payload.status = 1;//online 
                req.payload.deviceType = req.payload.devType;
                req.payload.deviceMake = req.payload.devMake;
                req.payload.deviceModel = req.payload.devModel;
                req.payload.fcmTopic = customerId + '00' + Math.floor(1000 + Math.random() * 9000);
                updateLoginStatus(req.payload, (err, res) => { });//update login  status 
                return resolve(customerId);
            })
        });
    }//insert data

    const sendVerificationCode = (data) => {
        referralCampaigns.referralCodeHandler({
            "userId": (data).toString(),
            "userType": 1,
            "firstName": req.payload.firstName,
            "lastName": req.payload.lastName || '',
            "email": req.payload.email,
            "countryCode": req.payload.countryCode,
            "phoneNumber": req.payload.phone,
            "referralCode": req.payload.referralCode || '',
            "cityId": cityId,
            "zoneId": cityId,
            "currency": cityData.currency,
            "currencySymbol": cityData.currencySymbol
        }, (err, res) => { })
        // if (typeof req.payload.referralCode != 'undefined') {
        //     referralCodes.slaveSignUpReferral({
        //         code: req.payload.referralCode,
        //         userId: data.toString(),
        //         long: req.payload.longitude,
        //         lat: req.payload.latitude
        //     }, () => {
        //     });
        // }
        return new Promise((resolve, reject) => {
            let countryCode = req.payload.countryCode;
            let phoneNumber = req.payload.phone;
            let randomnumber = (configuration.OTP == true) ? Math.floor(1000 + Math.random() * 9000) : 1111;
            verificationCode.updateByPhone(req.payload.mobile, (err, result) => {
                if (err)
                    reject(dbErrResponse);
                var condition = {
                    type: 1, // 1-mobile ,2- email
                    verificationCode: randomnumber,
                    generatedTime: moment().valueOf(),
                    expiryTime: moment().add(1, 'd').startOf('day').valueOf(),
                    triggeredBy: 2, // 1- forgot password ,2 - registration
                    maxAttempt: 0,
                    maxCount: 1,
                    userId: (data).toString(),
                    userType: 1, // 1-slave, 2-master
                    givenInput: req.payload.phone,
                    attempts: [],
                    status: true,
                    verified: false,
                };
                verificationCode.post(condition, (err, response) => {
                    if (err)
                        reject(dbErrResponse);
                    let sms = {
                        to: countryCode + phoneNumber,
                        body: " verification code" + randomnumber,
                        from: configuration.MAILGUN_FROM_NAME,
                        trigger: configuration.REGISTRATION_OTP,
                    }
                    rabbitmqUtil.InsertQueue(rabbitMq.getChannelSms(), rabbitMq.queueSms, sms, (err, doc) => {

                    });
                    let dataa = {
                        sid: data.toString(),
                    };
                    resolve(dataa);
                });
            });
        });

    }//send verification code to register phone
    getCity()
        .then(insert)
        .then(sendVerificationCode)
        .then(data => {
            return reply({ message: error['registerUser']['200'][req.headers.lan], data: data }).code(200);
        }).catch(e => {
            logger.error("Provider registerUser API error =>", e)
            return reply({ message: e.message }).code(e.code);
        });

};

const responseCode = {
    status: {
        200: { message: error['registerUser']['200'][error['lang']], data: joi.any() },
        410: { message: error['registerUser']['410'][error['lang']] },
        500: { message: error['genericErrMsg']['500'][error['lang']] },
        504: { message: error['registerUser']['504'][error['lang']] }
    }
}//swagger response code

module.exports = {
    payload,
    handler,
    responseCode
};

