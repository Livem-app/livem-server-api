'use strict';

const entity = '/customer';
const error = require('../../error');
const post = require('./post');
const headerValidator = require('../../../../middleware/validator');

module.exports = [
    /**
        * @name POST /customer/registerUser
        */
    {
        method: 'POST',
        path: entity + '/registerUser',
        handler: post.handler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['postRegisterUser'],
            notes: "verify email ",
            auth: false,
            // response: post.responseCode,
            validate: {
                headers: headerValidator.headerLanValidator,
                payload: post.payload,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
];