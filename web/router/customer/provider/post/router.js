'use strict';

const entity = '/customer';

const post = require('./post'); 
const error = require('../../error'); 
const headerValidator = require('../../../../middleware/validator');

module.exports = [ 
    /**
     * @name POST /customer/location
     */
    {
        method: 'POST',
        path: entity + '/location',
        handler: post.handler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['getLocation'], 
            notes: "get all online provider around the user response in mqtt",
            auth: 'customerJWT',
            // response: post.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: post.payload,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }

    }, 
     
];