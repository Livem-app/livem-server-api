
'use strict';

const joi = require('joi');
const Async = require('async');
const moment = require('moment');
const logger = require('winston');
const error = require('../../error');
const ObjectID = require('mongodb').ObjectID;
const mqtt_module = require('../../../../../models/MQTT');
const provider = require('../../../../../models/provider');
const schedule = require('../../../../../models/schedule');
const category = require('../../../../../models/category');
const cities = require('../../../../../models/cities');
const dailySchedules = require('../../../../../models/dailySchedules');
const providerProfileViews = require('../../../../../models/providerProfileViews');
const configuration = require('../../../../../configuration'); 

const rabbitmqUtil = require("../../../../../models/rabbitMq/provider");
const rabbitMq = require("../../../../../models/rabbitMq");

const payload = joi.object({
    lat: joi.number().description('latitude').error(new Error('Latitude must be number')),
    long: joi.number().description('longitude').error(new Error('Longitude must be number')),
    bookingType: joi.number().integer().min(1).max(2).description('1- now , 2- scheduled').error(new Error('bookingType is missing')),
    scheduleDate: joi.string().optional().allow('').description('formate : YYYY-MM-DD HH:MM:SS'),
    scheduleTime: joi.number().optional().allow('').description('schedule time 30 mins ,60 mins and 120 mins'),
    deviceTime: joi.string().optional().allow('').description('end date format : YYYY-MM-DD HH:MM:SS').error(new Error('current devidce  date & time is missing')),
}).required();

/**
 * @method POST /customer/location
 * @description get provider by lat long and send provider to mqtt
 * @param {*} req 
 * @param {*} reply 
 * @property {number} lat
 * @property {number} long
 */
const handler = (req, reply) => {
    req.payload.customerId = req.auth.credentials._id;
    req.payload.scheduleDate = parseFloat(req.payload.scheduleDate);

    rabbitmqUtil.InsertQueue(rabbitMq.getChannelProvider(), rabbitMq.queueProvider,
        req.payload,
        (err, doc) => { 
        }); 
    return reply({ message: error['postLocation']['200'][req.headers.lan] }).code(200);
};

const responseCode = {
    // status: {
    //     200: { message: error['postLocation']['200'][error['lang']] }
    // }

}//swagger response code

module.exports = {
    payload,
    handler,
    responseCode
};