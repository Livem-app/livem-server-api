
'use strict';
const joi = require('joi');
const error = require('../../error');
const logger = require('winston');
const Async = require('async');
const ObjectID = require('mongodb').ObjectID;
const provider = require('../../../../../models/provider');

const params = joi.object({
    providerId: joi.string().required().description('provider id'),
    pageNo: joi.number().integer().required().description('0 defult, ').error(new Error('User type is incorrect Please enter valid type')),
}).required();
/**
 * @method GET /provider/reviewAndRating
 * @param {*} req 
 * @param {*} reply 
 */
const handler = (req, reply) => {
    let averageRating = 0;
    let reviewCount = 0;
    const getReview = (data) => {
        return new Promise((resolve, reject) => {
            let start = (req.params.pageNo * 5);
            start = start != 0 ? start + 1 : start;

            let condition = [
                { '$match': { _id: new ObjectID(req.params.providerId) } },
                { '$unwind': '$reviews' },
                { '$match': { 'reviews.review': { $ne: '' } } },
                { $sort: { "reviews.bookingId": -1 } },
                { $skip: start },
                { $limit: 5 }
            ];
            provider.readByAggregateMethod(condition, (err, res) => {
                if (err)
                    return reject({ message: error['genericErrMsg']['500'][req.headers.lan], code: 500 });
                // if (data === null)
                //     return reject({ message: error['getReviewAndRating']['404'][req.headers.lan], code: 404 });
                // averageRating = res[0].averageRating;
                // reviewCount = res[0].reviewCount;
                return resolve(res);
            });
            // customer.readReviewAndRating(req.params.providerId, start, (err, data) => {

            // });
        });
    }//get customer review

    const responseData = (data) => {
        var revObj = [];
        return new Promise((resolve, reject) => {
            if (data === null)
                return resolve(data);
            Async.forEach(data, (e, callback) => { 
                let rev = {
                    bookingId: e.reviews.bookingId || '',
                    review: e.reviews.review || "",
                    rating: e.reviews.rating || "",
                    reviewBy: e.reviews.firstName || "",
                    profilePic: e.reviews.profilePic || "",
                    reviewAt: e.reviews.reviewAt || '',
                };
                revObj.push(rev);
                return callback(null, revObj);
            }, (err, data) => {
                let res = {
                    reviews: revObj, 
                };
                return resolve(res);
            });
        });
    }//get customer review

    getReview()
        .then(responseData)
        .then(data => {
            return reply({ message: error['getProviderDetails']['200'][req.headers.lan], data: data }).code(200);
        }).catch(e => {
            logger.error("customer update password API error =>", e)
            return reply({ message: e.message }).code(e.code);
        });
};

const responseCode = {
    status: {
        200: { message: error['getProviderDetails']['200'][error['lang']], data: joi.any() },
        500: { message: error['genericErrMsg']['500'][error['lang']] }
    }

}//swagger response code

module.exports = {
    params,
    handler,
    responseCode
};