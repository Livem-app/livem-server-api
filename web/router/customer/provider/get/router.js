'use strict';

const entity = '/customer';

const error = require('../../error');
const getById = require('./getById');
const getByLatLong = require('./getByLatLong');
const review = require('./review');

const headerValidator = require('../../../../middleware/validator');

module.exports = [
    /**
    * @name GET /customer/provider/{lat}/{long}
    */
    {
        method: 'GET',
        path: entity + '/provider/{lat}/{long}',
        handler: getByLatLong.handler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['getProvider'],
            notes: 'list all online provider around user and get response from server',
            auth: 'customerJWT',
            // response: getByLatLong.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                params: getByLatLong.params,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
    /**
    * @name GET /customer/providerDetails/{providerId}/{lat}/{long}
    */
    {
        method: 'GET',
        path: entity + '/providerDetails/{providerId}/{lat}/{long}',
        handler: getById.handler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['getProviderDetails'],
            notes: 'This API , gets you the details of the provider in the customer app before sending a request to him.',
            auth: 'customerJWT',
            // response: getById.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                params: getById.params,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },

    {
        method: 'GET',
        path: entity + '/providerReview/{providerId}/{pageNo}',
        handler: review.handler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['getProviderDetails'],
            notes: 'This API , gets you the details of the provider in the customer app before sending a request to him.',
            auth: 'customerJWT',
            // response: getById.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                params: review.params,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },

];