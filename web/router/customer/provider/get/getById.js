
'use strict';
const joi = require('joi');
const Async = require('async');
const logger = require('winston');
const error = require('../../error');
const ObjectID = require('mongodb').ObjectID;
const provider = require('../../../../../models/provider');
const cities = require('../../../../../models/cities');
const providerProfileViews = require('../../../../../models/providerProfileViews');
const googleApi = require('../../../../../library/googleApi')

const params = joi.object({
    providerId: joi.string().required().description('provider id'),
    lat: joi.number().description('latitude').error(new Error('Latitude must be number')),
    long: joi.number().description('longitude').error(new Error('Longitude must be number')),
}).required();

/**
 * @method GET /customer/providerDetails/{providerId}/{lat}/{long}
 * @param {*} req 
 * @param {*} reply 
 * @property {string} providerId
 * @property {string} lat
 * @property {string} long
 */
const handler = (req, reply) => {
    let currency = 'USD';
    let currencySymbol = '$';
    let distanceMatrix = 0;
    let distance = 0;
    const getProvider = () => {
        return new Promise((resolve, reject) => {
            try {
                provider.readByAggregate(req.params.lat, req.params.long, { _id: new ObjectID(req.params.providerId) }, (err, res) => {
                    return err ? reject({ message: error['genericErrMsg']['500'], code: 500 })
                        : (res.length === 0) ? reject({ message: error['getProviderDetails']['404'][req.headers.lan], code: 404 })
                            : resolve(res[0]);
                });
            } catch (e) {
                return reject({ message: error['getProviderDetails']['404'][req.headers.lan], code: 404 });
            }

        });
    }//get provider by id
    const getDistance = (data) => {
        return new Promise((resolve, reject) => {
            let origin = req.params.lat + ',' + req.params.long;
            let destination = data.location.latitude + ',' + data.location.longitude;
            googleApi.calculateDistance(origin, destination, '', '')
                .then(dis => {
                    distance = dis.distance || 0;
                    return resolve(data);
                }).catch(e => {
                    return resolve(data);
                });
        });
    }
    const getCity = (data) => {
        return new Promise((resolve, reject) => {
            cities.read({ _id: new ObjectID(data.cityId) }, (err, res) => {
                if (err) {
                    return reject({ message: error['genericErrMsg']['500'], code: 500 })
                } else {
                    currencySymbol = res.currencySymbol;
                    currency = res.currency;
                    distanceMatrix = res.distanceMatrix;
                    return resolve(data);
                }
            });
        });
    }

    const responseData = (data) => {
        return new Promise((resolve, reject) => {
            let updateQuery = {
                providerId: req.params.providerId,
                customerId: req.auth.credentials._id,
                lat: req.params.lat,
                long: req.params.long
            };
            providerProfileViews.updateByProviderId(updateQuery, (err, res) => { });
            let revObj = [];
            let condition = [
                { '$match': { _id: new ObjectID(req.params.providerId) } },
                { '$unwind': '$reviews' },
                { '$match': { 'reviews.review': { $ne: '' } } },
                { $sort: { "reviews.bookingId": -1 } },
                { $skip: 0 },
                { $limit: 5 }
            ];
            provider.readByAggregateMethod(condition, (err, res) => {
                if (err)
                    return reject({ message: error['genericErrMsg']['500'][req.headers.lan], code: 500 });
                else if (res === null) {data
                    let distanceUnit = distanceMatrix == 0 ? 1000 : 1608;
                    let proDistance = data.distance / distanceUnit;
                    let responseData = {
                        'firstName': data.firstName ? data.firstName : '',
                        'lastName': data.lastName ? data.lastName : '',
                        'profilePic': data.profilePic || '',
                        'email': data.email ? data.email : '',
                        'bannerImage': data.image ? data.image : '',
                        'lastActive': data.lastActive || '',
                        'photo': data.image ? data.image : '',
                        'noOfReview': data.reviewCountNum || 0,
                        'rating': data.averageRating || 0,
                        'location': data.location ? data.location : [],
                        'rateCard': data.rateCard ? data.rateCard : [],
                        'countryCode': data.countryCode ? data.countryCode : '',
                        'mobile': data.mobile ? data.mobile : '',
                        'about': data.about ? data.about : '',
                        'events': data.events || [],
                        'rules': data.rules ? data.rules : '',
                        'musicGenres': data.musicGeners ? data.musicGeners : '',
                        'instrument': data.instruments || '',
                        'review': [],
                        'services': data.services || [],
                        'distance': proDistance > 0 ? parseFloat((proDistance).toFixed(2)) : 0.00 || 0.00,
                        'amount': (data.services) ? data.services[0].price : 0,
                        'youtubeUrlLink': data.link || '',
                        'currencySymbol': currencySymbol,
                        'currency': currency,
                        "distanceMatrix": distanceMatrix || 0,
                        'status': data.status == 3 ? 1 : 0,

                    }
                    resolve(responseData);

                } else {
                    Async.forEach(res, (e, callback) => {
                        let rev = {
                            bookingId: e.reviews.bookingId || '',
                            review: e.reviews.review || "",
                            rating: e.reviews.rating || "",
                            reviewBy: e.reviews.firstName || "",
                            profilePic: e.reviews.profilePic || "",
                            reviewAt: e.reviews.reviewAt || '',
                        };
                        revObj.push(rev);
                        return callback(null, revObj);
                    }, (err, datas) => {
                        let distanceUnit = distanceMatrix == 0 ? 1000 : 1608;
                        let proDistance = data.distance / distanceUnit;
                        let responseData = {
                            'firstName': data.firstName ? data.firstName : '',
                            'lastName': data.lastName ? data.lastName : '',
                            'profilePic': data.profilePic || '',
                            'email': data.email ? data.email : '',
                            'bannerImage': data.image ? data.image : '',
                            'lastActive': data.lastActive || '',
                            'photo': data.image ? data.image : '',
                            'noOfReview': data.reviewCountNum || 0,
                            'rating': data.averageRating || 0,
                            'location': data.location ? data.location : [],
                            'rateCard': data.rateCard ? data.rateCard : [],
                            'countryCode': data.countryCode ? data.countryCode : '',
                            'mobile': data.mobile ? data.mobile : '',
                            'about': data.about ? data.about : '',
                            'events': data.events || [],
                            'rules': data.rules ? data.rules : '',
                            'musicGenres': data.musicGeners ? data.musicGeners : '',
                            'instrument': data.instruments || '',
                            'review': revObj || [],
                            'services': data.services || [],
                            'distance': proDistance > 0 ? parseFloat((proDistance).toFixed(2)) : 0.00 || 0.00,
                            'amount': (data.services) ? data.services[0]?data.services[0].price : 0:0,
                            'youtubeUrlLink': data.link || '',
                            'currencySymbol': currencySymbol,
                            'currency': currency,
                            "distanceMatrix": distanceMatrix || 0,
                            'status': data.status == 3 ? 1 : 0,

                        }
                        resolve(responseData);
                    });
                }
            });
        });
    }//response data

    getProvider()
        .then(getCity)
        .then(responseData)
        .then(data => {
            return reply({ message: error['getProviderDetails']['200'][req.headers.lan], data: data }).code(200);
        }).catch(e => {
            logger.error("customer getProviderDetails API error =>", e)
            return reply({ message: e.message }).code(e.code);
        });
};

const responseCode = {
    status: {
        500: { message: error['genericErrMsg']['500'][error['lang']] },
        404: { message: error['getProviderDetails']['404'][error['lang']] },
        200: {
            message: error['getProviderDetails']['200'][error['lang']],
            data: joi.any()
        },
    }

}//swagger response code

module.exports = {
    params,
    handler,
    responseCode
};