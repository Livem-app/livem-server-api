
'use strict';

const joi = require('joi');
const Async = require('async');
const error = require('../../error');
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;
const provider = require('../../../../../models/provider');
const configuration = require('../../../../../configuration');
const cities = require('../../../../../models/cities');

const params = joi.object({
    lat: joi.number().description('latitude').error(new Error('Latitude must be number')),
    long: joi.number().description('longitude').error(new Error('Longitude must be number')),
}).required();

/**
 * @method GET /customer/provider/{lat}/{long}
 * @description get all prvider
 * @param {*} req 
 * @param {*} reply 
 * @property {number} lat
 * @property {number} long
 */
const handler = (req, reply) => {
    let cityId = "";
    let currencySymbol = '$';
    let currency = 'USD';
    let distanceMatrix = 0;
    const getCity = (data) => {
        return new Promise((resolve, reject) => {
            // turf.isWithinPolygons(req.lat, req.long, (err, res) => {
            //     if (res == null) { 
            //         resolve(data);

            //     } else {
            //         currencySymbol = res ? res.currencySymbol : '$' || '$';
            //         currency = res ? res.currency : 'USD' || 'USD';
            //         resolve(data);
            //     }
            // });
            let con = {
                polygons: {
                    $geoIntersects: {
                        $geometry: {
                            type: 'Point',
                            coordinates: [parseFloat(req.params.long || 0.0), parseFloat(req.params.lat || 0.0)]
                        }
                    }
                }
            };
            cities.read(con, (err, res) => {
                if (err) {
                    return reject({ message: error['genericErrMsg']['500']['1'], code: 500 });
                }
                if (res === null) {
                    logger.error("city not operational")
                    return reject({ message: "Currently we are not operational in your region , please contact liveM support.", code: 404 });

                } else {
                    currencySymbol = res ? res.currencySymbol : '$' || '$';
                    currency = res ? res.currency : 'USD' || 'USD';
                    distanceMatrix = res ? res.distanceMatrix : 0;
                    cityId = (res._id).toString();
                    resolve(data);
                }

            });
        });
    }

    const getAllProvider = () => {
        return new Promise((resolve, reject) => {
            provider.readBylatLong(req.params.lat, req.params.long, cityId, (err, res) => {
                return err ? reject({ message: error['genericErrMsg']['500'][req.headers.lan], code: 500 })
                    : resolve(res);
            });
        });
    }//get all provider

    const responseData = (data) => {
        return new Promise((resolve, reject) => {
            if (data.length == 0) {
                return reject({ message: "Currently no providers are available at this location.", code: 404 });
            }
            let dataArr = [];
            Async.forEach(data, (e, callbackloop) => {
                let distanceUnit = distanceMatrix == 0 ? 1000 : 1608;
                let proDistance = e.distance / distanceUnit;
                if (proDistance < e.radius) {
                    let st = e.status == 3 ? 1 : 0;
                    if (e.loggedIn == false)
                        st = 0;
                    let dt = {
                        'id': (e._id).toString(),
                        'firstName': e.firstName ? e.firstName : '',
                        'lastName': e.lastName ? e.lastName : '',
                        'image': e.profilePic ? e.profilePic : '',
                        'location': e.location ? e.location : '',
                        'bannerImage': e.image ? e.image : '',
                        'status': st,
                        'distance': proDistance > 0 ? parseFloat((proDistance).toFixed(2)) : 0.00 || 0,
                        'youtubeUrlLink': e.link || '',
                        'averageRating': e.averageRating,
                        'amount': (e.services) ? e.services[0] ? e.services[0].price : 0 : 0,
                        "currencySymbol": currencySymbol || "$",
                        "currency": currency || "USD",
                        "distanceMatrix": distanceMatrix || 0

                    };
                    dataArr.push(dt);
                    callbackloop(null, dataArr);
                } else {
                    callbackloop(null, dataArr);
                }


            }, (loopErr) => {
                dataArr.sort((a, b) => {
                    return ((a.id > b.id) ? 1 : -1);
                });
                resolve(dataArr);
            });
        });
    }//response data

    getCity()
        .then(getAllProvider)
        .then(responseData)
        .then(data => {
            return reply({ message: error['getProvider']['200'][req.headers.lan], data: data }).code(200);
        }).catch(e => {
            logger.error("customer getProvider API error =>", e)
            return reply({ message: e.message }).code(e.code);
        })
};

const responseCode = {
    status: {
        500: { message: error['genericErrMsg']['500'][error['lang']] },
        // 404: { message: error['getProvider']['404'][error['lang']] },
        200: {
            message: error['getProvider']['200'][error['lang']],
            data: joi.any()
        },
    }

}//swagger response code

module.exports = {
    params,
    handler,
    responseCode
};