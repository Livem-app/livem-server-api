'use strict';

const joi = require('joi');
const Async = require('async');
const logger = require('winston');
const moment = require('moment');
const ObjectID = require('mongodb').ObjectID;
const Timestamp = require('mongodb').Timestamp;
const error = require('../../error');


const schedule = require('../../../../../models/schedule');
const provider = require('../../../../../models/provider');
const customer = require('../../../../../models/customer');
const services = require('../../../../../models/services');
const category = require('../../../../../models/category');
const appConfig = require('../../../../../models/appConfig');
const configuration = require('../../../../../configuration');
const dailySchedules = require('../../../../../models/dailySchedules');



const payloadValidator = joi.object({
    latitude: joi.number().required().description('latitude').error(new Error('Latitude must be number')),
    longitude: joi.number().required().description('longitude').error(new Error('Longitude must be number')),
    providerId: joi.any().required().description('provider id'),
    bookingDate: joi.string().required().description("Booking date YYYY-MM-DD HH:MM:SS for schedule booking"),
    scheduleTime: joi.number().integer().required().description('Schedule booking for Minit like 30'),
    deviceTime: joi.string().optional().required().description('end date format : YYYY-MM-DD HH:MM:SS').error(new Error('current devidce  date & time is missing')),
}).required();//validator



/**
 * @method POST /customer/booking
 * @description -create booking
 * @param {*} req 
 * @param {*} reply 
 */
const APIHandler = (req, reply) => {
    console.log("req", req.payload)
    const getProvider = () => {
        return new Promise((resolve, reject) => {
            try {
                provider.getProviderById(req.payload.providerId, (err, res) => {
                    return err ? reject(dbErrResponse)
                        : (res === null) ? reject({ message: req.i18n.__('customerProviderDetailsGet')['404'], code: 404 })
                            : resolve(res);
                });
            } catch (e) {
                return resolve(data);
            }

        });
    }//get provider by id
    const checkScheduleAvailable = (data) => {
        return new Promise((resolve, reject) => {

            let serverTime = (moment().unix());
            let deviceTime = moment(req.payload.deviceTime, 'YYYY-MM-DD HH:mm:ss').unix();
            let timeZone = serverTime - deviceTime;

            let scheduledate = parseFloat(req.payload.bookingDate);//moment().unix();
            let strtDateTime = scheduledate;// + timeZone;
            let endTime = scheduledate + (req.payload.scheduleTime * 60);
            let condition = [
                { '$unwind': '$schedule' },
                {
                    '$match': {
                        "schedule.providerId": new ObjectID(req.payload.providerId),
                        'schedule.startTime': { '$lte': strtDateTime },
                        'schedule.endTime': { '$gte': strtDateTime }
                    }
                }

            ];
            dailySchedules.readByAggregate(condition, (err, res) => {
                console.log('resssssssssssssssssss->>>>>>>>',strtDateTime)
                if (err) {
                    return reject(dbError);
                } else if (res.length == 0) {
                    console.log("-dddddddddddddddddddddd-", res)
                    return reject({ message: "Unfortunately the artist " + data.firstName + " is not available at that date and time, please try to another date/time or artist.", code: 414 });
                } else {
                    console.log("--", res)
                    Async.forEach(res, function (sche, callbackloop) {
                        let scheduleData = sche.schedule;
                        let booked = scheduleData.booked
                        if (scheduleData.startTime <= strtDateTime && scheduleData.endTime >= endTime) {
                            Async.forEach(booked, function (bookSche, callbackloopp) {
                                if (
                                    ((strtDateTime + 600) <= bookSche.start && endTime <= bookSche.start) ||
                                    ((strtDateTime + 600) >= bookSche.end && endTime >= bookSche.end)
                                ) {
                                    logger.info("schedule is free")
                                }
                                else {
                                    return reject({ message: "Schedule not available", code: 415 });
                                }

                                callbackloopp(null)
                            }, (err, res) => {
                                callbackloop(null);
                            });

                        } else {
                            callbackloop('false');
                        }
                    }, function (loopErr, res) {
                        if (loopErr) {
                            return reject({ message: "Schedule not available", code: 413 });
                        } else {
                            return resolve(data);
                        }

                    });
                }
            });
        });
    }
    getProvider().
        then(checkScheduleAvailable)
        .then(data => {
            return reply({ message: "Got The Details" }).code(200);
        })
        .catch(e => {
            logger.error("Customer postBooking API error =>", e)
            return reply({ message: e.message }).code(e.code);
        });
};//API handler

const responseCode = {

}//swagger response code

module.exports = {
    payloadValidator,
    APIHandler,
    responseCode
};