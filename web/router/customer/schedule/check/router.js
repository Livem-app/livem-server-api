'use strict';

const entity = '/customer';
const post = require('./post');
const error = require('../../error');
const headerValidator = require('../../../../middleware/validator');

module.exports = [

    /**
     * @name POST /customer/booking
     */
    {
        method: 'POST',
        path: entity + '/schedule',
        handler: post.APIHandler,
        config: {
            tags: ['api', 'booking'],
            description: "Check schedule",
            notes: 'checkSchedule',
            auth: 'customerJWT',
            // response: post.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: post.payloadValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    }
];