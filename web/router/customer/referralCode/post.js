'use strict';
const joi = require('joi');
const logger = require('winston');
const error = require('../error');
const customer = require('../../../../models/customer');
const cities = require('../../../../models/cities');
const referralCodes = require('../../../../models/referralCampaigns/referralCode');

const payload = joi.object({
    referralCode: joi.string().required().min(4).description('Code required'), 
}).required();//payload validator

/**
 * @method POST /customer/referralCodeValidation
 * @param {*} req 
 * @param {*} reply 
 */
const handler = (req, reply) => {
    const dbErrResponse = { message: error['genericErrMsg']['500'][req.headers.lan], code: 500 };

    let referralCode = req.payload.referralCode;
    referralCodes.getDetailsByCode(referralCode, (error, response) => {
        if (error) {
            return reply({
                message: "Error while fetching details",
                data: {
                    status: false
                }
            }).code(500);
        } else if (response.length == 0) {
            return reply({
                message: "Invalid referral code",
                data: {
                    status: false
                }
            }).code(402);
        } else {
            return reply({
                message: "success",
                data: {
                    status: true
                }
            }).code(200);
        }
    })

}
const responseCode = {
    // status: {
    //     200: { message: error['postReferralCodeValidation']['200'][error['lang']], data: joi.any() },
    //     400: { message: error['postReferralCodeValidation']['400'][error['lang']] },
    //     401: { message: error['postReferralCodeValidation']['401'][error['lang']] },
    //     500: { message: error['genericErrMsg']['500'][error['lang']] }
    // }

}//swagger response code

module.exports = {
    payload,
    handler,
    responseCode
};