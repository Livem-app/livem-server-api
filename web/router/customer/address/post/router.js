'use strict';

const entity = '/customer';

const post = require('./post');
const error = require('../../error');
const headerValidator = require('../../../../middleware/validator');

module.exports = [

    /**
    * @name POST /provider/address
    */
    {
        method: 'POST',
        path: entity + '/address',
        handler: post.handler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['postAddress'],
            notes: "500 - internal server error,\n\n 200 - success",
            auth: 'customerJWT',
            response: post.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: post.payload,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },


];