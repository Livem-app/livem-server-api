'use strict';

const entity = '/customer';
 
const error = require('../../error');
const deleteById = require('./delete');
const headerValidator = require('../../../../middleware/validator');

module.exports = [

     
    /**
    * @name DELETE /prvider/address/{id}
    */
    {
        method: 'DELETE',
        path: entity + '/address/{id}',
        handler: deleteById.handler,
        config: {
            tags: ['api', entity],
            description:  error['apiDescription']['deleteAddress'],
            notes: "500 - internal server error,\n\n 200 - success",
            auth: 'customerJWT',
            response: deleteById.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                params: deleteById.params,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
 
];