'use strict';

const entity = '/customer';

const get = require('./get'); 
const error = require('../../error');
const headerValidator = require('../../../../middleware/validator');

module.exports = [

    /**
    * @name GET /customer/address
    */
    {
        method: 'GET',
        path: entity + '/address',
        handler: get.handler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['getAddress'],
            notes: "Get all saved address",
            auth: 'customerJWT',
            response: get.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },

     
];