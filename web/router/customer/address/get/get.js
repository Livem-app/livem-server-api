
'use strict';

const joi = require('joi');
const logger = require('winston');
const error = require('../../error');
const address = require('../../../../../models/address');

/**
 * @method GET /customer/addresss
 * @description this api get all saved address
 * @param {*} req  
 * @param {*} reply 
 */
const handler = (req, reply) => {
    address.readByUserId(req.auth.credentials._id, (err, res) => {
        return err ? reply({ message: error['genericErrMsg']['500'][req.headers.lan] }).code(500)
            : reply({ message: error['getAddress']['200'][req.headers.lan], data: res || [] }).code(200);
    });
};

const responseCode = {
    status: {
        200: { message: error['getAddress']['200'][error['lang']], data: joi.any() },
        500: { message: error['genericErrMsg']['500'][error['lang']] }
    }
}//swagger response code

module.exports = {
    handler,
    responseCode
};