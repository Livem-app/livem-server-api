'use strict'

const get = require('./get')
const post = require('./post');
const patch = require('./patch');
const deletes = require('./delete');

module.exports = [].concat(
    get,
    post,
    patch,
    deletes
);