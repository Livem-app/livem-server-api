'use strict';

const entity = '/customer';

const patch = require('./patch');
const error = require('../../error');
const headerValidator = require('../../../../middleware/validator');

module.exports = [
    /**
    * @name PATCH /provider/address
    */
    {
        method: 'PATCH',
        path: entity + '/address',
        handler: patch.handler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['patchAddress'],
            notes: "Update address by address id",
            auth: 'customerJWT',
            response: patch.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: patch.payload,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
];