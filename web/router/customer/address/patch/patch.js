'use strict';

const joi = require('joi');
const util = require('util');
const logger = require('winston');
const error = require('../../error');
const address = require('../../../../../models/address');


const payload = joi.object({
    id: joi.string().required().description('address Id'),
    addLine1: joi.string().required().description('address line 1'),
    addLine2: joi.any().description('address line 2'),
    city: joi.any().description('city'),
    state: joi.any().description('state'),
    country: joi.any().description('country'),
    placeId: joi.any().description('placeId'),
    pincode: joi.number().integer().description('pincode').error(new Error('Pincode must be number')),
    latitude: joi.number().required().description('latitude').error(new Error('Latitude must be number')),
    longitude: joi.number().required().description('longitude').error(new Error('Longitude must be number')),
    taggedAs: joi.string().required().description('office / home'),
    userType: joi.number().integer().min(1).max(2).description('1 - slave,2 - master').error(new Error('Please enter valid user type')),
}).required();//validator

/**
 * @method PATCH /customer/address
 * @description insert saved address
 * @param {*} req 
 * @param {*} reply 
 * @property {string} id -Address id
 * @property {string} addLine1 - Address Line 1
 * @property {string} addLine2 - Address Line 2
 * @property {string} city - city
 * @property {string} state - state
 * @property {string} country -country
 * @property {string} placeId -placeId
 * @property {number} pincode -pincode
 * @property {number} latitude
 * @property {number} longitude
 * @property {string} taggedAs
 * @property {number} userType
 * 
 */
const handler = (req, reply) => {
    let dataToUpdate = {};
    if (!(util.isNullOrUndefined(req.payload.addLine1)))
        dataToUpdate.addLine1 = req.payload.addLine1;
    if (!(util.isNullOrUndefined(req.payload.addLine2)))
        dataToUpdate.addLine2 = req.payload.addLine2;
    if (!(util.isNullOrUndefined(req.payload.city)))
        dataToUpdate.city = req.payload.city;
    if (!(util.isNullOrUndefined(req.payload.state)))
        dataToUpdate.state = req.payload.state;
    if (!(util.isNullOrUndefined(req.payload.country)))
        dataToUpdate.country = req.payload.country;
    if (!(util.isNullOrUndefined(req.payload.placeId)))
        dataToUpdate.placeId = req.payload.placeId;
    if (!(util.isNullOrUndefined(req.payload.pincode)))
        dataToUpdate.pincode = req.payload.pincode;
    if (!(util.isNullOrUndefined(req.payload.latitude)))
        dataToUpdate.latitude = req.payload.latitude;
    if (!(util.isNullOrUndefined(req.payload.longitude)))
        dataToUpdate.longitude = req.payload.longitude;
    if (!(util.isNullOrUndefined(req.payload.taggedAs)))
        dataToUpdate.taggedAs = req.payload.taggedAs;
    if (!(util.isNullOrUndefined(req.payload.userType)))
        dataToUpdate.userType = req.payload.userType;

    address.updateById(req.payload.id, dataToUpdate, (err, res) => {
        return (err) ? reply({ message: error['genericErrMsg']['500'][req.headers.lan] }).code(500)
            : reply({ message: error['patchAddress']['200'][req.headers.lan] }).code(200);
    });

};//API handler

const responseCode = {
    status: {
        200: { message: error['patchAddress']['200'][error['lang']] },
        500: { message: error['genericErrMsg']['500'][error['lang']] }
    }
}//swagger response code


module.exports = {
    handler,
    payload,
    responseCode
};