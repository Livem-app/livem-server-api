'use strict';

const joi = require('joi');
const logger = require('winston');
const error = require('../../error');
const customer = require('../../../../../models/customer'); 


const payload  = joi.object({
    userType: joi.any().description('1- slave , 2- master').error(new Error('User type is incorrect Please enter valid type')),
    email: joi.any().description('email addresss').error(new Error('email is invalid PPlease enter correct')),
}).required();
/**
 * @method PATCH /customer/email
 * @param {*} req 
 * @param {*} reply 
 * @property {number} userType
 * @property {string} email
 */
const handler = (req, reply) => {
    customer.read({ 'email': req.payload.email }, (err, res) => {
        if (err)
            return reply({ message: error['genericErrMsg']['500'][req.headers.lan] }).code(500);
        if (res != null) {
            return reply({ message: error['patchEmail']['412'][req.headers.lan] }).code(412);
        } else {
            customer.findOneAndUpdate(req.auth.credentials._id, { $set: { email: req.payload.email } }, (err, res) => {
                if (err)
                    return reply({ message: error['genericErrMsg']['500'][req.headers.lan] }).code(500);
                else {
                    return reply({ message: error['patchEmail']['200'][req.headers.lan] }).code(200);
                }
            });
        }
    });

};

const responseCode = {
    status: {
        200: { message: error['patchEmail']['200'][error['lang']] },
        412: { message: error['patchEmail']['412'][error['lang']] },
        500: { message: error['genericErrMsg']['500'][error['lang']] }
    }
}//swagger response code


module.exports = {
    payload,
    handler,
    responseCode
};