'use strict';

const entity = '/customer';

const email = require('./email');
const error = require('../../error');
const profile = require('./profile');
const patchPassword = require('./password');
const patchPasswordMe = require('./passwordMe');
const patchPhoneNumber = require('./phoneNumber');
const headerValidator = require('../../../../middleware/validator');

module.exports = [
    /**
     * @name PATCH /customer/profile/me
     */
    {
        method: 'PATCH',
        path: entity + '/profile/me',
        handler: profile.handler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['patchProfile'],
            notes: 'This API enables the user to edit his details on the profile Page.',
            auth: 'customerJWT',
            response: profile.responseCode,
            validate: {
                payload: profile.payload,
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },

    /**
     * @name PATCH /app/email
     */
    {
        method: 'PATCH',
        path: entity + '/email',
        handler: email.handler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['patchEmail'],
            notes: "This API enables the user to change his email addresss.",
            auth: 'customerJWT',
            response: email.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: email.payload,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },

    /**
    * @name PATCH /app/phoneNumber
    */
    {
        method: 'PATCH',
        path: entity + '/phoneNumber',
        handler: patchPhoneNumber.handler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['patchPhoneNumber'],
            notes: 'This API enables the user to change his phne number.It is accepted once the phone number is verified with a verification code.',
            auth: 'customerJWT',
            response: patchPhoneNumber.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: patchPhoneNumber.payload,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },

    /**
    * @name PATCH /slave/password/me
    */
    {
        method: 'PATCH',
        path: entity + '/password/me',
        handler: patchPasswordMe.handler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['patchPasswordMe'],
            notes: 'api to change password by user.',
            auth: 'customerJWT',
            response: patchPasswordMe.responseCode,
            validate: {
                payload: patchPasswordMe.payload,
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },

    /**
     * @name PATCH /app/password
     */
    {
        method: 'PATCH',
        path: entity + '/password',
        handler: patchPassword.handler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['patchPasswordMe'],
            notes: 'This API allows a user to reset password.',
            auth: false,
            response: patchPassword.responseCode,
            validate: {
                headers: headerValidator.headerLanValidator,
                payload: patchPassword.payload,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
];