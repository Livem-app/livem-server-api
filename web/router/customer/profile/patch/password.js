'use strict';

const joi = require('joi');
const Bcrypt = require('bcrypt');
const logger = require('winston');
const error = require('../../error'); 
const sendMsg = require('../../../../../library/twilio');
const customer = require('../../../../../models/customer'); 
const configuration = require('../../../../../configuration')
const verificationCode = require('../../../../../models/verificationCode');

const payload = joi.object({
    password: joi.string().required().description('provide strong password'),
    userId: joi.string().required().description('user id'),
    userType: joi.number().required().integer().min(1).max(2).required().description('1- slave , 2- master').error(new Error('User type is incorrect Please enter valid type'))
}).required();

/**
 * @method PATCH /customer/password
 * @param {*} req 
 * @param {*} reply 
 */
let handler = (req, reply) => {
    const dbErrResponse = { message: error['genericErrMsg']['500'][req.headers.lan], code: 500 };
    const checkVerification = (data) => {
        return new Promise((resolve, reject) => {
            var condition = {
                userType: 1,
                triggeredBy: 2,
                userId: req.payload.userId.toString()
            }
            verificationCode.read(condition, (err, result) => {
                return err ? reject(dbErrResponse)
                    : (result === null) ? reject({ message: error['patchPassword']['404'][req.headers.lan], code: 404 })
                        : resolve(result);

            })
        })
    }
    const postPassword = (data) => {
        return new Promise((resolve, reject) => {
            let updateData = {
                $set: {
                    password: Bcrypt.hashSync(req.payload.password, parseInt(configuration.SALT_ROUND)),
                }
            };
            customer.findOneAndUpdate(data.userId, updateData, (err, res) => {
                return err ? reject(dbErrResponse) : resolve(res);
            })

        });
    }
    checkVerification()
        .then(postPassword)
        .then(data => {
            return reply({ message: error['patchPassword']['200'][req.headers.lan] }).code(200);
        }).catch(e => {
            logger.error("customer post review and rating API error =>", e)
            return reply({ message: e.message }).code(e.code);
        });
};

const responseCode = {
    status: {
        200: { message: error['patchPassword']['200'][error['lang']] },
        404: { message: error['patchPassword']['404'][error['lang']] },
        500: { message: error['genericErrMsg']['500'][error['lang']] }
    }
}//swagger response code


module.exports = {
    payload,
    handler,
    responseCode
};