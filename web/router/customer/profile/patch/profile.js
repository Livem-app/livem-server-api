'use strict';

const joi = require('joi');
const Async = require('async');
const logger = require('winston');
const error = require('../../error');
const customer = require('../../../../../models/customer');
const category = require('../../../../../models/category');
const userList = require('../../../../commonModels/userList');
const ObjectID = require('mongodb').ObjectID;


const payload = joi.object({
    profilePic: joi.any().description('profile'),
    firstName: joi.string().description('first name'),
    lastName: joi.string().allow("").description('last name'),
    about: joi.string().allow("").description('about'),
    preferredGenres: joi.any().allow("").description(' Preferred Genres'),
    dateOfBirth: joi.string().allow("").description('Date of birth format : YYYY-MM-DD'),
}).required();

/**
 * @method PATCH /customer/patchProfile
 * @param {*} req 
 * @param {*} reply 
 */
let handler = (req, reply) => {
    let genresData = [];

    const updateData = (data) => {
        return new Promise((resolve, reject) => {
            let dataToUpdate = {};
            let userData = {};
            if (typeof req.payload.firstName != 'undefined') {
                userData.firstName = req.payload.firstName;
                userData.lastName = req.payload.lastName;
                dataToUpdate.firstName = req.payload.firstName;
                dataToUpdate.lastName = req.payload.lastName;
            }

            if (typeof req.payload.profilePic != 'undefined') {
                userData.profilePic = req.payload.profilePic;
                dataToUpdate.profilePic = req.payload.profilePic;
            }


            if (typeof req.payload.about != 'undefined')
                dataToUpdate.about = req.payload.about;

            if (typeof req.payload.preferredGenres != 'undefined')
                dataToUpdate.preferredGenres = req.payload.preferredGenres;


            if (typeof req.payload.dateOfBirth != 'undefined')
                dataToUpdate.dateOfBirth = req.payload.dateOfBirth;
            if (Object.keys(userData).length != 0) {
                userList.updateUser(req.auth.credentials._id,
                    userData
                )//insert userlist table use for simple chat-module
            }
            if (Object.keys(dataToUpdate).length === 0)
                return reply({ message: error['patchProfile']['400'][req.headers.lan] }).code(400);
            let updateQuery = { $set: dataToUpdate };
            customer.findOneAndUpdate(req.auth.credentials._id, updateQuery, (err, res) => {
                return err ? reject({ message: error['genericErrMsg']['500'][req.headers.lan], code: 500 }) :
                    resolve(res);
            });
        });
    }//update profile

    updateData()
        .then(data => {
            return reply({ message: error['patchProfile']['200'][req.headers.lan] }).code(200);
        }).catch(e => {
            logger.error("Customer patchProfile API error =>", e)
            return reply({ message: e.message }).code(e.code);
        });
};

const responseCode = {
    status: {
        200: { message: error['patchProfile']['200'][error['lang']] },
        400: { message: error['patchProfile']['400'][error['lang']] },
        500: { message: error['genericErrMsg']['500'][error['lang']] }
    }
}//swagger response code


module.exports = {
    payload,
    handler,
    responseCode
};