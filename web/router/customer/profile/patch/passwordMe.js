
'use strict';

const joi = require('joi');
const util = require('util');
const error = require('../../error');
const Bcrypt = require('bcrypt');
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId
const customer = require('../../../../../models/customer');
const configuration = require('../../../../../configuration')

const payload = joi.object({
    oldPassword: joi.string().required().description('old password'),
    newPassword: joi.string().required().description('new password'),
}).required();//payload of provider update profile api

/**
 * @method PATCH /customer/password/me
 * @param {*} req 
 * @param {*} reply 
 * @property {string} oldPassword
 * @property {string} newPassword
 */
const handler = (req, reply) => {
    const dbErrResponse = { message: error['genericErrMsg']['500'][req.headers.lan], code: 500 };
    let dataToUpdate = {};
    const getCustomer = function () {
        return new Promise((resolve, reject) => {
            customer.read({ _id: new ObjectID(req.auth.credentials._id) }, (err, res) => {
                return err ? reject(dbErrResponse) : resolve(res);
            });
        });
    }///get customer

    const updatePassword = function (data) {
        return new Promise((resolve, reject) => {
            if (data === null)
                reject({ message: error['patchPassword']['404'][req.headers.lan], code: 404 })
            let isPasswordValid = Bcrypt.compareSync(req.payload.oldPassword, data.password);
            if (isPasswordValid) {
                let password = Bcrypt.hashSync(req.payload.newPassword, parseInt(configuration.SALT_ROUND));
                customer.findOneAndUpdate(req.auth.credentials._id, { $set: { password: password } }, (err, res) => {
                    return err ? reject(err) : resolve(data);
                })

            } else {
                return reply({ message: error['patchPassword']['401'][req.headers.lan] }).code(401);
            }



        });
    }//update padssword

    getCustomer()
        .then(updatePassword)
        .then(data => {
            return reply({ message: error['patchPassword']['200'][req.headers.lan] }).code(200);
        }).catch(e => {
            logger.error("customer update password API error =>", e)
            return reply({ message: e.message }).code(e.code);
        });

};

const responseCode = {
    status: {
        200: { message: error['patchPassword']['200'][error['lang']] },
        404: { message: error['patchPassword']['404'][error['lang']] },
        404: { message: error['patchPassword']['401'][error['lang']] },
        500: { message: error['genericErrMsg']['500'][error['lang']] }
    }
}//swagger response code

module.exports = {
    payload,
    handler,
    responseCode
};