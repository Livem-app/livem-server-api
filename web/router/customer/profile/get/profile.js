
'use strict';
const joi = require('joi');
const logger = require('winston');
const error = require('../../error');
const ObjectID = require('mongodb').ObjectID;
const customer = require('../../../../../models/customer');

/**
 * @method GET /customer/getProfile
 * @description  
 * @param {*} req  
 * @param {*} reply 
 * 
 */
const handler = (req, reply) => {
    const getCustomer = () => {
        return new Promise((resolve, reject) => {
            customer.read({ _id: new ObjectID(req.auth.credentials._id) }, (err, res) => {
                return err ? reject(reject({ message: error['genericErrMsg']['500'][req.headers.lan], code: 500 }))
                    : (res == null) ? reject(({ message: error['getProfile']['404'][req.headers.lan], code: 404 }))
                        : resolve(res);
            });
        });
    }
    const responseData = (data) => {
        return new Promise((resolve, reject) => {
            let dataArr = {
                'firstName': data.firstName ? data.firstName : '',
                'lastName': data.lastName ? data.lastName : '',
                'dateOfBirth': data.dateOfBirth ? data.dateOfBirth : '',
                'preferredGenres': data.preferredGenres ? data.preferredGenres : '',
                'email': data.email ? data.email : "",
                'profilePic': data.profilePic ? data.profilePic : "",
                'about': data.about ? data.about : "",
            }
            data.phone.forEach(function (e) {
                if (e.isCurrentlyActive == true) {
                    dataArr.countryCode = e.countryCode;
                    dataArr.phone = e.phone;
                }
            }, resolve(dataArr));
        });
    }
    getCustomer()
        .then(responseData)
        .then(data => {
            return reply({ message: error['getProfile']['200'][req.headers.lan], data: data }).code(200);
        }).catch(e => {
            logger.error("customer getProfile API error =>", e)
            return reply({ message: e.message }).code(e.code);
        })
};

const responseCode = {
    status: {
        500: { message: error['genericErrMsg']['500'][error['lang']] },
        404: { message: error['getProfile']['404'][error['lang']] },
        200: {
            message: error['getProfile']['200'][error['lang']],
            data: joi.any()
        },
    }

}//swagger response code

module.exports = {
    handler,
    responseCode
};