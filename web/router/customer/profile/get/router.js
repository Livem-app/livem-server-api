'use strict';

const entity = '/customer';
const profile = require('./profile'); 
const error = require('../../error'); 
const headerValidator = require('../../../../middleware/validator');

module.exports = [
     
    /**
     * @name GET /customer/profile/me
     */
    {
        method: 'GET',
        path: entity + '/profile/me',
        handler: profile.handler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['getProfile'],
            notes: 'This API is to get the information of the uer on his Profile Page.',
            auth: 'customerJWT',
            response: profile.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
 
];