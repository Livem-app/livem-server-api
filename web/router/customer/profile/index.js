'use strict'

const get = require('./get');

const patch = require('./patch');

module.exports = [].concat(
    get,
    patch
);