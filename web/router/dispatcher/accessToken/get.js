
'use strict';
const joi = require('joi');
const logger = require('winston');
const error = require('../error');
const appConfig = require('../../../../models/appConfig');
const Auth = require('../../../middleware/authentication');

/**
 * @method GET /customer/accessToken
 * @description this api used to get access token
 * @param {*} req 
 * @property {string} req.lan
 * @param {*} reply 
 * 
 */
const handler = (req, reply) => {
    appConfig.read((err, res) => {
        if (err)
            return reply({ message: error['genericErrMsg']['500'][req.headers.lan] }).code(500);
        if (res === null)
            return reply({ message: error['getAccessToken']['404'][req.headers.lan] }).code(404);
        let authToken = Auth.SignJWT({
            _id: req.auth.credentials._id,
            key: 'acc',
        }, req.auth.credentials.sub, res.accessToken); //sign a new JWT
        return reply({ message: error['getAccessToken']['200'][req.headers.lan], data: authToken }).code(200);
    });
};

const responseCode = {
    status: {
        500: { message: error['genericErrMsg']['500'][error['lang']] },
        404: { message: error['getAccessToken']['404'][error['lang']] },
        200: {
            message: error['getAccessToken']['200'][error['lang']],
            data: joi.any()
        },
    }

}//swagger response code

module.exports = {
    handler,
    responseCode
};