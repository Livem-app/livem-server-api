'use strict';
const joi = require('joi');
const logger = require('winston');
const entity = '/dispatcher';
const get = require('./get'); 
const error = require('../error');
const headerValidator = require('../../../middleware/validator');
const webSocket = require('../../../../models/MQTT')
module.exports = [

    /**
    * @name GET /app/accessToken
    */
    {
        method: 'GET',
        path: entity + '/accessToken',
        handler: get.handler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['getAccessToken'],
            notes: 'Get Accesss token',
            auth: 'refJwt',
            response: get.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
  
    {
        method: 'POST',
        path: entity + '/mqtt',
        config: {
            handler: function (req, reply) { 

                var msg = { auther: '3embed' }
                webSocket.publish(req.payload.topic, msg, {}, function (mqttErr, mqttRes) {
                    if (mqttErr)
                        reply({ err: 1, message: mqttErr.message }).code(500)
                    else
                        reply({ err: 0, data: mqttRes }).code(200)
                })

            },
            validate: {
                payload: {
                    topic: joi.string().description("topic is required")
                }
            },
            plugins: {
                'hapi-swagger': {
                    responses: {
                        '200': {
                            'description': 'Success',
                        }
                    }
                }

            },
            description: 'Test API',
            notes: 'test',
            tags: ['api', 'test'],
        }

    }

];