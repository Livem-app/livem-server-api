'use strict';

const entity = '/dispatcher';
const error = require('../error');
const get = require('./get');
const getByRegex = require('./getByRegex');
const getByBookingSatus = require('./getByBookingSatus');
const headerValidator = require('../../../middleware/validator');

module.exports = [

     
    // {
    //     method: 'GET',
    //     path: entity + '/provider/{pageIndex}',
    //     handler: get.handler,
    //     config: {
    //         tags: ['api', entity],
    //         description: error['apiDescription']['getProvider'],
    //         notes: 'dispatcher',
    //         auth: 'dispatcherJWT',
    //         response: get.responseCode,
    //         validate: {
    //             params: get.params,
    //             headers: headerValidator.headerAuthValidator,
    //             failAction: (req, reply, source, error) => {
    //                 return reply({ message: error.output.payload.message }).code(error.output.statusCode);
    //             }
    //         }
    //     }
    // },

    {
        method: 'GET',
        path: entity + '/provider/{bookingStatus}/{pageIndex}',
        handler: getByBookingSatus.handler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['getByBookingStatus'],
            auth: 'dispatcherJWT',
            response: getByBookingSatus.responseCode,
            validate: {
                params: getByBookingSatus.params,
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },

    {
        method: 'GET',
        path: entity + '/provider/autoComplete/{type}/{input}',
        handler: getByRegex.handler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['getByRegex'],
            auth: 'dispatcherJWT',
            // response: getByRegex.responseCode,
            validate: {
                params: getByRegex.params,
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
    {
        method: 'GET',
        path: entity + '/providers/{searchStr}/{pageIndex}',
        handler: get.handler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['getProvider'],
            notes: 'dispatcher',
            auth: 'dispatcherJWT',
            response: get.responseCode,
            validate: {
                params: get.params,
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
    


];