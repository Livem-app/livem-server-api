
'use strict';

const joi = require('joi');
const Async = require('async');
const error = require('../error');
const moment = require('moment');//date-time
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;
const provider = require('../../../../models/provider');
const bookings = require('../../../../models/bookings'); 

const params = joi.object({
    type: joi.number().integer().min(1).max(3).description('type of input 1:name 2:email 3:phone').error(new Error('input type is missing')),
    input: joi.string().description('input data').error(new Error('input is missing')),
}).required();

Array.prototype.myPhone = function (obj) {
    return this.filter(function (item) {
        for (var prop in obj)
            if (!(prop in item) || obj[prop] !== item[prop])
                return false;
        return true;
    });
};
const handler = (req, reply) => {

    const getUser = () => {
        return new Promise((resolve, reject) => {
            let query;
            switch (req.params.type) {
                case 1:
                    query = { "firstName": { $regex: req.params.input, $options: 'i' } };
                    break;
                case 2:
                    query = { "email": { $regex: req.params.input, $options: 'i' } };
                    break;
                case 3:
                    query = { "phone.phone": { $regex: req.params.input, $options: 'i' } };
                    break;
                default:
                    query = { "firstName": { $regex: req.params.input, $options: 'i' } }
                    break;
            }

            provider.readAll(query, (err, res) => {
                return err ? reject(err) : resolve(res);

            });
        });
    };//get User by  email or phone number
    const responseData = (data) => {
        let providerArr = [];
        return new Promise((resolve, reject) => {
            if (data !== null)
                data.forEach((pro) => {
                    var obj = { items: pro.phone };
                    var arrayFound = obj.items.myPhone({ "isCurrentlyActive": true });
                    let resData = {
                        firstName: pro.firstName || '',
                        lastName: pro.lastName || '',
                        email: pro.email || '',
                        countryCode: arrayFound[0].countryCode,
                        phone: arrayFound[0].phone || "",
                        profilePic: pro.profilePic || '',

                    }
                    providerArr.push(resData);
                }, resolve(providerArr));

        });
    }
    getUser()
        .then(responseData)
        .then(data => {

            return reply({ message: error['postSignIn']['200'][req.headers.lan], data: data }).code(200);
        }).catch(e => {
            logger.error("Dispatcher Login API error =>", e)
            return reply({ message: e.message }).code(e.code);
        });

};

const responseCode = {
    // status: {
    //     500: { message: error['genericErrMsg']['500'][error['lang']] },
    //     200: {
    //         message: error['postSignIn']['200'][error['lang']],
    //         data: joi.any()
    //     },
    //     404: { message: error['postSignIn']['404'][error['lang']] },
    //     401: { message: error['postSignIn']['401'][error['lang']] }
    // }

}//swagger response code


module.exports = {
    params,
    handler,
    responseCode
};