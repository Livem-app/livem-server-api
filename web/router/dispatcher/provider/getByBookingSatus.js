
'use strict';

const joi = require('joi');
const Async = require('async');
const error = require('../error');
const moment = require('moment');//date-time
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;
const provider = require('../../../../models/provider');
const bookings = require('../../../../models/bookings'); 


const params = joi.object({
    bookingStatus: joi.string().description('bookingStatus 0- free 6- OntheWay , 7- Arrived, 8- started').error(new Error('bookingStatus is missing')),
    pageIndex: joi.number().description('Page Index').error(new Error('Page Index must be number')),
}).required();

Array.prototype.myFind = function (obj) {
    return this.filter(function (item) {
        for (var prop in obj)
            if (!(prop in item) || obj[prop] !== item[prop])
                return false;
        return true;
    });
};
const handler = (req, reply) => {
    var limit = 20;
    var skip = 20 * req.params.pageIndex;
    Async.series([
        function (callback) {
            if (req.params.bookingStatus == 1) {//1:all 0:free,6:ontheway 7:arrive 8:start
                var con = { "status": 3, "loggedIn": true };
            } else {
                var con = { "status": 3, "loggedIn": true, "bookingStatus": parseInt(req.params.bookingStatus) };
            }

            provider.readAllByLimit({ q: con, sort: { _id: -1 }, limit: limit, skip: skip }, (err, res) => {
                if (err) { 
                    callback(err);
                } else {
                    var onlinePro = [];
                    Async.forEach(res, function (item, callbackloop) {
                        var cons = { status: { "$nin": [1, 2, 10, 11, 12] }, providerId: item._id.toString() };
                        bookings.bookingCount(cons, (err, count) => {
                            if (err) { 
                                callback(err);
                            } else {
                                var fname = (typeof item.firstName == 'undefined' || item.firstName == null) ? "" : item.firstName;
                                var lname = (typeof item.lasrName == 'undefined' || item.lasrName == null) ? "" : item.lasrName;
                                var name = fname + " " + lname;
                                var obj = { items: item.phone };
                                var arrayFound = obj.items.myFind({ "isCurrentlyActive": true });
                                onlinePro.push({
                                    '_id': item._id.toString(),
                                    'latitude': item.location.latitude,
                                    'longitude': item.location.longitude,
                                    'image': item.profilePic || '',
                                    'status': item.status,
                                    'email': item.email || '',
                                    'lastActive': item.lastActive,
                                    'lastOnline': moment().unix() - item.lastActive,
                                    'serverTime': moment().unix(),
                                    'name': name || '',
                                    'batteryPercentage': item.mobileDevices.batteryPercentage || '',
                                    'appversion': item.mobileDevices.appversion || '',
                                    'phone': arrayFound[0].countryCode + arrayFound[0].phone || "",
                                    'deviceType': item.mobileDevices.deviceType || '',
                                    'locationCheck': item.locationCheck == 0 ? 'off' : 'on',
                                    'bookingCount': count,
                                    'time': moment.unix(item.lastActive).fromNow()
                                });

                                callbackloop(null, onlinePro);
                            }
                        }); 

                    }, function (loopErr) {
                        callback(null, onlinePro);
                    });

                }
            });
        },

        function (callback) {//ONLINE COUNT
            if (req.params.bookingStatus == 1) {//1:all 0:free,6:ontheway 7:arrive 8:start
                var con = { "status": 3, "loggedIn": true };
            } else {
                var con = { "status": 3, "loggedIn": true, "bookingStatus": parseInt(req.params.bookingStatus) };
            }
            provider.count(con, (err, res) => {
                if (err) { 
                    callback(err);
                } else { 
                    callback(null, res);
                }
            });
        },

    ], function (err, result) {
        if (err)
            return reply({ message: error['genericErrMsg']['500'][req.headers.lan] }).code(500);
        var data = {
            onlinePro: result[0],
            count: result[1],

        }
        return reply({ message: error['disProvider']['200'][req.headers.lan], data: data }).code(200);
    });
};

const responseCode = {
    status: {
        // 500: { message: error['genericErrMsg']['500'][error['lang']] },
        // 200: {
        //     message: error['getProvider']['200'][error['lang']],
        //     data: joi.any()
        // },
    }

}//swagger response code

module.exports = {
    params,
    handler,
    responseCode
};