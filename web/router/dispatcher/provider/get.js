
// 'use strict';

// const joi = require('joi');
// const Async = require('async');
// const error = require('../error');
// const moment = require('moment');//date-time
// const logger = require('winston');
// const ObjectID = require('mongodb').ObjectID;
// const provider = require('../../../../models/provider');
// const bookings = require('../../../../models/bookings');
// const webSocket = require('../../../../models/webSocket');


// const params = joi.object({
//     pageIndex: joi.number().description('Page Index').error(new Error('Page Index must be number')),
// }).required();

// Array.prototype.myFind = function (obj) {
//     return this.filter(function (item) {
//         for (var prop in obj)
//             if (!(prop in item) || obj[prop] !== item[prop])
//                 return false;
//         return true;
//     });
// };
// const handler = (req, reply) => {

//     let limit = 20;
//     let skip = 20 * req.params.pageIndex;
//     Async.series([
//         function (callback) {
//             var con = { "status": 3, "loggedIn": true };
//             provider.readAllByLimit({ q: con, sort: { _id: -1 }, limit: limit, skip: skip }, (err, res) => {
//                 if (err) { 
//                     callback(err);
//                 } else { 
//                     var onlinePro = [];
//                     Async.forEach(res, function (item, callbackloop) {
//                         var cons = { status: { "$nin": [1, 2, 10, 11, 12] }, providerId: item._id.toString() };
//                         bookings.bookingCount(cons, (err, count) => {
//                             if (err) { 
//                                 callback(err);
//                             } else {
//                                 var fname = (typeof item.firstName == 'undefined' || item.firstName == null) ? "" : item.firstName;
//                                 var lname = (typeof item.lasrName == 'undefined' || item.lasrName == null) ? "" : item.lasrName;
//                                 var name = fname + " " + lname;
//                                 var obj = { items: item.phone };
//                                 var arrayFound = obj.items.myFind({ "isCurrentlyActive": true });
//                                 onlinePro.push({
//                                     '_id': item._id.toString(),
//                                     'latitude': item.location.latitude,
//                                     'longitude': item.location.longitude,
//                                     'image': item.profilePic,
//                                     'status': item.status,
//                                     'email': item.email,
//                                     'lastActive': item.lastActive,
//                                     'lastOnline': moment().unix() - item.lastActive,
//                                     'serverTime': moment().unix(),
//                                     'name': name,
//                                     'batteryPercentage': item.mobileDevices.batteryPercentage || '',
//                                     'appversion': item.mobileDevices.appversion,
//                                     'phone': arrayFound[0].countryCode + arrayFound[0].phone || "",
//                                     'deviceType': item.mobileDevices.deviceType || '',
//                                     'locationCheck': item.locationCheck == 0 ? 'off' : 'on',
//                                     'bookingCount': count,
//                                     'time': moment.unix(item.lastActive).fromNow()
//                                 });

//                                 callbackloop(null, onlinePro);
//                             }
//                         }); 

//                     }, function (loopErr) {
//                         callback(null, onlinePro);
//                     });

//                 }
//             });
//         },
//         function (callback) {
//             var con = { "status": 4, "loggedIn": true };
//             provider.readAllByLimit({ q: con, sort: { _id: -1 }, limit: limit, skip: skip }, (err, res) => {
//                 if (err) {
//                     callback(err);
//                 } else {
//                     var offlinePro = [];
//                     Async.forEach(res, function (item, callbackloop) {
//                         var cons = { status: { "$nin": [1, 2, 10, 11, 12] }, providerId: item._id.toString() };
//                         bookings.bookingCount(cons, (err, count) => {
//                             if (err) { 
//                                 callback(err);
//                             } else {
//                                 var fname = (typeof item.firstName == 'undefined' || item.firstName == null) ? "" : item.firstName;
//                                 var lname = (typeof item.lasrName == 'undefined' || item.lasrName == null) ? "" : item.lasrName;
//                                 var name = fname + " " + lname;
//                                 var obj = { items: item.phone };
//                                 var arrayFound = obj.items.myFind({ "isCurrentlyActive": true });
//                                 offlinePro.push({
//                                     '_id': item._id.toString(),
//                                     'latitude': item.location.latitude,
//                                     'longitude': item.location.longitude,
//                                     'image': item.profilePic,
//                                     'status': item.status,
//                                     'email': item.email,
//                                     'lastActive': item.lastActive,
//                                     'lastOnline': moment().unix() - item.lastActive,
//                                     'serverTime': moment().unix(),
//                                     'name': name,
//                                     'batteryPercentage': item.mobileDevices.batteryPercentage || '',
//                                     'appversion': item.mobileDevices.appversion,
//                                     'phone': arrayFound[0].countryCode + arrayFound[0].phone || "",
//                                     'deviceType': item.mobileDevices.deviceType || '',
//                                     'locationCheck': item.locationCheck == 0 ? 'off' : 'on',
//                                     'bookingCount': count,

//                                 });
//                                 callbackloop(null, offlinePro);
//                             }
//                         }); 

//                     }, function (loopErr) {
//                         callback(null, offlinePro);
//                     });
//                 }
//             });
//         },
//         function (callback) {
//             //{'$or' : [{'booked' : 1}, {'status' : 5}]}
//             var con = { bookingStatus: { "$nin": [0] } };
//             provider.readAllByLimit({ q: con, sort: { _id: -1 }, limit: limit, skip: skip }, (err, res) => {
//                 if (err) {
//                     callback(err);
//                 } else {
//                     var busyPro = [];
//                     Async.forEach(res, function (item, callbackloop) {
//                         var cons = { status: { "$nin": [1, 2, 10, 11, 12] }, providerId: item._id.toString() };
//                         bookings.bookingCount(cons, (err, count) => {
//                             if (err) { 
//                                 callback(err);
//                             } else {
//                                 var fname = (typeof item.firstName == 'undefined' || item.firstName == null) ? "" : item.firstName;
//                                 var lname = (typeof item.lasrName == 'undefined' || item.lasrName == null) ? "" : item.lasrName;
//                                 var name = fname + " " + lname;
//                                 var obj = { items: item.phone };
//                                 var arrayFound = obj.items.myFind({ "isCurrentlyActive": true });
//                                 // busyPro.push({
//                                 //     '_id': item._id.toString(),
//                                 //     'latitude': item.location.latitude,
//                                 //     'longitude': item.location.longitude,
//                                 //     'image': item.profilePic,
//                                 //     'status': item.status,
//                                 //     'email': item.email,
//                                 //     'lastActive': item.lastActive,
//                                 //     'lastOnline': moment().unix() - item.lastActive,
//                                 //     'serverTime': moment().unix(),
//                                 //     'name': name,
//                                 //     'batteryPercentage': item.mobileDevices.batteryPercentage || '',
//                                 //     'appversion': item.mobileDevices.appversion,
//                                 //     'phone': arrayFound[0].countryCode + arrayFound[0].phone || "",
//                                 //     'deviceType': item.mobileDevices.deviceType || '',
//                                 //     'locationCheck': item.locationCheck == 0 ? 'off' : 'on',
//                                 //     'bookingCount': count
//                                 // });
//                                 callbackloop(null, busyPro);
//                             }
//                         }); 

//                     }, function (loopErr) {
//                         callback(null, busyPro);
//                     });
//                 }
//             });
//         },
//         function (callback) {
//             var con = { "loggedIn": false };
//             provider.readAllByLimit({ q: con, sort: { _id: -1 }, limit: limit, skip: skip }, (err, res) => {
//                 if (err) {
//                     callback(err);
//                 } else {
//                     var logoutPro = [];
//                     Async.forEach(res, function (item, callbackloop) {
//                         var cons = { status: { "$nin": [1, 2, 10, 11, 12] }, providerId: item._id.toString() };
//                         bookings.bookingCount(cons, (err, count) => {
//                             if (err) { 
//                                 callback(err);
//                             } else {
//                                 var fname = (typeof item.firstName == 'undefined' || item.firstName == null) ? "" : item.firstName;
//                                 var lname = (typeof item.lasrName == 'undefined' || item.lasrName == null) ? "" : item.lasrName;
//                                 var name = fname + " " + lname;
//                                 var obj = { items: item.phone };
//                                 var arrayFound = obj.items.myFind({ "isCurrentlyActive": true });
//                                 logoutPro.push({
//                                     '_id': item._id.toString(),
//                                     'latitude': item.location.latitude,
//                                     'longitude': item.location.longitude,
//                                     'image': item.profilePic,
//                                     'status': item.status,
//                                     'email': item.email,
//                                     'lastActive': item.lastActive,
//                                     'lastOnline': moment().unix() - item.lastActive,
//                                     'serverTime': moment().unix(),
//                                     'name': name,
//                                     'batteryPercentage': item.mobileDevices.batteryPercentage || '',
//                                     'appversion': item.mobileDevices.appversion,
//                                     'phone': arrayFound[0].countryCode + arrayFound[0].phone || "",
//                                     'deviceType': item.mobileDevices.deviceType || '',
//                                     'locationCheck': item.locationCheck == 0 ? 'off' : 'on',
//                                     'bookingCount': count
//                                 });
//                                 callbackloop(null, logoutPro);
//                             }
//                         }); 

//                     }, function (loopErr) {
//                         callback(null, logoutPro);
//                     });
//                 }
//             });
//         },
//         function (callback) {
//             var con = { "status": 7, "loggedIn": true };
//             provider.readAllByLimit({ q: con, sort: { _id: -1 }, limit: limit, skip: skip }, (err, res) => {
//                 if (err) {
//                     callback(err);
//                 } else {
//                     var logoutPro = [];
//                     Async.forEach(res, function (item, callbackloop) {
//                         var cons = { status: { "$nin": [1, 2, 10, 11, 12] }, providerId: item._id.toString() };
//                         bookings.bookingCount(cons, (err, count) => {
//                             if (err) { 
//                                 callback(err);
//                             } else {
//                                 var fname = (typeof item.firstName == 'undefined' || item.firstName == null) ? "" : item.firstName;
//                                 var lname = (typeof item.lasrName == 'undefined' || item.lasrName == null) ? "" : item.lasrName;
//                                 var name = fname + " " + lname;
//                                 var obj = { items: item.phone };
//                                 var arrayFound = obj.items.myFind({ "isCurrentlyActive": true });
//                                 logoutPro.push({
//                                     '_id': item._id.toString(),
//                                     'latitude': item.location.latitude,
//                                     'longitude': item.location.longitude,
//                                     'image': item.profilePic,
//                                     'status': item.status,
//                                     'email': item.email,
//                                     'lastActive': item.lastActive,
//                                     'lastOnline': moment().unix() - item.lastActive,
//                                     'serverTime': moment().unix(),
//                                     'name': name,
//                                     'batteryPercentage': item.mobileDevices.batteryPercentage || '',
//                                     'appversion': item.mobileDevices.appversion,
//                                     'phone': arrayFound[0].countryCode + arrayFound[0].phone || "",
//                                     'deviceType': item.mobileDevices.deviceType || '',
//                                     'locationCheck': item.locationCheck == 0 ? 'off' : 'on',
//                                     'bookingCount': count
//                                 });
//                                 callbackloop(null, logoutPro);
//                             }
//                         }); 

//                     }, function (loopErr) {
//                         callback(null, logoutPro);
//                     });
//                 }
//             });
//         },
//         function (callback) {//ONLINE COUNT
//             var con = { "status": 3, "loggedIn": true };
//             provider.count(con, (err, res) => {
//                 if (err) { 
//                     callback(err);
//                 } else { 
//                     callback(null, res);
//                 }
//             });
//         },
//         function (callback) {//OFFLINE COUNT
//             var con = { "status": 4, "loggedIn": true };
//             provider.count(con, (err, res) => {
//                 if (err) { 
//                     callback(err);
//                 } else {
//                     callback(null, res);
//                 }
//             });
//         },
//         function (callback) {//BUSY COUNT
//             var con = { bookingStatus: { "$nin": [0] } };
//             provider.count(con, (err, res) => {
//                 if (err) { 
//                     callback(err);
//                 } else {
//                     callback(null, res);
//                 }
//             });
//         },
//         function (callback) {//LOGOUT COUNT
//             var con = { "loggedIn": false };
//             provider.count(con, (err, res) => {
//                 if (err) { 
//                     callback(err);
//                 } else {
//                     callback(null, res);
//                 }
//             });
//         },
//         function (callback) {//INACTIVE COUNT
//             var con = { "status": 7, "loggedIn": true };
//             provider.count(con, (err, res) => {
//                 if (err) { 
//                     callback(err);
//                 } else {
//                     callback(null, res);
//                 }
//             });
//         }
//     ], function (err, result) {
//         if (err)
//             return reply({ message: error['genericErrMsg']['500'][req.headers.lan] }).code(500);
//         var data = {
//             onlinePro: result[0],
//             offlinePro: result[1],
//             busyPro: [],
//             logoutPro: result[3] || 0,
//             inactivePro: result[4] || 0,
//             onlineProCount: result[5],
//             offlineProCount: result[6],
//             busyProCount: 0,
//             logoutProCount: result[8],
//             inactiveProCount: result[9]
//         }
//         webSocket.publish('provider/' + req.auth.credentials._id, data, { qos: 2 }, function (mqttErr, mqttRes) {
//             if (mqttRes)
//                 return reply({ message: error['disProvider']['200'][req.headers.lan], data: data }).code(200);
//         });
//     });
// };

// const responseCode = {
//     status: {
//         // 500: { message: error['genericErrMsg']['500'][error['lang']] },
//         // 200: {
//         //     message: error['getProvider']['200'][error['lang']],
//         //     data: joi.any()
//         // },
//     }

// }//swagger response code

// module.exports = {
//     params,
//     handler,
//     responseCode
// };



'use strict';

const joi = require('joi');
const Async = require('async');
const error = require('../error');
const moment = require('moment');//date-time
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;
const provider = require('../../../../models/provider');
const bookings = require('../../../../models/bookings');
const webSocket = require('../../../../models/webSocket');


const params = joi.object({
    searchStr: joi.string().description('search string').error(new Error('search must be number')),
    pageIndex: joi.number().description('Page Index').error(new Error('Page Index must be number')),
}).required();

Array.prototype.myFind = function (obj) {
    return this.filter(function (item) {
        for (var prop in obj)
            if (!(prop in item) || obj[prop] !== item[prop])
                return false;
        return true;
    });
};
const handler = (req, reply) => {

    let limit = 20;
    let skip = 20 * req.params.pageIndex;
    Async.series([
        function (callback) {
            var con = {};
            if (req.params.searchStr != "0") {
                con = {
                    "status": 3, "loggedIn": true, "deactive": { "$ne": true },
                    "$or": [{ 'firstName': { '$regex': req.params.searchStr, '$options': 'i' } },
                    { 'lastName': { '$regex': req.params.searchStr, '$options': 'i' } }]
                };
                if (req.user && req.user.city != '0') {
                    con = {
                        "status": 3, "loggedIn": true, "deactive": { "$ne": true }, 'cityId': req.user.city,
                        "$or": [{ 'firstName': { '$regex': req.params.searchStr, '$options': 'i' } },
                        { 'lastName': { '$regex': req.params.searchStr, '$options': 'i' } }]
                    };
                }
            } else {
                con = { "status": 3, "loggedIn": true, "deactive": { "$ne": true } };
                if (req.user && req.user.city != '0') {
                    con = { "status": 3, "loggedIn": true, 'cityId': req.user.city, "deactive": { "$ne": true } };
                }
            }

            provider.readAllByLimit({ q: con, sort: { _id: -1 }, limit: limit, skip: skip }, (err, res) => {
                if (err) {
                    callback(err);
                } else {
                    var onlinePro = [];
                    Async.forEach(res, function (item, callbackloop) {
                        var fname = (typeof item.firstName == 'undefined' || item.firstName == null) ? "" : item.firstName;
                        var lname = (typeof item.lasrName == 'undefined' || item.lasrName == null) ? "" : item.lasrName;
                        var name = fname + " " + lname;
                        var obj = { items: item.phone };
                        var arrayFound = obj.items.myFind({ "isCurrentlyActive": true });
                        onlinePro.push({
                            '_id': item._id.toString(),
                            'latitude': item.location.latitude,
                            'longitude': item.location.longitude,
                            'image': item.profilePic,
                            'status': item.status,
                            'email': item.email,
                            'lastActive': item.lastActive,
                            'lastOnline': moment().unix() - item.lastActive,
                            'serverTime': moment().unix(),
                            'name': name,
                            'batteryPercentage': item.mobileDevices ? item.mobileDevices.batteryPercentage : '',
                            'appversion': item.mobileDevices ? item.mobileDevices.appVersion : "",
                            'phone': arrayFound[0].countryCode + arrayFound[0].phone || "",
                            'deviceType': item.mobileDevices ? item.mobileDevices.deviceType : '',
                            'locationCheck': item.locationCheck == 0 ? 'off' : 'on',
                            'bookingCount': item.totalOnGoingBooking || 0,
                            'review': item.reviewCountNum || 0,
                            'averageRating': item.averageRating || 0,
                            'time': moment.unix(item.lastActive).fromNow()
                        });

                        callbackloop(null, onlinePro);
                    }, function (loopErr) {
                        callback(null, onlinePro);
                    });

                }
            });
        },
        function (callback) {
            var con = {};
            if (req.params.searchStr != "0") {
                con = {
                    "status": 4, "loggedIn": true, "deactive": { "$ne": true },
                    "$or": [{ 'firstName': { '$regex': req.params.searchStr, '$options': 'i' } },
                    { 'lastName': { '$regex': req.params.searchStr, '$options': 'i' } }]
                };
                if (req.user && req.user.city != '0') {
                    con = {
                        "status": 4, "loggedIn": true, 'cityId': req.user.city, "deactive": { "$ne": true },
                        "$or": [{ 'firstName': { '$regex': req.params.searchStr, '$options': 'i' } },
                        { 'lastName': { '$regex': req.params.searchStr, '$options': 'i' } }]
                    };
                }
            } else {
                con = { "status": 4, "loggedIn": true, "deactive": { "$ne": true } };
                if (req.user && req.user.city != '0') {
                    con = { "status": 4, "loggedIn": true, 'cityId': req.user.city, "deactive": { "$ne": true } };
                }
            }
            provider.readAllByLimit({ q: con, sort: { _id: -1 }, limit: limit, skip: skip }, (err, res) => {
                if (err) {
                    callback(err);
                } else {
                    var offlinePro = [];
                    Async.forEach(res, function (item, callbackloop) {
                        var fname = (typeof item.firstName == 'undefined' || item.firstName == null) ? "" : item.firstName;
                        var lname = (typeof item.lasrName == 'undefined' || item.lasrName == null) ? "" : item.lasrName;
                        var name = fname + " " + lname;
                        var obj = { items: item.phone };
                        var arrayFound = obj.items.myFind({ "isCurrentlyActive": true });
                        offlinePro.push({
                            '_id': item._id.toString(),
                            'latitude': item.location.latitude,
                            'longitude': item.location.longitude,
                            'image': item.profilePic,
                            'status': item.status,
                            'email': item.email,
                            'lastActive': item.lastActive,
                            'lastOnline': moment().unix() - item.lastActive,
                            'serverTime': moment().unix(),
                            'name': name,
                            'batteryPercentage': item.mobileDevices ? item.mobileDevices.batteryPercentage : '',
                            'appversion': item.mobileDevices ? item.mobileDevices.appVersion : "",
                            'phone': arrayFound[0].countryCode + arrayFound[0].phone || "",
                            'deviceType': item.mobileDevices ? item.mobileDevices.deviceType : '',
                            'locationCheck': item.locationCheck == 0 ? 'off' : 'on',
                            'bookingCount': item.totalOnGoingBooking || 0,
                            'review': item.reviewCountNum || 0,
                            'averageRating': item.averageRating || 0,
                            'time': moment.unix(item.lastActive).fromNow()

                        });
                        callbackloop(null, offlinePro);

                    }, function (loopErr) {
                        callback(null, offlinePro);
                    });
                }
            });
        },

        function (callback) {
            var con = {};
            if (req.params.searchStr != "0") {
                con = {
                    "loggedIn": { "$ne": true }, 'status': { "$in": [2, 3, 4, 7] }, "deactive": { "$ne": true },
                    "$or": [{ 'firstName': { '$regex': req.params.searchStr, '$options': 'i' } },
                    { 'lastName': { '$regex': req.params.searchStr, '$options': 'i' } }]
                };
                if (req.user && req.user.city != '0') {
                    con = {
                        "loggedIn": { "$ne": true }, 'status': { "$in": [2, 3, 4, 7] }, 'cityId': req.user.city, "deactive": { "$ne": true },
                        "$or": [{ 'firstName': { '$regex': req.params.searchStr, '$options': 'i' } },
                        { 'lastName': { '$regex': req.params.searchStr, '$options': 'i' } }]
                    };
                }
            } else {
                con = { "loggedIn": { "$ne": true }, 'status': { "$in": [2, 3, 4, 7] }, "deactive": { "$ne": true } };
                if (req.user && req.user.city != '0') {
                    con = { "loggedIn": { "$ne": true }, 'status': { "$in": [2, 3, 4, 7] }, 'cityId': req.user.city, "deactive": { "$ne": true } };
                }
            }

            provider.readAllByLimit({ q: con, sort: { _id: -1 }, limit: limit, skip: skip }, (err, res) => {
                if (err) {
                    callback(err);
                } else {
                    var logoutPro = [];
                    Async.forEach(res, function (item, callbackloop) {
                        var fname = (typeof item.firstName == 'undefined' || item.firstName == null) ? "" : item.firstName;
                        var lname = (typeof item.lasrName == 'undefined' || item.lasrName == null) ? "" : item.lasrName;
                        var name = fname + " " + lname;
                        var obj = { items: item.phone };
                        var arrayFound = obj.items.myFind({ "isCurrentlyActive": true });
                        logoutPro.push({
                            '_id': item._id.toString(),
                            'latitude': item.location.latitude,
                            'longitude': item.location.longitude,
                            'image': item.profilePic,
                            'status': 8,
                            'email': item.email,
                            'lastActive': item.lastActive,
                            'lastOnline': moment().unix() - item.lastActive,
                            'serverTime': moment().unix(),
                            'name': name,
                            'batteryPercentage': item.mobileDevices ? item.mobileDevices.batteryPercentage : '',
                            'appversion': item.mobileDevices ? item.mobileDevices.appVersion : '',
                            'phone': arrayFound[0].countryCode + arrayFound[0].phone || "",
                            'deviceType': item.mobileDevices ? item.mobileDevices.deviceType : '',
                            'locationCheck': item.locationCheck == 0 ? 'off' : 'on',
                            'bookingCount': item.totalOnGoingBooking || 0,
                            'review': item.reviewCountNum || 0,
                            'averageRating': item.averageRating || 0,
                            'time': moment.unix(item.lastActive).fromNow()
                        });
                        callbackloop(null, logoutPro);

                    }, function (loopErr) {
                        callback(null, logoutPro);
                    });
                }
            });
        },
        function (callback) {
            var con = {};
            if (req.params.searchStr != "0") {
                con = {
                    "status": 7, "loggedIn": true, "deactive": { "$ne": true },
                    "$or": [{ 'firstName': { '$regex': req.params.searchStr, '$options': 'i' } },
                    { 'lastName': { '$regex': req.params.searchStr, '$options': 'i' } }]
                };
                if (req.user && req.user.city != '0') {
                    con = {
                        "status": 7, "loggedIn": true, 'cityId': req.user.city, "deactive": { "$ne": true },
                        "$or": [{ 'firstName': { '$regex': req.params.searchStr, '$options': 'i' } },
                        { 'lastName': { '$regex': req.params.searchStr, '$options': 'i' } }]
                    };
                }
            } else {
                con = { "status": 7, "loggedIn": true, "deactive": { "$ne": true } };
                if (req.user && req.user.city != '0') {
                    con = { "status": 7, "loggedIn": true, 'cityId': req.user.city, "deactive": { "$ne": true } };
                }
            }
            provider.readAllByLimit({ q: con, sort: { _id: -1 }, limit: limit, skip: skip }, (err, res) => {
                if (err) {
                    callback(err);
                } else {
                    var logoutPro = [];
                    Async.forEach(res, function (item, callbackloop) {
                        var fname = (typeof item.firstName == 'undefined' || item.firstName == null) ? "" : item.firstName;
                        var lname = (typeof item.lasrName == 'undefined' || item.lasrName == null) ? "" : item.lasrName;
                        var name = fname + " " + lname;
                        var obj = { items: item.phone };
                        var arrayFound = obj.items.myFind({ "isCurrentlyActive": true });
                        logoutPro.push({
                            '_id': item._id.toString(),
                            'latitude': item.location.latitude,
                            'longitude': item.location.longitude,
                            'image': item.profilePic,
                            'status': item.status,
                            'email': item.email,
                            'lastActive': item.lastActive,
                            'lastOnline': moment().unix() - item.lastActive,
                            'serverTime': moment().unix(),
                            'name': name,
                            'batteryPercentage': item.mobileDevices ? item.mobileDevices.batteryPercentage : '',
                            'appversion': item.mobileDevices ? item.mobileDevices.appVersion : "",
                            'phone': arrayFound[0].countryCode + arrayFound[0].phone || "",
                            'deviceType': item.mobileDevices ? item.mobileDevices.deviceType : '',
                            'locationCheck': item.locationCheck == 0 ? 'off' : 'on',
                            'bookingCount': item.totalOnGoingBooking || 0,
                            'review': item.reviewCountNum || 0,
                            'averageRating': item.averageRating || 0,
                            'time': moment.unix(item.lastActive).fromNow()
                        });
                        callbackloop(null, logoutPro);

                    }, function (loopErr) {
                        callback(null, logoutPro);
                    });
                }
            });
        },
        function (callback) {//ONLINE COUNT
            var con = {};
            if (req.params.searchStr != "0") {
                con = {
                    "status": 3, "loggedIn": true, "deactive": { "$ne": true },
                    "$or": [{ 'firstName': { '$regex': req.params.searchStr, '$options': 'i' } },
                    { 'lastName': { '$regex': req.params.searchStr, '$options': 'i' } }]
                };
                if (req.user && req.user.city != '0') {
                    con = {
                        "status": 3, "loggedIn": true, 'cityId': req.user.city, "deactive": { "$ne": true },
                        "$or": [{ 'firstName': { '$regex': req.params.searchStr, '$options': 'i' } },
                        { 'lastName': { '$regex': req.params.searchStr, '$options': 'i' } }]
                    };
                }
            } else {
                con = { "status": 3, "loggedIn": true, "deactive": { "$ne": true } };
                if (req.user && req.user.city != '0') {
                    con = { "status": 3, "loggedIn": true, 'cityId': req.user.city, "deactive": { "$ne": true } };
                }
            }
            provider.count(con, (err, res) => {
                if (err) {
                    callback(err);
                } else {
                    callback(null, res);
                }
            });
        },
        function (callback) {//OFFLINE COUNT
            var con = {};
            if (req.params.searchStr != "0") {
                con = {
                    "status": 4, "loggedIn": true, "deactive": { "$ne": true },
                    "$or": [{ 'firstName': { '$regex': req.params.searchStr, '$options': 'i' } },
                    { 'lastName': { '$regex': req.params.searchStr, '$options': 'i' } }]
                };
                if (req.user && req.user.city != '0') {
                    con = {
                        "status": 4, "loggedIn": true, 'cityId': req.user.city, "deactive": { "$ne": true },
                        "$or": [{ 'firstName': { '$regex': req.params.searchStr, '$options': 'i' } },
                        { 'lastName': { '$regex': req.params.searchStr, '$options': 'i' } }]
                    };
                }
            } else {
                con = { "status": 4, "loggedIn": true, "deactive": { "$ne": true } };
                if (req.user && req.user.city != '0') {
                    con = { "status": 4, "loggedIn": true, 'cityId': req.user.city, "deactive": { "$ne": true } };
                }
            }
            provider.count(con, (err, res) => {
                if (err) {
                    callback(err);
                } else {
                    callback(null, res);
                }
            });
        },

        function (callback) {//LOGOUT COUNT
            var con = {};
            if (req.params.searchStr != "0") {
                con = {
                    "loggedIn": { "$ne": true }, 'status': { "$in": [2, 3, 4, 7] }, "deactive": { "$ne": true },
                    "$or": [{ 'firstName': { '$regex': req.params.searchStr, '$options': 'i' } },
                    { 'lastName': { '$regex': req.params.searchStr, '$options': 'i' } }]
                };
                if (req.user && req.user.city != '0') {
                    con = {
                        "loggedIn": { "$ne": true }, 'status': { "$in": [2, 3, 4, 7] }, "deactive": { "$ne": true }, 'cityId': req.user.city,
                        "$or": [{ 'firstName': { '$regex': req.params.searchStr, '$options': 'i' } },
                        { 'lastName': { '$regex': req.params.searchStr, '$options': 'i' } }]
                    };
                }
            } else {
                con = { "loggedIn": { "$ne": true }, 'status': { "$in": [2, 3, 4, 7] }, "deactive": { "$ne": true } };
                if (req.user && req.user.city != '0') {
                    con = { "loggedIn": { "$ne": true }, 'status': { "$in": [2, 3, 4, 7] }, "deactive": { "$ne": true }, 'cityId': req.user.city };
                }
            }
            provider.count(con, (err, res) => {
                if (err) {
                    callback(err);
                } else {
                    callback(null, res);
                }
            });
        },
        function (callback) {//INACTIVE COUNT
            var con = {};
            if (req.params.searchStr != "0") {
                con = {
                    "status": 7, "loggedIn": true, "deactive": { "$ne": true },
                    "$or": [{ 'firstName': { '$regex': req.params.searchStr, '$options': 'i' } },
                    { 'lastName': { '$regex': req.params.searchStr, '$options': 'i' } }]
                };
                if (req.user && req.user.city != '0') {
                    con = {
                        "status": 7, "loggedIn": true, 'cityId': req.user.city, "deactive": { "$ne": true },
                        "$or": [{ 'firstName': { '$regex': req.params.searchStr, '$options': 'i' } },
                        { 'lastName': { '$regex': req.params.searchStr, '$options': 'i' } }]
                    };
                }
            } else {
                con = { "status": 7, "loggedIn": true, "deactive": { "$ne": true } };
                if (req.user && req.user.city != '0') {
                    con = { "status": 7, "loggedIn": true, 'cityId': req.user.city, "deactive": { "$ne": true } };
                }
            }
            provider.count(con, (err, res) => {
                if (err) {
                    callback(err);
                } else {
                    callback(null, res);
                }
            });
        }
    ], function (err, result) {
        if (err)
            return reply({ message: error['genericErrMsg']['500'][req.headers.lan] }).code(500);
        var data = {
            onlinePro: result[0],
            offlinePro: result[1],
            logoutPro: result[2] || 0,
            inactivePro: result[3] || 0,
            onlineProCount: result[4],
            offlineProCount: result[5],
            logoutProCount: result[6],
            inactiveProCount: result[7]
        }
        return reply({ message: error['disProvider']['200'][req.headers.lan], data: data }).code(200);
    });
};

const responseCode = {
    status: {
        // 500: { message: error['genericErrMsg']['500'][error['lang']] },
        // 200: {
        //     message: error['getProvider']['200'][error['lang']],
        //     data: joi.any()
        // },
    }

}//swagger response code

module.exports = {
    params,
    handler,
    responseCode
};