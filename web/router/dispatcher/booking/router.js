'use strict';

const entity = '/dispatcher';
const error = require('../error');
const getByProviderId = require('./getByProviderId');
const headerValidator = require('../../../middleware/validator');

module.exports = [
    /**
     * @name GET /booking/{providerId}/{pageIndex}
     */

    {
        method: 'GET',
        path: entity + '/booking/{providerId}/{pageIndex}',
        handler: getByProviderId.handler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['getBookingByProviderId'],
            notes: 'dispatcher',
            auth: 'dispatcherJWT',
            // response: getByProviderId.responseCode,
            validate: {
                params: getByProviderId.params,
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },



];