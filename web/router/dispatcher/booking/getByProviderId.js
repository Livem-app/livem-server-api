
'use strict';

const joi = require('joi');
const Async = require('async');
const error = require('../error');
const moment = require('moment');//date-time
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;
const provider = require('../../../../models/provider');
const customer = require('../../../../models/customer');
const webSocket = require('../../../../models/webSocket');
const assignedBookings = require('../../../../models/assignedBookings');


const params = joi.object({
    providerId: joi.string().description('providerId').error(new Error('providerId is missing')),
    pageIndex: joi.number().description('Page Index').error(new Error('Page Index must be number')),
}).required();

Array.prototype.myPhone = function (obj) {
    return this.filter(function (item) {
        for (var prop in obj)
            if (!(prop in item) || obj[prop] !== item[prop])
                return false;
        return true;
    });
};
const handler = (req, reply) => {
    var condition = { providerId: req.params.providerId };//,'status': {$in: [0, 2, 5, 6, 21, 22, 15, 16]}
    var limit = 20;
    var skip = 20 * req.params.pageIndex;
    var dispatchedAppt = [];
    Async.series([
        function (callback) {
            assignedBookings.readAllByLimit({ q: condition, sort: { _id: -1 }, limit: limit, skip: skip }, (err, res) => {
                if (err) {
                    callback(err);
                } else {

                    Async.forEach(res, function (booking, callbackloop) {
                        var con1 = { _id: ObjectID(booking.providerId) };
                        provider.read(con1, (err, provider) => {
                            var con2 = { _id: ObjectID(booking.slaveId) };
                            customer.read(con2, (err, slave) => {
                                var obj = { items: slave.phone };
                                var arrayFound = obj.items.myFind({ "isCurrentlyActive": true });
                                var customer = {
                                    name: slave.firstName + ' ' + slave.lastName || '',
                                    profilePic: slave.profilePic || '',
                                    email: slave.email || '',
                                    phone: arrayFound[0].countryCode + arrayFound[0].phone || "",

                                }
                                var obj1 = { items: provider.phone };
                                var arrayFound1 = obj1.items.myFind({ "isCurrentlyActive": true });
                                var providers = {
                                    name: provider.firstName + '' + provider.lastName,
                                    profilePic: provider.profilePic || '',
                                    email: provider.email || '',
                                    phone: arrayFound1[0].countryCode + arrayFound1[0].phone || "",
                                    currencySymbol : provider.currencySymbol || '',
                                    currency : provider.currency || '',

                                }
                                var Address = {
                                    addLine1: booking.addLine1,
                                    addLine2: booking.addLine2,
                                    city: booking.city,
                                    state: booking.state,
                                    country: booking.country,
                                    pincode: booking.pincode,
                                    placeId: booking.placeId,
                                    latitude: booking.latitude,
                                    longitude: booking.longitude
                                } 
                                var appt = {
                                    'bookingId': booking.bookingId,
                                    'status': booking.status,
                                    'statusMsg':"",
                                    'customer_notes': booking.customer_notes || '',
                                    'customer': customer,
                                    'provider': providers,
                                    'time': booking.bookingRequestedFor,
                                    'services': booking.service.name || [],
                                    'category': booking.category.name || '',
                                    'cancelAmount': booking.cancel_amount || 0,
                                    'bookingType': booking.bookingType,
                                    'totalAmount': booking.total || 0,
                                    'address': Address,
                                    'currencySymbol':booking.currencySymbol,
                                    'currency':booking.currency,
                                    'accounting':booking.accounting,
                                    'service':booking.service,

                                };
                                var jobStatus = {};
                                Async.forEach(booking.jobStatusLogs, (e, cb) => {
                                    switch (e.status) {
                                        case 1:
                                            jobStatus.requestedTime = e.timeStamp;
                                            break;
                                        case 2:
                                            jobStatus.receivedTime = e.timeStamp;
                                            break;
                                        case 3:
                                            jobStatus.acceptedTime = e.timeStamp;
                                            break;
                                        case 4:
                                            jobStatus.rejectedTime = e.timeStamp;
                                            break;
                                        case 5:
                                            jobStatus.expiredTime = e.timeStamp;
                                            break;
                                        case 6:
                                            jobStatus.onthewayTime = e.timeStamp;
                                            break;
                                        case 7:
                                            jobStatus.arrivedTime = e.timeStamp;
                                            break;
                                        case 8:
                                            jobStatus.startedTime = e.timeStamp;
                                            break;
                                        case 9:
                                            jobStatus.completedTime = e.timeStamp;
                                            break;
                                        case 10:
                                            jobStatus.raiseInvoiceTime = e.timeStamp;
                                            break;

                                        default:
                                            break;

                                    }
                                    cb(null);
                                }, (err, re) => {
                                    appt.jobStatusLogs = jobStatus;
                                });
                                dispatchedAppt.push(appt);
                                callbackloop(null, dispatchedAppt);
                            });
                        });

                    }, function (loopErr) {
                        callback(null, dispatchedAppt);
                    });

                }
            });
        }
    ], function (err, result) {
        if (err)
            return reply({ message: error['genericErrMsg']['500'][req.headers.lan] }).code(500);
        return reply({ message: error['disProvider']['200'][req.headers.lan], data: dispatchedAppt }).code(200);
    });
};

const responseCode = {
    status: {
        // 500: { message: error['genericErrMsg']['500'][error['lang']] },
        // 200: {
        //     message: error['getProvider']['200'][error['lang']],
        //     data: joi.any()
        // },
    }

}//swagger response code

module.exports = {
    params,
    handler,
    responseCode
};