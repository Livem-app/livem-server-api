'use strict';

const booking = require('./booking');

const provider = require('./provider');

const onBoarding = require('./onBoarding');

const accessToken =require('./accessToken');

module.exports = [].concat(
    booking,
    provider,
    onBoarding,
    accessToken
);