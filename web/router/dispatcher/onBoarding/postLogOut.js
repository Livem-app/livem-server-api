'use strict';

const joi = require('joi');
const md5 = require('md5');;
const error = require('../error');
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;
const dispatchers = require('../../../../models/dispatchers');


const handler = (req, reply) => {

    const logOut = () => {
        return new Promise((resolve, reject) => {
            let updateQuery = {
                query: { _id: new ObjectID(req.auth.credentials._id) },
                data: { $set: { loggedIn: false, accessCode: '' } }
            };
            dispatchers.findOneAndUpdate(updateQuery, (err, res) => {
                return err ? reject({ message: error['genericErrMsg']['500'][req.headers.lan], code: 500 })
                    : resolve(res);
            });
        });
    };

    logOut()
        .then(data => {
            return reply({ message: error['postLogOut']['200'][req.headers.lan] }).code(200);
        }).catch(e => {
            logger.error("Dispatcher LogOut API error =>", e)
            return reply({ message: e.message }).code(e.code);
        });

};

const responseCode = {
    status: {
        500: { message: error['genericErrMsg']['500'][error['lang']] },
        200: { message: error['postLogOut']['200'][error['lang']] }
    }

}//swagger response code


module.exports = {
    handler,
    responseCode
};