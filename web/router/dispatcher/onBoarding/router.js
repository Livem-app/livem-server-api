'use strict';

const entity = '/dispatcher';
const error = require('../error');
const postSignIn = require('./postSignIn');
const postLogOut = require('./postLogOut');
const get = require('../provider/get');
const headerValidator = require('../../../middleware/validator');

module.exports = [
   
    /**
     * @name POST /dispatcher/signIn
     */

    {
        method: 'POST',
        path: entity + '/signIn',
        handler: postSignIn.handler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['postRegisterUser'],
            notes: 'dispatcher',
            auth: false,
            response: postSignIn.responseCode,
            validate: {
                headers: headerValidator.headerLanValidator,
                payload: postSignIn.payload,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },

    /**
     * @name POST /dispatcher/logout
     */
    {
        method: 'POST',
        path: entity + '/logout',
        handler: postLogOut.handler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['postLogOut'],
            notes: 'dispatcher',
            auth: 'dispatcherJWT',
            response: postLogOut.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },

];