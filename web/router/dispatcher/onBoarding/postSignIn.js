'use strict';

const joi = require('joi');
const md5 = require('md5'); ;
const error = require('../error');
const logger = require('winston');
const appConfig = require('../../../../models/appConfig');
const Auth = require('../../../middleware/authentication'); 
const dispatchers = require('../../../../models/dispatchers');

const payload = joi.object({
    email: joi.string().email().required().description('email').error(new Error('Email is missing or incorrect')),
    password: joi.string().required().description('password'),
}).required();//payload of signin api


const handler = (req, reply) => {

    const getUser = () => {
        return new Promise((resolve, reject) => {
            let condition = { email: req.payload.email };
            dispatchers.read(condition, (err, res) => {
                if (err)
                    return reject({ message: error['genericErrMsg']['500'][req.headers.lan], code: 500 })
                if (res === null)
                    return reject({ message: error['postSignIn']['404'][req.headers.lan], code: 404 });
                else
                    resolve(res)
            });
        });
    };//get User by  email or phone number

    const checkUser = (data) => {
        return new Promise((resolve, reject) => {
            if (md5(req.payload.password) == data.password) {
                return resolve(data);
            } else {
                return reject({ message: error['postSignIn']['401'][req.headers.lan], code: 401 });
            }

        });
    }//check provider valid or not

    const jwtToken = (data) => {
        return new Promise((resolve, reject) => {
            appConfig.read((err, config) => {
                if (err)
                    reject({ message: error['genericErrMsg']['500'][req.headers.lan], code: 500 });
                else {
                    let authToken = Auth.SignJWT(
                        {
                            _id: data._id.toString(),
                            key: 'acc'
                        }, 'dispatcher', config.accessToken);//sign a new JWT
                    data.authToken = authToken;
                    resolve(data);
                }
            });
        });
    }//generate JWT token

    getUser()
        .then(checkUser)
        .then(jwtToken)
        .then(data => {
            let responseData = {
                token: data.authToken,
                id: data._id,
                email: data.email,
                name: data.name || '',
                cityName: data.city_name || ''
            };
            return reply({ message: error['postSignIn']['200'][req.headers.lan], data: responseData }).code(200);
        }).catch(e => {
            logger.error("Dispatcher Login API error =>", e)
            return reply({ message: e.message }).code(e.code);
        });

};

const responseCode = {
    status: {
        500: { message: error['genericErrMsg']['500'][error['lang']] },
        200: {
            message: error['postSignIn']['200'][error['lang']],
            data: joi.any()
        },
        404: { message: error['postSignIn']['404'][error['lang']] },
        401: { message: error['postSignIn']['401'][error['lang']] }
    }

}//swagger response code
 

module.exports = {
    payload,
    handler,
    responseCode
};