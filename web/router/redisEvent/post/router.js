
'use strict';

const entity = "webhooks";

const post = require('./post');

module.exports = [
    /**
    * 'Stripe webhook for handling an event ',
    */
    {
        method: 'POST',
        path: '/' + entity + '/redisEvent',
        handler: post.APIHandler,
        config: {
            tags: ['api', entity],
            description: 'Redis expire event ',
            notes: 'Redis expire event ',
            response: post.responseCode,
            validate: {
                payload: post.payload,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    }
]