'use strict';

const Joi = require('joi');
const logger = require('winston');
var async = require("async");
var moment = require('moment');
const ObjectID = require('mongodb').ObjectID;
const provider = require('../../../commonModels/provider');
const booking = require('../../../commonModels/booking');

// const client = redis.client;
// var geo = require('georedis').initialize(client);

const payloadValidator = Joi.object({
    pattern: Joi.any().required().description('pattern').error(new Error('pattern is missing')),
    channel: Joi.any().required().description('channel').error(new Error('channel is missing')),
    message: Joi.any().required().description('message').error(new Error('message is missing'))
}).required();

const APIHandler = (req, reply) => {
    redisEventListner(req.payload.pattern, req.payload.channel, req.payload.message);
    reply({ message: 'success for redisEnvent' }).code(200);
};

/**
 * 
 * @param {*} pattern 
 * @param {*} channel 
 * @param {*} message 
 */
function redisEventListner(pattern, channel, message) {
    switch (channel) {
        case '__keyevent@0__:hset':
            break;
        case '__keyevent@0__:del':
            break;
        case '__keyevent@0__:expired':
            var data = message.split("_");
            switch (data[0]) {
                case 'bid':
                    logger.silly("Booking Expired - Booking Id :", data[1]);
                    booking.expired(data[1]);
                    break;
                case 'presence':
                    logger.silly("Provider TimeOut - ProviderId :", data[1]);
                    provider.timeOut(data[1]);
                    break;
            }
            break;
    }
}



module.exports = { payloadValidator, APIHandler };