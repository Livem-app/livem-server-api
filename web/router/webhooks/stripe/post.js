'use strict';

const Joi = require('joi');
const logger = require('winston');
const Async = require('async');
const rabbitmqUtil = require('../../../../models/rabbitMq/stripeUtil');
const rabbitMq = require('../../../../models/rabbitMq');
const provider = require('../../../../models/customer');
const Timestamp = require('mongodb').Timestamp;
const moment = require('moment');
const payload = Joi.any();

const APIHandler = (req, reply) => {
    // provider.readAll({ "mobileDevices.deviceType": 2 }, (err, ress) => {
    //     Async.forEach(ress, function (res, callback1) {
    //         console.log("res", res._id);

    //         provider.findOneAndUpdate(res._id, {

    //             $set: {
    //                 "mobileDevices.deviceTypeText": "Android"
    //             }

    //         }, (err, res) => {
    //             callback1(null, res);
    //         })

    //     }, function (err, res) {

    //     });


    //     // console.log("new Timestamp()", Timestamp(res.createdDt), )
    // })

    let eventData = req.payload;
    rabbitmqUtil.InsertQueueStripeEvent(rabbitMq.getChannelStripeEvent(), rabbitMq.QueueStripeEvent, eventData, (err, doc) => {
    });
    return reply({ message: 'success' }).code(200);
};

const responseCode = {
    status: {
        200: { message: 'success' },
    }
}//swagger response code

module.exports = { payload, APIHandler, responseCode };