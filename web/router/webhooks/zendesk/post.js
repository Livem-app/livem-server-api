'use strict';

const Joi = require('joi');
const logger = require('winston');

const customer = require('../../../../models/customer');
const provider = require('../../../../models/provider');
const fcm = require('../../../../library/fcm');

const payload = Joi.any();

const APIHandler = (req, reply) => { 
    sendPush(req);
    return reply({ message: 'success' }).code(200);




};

const sendPush = (req) => {
    return new Promise((resolve, reject) => {
        customer.read({ zendeskId: parseFloat(req.payload.requisterId) }, (err, res) => {
            if (err) {
                return reject(err);
            } else if (res) {
                return resolve(res);
            } else {
                provider.read({ zendeskId: parseFloat(req.payload.requisterId) }, (err, res) => {
                    if (err) {
                        return reject(err);
                    } else if (res) {
                        return resolve(res);
                    }
                });
            }
        })
    }).then(data => {
        if (req.payload.requisterId) {

            let request = {
                fcmTopic: data.fcmTopic,
                action: 70,
                pushType: 5,
                title: "zendesk update",
                msg: "Zendesk- " + req.payload.title + " updated",
                data: req.payload,
                deviceType: 1
            }
            fcm.notifyFcmTpic(request, (e, r) => {
            }); 
        }
    }).catch(e => { 
    });
}
const responseCode = {
    status: {
        200: { message: 'success' },
    }
}//swagger response code

module.exports = { payload, APIHandler, responseCode };