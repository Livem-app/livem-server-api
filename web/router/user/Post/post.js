'use strict'
const Joi = require("joi");
const zendesk=require('./../../../../models/zendesk');
const Validator = Joi.object( {
    name: Joi.string().required().description('The user name'),
    email: Joi.string().required().description('The user email'),
    role: Joi.string().required().description('The user role. Possible values are "end-user", "agent", or "admin"')
}).required();


let APIHandler = (request, reply) => 
{
    var url = zendesk.config.zd_api_url + '/users.json';
    var data = { "user": { "name": request.payload.name, "email": request.payload.email, "role": request.payload.role } };
    zendesk.users.post(data,url, function (err, result) {
        if (err) {
            return reply(err);
        }else{
            return reply(result);
        }
    })
}

module.exports={APIHandler,Validator}