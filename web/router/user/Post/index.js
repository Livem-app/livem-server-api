const post = require('./post')
module.exports ={
    method: 'POST',
    path: '/zendesk/user',
    handler: post.APIHandler,
    config: {
        description: 'Create User ',
        tags: ['api', 'user'],
        validate: {
            payload: post.Validator
        },
    }
}