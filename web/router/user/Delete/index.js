const Delete = require('./delete');

module.exports ={
    method: 'DELETE',
    path: '/zendesk/user',
    handler: Delete.APIHandler,
    config: {
        description: 'Delete User ',
        tags: ['api', 'user'],
        validate: {
            payload: Delete.Validator
        },
    }
}