'use-strick'
const Joi = require('joi');
const zendesk = require('./../../../../models/zendesk');
const Validator = Joi.object({
    id: Joi.string().required().description('The user id')
}).required();

const APIHandler = (request, reply) => {
    var query = zendesk.config.zd_api_url + '/users/' + parseInt(request.payload.id) + '.json'
    zendesk.users.deleteZen(query, function (err, result) {
        if (err) { 
            return reply(err);
        } else {
            return reply(result);
        }
    })
}

module.exports = { APIHandler, Validator }