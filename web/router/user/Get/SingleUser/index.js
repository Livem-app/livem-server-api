const get = require('./get');

module.exports = {
    method: 'GET',
    path: '/zendesk/user/{id}',
    handler: get.APIHandler,
    config: {
        description: 'get single user',
        tags: ['api', 'user'],
        validate: {
            params: get.Validator
        },
    }
}