'use strict'
const Joi = require("joi");
const zendesk=require('./../../../../../models/zendesk');
const Validator = Joi.object({
    id: Joi.string().required().description('get about user ex id 115947092331'),
}).required();


const APIHandler = (request, reply) => {
    var url =zendesk.config.zd_api_url + '/users/' + request.params.id + '.json'
    zendesk.users.get(url, function (err, result) {
        if (err) {
            return reply(result);
        }else{
            return reply(result);
        }
    })
}

module.exports = { APIHandler, Validator }