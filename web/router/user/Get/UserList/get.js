'use strict'
const Joi = require("joi");
const zendesk=require('./../../../../../models/zendesk');
const APIHandler = (request, reply) => {
    var url =zendesk.config.zd_api_url + '/users.json'
    zendesk.users.get(url, function (err, result) {
        if (err) {
            return reply(result);
        }else{
            return reply(result);
        }
    })
}

module.exports = { APIHandler }