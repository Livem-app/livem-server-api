const get = require('./get');

module.exports ={
    method: 'GET',
    path: '/zendesk/user/list',
    handler: get.APIHandler,
    config: {
        description: 'get single user',
        tags: ['api', 'user'],
    }
}