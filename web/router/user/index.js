/*const getSingleUserById = require('./getSingleUserById');
const postUser = require('./postUser');
const putUser = require('./putUser');
const deletetUser = require('./deleteUser')
const getUserList = require('./getUserList');*/

'use strict';
const Delete = require('./Delete');
const Get = require('./Get');
const Post = require('./Post');
const Put = require('./Put');

module.exports = [].concat(Delete,Get,Post,Put);
/**
 * display user  details by id
 * method  GET
 * Handler  getSingleUserById.APIHandler 
 **/
/*
module.exports = ([{
    method: 'GET',
    path: '/zendesk/user/{id}',
    handler: getSingleUserById.APIHandler,
    config: {
        description: 'get single user',
        tags: ['api', 'user'],
        validate: {
            params: getSingleUserById.Validator
        },
    }
},

/**
 * display user  list details 
 * method  GET
 * Handler  getUserList.APIHandler 
 **/
/*
{
    method: 'GET',
    path: '/zendesk/user/list',
    handler: getUserList.APIHandler,
    config: {
        description: 'get single user',
        tags: ['api', 'user'],
    }
},

/**
 * create user
 * method  POST
 * Handler  postUser.APIHandler 
 **/
/*
{
    method: 'POST',
    path: '/zendesk/user',
    handler: postUser.APIHandler,
    config: {
        description: 'Create User ',
        tags: ['api', 'user'],
        validate: {
            payload: postUser.Validator
        },
    }
},

/**
 * update user
 * method  PUT
 * Handler  postUser.APIHandler 
 **/
/*
{
    method: 'PUT',
    path: '/zendesk/user',
    handler: putUser.APIHandler,
    config: {
        description: 'Create User ',
        tags: ['api', 'user'],
        validate: {
            payload: putUser.Validator
        },
    }
},

/**
 * Delete user
 * method  Delete
 * Handler  deleteUser.APIHandler 
 **/
/*
{
    method: 'DELETE',
    path: '/zendesk/user',
    handler: deletetUser.APIHandler,
    config: {
        description: 'Delete User ',
        tags: ['api', 'user'],
        validate: {
            payload: deletetUser.Validator
        },
    }
},

])*/
