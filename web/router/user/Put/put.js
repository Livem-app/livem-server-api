'use-strick'
const Joi=require('joi');
const zendesk=require('./../../../../models/zendesk');
const Validator=Joi.object({
    id:Joi.string().required().description('The user id'),
    name: Joi.string().description('The user name'),
    role: Joi.string().description('The user role. Possible values are "end-user", "agent", or "admin"')
}).required();

let APIHandler = (request, reply) => 
{
    var query=zendesk.config.zd_api_url+'/users/'+request.payload.id+'.json'
    var data = { "user": { "id":request.payload.id, "name": request.payload.name, "email": request.payload.email, "role": request.payload.role } };
    zendesk.users.put(data,query, function (err, result) {
        if (err) {
            return reply(err);
        }else{ 
            return reply(result);
        }
    })
}

module.exports={APIHandler, Validator}