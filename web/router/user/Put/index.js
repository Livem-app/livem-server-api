const put = require('./put');

module.exports ={
    method: 'PUT',
    path: '/zendesk/user',
    handler: put.APIHandler,
    config: {
        description: 'Create User ',
        tags: ['api', 'user'],
        validate: {
            payload: put.Validator
        },
    }
}