'use strict';

const admin = require('./admin');

const customer = require('./customer');

const provider = require('./provider');

const dispatcher = require('./dispatcher');

const zendesk = require('./zendesk');
const user = require('./user');

const stripe = require('./stripe');

const webhooks = require('./webhooks');

const redisEvent = require('./redisEvent');

const campaignAndreferral = require('./campaignAndreferral')

const serverTime = require('./serverTime')


module.exports = [].concat(
    admin,
    customer,
    provider,
    dispatcher,
    zendesk,
    user,
    stripe,
    webhooks,
    redisEvent,
    campaignAndreferral,
    serverTime

);