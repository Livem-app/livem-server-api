/** @global */
const headerValidator = require('../../../../middleware/validator');
/** @namespace */
/** @namespace */
const get = require('./get/get');
/** @namespace */
const error = require('../../../../../statusMessages/responseMessage');
/** @global */
const Joi = require('joi')
/**
* A module that exports business API  routes to hapi server!
* @memberof CART-API-ROUTES 
*/
module.exports = [
    {
        method: 'GET',
        path: '/customerDetailsById/{customerId}',
        config: {
            tags: ['api', 'customer'],
            description: 'Get user details by user Id',
            notes: 'Get user details by user Id',
            auth: false,
            validate: {
                 /** @memberof remove */
                 params: get.customerDetailsByIdValidator,
                /** @memberof headerValidator */
                headers: headerValidator.language,
                /** @memberof headerValidator */
                failAction: headerValidator.customError
            },
            response: {
                status: {
                    200: { message: error['customer']['200']['0'], data: Joi.any() },
                    404: { message: error['customer']['404']['0'] },
                    500: { message: error['genericErrMsg']['500']['0'] }
                }
            }
        },
        /** @memberof get */
        handler: get.customerDetailsByIdHandler
    }


]