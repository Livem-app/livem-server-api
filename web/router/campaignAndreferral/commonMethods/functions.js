require("moment");
const underscore = require('underscore');
const Joi = require("joi");
const async = require("async");
const logger = require('winston');
const Promise = require('promise');
const moment = require('moment');

const ObjectID = require('mongodb').ObjectID;
const promoCampaigns = require('../../../../models/promoCampaigns/promoCampaigns');
const claims = require('../../../../models/claims/claims');
const error = require('../../../../statusMessages/responseMessage');

const promoCodes = require('../../../../models/promoCodes/promoCodes');

const commonFunctions = require('../commonMethods/functions');
// const email = require('../../../models/email/email');


function couponCode(count) {
    var randomText = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    for (var i = 0; i < count; i++)
        randomText += possible.charAt(Math.floor(Math.random() * possible.length));
    return randomText;
}

// Check promo code 
function checkPromoCode(data, finalCallBack) {



    let currentDate = new Date();

    let currentISODate = currentDate.toISOString();

    let offeredCampaign = [];

    let message = {};

    let discount = 0;

    let dataId = [];

    let promoId = [];

    let claimId = [];

    let promoDatas = [];

    let cartValue = data.requestData.cartValue;

    let deliveryFee = data.requestData.deliveryFee;

    let ResponseMessage = "";

    var finalPayableAmount = data.requestData.finalPayableAmount;

    // logger.info(data.finalPayableAmount);

    async.waterfall([
        function (callBack) {
            promoCodes.validateCoupon(data.requestData, (error, promoData) => {
                console.log("coupon code response");
                console.log(promoData)

                if (error) {
                    return callBack({
                        errMsg: "Database error",
                        statusCode: 405
                    })
                } else if (promoData.length == 0) {
                    return callBack({
                        errMsg: "This promo code is not valid.",
                        statusCode: 405
                    });
                } else {


                    promoDatas.push(promoData[0]);
                    return callBack(null, promoData);

                }
            });
        },
        // Check if the user has the code unlocked in the claims collection 
        function (promoData, callBack) {

            let promoPaymentMothod = promoDatas[0].paymentMethod
            logger.error("user Payment method");
            logger.error(data.requestData.paymentMethod);
            logger.error("promo payment method");
            logger.error(promoPaymentMothod);

            if (promoDatas.length == 0) {
                return callBack({
                    errMsg: "This promo code is not valid.",
                    statusCode: 405
                });
            } else if (promoPaymentMothod !== data.requestData.paymentMethod) {
                if (promoPaymentMothod == 3) {
                    var promoId = promoDatas[0]._id.toString();
                    var userId = data.requestData.userId;
                    var checkLockClaimData = {
                        promoId: promoId,
                        userId: userId
                    }
                    claims.lockedAndClaimedCount(checkLockClaimData, (checkLockPromoError, checkLockPromResponse) => {

                        return callBack(null, checkLockPromResponse);
                    });



                } else {
                    if (promoPaymentMothod == 1) {
                        return callBack({
                            errMsg: "This promo code is valid only for card.",
                            statusCode: 405
                        });
                    } else if (promoPaymentMothod == 2) {
                        return callBack({
                            errMsg: "This promo code is valid only for cash.",
                            statusCode: 405
                        });
                    } else {
                        return callBack({
                            errMsg: "This promo code is valid only for wallet.",
                            statusCode: 405
                        });
                    }

                }

            } else {
                var promoId = promoDatas[0]._id.toString();
                var userId = data.requestData.userId;
                var checkLockClaimData = {
                    promoId: promoId,
                    userId: userId
                }
                claims.lockedAndClaimedCount(checkLockClaimData, (checkLockPromoError, checkLockPromResponse) => {

                    return callBack(null, checkLockPromResponse);
                });

                claims.globalLockedAndClaimedCount(checkLockClaimData, (checkLockPromoError2, checkLockPromResponse2) => {

                    if (promoDatas[0].globalUsageLimit <= checkLockPromResponse2) {
                        return callBack({
                            errMsg: "Sorry , Code is not available currently",
                            statusCode: 405
                        });
                    }


                });
            }


        },
        function (checkLockPromResponse, callBack) {
            if (promoDatas[0].perUserLimit <= checkLockPromResponse) {
                return callBack({
                    errMsg: "You have reached maximum number of usage",
                    statusCode: 405
                });
            } else {



                return callBack(null, promoDatas);
            }

        },

        function (promoDatas, callBack) {
            if (promoDatas.length == 0) {
                return callBack({
                    errMsg: "This promo code is not valid.",
                    statusCode: 405
                });
            } else {

                if (promoDatas[0].minimumPurchaseValue > cartValue) {
                    return callBack({
                        errMsg: "Booking amount is not sufficient to use this promo code.",
                        statusCode: 405
                    });
                } else {
                    logger.info("minimum purchase value condition matched");
                    /*
                    @applicableOn 
                                1 = cart value
                                2 = delivery fee
                                3 = both
                     */

                    // Calculation for discount

                    var billingAmount = data.requestData.amount;
                    var discountData = promoDatas[0].discount;

                    logger.info("applicable on ------------ ", promoDatas[0].applicableOn);



                    if (promoDatas[0].applicableOn == 1) {
                        logger.info("Applicable on cart value");
                        /*
                        applicable on cart value
                         */
                        if (discountData.typeId == 1) {
                            discount = discountData.value;

                        } else {

                            var percentage = discountData.value;
                            var discountAmount = cartValue * (percentage / 100);
                            if (discountAmount > discountData.maximumDiscountValue) {
                                discount = discountData.maximumDiscountValue;
                            } else {
                                discount = discountAmount;
                            }
                        }


                    } else if (promoDatas[0].applicableOn == 2) {
                        logger.info("applicable on delviery fee")
                        /*
                        applicable on delivery fee
                         */

                        if (discountData.typeId == 1) {
                            if (discountData.value > deliveryFee) {
                                discount = deliveryFee;
                                ResponseMessage = "Yay!! You’ve earned yourself a free delivery!!"

                            } else {
                                discount = discountData.value
                            }

                        } else {

                            var percentage = discountData.value;
                            var discountAmount = deliveryFee * (percentage / 100);
                            if (discountAmount > discountData.maximumDiscountValue) {
                                discount = discountData.maximumDiscountValue;
                            } else {
                                discount = discountAmount;
                            }
                        }

                    } else {
                        /*
                            @applicable on both
                         */
                        logger.error("applicable on both");

                        if (discountData.typeId == 1) {
                            if (finalPayableAmount <= discountData.value) {
                                discount = finalPayableAmount;
                            } else {
                                discount = discountData.value;
                            }


                        } else {
                            var totalAmount = deliveryFee + cartValue;
                            var percentage = discountData.value;
                            // var discountAmount = totalAmount * (percentage / 100);
                            var discountAmount = finalPayableAmount * (percentage / 100);
                            if (discountAmount > discountData.maximumDiscountValue) {
                                discount = discountData.maximumDiscountValue;
                            } else {
                                discount = discountAmount;
                            }
                        }
                    }







                }



                // Check time stamp is valid
                if (((promoDatas[0].startTime) < currentISODate) && ((promoDatas[0].endTime) > currentISODate)) {
                    // Check if user key exists in the promo data arary
                    if ("users" in promoDatas[0]) {
                        var promoUser = underscore.findWhere(promoDatas[0].users, {
                            userId: data.requestData.userId
                        });
                        // Check if current uesr exits in the array
                        if (promoUser) {

                            // check if per user limit crossed
                            // if not crossed the add to claim give discount
                            if (promoUser.claimCount == promoDatas[0].perUserLimit || promoUser.claimCount > promoDatas[0].perUserLimit) {
                                return callBack({
                                    errMsg: "You have already exhausted this coupon code on you previous order.",
                                    statusCode: 405
                                });

                            } else {
                                // add to claim 
                                // give discount
                                let updateClaimData = {
                                    "userId": data.requestData.userId,
                                    "promoId": promoDatas[0]._id.toString(),
                                    "lockedTimeStamp": currentDate,
                                    "unlockedTimeStamp": "N/A",
                                    "claimTimeStamp": "N/A",
                                    "discount": promoDatas[0].discount,
                                    "status": 'Locked'
                                };
                                return callBack(null, updateClaimData);
                            }
                        } else {
                            // If not exists then add to claim and give discount
                            let updateClaimData = {
                                "userId": data.requestData.userId,
                                "promoId": promoDatas[0]._id.toString(),
                                "lockedTimeStamp": currentDate,
                                "unlockedTimeStamp": "N/A",
                                "claimTimeStamp": "N/A",
                                "discount": promoDatas[0].discount,
                                "status": 'Locked'
                            };
                            return callBack(null, updateClaimData);

                        }
                    } else {
                        // Add user and give discount
                        // entry in claims collection with status unlocked
                        let updateClaimData = {
                            "userId": data.requestData.userId,
                            "promoId": promoDatas[0]._id.toString(),
                            "lockedTimeStamp": currentDate,
                            "unlockedTimeStamp": "N/A",
                            "claimTimeStamp": "N/A",
                            "discount": promoDatas[0].discount,
                            "status": 'Locked'
                        };
                        return callBack(null, updateClaimData);
                    }

                } else {
                    // Coupon expired
                    return callBack({
                        errMsg: "This promo code has expired",
                        statusCode: 405
                    });
                }
            }
        },
    ],

        (error, result) => {
            if (error) {
                return finalCallBack(error);
            } else {
                console.log("claimId------" + claimId);
                return finalCallBack(null, {
                    errMsg: 'Coupon code applied',
                    cartValue: cartValue,
                    deliveryFee: deliveryFee,
                    discountAmount: discount,
                    promoId: promoDatas[0]._id.toString()
                });
            }
        });
}


function sendEmail(emailTemplateName, DataToReplce, subject, toEmail) {
    logger.info("sending email........");



    // email.getTemplateAndSendEmail({
    //     templateName: emailTemplateName,
    //     toEmail: toEmail,
    //     subject: subject,
    //     keysToReplace: DataToReplce
    // }, () => {

    // });
    return resolve(true);
}


module.exports = {
    couponCode,
    checkPromoCode,
    sendEmail
}