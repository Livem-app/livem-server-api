require("moment");
const Joi = require("joi");
const async = require("async");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;
const city = require('../../../../../../models/cities/cities');

const error = require('../../../../../../statusMessages/responseMessage');
var handler = (request, reply) => {
    city.getAllCities({ "isDeleted": { "$ne": true } }, (err, city) => {
        if (err) {
            // logger.error('No response: ' + JSON.stringify(err));
            return reply({
                message: error['genericErrMsg']['500']['0']
            }).code(500);
        }

        var cities = [];
        async.forEach(city, (countryData, callbackloop) => {
            cities.push({
                "id": countryData._id.toString(),
                "cityName": countryData.city,
                "currency": countryData.currency,
                "currencySymbol": countryData.currencySymbol,
                
            });
            return callbackloop(null);
        }, (loopErr) => {
            // console.log("cities",cities)
            return reply({
                message: error['getData']['200']['0'],
                data: cities
            }).code(200);
        });
    });
}




let cityDetailsByCityIdsValidator = {

    cityIds: Joi.any().required().description('Mandatory Field.')
}

let cityDetailsByCityIdsHandler = (request, reply) => {

    var cityIds = request.params.cityIds;

    var cityIdsArray = cityIds.split(",");

    var cityIdsMongoIds = [];

    cityIdsArray.forEach(function (entry) {

        var cityIdsMongoId = new ObjectID(entry);

        cityIdsMongoIds.push(cityIdsMongoId);
    });

    // logger.error("mongo ids");

    // logger.error(cityIdsMongoIds);

    city.cityDetailsByCityId(cityIdsMongoIds, (cityDetailsError, cityDetailsResponse) => {

        // logger.error(cityDetailsError);

        if (cityDetailsError) {

            return reply({
                message: error['genericErrMsg']['500']['0']
            }).code(500);

        } else {

            return reply({
                message: error['getData']['200']['0'],
                data: cityDetailsResponse
            }).code(200);
        }
    });
}

let response = {
    status: {
        200: {
            message: error['getData']['200']['0']
        },
        500: {
            message: error['genericErrMsg']['500']['0']
        }
    }
}

// export handler and validator
module.exports = {
    // getUnlockedCodeCountByCampaignIdHandler,
    // getUnlockedCodeCountByCampaignIdValidator,
    // getClaimedDataByCampaignIdHandler,
    // getClaimedDataByCampaignIdValidator,
    handler,
    cityDetailsByCityIdsValidator,
    cityDetailsByCityIdsHandler,
    // response




}