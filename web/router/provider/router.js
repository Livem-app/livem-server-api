'use strict';

const onBoarding = require('./onBoarding');

const accessToken = require('./accessToken');

const address = require('./address');

const support = require('./support');

const bookings = require('./bookings');

const paymentGateway = require('./paymentGateway');

const reviewAndRating = require('./reviewAndRating');

const verificationCode = require('./verificationCode');

const cancelReasons = require('./cancelReasons');

const shedule = require('./shedule');

const validation = require('./validation');

const cities = require('./cities');

const category = require('./category');

const profile = require('./profile');

const appConfig = require('./appConfig');

const referralCode = require('./referralCode');

const customer = require('./customer');
module.exports = [].concat(
    customer,
    referralCode,
    category,
    appConfig,
    profile,
    validation,
    cities,
    accessToken,
    onBoarding,
    address,
    support,
    shedule,
    bookings,
    cancelReasons,
    paymentGateway,
    reviewAndRating,
    verificationCode,
);