'use strict';
const joi = require('joi');
const error = require('../error');
const logger = require('winston');
const provider = require('../../../../models/provider');
const cities = require('../../../../models/cities');

const payload = joi.object({
    code: joi.string().required().min(4).description('Code required'),
    userType: joi.number().required().min(1).max(2).description('1-customer, 2-provider'),
    lat: joi.number().required().description('latitude').error(new Error('Latitude must be number')),
    long: joi.number().required().description('longitude').error(new Error('Longitude must be number'))
}).required();

/**
 * @method POST /customer/referralCodeValidation
 * @param {*} req 
 * @param {*} reply 
 */
const handler = (req, reply) => {
    const dbErrResponse = { message: error['genericErrMsg']['500'][req.headers.lan], code: 500 };
    const checkReeralCode = (data) => {
        return new Promise((resolve, reject) => {
            provider.read({ $or: [{ referralCode: req.payload.code }, { referralcode: req.payload.code }] }, (err, referrer) => {
                if (err)
                    return reject(dbErrResponse);
                else if (referrer === null){ 
                    return reject({ message: error['postReferralCode']['401'][req.headers.lan], code: 401 });
                }
                    
                else
                    return resolve(referrer);
            });
        });
    }//check refreal code

    const valid = (data) => {
        return new Promise((resolve, reject) => {
            let con = {
                'polygons': {
                    $geoIntersects: {
                        $geometry: {
                            type: 'Point',
                            coordinates: [parseFloat(req.payload.long), parseFloat(req.payload.lat)]
                        }
                    }
                }
            };
            cities.read(con, (err, city) => {
                if (err)
                    return reject(dbErrResponse);

                if (city === null)
                    return reject({ message: error['postReferralCode']['401'][req.headers.lan], code: 401 });

                if (typeof data.cityId === 'undefined')
                    data.cityId = 0;

                //check if the referee & referrer cities are same
                let response = (data.cityId.toString() === city._id.toString()) ? resolve(city)
                    : reject({ message: error['postReferralCode']['402'][req.headers.lan], code: 402 });
            });
        });
    }
    checkReeralCode()
        .then(valid)
        .then(data => {
            return reply({ message: error['postReferralCodeValidation']['200'][req.headers.lan] }).code(200);
        }).catch(e => {
            logger.error("provider refreal error =>", e)
            return reply({ message: e.message }).code(e.code);
        });

}
const responseCode = {
    status: {
        200: { message: error['postReferralCode']['200'][error['lang']], data: joi.any() },
        401: { message: error['postReferralCode']['401'][error['lang']] },
        402: { message: error['postReferralCode']['402'][error['lang']] }, 
        500: { message: error['genericErrMsg']['500'][error['lang']] }
    }

}//swagger response code

module.exports = {
    payload,
    handler,
    responseCode
};