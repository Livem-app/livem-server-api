
'use strict';
const joi = require('joi');
const Async = require('async');
const logger = require('winston');
const error = require('../error');
const cancellationReasons = require('../../../../models/cancellationReasons');

const params = joi.object({
    userType: joi.number().integer().min(1).max(2).required().description('1- customer , 2- provider').error(new Error('User type is incorrect Please enter valid type')),
}).required();//params validator

/**
 * @method GET /customer/cancellationReasons/
 * @description get all cancellation Reasons for customer
 * @param {*} req 
 * @param {*} reply 
 */
const handler = (req, reply) => {
    cancellationReasons.readAll(2, (err, can_reason) => {
        if (err) {
            return reply({ message: error['genericErrMsg']['500'][req.headers.lan] }).code(500);
        } else {
            var resonaArr = [];
            if (can_reason.length > 0) {
                Async.forEach(can_reason, function (item, callbackloop) {
                    var canRes = {
                        res_id: item.res_id,
                        reason: item.reasons[0]
                    };
                    resonaArr.push(canRes);
                    callbackloop(null, resonaArr);
                }, function (loopErr) {
                    return reply({ message: error['getCancelReason']['200'][req.headers.lan], data: resonaArr }).code(200);
                });
            } else {
                return reply({ message: error['getCancelReason']['200'][req.headers.lan], data: resonaArr }).code(200);
            }

        }
    });
};

const responseCode = {
    status: {
        200: { message: error['getCancelReason']['200'][error['lang']], data: joi.any() },
        500: { message: error['genericErrMsg']['500'][error['lang']] }
    }

}//swagger response code

module.exports = {
    params,
    handler,
    responseCode
};