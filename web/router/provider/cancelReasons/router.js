'use strict';

const get = require('./get');
const error = require('../error');
const headerValidator = require('../../../middleware/validator');

const entity = '/provider';
module.exports = [

    /**
     * @name GET /provider/cancelReasons/{userType}
     */
    {
        method: 'GET',
        path: entity + '/cancelReasons/{userType}',
        handler: get.handler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['getCancellationReasons'],
            notes: "This api to list all cancellation reasons",
            auth: false,
            response: get.responseCode,
            validate: {
                headers: headerValidator.headerLanValidator,
                params: get.params,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },

];