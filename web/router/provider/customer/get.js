
'use strict';
const joi = require('joi');
const error = require('../error');
const logger = require('winston');
const Async = require('async');
const provider = require('../../../../models/provider');
const customer = require('../../../../models/customer');

const params = joi.object({
    pageNo: joi.number().integer().required().description('0 defult, ').error(new Error('User type is incorrect Please enter valid type')),
    customerId: joi.string().required().description('customer id ').error(new Error('customerId is  missing')),
}).required();
/**
 * @method GET /customer/reviewAndRating
 * @param {*} req 
 * @param {*} reply 
 */
const handler = (req, reply) => {
    let averageRating = 0;
    let reviewCount = 0;
    const getReview = (data) => {
        return new Promise((resolve, reject) => {
            let start = (req.params.pageNo * 5);
            customer.readReviewAndRating(req.params.customerId, start, (err, data) => {
                if (err)
                    return reject({ message: error['genericErrMsg']['500'][req.headers.lan], code: 500 });
                if (data === null)
                    return reject({ message: error['getReviewAndRating']['404'][req.headers.lan], code: 404 });
                averageRating = data.averageRating;
                reviewCount = data.reviewCount; 
                return resolve(data);
            });
        });
    }//get customer review

    const responseData = (data) => {
       
        var revObj = [];
        return new Promise((resolve, reject) => {
            if(data.reviews){
                Async.forEach(data.reviews, (e, callback) => {
                    let rev = {
                        review: e.review || "",
                        rating: e.rating || "",
                        reviewBy: e.firstName || "",
                        profilePic: e.profilePic || "",
                        reviewAt: e.reviewAt || '',
                    };
                    revObj.push(rev);
                    return callback(null, revObj);
                }, (err, results) => {
                    let res = {
                        reviews: revObj,
                        averageRating: averageRating || 0,
                        reviewCount: reviewCount || 0,
                        about: data.about || ""
                    };
                    return resolve(res);
                });

            }
            else{
                return resolve([]);
            }
            
        });
    }//get customer review

    getReview()
        .then(responseData)
        .then(data => {
            return reply({ message: error['getReviewAndRating']['200'][req.headers.lan], data: data }).code(200);
        }).catch(e => {
            logger.error("customer update password API error =>", e)
            return reply({ message: e.message }).code(e.code);
        });
};

const responseCode = {
    status: {
        200: { message: error['getReviewAndRating']['200'][error['lang']], data: joi.any() },
        500: { message: error['genericErrMsg']['500'][error['lang']] }
    }

}//swagger response code

module.exports = {
    params,
    handler,
    responseCode
};