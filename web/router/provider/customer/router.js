'use strict';

const entity = '/provider'; 
const get =require('./get'); 
const error = require('../error');
const headerValidator = require('../../../middleware/validator');

module.exports = [
    /**
     * @name GET /app/reviewAndRating/{pageNo}
     */
    {
        method: 'GET',
        path: entity + '/customer/reviewAndRating/{pageNo}/{customerId}',
        handler: get.handler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['getCustomerReviewAndRating'],
            notes: "This API is get all the review and rating.",
            auth: 'providerJWT',
            response: get.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                params: get.params,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    }
    
];