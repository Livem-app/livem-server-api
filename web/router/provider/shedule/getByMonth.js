
'use strict';
const joi = require('joi');
const error = require('../error');
const logger = require('winston');
const moment = require('moment');//date-time
const schedule = require('../../../../models/schedule');
const dailySchedules = require('../../../../models/dailySchedules');

const paramsValidator = joi.object({
    month: joi.required().description('month MM-YYYY').error(new Error('month is missing')),
}).required();//params of provider schedule month api

function getDateRange(startDate, endDate, dateFormat) {
    var dates = [],
        end = moment(endDate),
        diff = endDate.diff(startDate, 'days'); 
    if (!startDate.isValid() || !endDate.isValid() || diff < 0) {
        return;
    }
    dates.push(moment(startDate).format(dateFormat));
    for (var i = 0; i < diff; i++) {
        dates.push(startDate.add(1, 'd').format(dateFormat));
    }

    return dates;
}//get all Date

const APIHandler = (req, reply) => {
    let time = (req.params.month).split('-');
    let startDate = moment([time[1], time[0] - 1]);
    let endDate = moment(startDate).endOf('month');
    let start = startDate.unix();
    let end = endDate.unix();
    let now = moment(startDate.format('YYYY-MM-DD'));
    let then = moment(endDate.format('YYYY-MM-DD'));
    let adata = {};
    let slotData = [];
    let intervalDate = getDateRange(now, then, 'YYYY-MM-DD');
    const dbErrResponse = { message: error['genericErrMsg']['500'][req.headers.lan], code: 500 };

    let getSchedule = (data) => {
        return new Promise((resolve, reject) => {
            dailySchedules.getSchduleByMonth(req.auth.credentials._id, start, end, (err, result) => {
                return err ? reject(dbErrResponse) : resolve(result);

            })
        });
    }//get all schedule for pro
    let checkSchedule = (data) => {
        return new Promise((resolve, reject) => {
            data.forEach((sche) => {
                let scheduleArr = [];
                sche.schedule.forEach((e) => {
                    if (e.providerId == req.auth.credentials._id) {
                        scheduleArr.push(e);
                    }
                });
                adata[sche.date] = { _id: sche._id, date: sche.date, schedule: scheduleArr };
            }, resolve(adata));
        });
    }//check schedule for perticuler this provider
    let responseData = (data) => {
        return new Promise((resolve, reject) => {
            intervalDate.forEach((date) => {
                let stDate = moment(date).startOf('day').unix();
                if (adata[stDate] == 'undefined' || adata[stDate] == undefined) {
                    adata[stDate] = { date: stDate, schedule: [] };
                    slotData.push(adata[stDate])
                } else {
                    slotData.push(adata[stDate])
                }
            }, resolve(slotData));
        });
    }//response all date of the requested month

    getSchedule()
        .then(checkSchedule)
        .then(responseData)
        .then(data => {
            console.log('dataaaa->>>>>>>>',data);
            return reply({ message: error['getMonthSchedule']['200'][req.headers.lan], data: data }).code(200);
        }).catch(e => {
            logger.error("Provider get schedule month API error =>", e)
            return reply({ message: e.message }).code(e.code);
        })
};

const responseCode = {
    status: {
        200: {
            message: error['getMonthSchedule']['200'][error['lang']],
            data: joi.any()
        },
        500: { message: error['genericErrMsg']['500'][error['lang']] }
    }

}//swagger response code

module.exports = {
    APIHandler,
    responseCode,
    paramsValidator
};
