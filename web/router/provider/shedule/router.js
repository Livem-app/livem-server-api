'use strict';

const entity = '/provider';
const error = require('../error');
const get = require('./get');
const post = require('./post');
const getByMonth = require('./getByMonth');
const deleteById = require('./deleteById');
const headerValidator = require('../../../middleware/validator');

module.exports = [
    /**
    * @name GET /provider/schedule
   */
    {
        method: 'GET',
        path: entity + '/schedule',
        handler: get.APIHandler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['getSchedule'], 
            notes: 'This API is to fetch all create schedule',
            auth: 'providerJWT',
            response: get.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
    /**
     * @name GET /provider/schedule/{month}
    */
    {
        method: 'GET',
        path: entity + '/schedule/{month}',
        handler: getByMonth.APIHandler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['getByMonth'], 
            notes: 'This API is to fetch all create schedule by month wise',
            auth: 'providerJWT',
            response: getByMonth.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                params: getByMonth.paramsValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
    /**
     * @name POST /provider/schedule
    */
    {
        method: 'POST', // Methods Type
        path: entity + '/schedule',
        handler: post.APIHandler,
        config: {
            tags: ['api', entity],  
            description: error['apiDescription']['postSchedule'], 
            notes: "This api is to create schedule ",
            auth: 'providerJWT',
            // response: post.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: post.payloadValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
    /**
     * @name DELETE /provider/schedule
    */
    {
        method: 'DELETE', // Methods Type
        path: entity + '/schedule',
        handler: deleteById.APIHandler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['deleteSchedule'], 
            notes: 'This api is used to delete schedule.',
            auth: 'providerJWT',
            response: deleteById.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: deleteById.payloadValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
];