
'use strict';
const joi = require('joi');
const error = require('../error');
const Async = require('async');
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;
const moment = require('moment');//date-time
const schedule = require('../../../../models/schedule');
const dailySchedules = require('../../../../models/dailySchedules');

const payloadValidator = joi.object({
    scheduleId: joi.string().required().description('scheduleId').error(new Error('scheduleID')),
}).required();//params of provider schedule month api


const APIHandler = (req, reply) => {
    let con = [
        { '$unwind': '$schedule' },
        {
            '$match': {
                "schedule.scheduleId": new ObjectID(req.payload.scheduleId),
                "schedule.booked": { '$ne': [] },
            }
        }
    ]
    dailySchedules.readByAggregate(con, (err, res) => {
        if (err) {
            return reply({ message: error['genericErrMsg']['500'][req.headers.lan] }).code(500);
        } else {
            if (res.length != 0) {
                return reply({ message: error['deleteSchedule']['410'][req.headers.lan] }).code(410);
            } else {
                Async.series(
                    [
                        function (cb) {
                            dailySchedules.deleteSchedule(req.payload.scheduleId, (err, res) => {
                                return cb(err, res);
                            });
                        },
                        function (cb) {
                            schedule.deleteById(req.payload.scheduleId, (err, result) => {
                                return cb(err, result);
                            });
                        }
                    ],
                    function (err, data) {
                        if (err)
                            return reply({ message: error['genericErrMsg']['500'][req.headers.lan] }).code(500);
                        return reply({ message: error['deleteSchedule']['200'][req.headers.lan] }).code(200);
                    });
            }
        }
    });

};

const responseCode = {
    status: {
        200: {
            message: error['deleteSchedule']['200'][error['lang']],
            data: joi.any()
        },
        410: { message: error['deleteSchedule']['410'][error['lang']] },
        500: { message: error['genericErrMsg']['500'][error['lang']] }
    }

}//swagger response code

module.exports = { APIHandler, responseCode, payloadValidator };