
'use strict';
const joi = require('joi');
const error = require('../error');
const Async = require('async');
const logger = require('winston');
const schedule = require('../../../../models/schedule');

/**
 * @method GET /provider/schedule
 * @description get all schedule 
 * @param {*} req 
 * @param {*} reply 
 */
const APIHandler = (req, reply) => {
    Async.waterfall(
        [
            function (cb) {
                var addResult = [];
                schedule.getByProviderId(req.auth.credentials._id, (err, result) => {
                    if (err)
                        return reply({ message: error['genericErrMsg']['500'][req.headers.lan] }).code(500);
                    if (result.length != 0) {
                        result.forEach(schedule => {
                            addResult.push({
                                _id: schedule._id,
                                addresssId: schedule.addresssId || '',
                                price: schedule.price || '',
                                radius: schedule.radius || '',
                                startDate: schedule.start,
                                endDate: schedule.end,
                                days: schedule.days || [],

                            });
                        });
                        return cb(null, addResult);
                    } else {
                        return cb(null, addResult);
                    }
                });
            }
        ],
        function (err, data) {
            if (err)
                return reply({ message: error['genericErrMsg']['500'][req.headers.lan] }).code(500);
            return reply({ message: error['getSchedule']['200'][req.headers.lan], data: data }).code(200);
        });
};

const responseCode = {
    status: {
        200: {
            message: error['getSchedule']['200'][error['lang']],
            data: joi.any()
        },
        404: { message: error['getSchedule']['404'][error['lang']] },
        500: { message: error['genericErrMsg']['500'][error['lang']] }
    }

}//swagger response code

module.exports = {
    APIHandler,
    responseCode
};