'use strict';
const moment = require('moment');
const provider = require('../../../../models/provider');
const bookings = require('../../../../models/bookings');
const webSocket = require('../../../../models/webSocket');

Array.prototype.myFind = function (obj) {
    return this.filter(function (item) {
        for (var prop in obj)
            if (!(prop in item) || obj[prop] !== item[prop])
                return false;
        return true;
    });
};
const providerStatus = (dataArr, callback) => {
    let item;
    const readProvider = (data) => {
        return new Promise((resolve, reject) => {
            provider.getProviderById(dataArr._id, (err, res) => {
                item = res;
                return err ? reject(err) : resolve(res);
            });
        });
    }
    const readBooking = (data) => {
        return new Promise((resolve, reject) => {
            let cons = { status: { "$nin": [1, 2, 10, 11, 12] }, providerId: data._id.toString() };
            bookings.bookingCount(cons, (err, res) => {
                return err ? reject(err) : resolve(res);
            });
        });
    }
    const responseInWebSockt = (data) => {
        return new Promise((resolve, reject) => {
            var fname = (typeof item.firstName == 'undefined' || item.firstName == null) ? "" : item.firstName;
            var lname = (typeof item.lasrName == 'undefined' || item.lasrName == null) ? "" : item.lasrName;
            var name = fname + " " + lname;
            var obj = { items: item.phone };
            var arrayFound = obj.items.myFind({ "isCurrentlyActive": true });
            let status = (item.loggedIn == false) ? 8 : item.status;
            var proData = {
                '_id': item._id.toString(),
                'latitude': item.location.latitude,
                'longitude': item.location.longitude,
                'image': item.profilePic,
                'status': status,
                'email': item.email,
                'lastActive': item.lastActive,
                'lastOnline': moment().unix() - item.lastActive,
                'serverTime': moment().unix(),
                'name': name,
                'batteryPercentage': item.mobileDevices.batteryPercentage || '',
                'appversion': item.mobileDevices.appversion,
                'phone': arrayFound[0].countryCode + arrayFound[0].phone || "",
                'deviceType': item.mobileDevices.deviceType || '',
                'locationCheck': item.locationCheck || 0,
                'bookingCount': data,
            };
            webSocket.publish('dispatcher/provider', proData, {}, (mqttErr, mqttRes) => {
                return mqttErr ? reject(mqttErr) : resolve(mqttRes);
            });

        });
    }
    readProvider()
        .then(readBooking)
        .then(responseInWebSockt)
        .then(data => {
            return callback(data);
        }).catch(e => { 
            logger.error("Provider post review and rating API error =>", e)
            return callback(e);
        });
}
const liveTrack = (dataArr, callback) => {
    let item;
    const readProvider = (data) => {
        return new Promise((resolve, reject) => {
            provider.getProviderById(dataArr._id, (err, res) => {
                item = res;
                return err ? reject(err) : resolve(res);
            });
        });
    }
    const readBooking = (data) => {
        return new Promise((resolve, reject) => {
            let cons = { status: { "$nin": [1, 2, 10, 11, 12] }, providerId: data._id.toString() };
            bookings.bookingCount(cons, (err, res) => {
                return err ? reject(err) : resolve(res);
            });
        });
    }
    const responseInWebSockt = (data) => {
        return new Promise((resolve, reject) => {
            var fname = (typeof item.firstName == 'undefined' || item.firstName == null) ? "" : item.firstName;
            var lname = (typeof item.lasrName == 'undefined' || item.lasrName == null) ? "" : item.lasrName;
            var name = fname + " " + lname;
            var obj = { items: item.phone };
            var arrayFound = obj.items.myFind({ "isCurrentlyActive": true });
            var proData = {
                '_id': item._id.toString(),
                'latitude': dataArr.latitude ? dataArr.latitude : item.location.latitude,
                'longitude': dataArr.longitude ? dataArr.longitude : item.location.longitude,
                'image': item.profilePic,
                'status': item.status,
                'email': item.email,
                'lastActive': item.lastActive,
                'lastOnline': moment().unix() - item.lastActive,
                'serverTime': moment().unix(),
                'name': name,
                'batteryPercentage': item.mobileDevices.batteryPercentage || '',
                'appversion': item.mobileDevices.appversion,
                'phone': arrayFound[0].countryCode + arrayFound[0].phone || "",
                'deviceType': item.mobileDevices.deviceType || '',
                'locationCheck': item.locationCheck || 0,
                'bookingCount': data,
            };
            webSocket.publish('dispatcher/liveTrack', proData, {}, (mqttErr, mqttRes) => {
                return mqttErr ? reject(mqttErr) : resolve(mqttRes);
            });

        });
    }
    readProvider()
        .then(readBooking)
        .then(responseInWebSockt)
        .then(data => {
            return callback(data);
        }).catch(e => { 
            logger.error("Provider post review and rating API error =>", e)
            return callback(e);
        });
}

module.exports = {
    providerStatus,
    liveTrack
};