'use strict';

const joi = require('joi');
const error = require('../error'); 
const dispatcher = require('../../../commonModels/dispatcher');
const redis = require('../../../../models/redis');
const provider = require('../../../../models/provider');




/**
 * @function APIHandler
 * @param {*} req 
 * @param {*} reply 
 */
let APIHandler = (req, reply) => {
    const updateProvider = (data) => {
        let dataArr = { $set: { loggedIn: false, accessCode: '' } };
        return new Promise((resolve, reject) => {
            provider.findOneAndUpdate(req.auth.credentials._id, dataArr, (err, item) => {
                return err ? reject({ message: error['genericErrMsg']['500'][req.headers.lan], code: 500 }) :
                    resolve(item);
            });
        });
    }

    updateProvider()
        .then(data => {
            redis.delteSet('presence_' + req.auth.credentials._id);
            dispatcher.providerStatus({ _id: req.auth.credentials._id }, (err, res) => { });
            return reply({ message: error['patchLogOut']['200'][req.headers.lan] }).code(200);
        }).catch(e => {
            logger.error("Provider logout API error =>", e)
            return reply({ message: e.message }).code(e.code);
        });
};

const responseCode = {
    status: {
        200: { message: error['patchLogOut']['200'][error['lang']] },
        500: { message: error['genericErrMsg']['500'][error['lang']] }
    }

}//swagger response code


module.exports = {
    APIHandler,
    responseCode
};