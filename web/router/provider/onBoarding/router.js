'use strict';

const entity = '/provider';
const error = require('../error');
const postSignUp = require('./postSignUp');     
const postSignIn = require('./postSignIn'); 
const postLogout = require('./postLogout');  
const headerValidator = require('../../../middleware/validator'); 

module.exports = [
    
 
    
    /**
     * @name POST /provider/signUp
     */
    {
        method: 'POST',
        path: entity + '/signUp',
        handler: postSignUp.APIHandler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['postSignUp'], 
            notes: 'Provider signup API Error code',
            auth: false,
            response: postSignUp.responseCode,
            validate: {
                payload: postSignUp.payloadValidator,
                headers: headerValidator.headerLanValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
    
    /**
    * @name POST /provider/signIn
    */
    {
        method: 'POST',
        path: entity + '/signIn',
        handler: postSignIn.APIHandler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['postSignIn'], 
            notes: 'This API allows user to sign in to the application.',
            auth: false,
            response: postSignIn.responseCode,
            validate: {
                payload: postSignIn.payloadValidator,
                headers: headerValidator.headerLanValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
   
     
    /**
     * @name POST /app/logout
     */
    {
        method: 'POST',
        path: entity + '/logout',
        handler: postLogout.APIHandler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['postLogout'],  
            notes: 'This API allows the user to log out from the app from his Profile Page',
            auth: 'providerJWT',
            response: postLogout.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
    

];