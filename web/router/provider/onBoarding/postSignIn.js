'use strict';
const joi = require('joi');
const error = require('../error');
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;
const Timestamp = require('mongodb').Timestamp;
const Bcrypt = require('bcrypt');//hashing module  
const sendMsg = require('../../../../library/twilio');
const sendMail = require('../../../../library/mailgun');
const provider = require('../../../../models/provider');
const userList = require('../../../commonModels/userList');
const cities = require('../../../../models/cities');
const appConfig = require('../../../../models/appConfig');
const Auth = require('../../../middleware/authentication');
const mobileDevices = require('../../../../models/mobileDevices');
const dispatcher = require('../dispatcher');

const payloadValidator = joi.object({
    mobileOrEmail: joi.string().required().description('Email Id / mobile number').error(new Error('email / mobile is missing')),
    password: joi.string().required().description('Password').error(new Error('password is missing')),
    deviceType: joi.number().required().integer().min(1).max(3).description('1- IOS , 2- Android, 3- Web').error(new Error('device type is missing')),
    deviceId: joi.string().required().description('device id').error(new Error('device id is missing')),
    appVersion: joi.string().description('app version'),
    pushToken: joi.string().description('push token').error(new Error('push token is missing')),
    deviceMake: joi.string().description('Device Make'),
    deviceModel: joi.string().description('Device model'),
    deviceOsVersion: joi.string().description('device os version'),
    batteryPercentage: joi.number().description('batteryPercentage').error(new Error('Battery Percentage is missing')),
    locationHeading: joi.string().description('locationHeading').error(new Error('location Heading is missing')),
    deviceTime: joi.string().description('format : YYYY-MM-DD HH:MM:SS')
}).required();//payload of provider signin api


const APIHandler = (req, reply) => {
    const getProvider = function () {
        return new Promise((resolve, reject) => {
            let condition = {
                $or: [
                    { email: req.payload.mobileOrEmail },
                    { 'phone.phone': req.payload.mobileOrEmail, 'phone.isCurrentlyActive': true }
                ]
            }
            provider.read(condition, (err, result) => {
                const dbErrResponse = { message: error['genericErrMsg']['500'][1], code: 500 };
                return err ? reject(dbErrResponse) : resolve(result);
            })
        });
    };//get provider by  email or phone number

    const checkProvider = function (data) {
        return new Promise((resolve, reject) => {
            return data == null ? reject({ message: error['postSignIn']['400'][req.headers.lan], code: 400 }) :
                !(Bcrypt.compareSync(req.payload.password, data.password)) ? reject({ message: error['postSignIn']['401'][req.headers.lan], code: 401 }) :
                    data.status == 1 ? reject({ message: error['postSignIn']['409'][req.headers.lan], code: 409 }) :
                        data.status == 6 ? reject({ message: error['postSignIn']['410'][req.headers.lan], code: 410 }) :
                            data.status == 9 ? reject({ message: error['postSignIn']['410'][req.headers.lan], code: 410 }) :
                                resolve(data);

        });
    }//check provider valid or not

    const jwtToken = function (data) {
        return new Promise((resolve, reject) => {
            appConfig.read((err, config) => {
                if (err)
                    reject({ message: error['genericErrMsg']['500'][1], code: 500 });
                else {
                    let authToken = Auth.SignJWT(
                        {
                            _id: data._id.toString(),
                            key: 'acc'
                        }, 'provider', config.accessToken);//sign a new JWT
                    data.authToken = authToken;
                    resolve(data);
                }
            })
        });
    }//generate JWT token

    const responseData = function (data) {

        return new Promise((resolve, reject) => {
            let obj = { items: data.phone };
            let arrayFoundPhn = obj.items.myPhone({ "isCurrentlyActive": true });
            let fcmTopic = data._id + '00' + Math.floor(1000 + Math.random() * 9000);
            req.payload.userId = (data._id).toString();
            req.payload.userType = 2;//provider
            req.payload.status = data.status || 3;//online
            req.payload.fcmTopic = fcmTopic;
            req.payload.loggedIn = true;
            updateLoginStatus(req.payload, (err, res) => { 
                if (res) {
                    dispatcher.providerStatus({ _id: data._id }, (err, res) => { });
                }

            });//update login  status
            userList.updateUser(req.payload.userId,
                {
                    deviceType: req.payload.deviceType,
                    firebaseTopic: req.payload.fcmTopic
                }

            )//insert userlist table use for simple chat-module

            let responseData = {
                'id': data._id,
                'fcmTopic': fcmTopic || '',
                'token': data.authToken || '',
                'firstName': data.firstName ? data.firstName : "",
                'lastName': data.lastName ? data.lastName : "",
                'profilePic': data.profilePic ? data.profilePic : "",
                'referralCode': data.referralCode || "",
                'status': 3,
                'email': data.email || '',
                'mobile': arrayFoundPhn[0].phone || '',
                'countryCode': arrayFoundPhn[0].countryCode || '',
                requester_id: data.zendeskId || ""
            };
            resolve(responseData);
        });
    }//api response data

    getProvider()
        .then(checkProvider)
        .then(jwtToken)
        .then(responseData)
        .then(data => {

            return reply({ message: error['postSignIn']['200'][req.headers.lan], data: data }).code(200);
        }).catch(e => {
            logger.error("Provider Login API error =>", e)
            return reply({ message: e.message }).code(e.code);
        });

};

const responseCode = {
    status: {
        500: { message: error['genericErrMsg']['500'][error['lang']] },
        200: {
            message: error['postSignIn']['200'][error['lang']],
            data: {
                id: joi.any().example('59f6e0b6a4ca6f382e757bed'),
                fcmTopic: joi.any().example('59f6e0b6a4ca6f382e757bed_2864'),
                token: joi.any().example("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1OWRkZWUwOGU5MTIyMDY4YjFhMjQ0NmIiLCJrZXkiOiJhY2MiLCJhY2Nlc3NDb2RlIjo5MDk3LCJpYXQiOjE1MDc3MTgzNzgsImV4cCI6MTUwNzgwNDc3OCwic3ViIjoidXNlciJ9._QIsju37hnWSbIz1TU1RMpN8wdtWeQnJ4Db7JeKZMho"),
                firstName: joi.any().example("John"),
                lastName: joi.any().example("Shinha"),
                profilePic: joi.any().example('https://s3.amazonaws.com/livemapplication/Provider/ProfilePics/1509351606290_0_01.png'),
                referralCode: joi.any().example('QJDJD'),
                mobile: joi.any().example("9876543210"),
                countryCode: joi.any().example("+91"),
                email: joi.any().example("example@domain.com"),
                status: joi.any().example("3"),
                requester_id: joi.any().example("12344324324")
            }
        },
        400: { message: error['postSignIn']['400'][error['lang']] },
        401: { message: error['postSignIn']['401'][error['lang']] },
        410: { message: error['postSignIn']['410'][error['lang']] },
        409: { message: error['postSignIn']['409'][error['lang']] },
    }

}//swagger response code

Array.prototype.myPhone = function (obj) {
    return this.filter(function (item) {
        for (var prop in obj)
            if (!(prop in item) || obj[prop] !== item[prop])
                return false;
        return true;
    });
};//find phone number in phone array


const loginSchema = joi.object({
    userId: joi.string().required(),
    fcmTopic: joi.string(),
    status: joi.number().integer().required(),
    userType: joi.number().integer().required(),
    deviceId: joi.string().required(),
    deviceType: joi.number().integer().required(),
    appversion: joi.string(),
    deviceOsVersion: joi.any(),
    devMake: joi.string(),
    locationHeading: joi.string(),
    devModel: joi.string(),
    deviceTime: joi.any(),
    loggedIn: joi.any().default("")


}).unknown().required();//params
const updateLoginStatus = (params, cb) => {
    let data = joi.attempt(params, loginSchema);
    mobileDevices.updateByUserId(data.userId, (err, res) => {
        mobileDevices.updateByDeviceId(data, (err, res) => {
            provider.updateByProviderId(data, (err, res) => {
                cb(null, res);
            });
        });
    });
}

module.exports = {
    payloadValidator,
    APIHandler,
    responseCode
};