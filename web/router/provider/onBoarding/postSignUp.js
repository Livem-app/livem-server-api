'use strict';
const joi = require('joi');
var CRYPTO = require('crypto');
const error = require('../error');
const Async = require('async');
const Bcrypt = require('bcrypt');//hashing module  
const logger = require('winston');
const moment = require('moment');//date-time

const cities = require('../../../../models/cities');
const events = require('../../../../models/events');
const address = require('../../../../models/address');
const sendMsg = require('../../../../library/twilio');
const sendMail = require('../../../../library/mailgun');
const services = require('../../../../models/services');
const provider = require('../../../../models/provider');
const appConfig = require('../../../../models/appConfig');
const documents = require('../../../../models/documents');
const campaigns = require('../../../../models/campaigns');
const configuration = require('../../../../configuration');
const mobileDevices = require('../../../../models/mobileDevices');
const verificationCode = require('../../../../models/verificationCode');
const rabbitmqUtil = require('../../../../models/rabbitMq/twilio');
const rabbitMq = require('../../../../models/rabbitMq');
const Timestamp = require('mongodb').Timestamp;
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId

const payloadValidator = joi.object({
    firstName: joi.string().required().description('first name').error(new Error('firat name is missing')),
    lastName: joi.any().description('last name'),
    email: joi.string().email().required().description('email').error(new Error('Email is missing or incorrect')),
    password: joi.string().required().description('password').error(new Error('password is missing')),
    countryCode: joi.string().required().description('country code').error(new Error('country code is missing')),
    mobile: joi.string().required().description('mobile').error(new Error('mobile number is missing')),
    dob: joi.string().description('Date of birth format : YYYY-MM-DD'),
    link: joi.string().description('Youtube / Vimeo link').error(new Error('Video link is missing')),
    catlist: joi.string().required().description('catlist comma separated category id').error(new Error('catlist is missing')),
    cityId: joi.string().description('city id').error(new Error('cityID is missing')),
    latitude: joi.number().description(' latitude').error(new Error('Latitude must be number')),
    longitude: joi.number().description('longitude ').error(new Error('Longitude must be number')),
    profilePic: joi.string().description('profile pic'),
    deviceId: joi.string().required().description('Device id').error(new Error('Device id is missing')),
    deviceType: joi.number().required().integer().min(1).max(2).description('1 - Ios,2 - Android,3- Web').error(new Error('Device type is missing')),
    deviceMake: joi.string().description('Device Make'),
    deviceModel: joi.string().description('Device Model'),
    deviceOsVersion: joi.string().description('device os version'),
    batteryPercentage: joi.number().description('batteryPercentage').error(new Error('Battery Percentage is missing')),
    locationHeading: joi.string().description('locationHeading').error(new Error('location Heading is missing')),
    appVersion: joi.string().description('App Version'),
    pushToken: joi.string().description('push token').error(new Error('push token is missing')),
    deviceTime: joi.string().description('format : YYYY-MM-DD HH:MM:SS'),
    document: joi.array().description('[{"fid":"","data" : ""}]'),
    addLine1: joi.string().required().description('address line 1'),
    addLine2: joi.string().allow('').description('address line 2'),
    city: joi.string().allow('').description('city'),
    state: joi.string().allow('').description('state'),
    country: joi.string().allow('').description('country'),
    placeId: joi.string().allow('').description('placeId'),
    pincode: joi.any().allow('').description('pincode').error(new Error('Pincode must be number')),
    taggedAs: joi.string().allow('').description('office / home'),
    referralCode: joi.any().description('referral code optional')
}).required();//payload of provider signin api

const loginSchema = joi.object({
    userId: joi.string().required(),
    fcmTopic: joi.string(),
    status: joi.number().integer().required(),
    userType: joi.number().integer().required(),
    deviceId: joi.string().required(),
    deviceType: joi.number().integer().required(),
    appversion: joi.string(),
    deviceOsVersion: joi.any(),
    devMake: joi.string(),
    locationHeading: joi.string(),
    devModel: joi.string(),
    deviceTime: joi.any(),
    loggedIn: joi.any().default("")


}).unknown().required();//params

const updateLoginStatus = (params, cb) => {
    let data = joi.attempt(params, loginSchema);
    mobileDevices.updateByUserId(data.userId, (err, res) => {
        mobileDevices.updateByDeviceId(data, (err, res) => {
            provider.updateByProviderId(data, (err, res) => {
                cb(null, res);
            });
        });
    });
}

const postAddress = (req, reply) => {
    const postData = (data) => {
        return new Promise((resolve, reject) => {
            let dataArr = {
                addLine1: req.addLine1 ? req.addLine1 : '',
                addLine2: req.addLine2 ? req.addLine2 : '',
                city: req.city ? req.city : '',
                state: req.state ? req.state : '',
                country: req.country ? req.country : '',
                placeId: req.placeId ? req.placeId : '',
                pincode: req.pincode ? req.pincode : '',
                latitude: req.latitude ? req.latitude : '',
                longitude: req.longitude ? req.longitude : '',
                taggedAs: req.taggedAs,
                userType: 2, // 1 -slave, 2-master
                user_Id: new ObjectID(req.userId), //get the id from the extracted token
                defaultAddress: 1
            };
            address.insert(dataArr, (err, res) => {
                return err ? reject({ message: error['genericErrMsg']['500'][req.headers.lan], code: 500 })
                    : resolve(res);
            });
        });
    }//insert data in saved address

    const updateAddress = (data) => {
        return new Promise((resolve, reject) => {
            provider.updateAddress(req.userId, data, (err, res) => {
                return err ? reject({ message: error['genericErrMsg']['500'][req.headers.lan], code: 500 })
                    : resolve(res);
            });
        });
    }//update address id in customer

    postData()
        .then(updateAddress)
        .then(data => {
            return reply(true);
        }).catch(e => {
            logger.error("customer postAddress API error =>", e)
            return reply(false);
        });
};
/**
 * @function POST /provider/signUp
 * @description -New Provider registration
 * @param {*} req 
 * @param {*} reply 
 */

let APIHandler = (req, reply) => {
    let radius = 5;
    let currencySymbol = '$';
    let currency = 'USD';
    let cityName = '';
    const dbErrResponse = { message: error['genericErrMsg']['500'][req.headers.lan], code: 500 };
    const getReferralCodeAsync = (codeLength) => {
        return new Promise((resolve, reject) => {
            let chars = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ";
            var rnd = CRYPTO.randomBytes(codeLength)
                , value = new Array(codeLength)
                , len = chars.length;

            for (var i = 0; i < codeLength; i++) {
                value[i] = chars[rnd[i] % len]
            }
            campaigns.read({ code: value.join('').toUpperCase() }, (err, doc) => {
                if (err)
                    return reject(err);
                if (doc === null)
                    return resolve(value.join('').toUpperCase());
                return resolve(false);
            });
        });
    }
    var event = [];
    const getEvents = (data) => {
        return new Promise((resolve, reject) => {

            events.readAll((err, result) => {
                if (err)
                    reject(dbErrResponse)
                if (result != null) {
                    Async.forEach(result, (e, callback) => {
                        e.status = true;
                        event.push(e);
                        callback(null, event);
                    }, (err, data) => {
                        if (err)
                            return reject(dbErrResponse);
                        else {
                            return resolve(data);
                        }
                    });
                }
            });
        });
    }// get Events
    const getReferralCode = () => {
        return new Promise((resolve, reject) => {
            getReferralCodeAsync(6)
                .then(code => {
                    if (!code) {
                        return getReferralCodeAsync(6)
                            .then(code => {
                                resolve(code);
                            });//recursive
                    } else {
                        resolve(code);
                    }
                }).catch(e => {
                    logger.error("refreal code is not generate", e)
                });
        });
    }
    const getAppConfig = (data) => {
        return new Promise((resolve, reject) => {
            appConfig.read((err, res) => {
                if (err) {
                    return reject(dbErrResponse);
                } else {
                    radius = res.locateProvider || 0;
                    return resolve(data);
                }
            });
        });
    }
    const getCity = (data) => {
        return new Promise((resolve, reject) => {
            cities.read({ _id: new ObjectID(req.payload.cityId) }, (err, res) => {
                if (err) {
                    return reject(dbErrResponse);
                } else {
                    currencySymbol = res.currencySymbol || '$';
                    currency = res.currency || 'USD';
                    cityName = res.city || "";
                    return resolve(data);
                }
            });
        });
    }
    const insertData = (data) => {
        return new Promise((resolve, reject) => {
            let catlist = [];
            let catArr = req.payload.catlist.split(",");
            catArr.forEach(cat => {
                catlist.push({ cid: ObjectID(cat), status: 0 });
            });
            let userdata = {
                email: req.payload.email,
                password: Bcrypt.hashSync(req.payload.password, parseInt(configuration.SALT_ROUND)), //hash the password and store in db
                firstName: req.payload.firstName,
                lastName: req.payload.lastName || '',
                profilePic: req.payload.profilePic || '',
                phone: [{
                    countryCode: req.payload.countryCode,
                    phone: req.payload.mobile ? req.payload.mobile : "",
                    isCurrentlyActive: true
                }],
                cityId: req.payload.cityId,
                currencySymbol: currencySymbol || '$',
                currency: currency || 'USD',
                cityName: cityName,
                dob: req.payload.dob || '',
                link: req.payload.link || '',
                catlist: catlist,
                subCatlist: [],
                referralCode: data || "",
                radius: radius,
                status: 0,
                statusMsg: 'New',
                events: event || [],
                profileStatus: 1,
                location: { longitude: parseFloat(req.payload.longitude), latitude: parseFloat(req.payload.latitude) },
                createdDt: moment().unix(),
                createTs: new Timestamp(),
                createDate: new Date(),
                defualtLocation: { longitude: parseFloat(req.payload.longitude), latitude: parseFloat(req.payload.latitude) },

            };
            resolve(userdata);
        });
    }//response data

    const checkProvider = function (data) {
        return new Promise((resolve, reject) => {
            let condition = {
                $or: [{ email: data.email },
                {
                    'phone.phone': req.payload.mobile,
                    'phone.countryCode': req.payload.countryCode,
                    'phone.isCurrentlyActive': true
                }],
                status: { $ne: 0 }
            };

            provider.read(condition, (err, result) => {
                return err ? reject(dbErrResponse) : result ?
                    reject({ message: error['postSignUp']['412'][req.headers.lan], code: 412 }) : resolve(data);

            })
        });
    }//chack provider  by email id or mobile

    const getService = function (data) {
        return new Promise((resolve, reject) => {
            var servies = [];
            services.getByCatId((req.payload.catlist).toString(), (err, result) => {
                if (err)
                    return callback(err);
                if (result === null)
                    return callback(null, true);
                Async.forEach(result, function (res, callback1) {
                    var serviceData = {
                        id: res._id,
                        name: res.ser_name,
                        unit: res.unit,
                        price: parseInt(res.is_unit)
                    };
                    servies.push(serviceData);
                    callback1(null, servies);
                }, function (err, res) {
                    if (err)
                        reject(dbErrResponse);
                    data.services = servies;
                    resolve(data);
                });
            });
        });
    }//get service caegory by providerId

    const postData = function (data) {
        return new Promise((resolve, reject) => {
            provider.postSignUp(data, (err, response) => {
                if (err)
                    reject(dbErrResponse);
                let providerId = (response.value == null) ? response.lastErrorObject.upserted : response.value._id;
                let responseData = {
                    'firstName': req.payload.firstName ? req.payload.firstName : "",
                    'lastName': req.payload.lastName ? req.payload.lastName : "",
                    'profilePic': req.payload.profilePic ? req.payload.profilePic : "",
                    'status': 0,
                    'email': req.payload.email,
                    'mobile': req.payload.mobile,
                    'countryCode': req.payload.countryCode,
                    'dob': req.payload.dob ? req.payload.dob : "",
                };
                req.payload.userId = (providerId).toString();
                req.payload.userType = 2;//provider
                req.payload.status = 0;//online 
                updateLoginStatus(req.payload, (err, res) => { });//update login  status 

                postAddress(req.payload, (err, res) => { });
                data.userId = providerId;
                resolve(data);
            });
        });
    }//insert data and update login status

    const sendSms = function (data) {
        return new Promise((resolve, reject) => {
            let docData = {
                master_id: new ObjectID(data.userId),
                Fields: req.payload.document
            }
            documents.post(docData, (err, response) => { })
            var countryCode = req.payload.countryCode;
            var phoneNumber = req.payload.mobile;
            var randomnumber = (configuration.OTP == true) ? Math.floor(1000 + Math.random() * 9000) : 1111;
            verificationCode.updateByPhone(req.payload.mobile, (err, result) => {
                if (err)
                    reject(dbErrResponse);
                var condition = {
                    type: 1, // 1-mobile ,2- email
                    verificationCode: randomnumber,
                    generatedTime: moment().valueOf(),
                    expiryTime: moment().add(1, 'd').startOf('day').valueOf(),
                    triggeredBy: 2, // 1- forgot password ,2 - registration
                    maxAttempt: 0,
                    maxCount: 1,
                    userId: (data.userId).toString(),
                    userType: 2, // 1-slave, 2-master
                    givenInput: req.payload.mobile,
                    attempts: [],
                    status: true,
                    verified: false,
                };
                verificationCode.post(condition, (err, response) => {
                    if (err)
                        reject(dbErrResponse);
                    let sms = {
                        to: countryCode + phoneNumber,
                        body: " verification code" + randomnumber,
                        from: configuration.MAILGUN_FROM_NAME,
                        trigger: configuration.REGISTRATION_OTP,
                    }
                    rabbitmqUtil.InsertQueue(rabbitMq.getChannelSms(), rabbitMq.queueSms, sms, (err, doc) => {

                    });
                    // sendMsg.sendSms(sms, (e, cb) => { })
                    resolve(data);
                });
            });
        })
    }//send verification code


    getReferralCode()
        .then(getEvents)
        .then(getAppConfig)
        .then(getCity)
        .then(insertData)
        .then(checkProvider)
        .then(getService)
        .then(postData)
        .then(sendSms)
        .then(data => {
            let responseData = {
                providerId: (data.userId).toString(),
            };
            return reply({ message: error['postSignUp']['200'][req.headers.lan], data: responseData }).code(200);
        }).catch(e => {
            logger.error("Provider signup API error =>", e)
            return reply({ message: e.message }).code(e.code);
        });
};

const responseCode = {
    status: {
        200: { message: error['postSignUp']['200'][error['lang']], data: joi.any() },
        412: { message: error['postSignUp']['412'][error['lang']] },
        500: { message: error['genericErrMsg']['500'][error['lang']] },
        504: { message: error['postSignUp']['504'][error['lang']] }

    }

}//swagger response code



module.exports = {
    payloadValidator,
    APIHandler,
    responseCode
};