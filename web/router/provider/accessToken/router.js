'use strict';

const entity = '/provider';
const get = require('./get');
const error = require('../error');
const headerValidator = require('../../../middleware/validator');

module.exports = [

    /**
    * @name GET /app/accessToken
    */
    {
        method: 'GET',
        path: entity + '/accessToken',
        handler: get.APIHandler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['getAccessToken'],
            notes: 'This api to get access token based on refresh token.',
            auth: 'refJwt',
            response: get.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },

];