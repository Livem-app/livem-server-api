'use strict';

const entity = '/provider';
const error = require('../error');
const postResendOtp = require('./postResendOtp');
const postVerifyPhoneNumber = require('./postVerifyPhoneNumber');
const postForgotPassword = require('./postForgotPassword');
const postVerifyVerificationCode = require('./postVerifyVerificationCode');

const headerValidator = require('../../../middleware/validator');

module.exports = [

    /**
     * @name POST /provider/verifyPhoneNumber
     */
    {
        method: 'POST',
        path: entity + '/verifyPhoneNumber',
        handler: postVerifyPhoneNumber.APIHandler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['postVerifyPhoneNumber'],
            notes: 'This API verify the phone number after signup. ',
            auth: false,
            response: postVerifyPhoneNumber.responseCode,
            validate: {
                payload: postVerifyPhoneNumber.payloadValidator,
                headers: headerValidator.headerLanValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },

    /**
     * @name POST /customer/forgotPassword
     */
    {
        method: 'POST',
        path: entity + '/forgotPassword',
        handler: postForgotPassword.handler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['postForgotPassword'],
            notes: 'This API allows the user to reset the password. The user may mention his email ID or Phone Number in order to verify the account. In case of an email ID a link is sent to the email ID, in case of a mobile number a verification code is sent.',
            auth: false,
            response: postForgotPassword.responseCode,
            validate: {
                headers: headerValidator.headerLanValidator,
                payload: postForgotPassword.payload,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },

    /**
     * @name POST /customer/verifyVerificationCode
     */
    {
        method: 'POST',
        path: entity + '/verifyVerificationCode',
        handler: postVerifyVerificationCode.handler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['postVerifyVerificationCode'],
            notes: 'This API is used to verify the Verification code which the user receives from Twilio',
            auth: false,
            response: postVerifyVerificationCode.responseCode,
            validate: {
                headers: headerValidator.headerLanValidator,
                payload: postVerifyVerificationCode.payload,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },

    /**
    * @name POST /app/resendOtp
    */
    {
        method: 'POST',
        path: entity + '/resendOtp',
        handler: postResendOtp.APIHandler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['postResendOtp'], 
            notes: 'This API to send otp while user forgot password',
            auth: false,
            response: postResendOtp.responseCode,
            validate: {
                headers: headerValidator.headerLanValidator,
                payload: postResendOtp.payloadValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
];