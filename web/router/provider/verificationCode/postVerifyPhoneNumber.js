'use strict';
const joi = require('joi');
const error = require('../error');
const logger = require('winston');
const moment = require('moment');//date-time
var Bcrypt = require('bcrypt');//hashing module   

const ObjectID = require('mongodb').ObjectID;
const provider = require('../../../../models/provider');
const appConfig = require('../../../../models/appConfig');
const verificationCode = require('../../../../models/verificationCode');
const email = require('../../../commonModels/email/provider');
const userList = require('../../../commonModels/userList');
const zendesk = require('./../../../../models/zendesk');
const wallet = require('../../../commonModels/wallet');
const payloadValidator = joi.object({
    code: joi.number().integer().required().description('provide code'),
    providerId: joi.string().required().description('provider id'),
}).required();//payload of provider signin api

/**
 * @method POST /provider/verifyPhoneNumber
 * @description verify the phone number after signUp
 * @param {*} req 
 * @param {*} reply 
 * @property {integer} code -otp
 * @property {string} providerId -providerId
 */
const APIHandler = (req, reply) => {
    const dbErrResponse = { message: error['genericErrMsg']['500'][req.headers.lan], code: 500 };
    let zendeskId = "";
    const checkProvider = (data) => {
        return new Promise((resolve, reject) => {
            provider.getProviderById(req.payload.providerId, (e, r) => {
                return e ? reject(dbErrResponse) : (r == null) ?
                    reject({ message: error['VerifyPhoneNumber']['404'][req.headers.lan], code: 404 }) : resolve(r);
            });
        });
    }//check provider valid or not
    const verifyOtp = (data) => {
        return new Promise((resolve, reject) => {
            verificationCode.getByProviderId(req.payload.providerId, (err, d) => {
                if (err)
                    reject(dbErrResponse);
                if (d == null)
                    reject(dbErrResponse);
                if (d.verified == true)
                    reject({ message: error['VerifyPhoneNumber']['413'][req.headers.lan], code: 413 });
                appConfig.read((err, res) => {
                    var time = moment(d.generatedTime).add(500, 'seconds').valueOf();
                    if (time < moment().valueOf()) {
                        reject({ message: error['VerifyPhoneNumber']['410'][req.headers.lan], code: 410 });
                    }
                    else if (d.maxAttempt < res.maxAttempOtp) {
                        if (d.verificationCode == req.payload.code) {
                            var updateQuery = {
                                query: { userId: req.payload.providerId, status: true },
                                data: {
                                    $set: { maxAttempt: d.maxAttempt + 1, verified: true },
                                    $push: {
                                        attempts: {
                                            enteredValue: req.payload.code,
                                            verifiedOn: moment().valueOf(),
                                            success: true
                                        }
                                    }
                                },
                            }
                            verificationCode.updateAfterVerify(updateQuery, (err, res) => { })
                            provider.updateVerifyPhone(req.payload.providerId, (e, r) => {
                                return e ? reject(e) : resolve(data)
                            })
                        } else {
                            var updateQuery = {
                                query: { userId: req.payload.providerId, status: true },
                                data: {
                                    $set: { maxAttempt: d.maxAttempt + 1, verified: false },
                                    $push: {
                                        attempts: {
                                            enteredValue: req.payload.code,
                                            verifiedOn: moment().valueOf(),
                                            success: false
                                        }
                                    }
                                },
                            };
                            verificationCode.updateAfterVerify(updateQuery, (err, res) => {
                                return err ? reject(dbErrResponse) :
                                    reject({ message: error['VerifyPhoneNumber']['404'][req.headers.lan], code: 404 })
                            })
                        }
                    } else {
                        reject({ message: error['VerifyPhoneNumber']['429'][req.headers.lan], code: 429 })
                    }
                });
            })
        });
    }//verify otp
    const zendeskCreateUsaer = (data) => { 
        return new Promise((resolve, reject) => {
            var url = zendesk.config.zd_api_url + '/users.json';
            var dataArr = { "user": { "name": data.firstName, "email": data.email, "role": 'end-user' } };
            zendesk.users.post(dataArr, url, function (err, result) {
                if (err) { 
                    return reject(dbErrResponse);
                } else { 
                    zendeskId = result.user ? result.user.id : "";
                    return resolve(data);
                }
            });
        });
    }//creat zendesk user
    const createUserForWallet = (data) => { 
        return new Promise((resolve, reject) => {
            userList.createUser(
                req.payload.providerId,
                data.firstName,
                data.lastName,
                data.profilePic,
                data.phone[0].countryCode,
                data.phone[0].phone,
                2,
                '',
                data.mobileDevices.deviceType
            )//insert userlist table use for simple chat-module
            let dataArr = {
                userType: 2,
                userId: req.payload.providerId,
            };
            wallet.user.users.userCreate(dataArr, (err, walletId) => {
                if (err)
                    return reject(dbErrResponse);

                if (walletId == '')
                    return reject({ message: error['postSignUp']['504'][req.headers.lan], code: 504 });

                provider.findOneAndUpdate(req.payload.providerId,
                    {
                        $set: {
                            walletId: new ObjectID(walletId),
                            zendeskId: zendeskId
                        }
                    },
                    (err, res) => {
                        return err ? reject(dbErrResponse) : resolve(data);
                    });
            });
        });
    }

    checkProvider()
        .then(verifyOtp)
        .then(zendeskCreateUsaer)
        .then(createUserForWallet)
        .then(d => {
            let responseData = {
                'firstName': d.firstName ? d.firstName : "",
                'lastName': d.lastName ? d.lastName : "",
                'profilePic': d.profilePic ? d.profilePic : "",
                'status': 1,
                'email': d.email,
            };
            let params = {
                toName: d.firstName,
                to: d.email
            };
            email.signup(params);
            // email.welcomeMasterEmail(params);//send mail to provider
            return reply({ message: error['VerifyPhoneNumber']['200'][req.headers.lan], data: responseData }).code(200);
        }).catch(e => {
            logger.error("Provider VerifyPhoneNumber API error =>", e)
            return reply({ message: e.message }).code(e.code);
        })

};

const responseCode = {
    status: {
        500: { message: error['genericErrMsg']['500'][error['lang']] },
        400: { message: error['VerifyPhoneNumber']['400'][error['lang']] },
        404: { message: error['VerifyPhoneNumber']['404'][error['lang']] },
        410: { message: error['VerifyPhoneNumber']['410'][error['lang']] },
        413: { message: error['VerifyPhoneNumber']['413'][error['lang']] },
        429: { message: error['VerifyPhoneNumber']['429'][error['lang']] },
        200: {
            message: error['VerifyPhoneNumber']['200'][error['lang']],
            data: {
                firstName: joi.any().example("John"),
                lastName: joi.any().example("Shinha"),
                profilePic: joi.any().example('https://s3.amazonaws.com/livemapplication/Provider/ProfilePics/1509351606290_0_01.png'),
                email: joi.any().example("example@domain.com"),
                status: joi.any().example("1"),
            }
        },
    }

}//swagger response code


module.exports = {
    payloadValidator,
    APIHandler,
    responseCode
};