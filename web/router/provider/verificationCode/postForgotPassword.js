'use strict';

const joi = require('joi');
const error = require('../error');
const moment = require('moment');
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;
const email = require('../../../commonModels/email/provider');
const provider = require('../../../../models/provider');
const appConfig = require('../../../../models/appConfig');
const configuration = require('../../../../configuration')
const verificationCode = require('../../../../models/verificationCode');
const rabbitmqUtil = require('../../../../models/rabbitMq/twilio');
const rabbitMq = require('../../../../models/rabbitMq');


const payload = joi.object({
    emailOrPhone: joi.string().required().description('enter email or phone number'),
    countryCode: joi.string().description('country code'),
    userType: joi.number().integer().min(1).max(2).required().description('1- customer , 2- provider').error(new Error('User type is incorrect Please enter valid type')),
    type: joi.number().integer().min(1).max(2).required().description('1- phone , 2- email').error(new Error('Type is incorrect Please enter valid type')),
}).required();

const handler = (req, reply) => {
    const dbErrResponse = { message: error['genericErrMsg']['500'][req.headers.lan], code: 500 };
    const getProvider = (data) => {
        return new Promise((resolve, reject) => {
            const condition = (req.payload.type == 1) ?
                { 'phone.phone': req.payload.emailOrPhone, 'phone.countryCode': req.payload.countryCode, 'phone.isCurrentlyActive': true }
                : { email: req.payload.emailOrPhone };
            provider.read(condition, (err, res) => {
                if (err) {
                    return reject(dbErrResponse);
                }
                else if (res != null) {
                    if (res.status == 1) {
                        return reject({ message: error['postForgotPassword']['430'][req.headers.lan], code: 430 })
                    } else {
                        return resolve(res);
                    }

                } else {
                    return (req.payload.type == 1) ?
                        reject({ message: error['postForgotPassword']['406'][req.headers.lan], code: 406 })
                        : reject({ message: error['postForgotPassword']['409'][req.headers.lan], code: 409 });
                }
            });
        });
    }//get provider details

    const sendOtp = (data) => {
        return new Promise((resolve, reject) => {
            let phoneNumber;
            let countryCode;
            data.phone.forEach(function (e) {
                if (e.isCurrentlyActive == true) {
                    phoneNumber = e.phone;
                    countryCode = e.countryCode;
                }
            }, this);
            let randomnumber = (configuration.OTP == true) ? Math.floor(1000 + Math.random() * 9000) : 1111;
            let condition = { userId: data._id.toString(), 'expiryTime': { '$gt': moment().valueOf() } };
            verificationCode.readAllData(condition, (err, result) => {
                if (err)
                    return reject(dbErrResponse);
                appConfig.read((err, res) => { 
                    if (result.length < res.maxAttempForgotPassword || result === null) {
                        let condition = {
                            type: req.payload.type, // 1-phone ,2- email
                            verificationCode: randomnumber,
                            generatedTime: moment().valueOf(),
                            expiryTime: moment().add(1, 'd').startOf('day').valueOf(),
                            triggeredBy: 2, // 2- forgot password ,2 - registration
                            maxAttempt: 0,
                            maxCount: 1,
                            userId: data._id.toString(),
                            userType: req.payload.userType, // 1-slave, 2-master
                            givenInput: req.payload.emailOrPhone,
                            status: true,
                            verified: false,
                            attempts: []
                        };
                        let updateQuery = {
                            query: { givenInput: req.payload.emailOrPhone },
                            data: {
                                $set: { status: false }
                            },
                            options: { multi: true }
                        };
                        verificationCode.updateByPhone(req.payload.emailOrPhone, (err, result) => {
                            if (err)
                                return reject(dbErrResponse);
                            verificationCode.post(condition, (err, respons) => {
                                if (err)
                                    return reject(dbErrResponse);
                                switch (req.payload.type) {
                                    case 1:

                                    let paramsData = {
                                        to: countryCode + phoneNumber,
                                        body: "Verificaion code " + randomnumber,
                                        trigger: 'Forgot password'

                                    } 
                                    rabbitmqUtil.InsertQueue(rabbitMq.getChannelSms(), rabbitMq.queueSms, paramsData, (err, doc) => {

                                    });
                                        // smsGatway.sendSms(countryCode + phoneNumber, "Verificaion code " + randomnumber, 'Forgot password', 'log', (rs) => {

                                        return resolve(data._id);
                                        // });
                                        break;
                                    case 2:
                                        var params = {
                                            id: data._id,
                                            toName: data.firstName + " " + data.lastName,
                                            to: data.email,
                                            randnumber: randomnumber,
                                            userType: req.payload.userType
                                        };

                                        email.forgotPassword(params);
                                        return resolve(data._id);
                                        break;
                                }
                            });
                        });
                    } else {
                        return reject({ message: error['postForgotPassword']['429'][req.headers.lan], code: 429 })
                    }
                });
            });
        });
    }//send verification code

    getProvider()
        .then(sendOtp)
        .then(data => {
            return reply({ message: error['postForgotPassword']['200'][req.headers.lan], data: { sid: data } }).code(200);
        }).catch(e => {
            logger.error("Provider postForgotPassword error =>", e)
            return reply({ message: e.message }).code(e.code);
        });
};

const responseCode = {
    status: {
        200: { message: error['postForgotPassword']['200'][error['lang']], data: joi.any() },
        400: { message: error['postForgotPassword']['400'][error['lang']] },
        404: { message: error['postForgotPassword']['404'][error['lang']] },
        406: { message: error['postForgotPassword']['406'][error['lang']] },
        409: { message: error['postForgotPassword']['409'][error['lang']] },
        429: { message: error['postForgotPassword']['429'][error['lang']] },
        430: { message: error['postForgotPassword']['430'][error['lang']] },
        500: { message: error['genericErrMsg']['500'][error['lang']] }
    }
}//swagger response code


module.exports = {
    payload,
    handler,
    responseCode
};