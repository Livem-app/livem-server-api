'use strict';

const joi = require('joi');
const error = require('../error');
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;
const provider = require('../../../../models/provider');
const savedAddress = require('../../../../models/address');


const payloadValidator = joi.object({
    addLine1: joi.string().required().description('address line 1'),
    addLine2: joi.string().allow("").description('address line 2'),
    city: joi.string().allow("").description('city'),
    state: joi.string().allow("").description('state'),
    country: joi.string().allow("").description('country'),
    placeId: joi.string().allow("").description('placeId'),
    pincode: joi.any().allow("").description('pincode').error(new Error('Pincode must be number')),
    latitude: joi.number().required().description('latitude').error(new Error('Latitude must be number')),
    longitude: joi.number().required().description('longitude').error(new Error('Longitude must be number')),
    taggedAs: joi.string().allow("").required().description('office / home'),
    userType: joi.number().integer().min(1).max(2).description('1 - customer,2 - provider').error(new Error('Please enter valid user type')),

}).required();//validator

/**
 * @method POST /customer/address
 * @description insert saved address
 * @param {*} req 
 * @param {*} reply 
 * @property {string} addLine1 - Address Line 1
 * @property {string} addLine2 - Address Line 2
 * @property {string} city - city
 * @property {string} state - state
 * @property {string} country -country
 * @property {string} placeId -placeId
 * @property {number} pincode -pincode
 * @property {number} latitude
 * @property {number} longitude
 * @property {string} taggedAs
 * @property {number} userType
 * 
 */
const APIHandler = (req, reply) => {
    const postData = (data) => {
        return new Promise((resolve, reject) => {
            let dataArr = {
                addLine1: req.payload.addLine1 ? req.payload.addLine1 : '',
                addLine2: req.payload.addLine2 ? req.payload.addLine2 : '',
                city: req.payload.city ? req.payload.city : '',
                state: req.payload.state ? req.payload.state : '',
                country: req.payload.country ? req.payload.country : '',
                placeId: req.payload.placeId ? req.payload.placeId : '',
                pincode: req.payload.pincode ? req.payload.pincode : '',
                latitude: req.payload.latitude ? req.payload.latitude : '',
                longitude: req.payload.longitude ? req.payload.longitude : '',
                taggedAs: req.payload.taggedAs ? req.payload.taggedAs : '',
                userType: 2, // 1 -slave, 2-master
                user_Id: new ObjectID(req.auth.credentials._id), //get the id from the extracted token
                defaultAddress: 0
            };
            savedAddress.insert(dataArr, (err, res) => {
                return err ? reject({ message: error['genericErrMsg']['500'][req.headers.lan], code: 500 })
                    : resolve(res);
            });
        });
    }//insert data in saved address

    const updateAddressInCustomer = (data) => {
        return new Promise((resolve, reject) => {
            provider.updateAddress(req.auth.credentials._id, data, (err, res) => {
                return err ? reject({ message: error['genericErrMsg']['500'][req.headers.lan], code: 500 })
                    : resolve(res);
            });
        });
    }//update address id in customer

    postData()
        .then(updateAddressInCustomer)
        .then(data => {
            return reply({ message: error['postAddress']['200'][req.headers.lan] }).code(200);
        }).catch(e => {
            logger.error("customer postAddress API error =>", e)
            return reply({ message: e.message }).code(e.code);
        });
};

const responseCode = {
    status: {
        200: { message: error['postAddress']['200'][error['lang']] },
        500: { message: error['genericErrMsg']['500'][error['lang']] }
    }
}//swagger response code


module.exports = {
    payloadValidator,
    APIHandler,
    responseCode
};