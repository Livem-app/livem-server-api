'use strict';

const joi = require('joi');
const error = require('../error');
const logger = require('winston');
const provider = require('../../../../models/provider');
const savedAddress = require('../../../../models/address');

const paramsValidator = joi.object({
    id: joi.string().description('address id'),
}).required();//validator

/**
 * @method DELETE /provider/address
 * @description delete saved address
 * @param {*} req 
 * @param {*} reply 
 * @property {string} id - Address id
 */
const APIHandler = (req, reply) => {
    const deleteAddress = (data) => {
        return new Promise((resolve, reject) => {
            savedAddress.deleteById(req.params.id, (err, res) => {
                return err ? reject({ message: error['genericErrMsg']['500'][req.headers.lan], code: 500 })
                    : resolve(req.params.id);
            });
        });
    }//insert data in saved address

    const deleteAddressInProvider = (data) => {
        return new Promise((resolve, reject) => {
            provider.deleteAddress(req.auth.credentials._id, data, (err, res) => {
                return err ? reject({ message: error['genericErrMsg']['500'][req.headers.lan], code: 500 })
                    : resolve(res);
            });
        });
    }//delete address id in customer

    deleteAddress()
        .then(deleteAddressInProvider)
        .then(data => {
            return reply({ message: error['deleteAddress']['200'][req.headers.lan] }).code(200);
        }).catch(e => {
            logger.error("customer postAddress API error =>", e)
            return reply({ message: e.message }).code(e.code);
        });

};//API  logic

const responseCode = {
    status: {
        200: { message: error['deleteAddress']['200'][error['lang']] },
        500: { message: error['genericErrMsg']['500'][error['lang']] }
    }

}//swagger response code

module.exports = {
    APIHandler,
    responseCode,
    paramsValidator
};