'use strict';

const entity = '/provider';
const get =require('./get');
const post = require('./post');
const error = require('../error');
const patchById =require('./patch');
const deleteById = require('./delete');
const headerValidator = require('../../../middleware/validator');

module.exports = [
    /**
    * @name DELETE /prvider/address/{id}
    */
    {
        method: 'DELETE',
        path: entity + '/address/{id}',
        handler: deleteById.APIHandler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['deleteAddress'],
            notes: "500 - internal server error,\n\n 200 - success",
            auth: 'providerJWT',
            response: deleteById.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                params: deleteById.paramsValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },

    /**
    * @name POST /provider/address
    */
    {
        method: 'POST',
        path: entity + '/address',
        handler: post.APIHandler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['postAddress'],
            notes: "500 - internal server error,\n\n 200 - success",
            auth: 'providerJWT',
            response: post.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: post.payloadValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },

    /**
    * @name GET /provider/address
    */
    {
        method: 'GET',
        path: entity + '/address',
        handler: get.handler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['getAddress'],
            notes: "Get all saved address",
            auth: 'providerJWT',
            response: get.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
    
    /**
    * @name PATCH /provider/address
    */
    {
        method: 'PATCH',
        path: entity + '/address',
        handler: patchById.handler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['patchAddress'],
            notes: "Update address by address id",
            auth: 'providerJWT',
            response: patchById.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: patchById.payload,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
];