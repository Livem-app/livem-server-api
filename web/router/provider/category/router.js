'use strict';

const entity = '/provider';
const get = require('./get');
const error = require('../error');
const headerValidator = require('../../../middleware/validator');

module.exports = [
    /**
      * @name GET /provider/serviceCateogries/{cityId}
      */
    {
        method: 'GET',
        path: entity + '/serviceCateogries/{cityId}',
        handler: get.APIHandler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['getServiceCategory'],
            notes: " This API is used to fetch the different active categories available in that city , this API will also return the necessary documents ( mandatory and non-mandatory ) that the provider needs to upload when he registers on the app  \n\
                     Type -1 : Text Box , Type -2 : Date & Time , Type -3 : Upload Document. ", // We use Joi plugin to validate request 
            auth: false,
            response: get.responseCode,
            validate: {
                headers: headerValidator.headerLanValidator,
                params: get.paramsValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }

        }
    },


];