
'use strict';
const joi = require('joi');
const error = require('../error');
const Async = require('async');
const logger = require('winston');
const category = require('../../../../models/category');
const documentTypes = require('../../../../models/documentTypes');
const services = require('../../../../models/services')

const paramsValidator = joi.object({
    cityId: joi.string().required().description('city id').error(new Error('cityId is missing'))
}).required();//params of provider serviceCategory api

/**
 * @method GET /provider/serviceCateogries/{cityId}
 * @description available categories in city 
 * @param {*} req 
 * @param {*} reply 
 * @property {string} cityId
 */
const APIHandler = (req, reply) => {
    let Hourly = [];
    category.getByCityId(req.params.cityId, (err, result) => {
        if (err)
            return reply({ message: error['genericErrMsg']['500'][req.headers.lan] }).code(500);
        if (result.length === 0)
            return reply({ message: error['getServiceCategory']['404'][req.headers.lan] }).code(404);
        Async.forEach(result, (cat, callback) => {
            try { 
               
                services.getByCatId(cat._id, (err, res) => {
                    if (res.length != 0) { 
                        documentTypes.getByCategoryId(cat._id, (err, res) => {
                            if (err)
                                callback(e);
                            if (res === null)
                                callback(null, res);
                            Hourly.push({ catId: cat._id, catName: cat.cat_name, document: res });
                            callback(null, Hourly);
                        });
                    } else {
                        callback(null, Hourly);
                    }
                })

            } catch (e) {
                callback(e, null);
            }
        }, (err, data) => {
            if (err)
                return reply({ message: error['genericErrMsg']['500'][req.headers.lan] }).code(500);
            else if (data === null)
                return reply({ message: error['getServiceCategory']['404'][req.headers.lan] }).code(404);
            else
                return reply({ message: error['getServiceCategory']['200'][req.headers.lan], data: Hourly }).code(200);
        });
    });
};

const responseCode = {
    status: {
        200: {
            message: error['getServiceCategory']['200'][error['lang']],
            data: joi.any().example([{ catId: 'string', catName: 'string', document: [] }])
        },
        404: { message: error['getServiceCategory']['404'][error['lang']] },
        500: { message: error['genericErrMsg']['500'][error['lang']] }
    }

}//swagger response code

module.exports = {
    APIHandler,
    responseCode,
    paramsValidator
};