'use strict';

const joi = require('joi');
const error = require('../error'); 
const moment = require('moment');
const logger = require('winston');
const sendMsg = require('../../../../library/twilio');
const provider = require('../../../../models/provider'); 
const configuration = require('../../../../configuration')
const verificationCode = require('../../../../models/verificationCode');


const payloadValidator = joi.object({
    userType: joi.number().required().integer().min(1).max(2).required().description('1- slave , 2- master').error(new Error('User type is incorrect Please enter valid type')),
    countryCode: joi.string().required().description('country code'),
    phone: joi.string().required().description('phone').error(new Error('phone nubmer invalid please enter correct'))
}).required();

let APIHandler = (req, reply) => {
    const dbErrResponse = { message: error['genericErrMsg']['500'][req.headers.lan], code: 500 };
    const checkCustomerAndUpdate = (data) => {
        return new Promise((resolve, reject) => {
            provider.read({ 'phone.phone': req.payload.phone }, (err, res) => {
                if (err)
                    return reject(dbErrResponse);
                else if (res != null) {
                    return reject({ message: error['patchPhoneNumber']['412'][req.headers.lan], code: 412 });
                } else {
                    let dataQury = {
                        $set:
                        {
                            changePhone: req.payload.phone,
                            changeCountryCode: req.payload.countryCode
                        }
                    };
                    provider.findOneAndUpdate(req.auth.credentials._id, dataQury, (err, res) => {
                        return err ? reject(err) : resolve(res);
                    });
                }
            });
        });
    }
    const sendOtp = (data) => {
        return new Promise((resolve, reject) => {
            var randomnumber = (configuration.OTP == true) ? Math.floor(1000 + Math.random() * 9000) : 1111;
            var condition = {
                type: req.payload.type, // 1-phone ,2- email
                verificationCode: randomnumber,
                generatedTime: moment().valueOf(),
                expiryTime: moment().add(1, 'd').startOf('day').valueOf(),
                triggeredBy: 3, // 1- forgot password ,2 - registration
                maxAttempt: 0,
                maxCount: 1,
                userId: req.auth.credentials._id,
                userType: 2, // 1-slave, 2-master
                givenInput: req.payload.phone,
                status: true,
                verified: false,
                attempts: []
            };
            verificationCode.updateById(req.auth.credentials._id, (err, result) => {
                if (err)
                    reject(dbErrResponse);
                verificationCode.post(condition, (err, response) => {
                    if (err)
                        reject(dbErrResponse);
                    let sms = {
                        to: req.payload.countryCode + req.payload.phone,
                        body: " verification code" + randomnumber,
                        from: configuration.MAILGUN_FROM_NAME,
                        trigger: configuration.REGISTRATION_OTP,
                    }
                    sendMsg.sendSms(sms, (e, cb) => { })
                    let data = {
                        sid: req.auth.credentials._id,
                    };
                    resolve(data);
                });
            });
        });
    }
    checkCustomerAndUpdate()
        .then(sendOtp)
        .then(data => {
            return reply({ message: error['patchPhoneNumber']['200'][req.headers.lan], data: data }).code(200);
        }).catch(e => {
            logger.error("Provider post review and rating API error =>", e)
            return reply({ message: e.message }).code(e.code);
        });


};

const responseCode = {
    status: {
        200: { message: error['patchPhoneNumber']['200'][error['lang']], data: joi.any() },
        412: { message: error['patchPhoneNumber']['412'][error['lang']] },
        500: { message: error['genericErrMsg']['500'][error['lang']] }
    }
}//swagger response code


module.exports = {
    payloadValidator,
    APIHandler,
    responseCode
};