
'use strict';
const joi = require('joi');
const error = require('../error');
const Async = require('async');
const logger = require('winston');
const category = require('../../../../models/category');
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId
const provider = require('../../../../models/provider');
const events = require('../../../../models/events');
const cities = require('../../../../models/cities');
const address = require('../../../../models/address');

const APIHandler = (req, reply) => {

    let responseDataArr;
    let defualtAddress;
    let currencySymbol = '$';
    let currency = 'USD';
    let getProvider = function () {
        return new Promise((resolve, reject) => {
            provider.getProviderById(req.auth.credentials._id, (err, provider) => {
                const dbErrResponse = { message: error['genericErrMsg']['500'][req.headers.lan], code: 500 };
                return err ? reject(dbErrResponse) : resolve(provider);
            });
        });
    }
    const getCity = (data) => {
        return new Promise((resolve, reject) => {
            cities.read({ _id: new ObjectID(data.cityId) }, (err, res) => {
                if (err) {
                    return reject({ message: error['genericErrMsg']['500'], code: 500 })
                } else {
                    currencySymbol = res.currencySymbol;
                    currency = res.currency;
                    return resolve(data);
                }
            });
        });
    }
    const getAddress = (data) => {
        return new Promise((resolve, reject) => {
            address.read({ defaultAddress: 1, user_Id: new ObjectID(req.auth.credentials._id) },
                (err, res) => {
                    if (err) {

                        return resolve(data);
                    } else if (res) {
                        defualtAddress = res.addLine1 || "";
                        return resolve(data);
                    } else {
                        return resolve(data);
                    }

                });
        });
    }
    let responseData = function (data) {
        return new Promise((resolve, reject) => {
            if (data === null)
                reject({ message: error['getProfile']['404'][req.headers.lan], code: 404 })
            let obj = { items: data.phone };
            let arrayFoundPhn = obj.items.myFind({ "isCurrentlyActive": true });
            responseDataArr = {
                firstName: data.firstName || '',
                lastName: data.lastName || '',
                dob: data.dob != null ? data.dob : "",
                profilePic: data.profilePic != null ? data.profilePic : "",
                mobile: arrayFoundPhn[0].phone || '',
                countryCode: arrayFoundPhn[0].countryCode || '',
                email: data.email,
                services: data.services || [],
                about: data.about || "",
                link: data.link || "",
                rules: data.rules || "",
                instruments: data.instruments || "",
                profileStatus: data.profileStatus || 0,
                radius: data.radius || "",
                address: defualtAddress || "",
                musicGeners: data.musicGeners || "",
                minRadius: 5,
                maxRadius: 30,
                events: data.events || [],
                currency: currency,
                currencySymbol: currencySymbol
            };
            resolve(data);
        });
    }
    const getCategory = (data) => {
        return new Promise((resolve, reject) => {
            category.read({ _id: new ObjectID(data.catlist[0].cid) }, (err, catData) => {
                if (catData != null) {
                    responseDataArr.catName = catData.cat_name || '';
                    resolve(responseDataArr);
                } else {
                    resolve(responseDataArr);
                }
            });
        });
    }
    let getEvents = function (data) {
        return new Promise((resolve, reject) => {
            events.readAll((err, result) => {
                let event = [];
                let eventIS = false;
                Async.forEach(result, function (e, callback1) {
                    if (data.events != undefined && data.events != '') {
                        eventIS = false;
                        Async.forEach(data.events, function (proEvent, callback) {
                            var proEvent = (proEvent._id).toString();
                            var EventId = (e._id).toString();
                            if (proEvent == EventId) {
                                e.status = true;
                                eventIS = true;
                                event.push(e);
                            }
                            callback(null, true);
                        }, function (err, data1) {
                            if (err)
                                return callback(err, null);
                            if (eventIS != true) {
                                e.status = false
                                event.push(e);
                            }
                            callback1(null, event);
                        });
                    } else {
                        e.status = false
                        event.push(e);
                        callback1(null, event);
                    }
                }, function (err, data1) {
                    if (err)
                        return reject(dbErrResponse);
                    data.events = event;
                    return resolve(data);
                });

            })
        });
    }
    getProvider()
        .then(getCity)
        .then(getAddress)
        .then(responseData)
        .then(getCategory)
        .then(getEvents)
        .then(data => {
            return reply({ message: error['getProfile']['200'][req.headers.lan], data: data }).code(200);
        }).catch(e => {
            logger.error("Provider get profile API error =>", e)
            return reply({ message: e.message }).code(e.code);
        })

};

const responseCode = {
    status: {
        500: { message: error['genericErrMsg']['500'][error['lang']] },
        200: {
            message: error['getProfile']['200'][error['lang']],
            data: joi.any()
        },
        404: { message: error['getProfile']['404'][error['lang']] },
    }

}//swagger response code

module.exports = {
    APIHandler,
    responseCode
};