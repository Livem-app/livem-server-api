'use strict';

const entity = '/provider';
const error = require('../error');
const getProfile = require('./get');
const patchProfile = require('./patch');
const patchEmail = require('./patchEmail');
const patchLocation = require('./patchLocation');
const patchLocationLogs = require('./patchLocationLogs');
const patchStatus = require('./patchStatus');
const patchPasswordMe = require('./patchPasswordMe');
const patchPassword = require('./patchPassword');
const patchPhoneNumber = require('./patchPhoneNumber');
const headerValidator = require('../../../middleware/validator');

module.exports = [
    
    /**
     * @name GET /provider/profile
     */
    {
        method: 'GET',
        path: entity + '/profile/me',
        handler: getProfile.APIHandler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['getProfile'],
            notes: 'This API is to get the information of the uer on his Profile Page',
            auth: 'providerJWT',
            // response: getProfile.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },

    /**
    * @name PATCH /provider/profile
    */
    {
        method: 'PATCH',
        path: entity + '/profile/me',
        handler: patchProfile.APIHandler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['patchProfile'],
            notes: 'This API enables the user to edit his details on the profile Page.',
            auth: 'providerJWT',
            response: patchProfile.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: patchProfile.payloadValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },

    /**
    * @name PATCH /app/email
    */
    {
        method: 'PATCH',
        path: entity + '/email',
        handler: patchEmail.APIHandler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['patchEmail'],
            notes: "This API enables the user to change his email addresss.",
            auth: 'providerJWT',
            // response: patchEmail.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: patchEmail.payloadValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
    /**
     * @name PATCH /provider/location
     */
    {
        method: 'PATCH',
        path: entity + '/location',
        handler: patchLocation.APIHandler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['patchLocation'], 
            auth: 'providerJWT',
            response: patchLocation.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: patchLocation.payloadValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
     /**
     * @name PATCH /provider/location
     */
    {
        method: 'PATCH',
        path: entity + '/locationLogs',
        handler: patchLocationLogs.APIHandler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['patchLocation'], 
            auth: 'providerJWT',
            // response: patchLocationLogs.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: patchLocationLogs.payloadValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
    /**
     * @name PATCH /provider/status
    */
    {
        method: 'PATCH',
        path: entity + '/status',
        handler: patchStatus.APIHandler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['patchStatus'], 
            notes: "update provider status",
            auth: 'providerJWT',
            // response: patchStatus.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: patchStatus.payloadValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
  
    /**
     * @name PATCH /provider/password/me
     */
    {
        method: 'PATCH',
        path: entity + '/password/me',
        handler: patchPasswordMe.APIHandler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['patchPasswordMe'], 
            notes: "api to change password by user.",
            auth: 'providerJWT',
            response: patchPasswordMe.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: patchPasswordMe.payloadValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
    /**
     * @name PATCH /app/password
     */
    {
        method: 'PATCH',
        path: entity + '/password',
        handler: patchPassword.APIHandler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['patchPassword'], 
            notes: 'This API allows a user to reset password.',
            auth: false,
            response: patchPassword.responseCode,
            validate: {
                headers: headerValidator.headerLanValidator,
                payload: patchPassword.payloadValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
    /**
     * @name PATCH /app/phoneNumber
     */
    {
        method: 'PATCH',
        path: entity + '/phoneNumber',
        handler: patchPhoneNumber.APIHandler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['patchPhoneNumber'], 
            notes: 'This API enables the user to change his phne number.It is accepted once the phone number is verified with a verification code.',
            auth: 'providerJWT',
            // response: patchPhoneNumber.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: patchPhoneNumber.payloadValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },

];