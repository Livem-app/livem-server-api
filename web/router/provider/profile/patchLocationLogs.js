
'use strict';
const joi = require('joi');
const error = require('../error');
const util = require('util');
const moment = require('moment');
const Async = require('async');//asynchronous module 
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId
const dispatcher = require('../../../commonModels/dispatcher');
const redis = require('../../../../models/redis');
const provider = require('../../../../models/provider');
const assignedBookings = require('../../../../models/assignedBookings');
const providerLocationLog = require('../../../../models/providerLocationLog');
const appConfig = require('../../../../models/appConfig');

const payloadValidator = joi.object({
    latitude_longitude: joi.any().required().description('latitude longitude ex:{"13.000,14.13123","13.000,14.13123"}').error(new Error('latitude longitude is missing ')),

}).required();//payload of provider update profile api

/**
 * @method PATCH /provider/location
 * @description update provider loaction
 * @param {*} req 
 * @param {*} reply 
 */
const APIHandler = (req, reply) => {
    assignedBookings.readAll({ providerId: req.auth.credentials._id }, (err, res) => {
        if (err)
            return reply({ message: error['genericErrMsg']['500'][req.headers.lan] }).code(500);
        if (res.length != 0) {
            var latLongArr = req.payload.latitude_longitude;

            for (var key = 0; key < res.length; key++) {

                var logData = {};
                logData[res[key].status.toString()] = { $each: latLongArr };

                
                let data = {
                    $push: logData
                }
                providerLocationLog.updateByBookingId(res[key].bookingId, data, (err, res) => {
                    if (err)
                        return reply({ message: error['genericErrMsg']['500'][req.headers.lan] }).code(500);
                    else
                        return reply({ message: error['patchLocation']['200'][req.headers.lan] }).code(200);
                });

            }
        }
    });



};

const responseCode = {
    // status: {
    //     200: { message: error['patchLocation']['200'][error['lang']] },
    //     500: { message: error['genericErrMsg']['500'][error['lang']] }
    // }
}//swagger response code

module.exports = {
    payloadValidator,
    APIHandler,
    responseCode
};