
'use strict';
const joi = require('joi');
const error = require('../error');
const util = require('util');
const moment = require('moment');
const Async = require('async');//asynchronous module 
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId
const Timestamp = require('mongodb').Timestamp;
const provider = require('../../../../models/provider');
const providerOnlineLogs = require('../../../../models/providerOnlineLogs');
const providerDailyOnlineLogs = require('../../../../models/providerDailyOnlineLogs');
const bookings = require('../../../../models/bookings');
const redis = require('../../../../models/redis');
const dispatcher = require('../../../commonModels/dispatcher');

const rabbitmqUtil = require("../../../../models/rabbitMq/payroll");
const rabbitMq = require("../../../../models/rabbitMq");

const payloadValidator = joi.object({
    status: joi.number().required().integer().min(3).max(4).required().description('3- Online , 4- Offline').error(new Error('status is incorrect')),
    latitude: joi.string().description('latitude').error(new Error('latitude is missing')),
    longitude: joi.string().description('longitude').error(new Error('longitude is missing')),
}).required();//payload of provider update profile api


function providerOnline(req) {
    var st = req.payload.status == 3 ? 'online' : 'offline'
    var updateQuery = {
        providerId: req.auth.credentials._id,
        status: st,
        latitude: parseFloat(req.payload.latitude),
        longitude: parseFloat(req.payload.longitude)
    };
    providerOnlineLogs.updateByProviderId(updateQuery, (err, res) => {
    });

}
function providerOnlineLogsForDate(req) {

    let timestamp = moment().unix();
    if (req.payload.status == 3) {
        let dailyQueryObj = {
            query: { providerId: new ObjectID(req.auth.credentials._id), date: moment().format('DD-MMM-YY'), timestamp: moment().startOf('day').unix() },
            data: {
                $set: { lastOnline: timestamp },
                $push: { logs: { timeStamp: timestamp, status: parseInt(req.payload.status), interval: 0 } }
            }
        };
        providerDailyOnlineLogs.updateByProviderId(dailyQueryObj, (err, res) => { });
    } else {
        providerDailyOnlineLogs.read({ providerId: new ObjectID(req.auth.credentials._id), date: moment().format('DD-MMM-YY'), timestamp: moment().startOf('day').unix() }, (err, doc) => {
            let lastOnline = doc ? doc.lastOnline || timestamp : timestamp;
            let totalOnline = doc ? doc.totalOnline || 0 : 0;
            totalOnline += (timestamp - lastOnline);
            let queryObj = {
                query: { providerId: new ObjectID(req.auth.credentials._id), date: moment().format('DD-MMM-YY'), timestamp: moment().startOf('day').unix() },
                data: {
                    $set: { totalOnline: totalOnline },
                    $push: {
                        logs: { timeStamp: timestamp, status: parseInt(req.payload.status), interval: timestamp - lastOnline },
                        shifts: { startTs: lastOnline, endTs: timestamp, interval: timestamp - lastOnline }
                    }
                }
            };
            providerDailyOnlineLogs.updateByProviderId(queryObj, (err, res) => { });
        })
    }


}
/**
 * @method PATCH /provider/status
 * @description update provider status like online or offline
 * @param {*} req 
 * @param {*} reply 
 */
const APIHandler = (req, reply) => {
    let dataToUpdate = {};
    let updateQuery;
    const dbErrResponse = { message: error['genericErrMsg']['500'][req.headers.lan], code: 500 };
    const getProvider = (data) => {
        return new Promise((resolve, reject) => {
            provider.getProviderById(req.auth.credentials._id, (err, provider) => {
                if (err) {
                    return reject(dbErrResponse);
                } else {
                    return resolve(provider);
                }

            });
        });
    }//get provider details

    const getStatus = (data) => {
        return new Promise((resolve, reject) => {
            dataToUpdate.status = parseInt(req.payload.status);
            dataToUpdate.statusMsg = parseInt(req.payload.status) == 3 ? 'Online' : 'Offline';
            dataToUpdate.statusUpdateTime = new Timestamp(1, moment().unix());
            switch (parseInt(req.payload.status)) {
                case 3:
                    if (data.profileStatus == 0) {
                        return reject({ message: error['patchStatus']['421'][req.headers.lan], code: 421 })
                    } else {
                        return resolve(data)
                    }
                    break;
                case 4:
                    if (data.defualtLocation) {
                        dataToUpdate.location =
                            {
                                longitude: data.defualtLocation.longitude ? data.defualtLocation.longitude : 0,
                                latitude: data.defualtLocation.latitude ? data.defualtLocation.latitude : 0
                            };
                    }
                    redis.delteSet('presence_' + req.auth.credentials._id);
                    return resolve(data)
                    break;
                default:
                    return reject(dbErrResponse)
                    break
            }

        });
    }

    const updateStatus = (data) => {
        return new Promise((resolve, reject) => {
            providerOnline(req);//log for each provider
            providerOnlineLogsForDate(req);//log for date wise 
            // rabbitmqUtil.InsertQueuePayroll(rabbitMq.getChannelPayroll(), rabbitMq.QueuePayroll, { id: req.auth.credentials._id }, () => { });
            provider.updateProfile(req.auth.credentials._id, dataToUpdate, (err, res) => {
                if (err) {
                    return reject(dbErrResponse)
                }
                else {
                    dispatcher.providerStatus({ _id: req.auth.credentials._id }, (err, res) => {
                    });
                    return resolve(data);
                }
            });
        });
    }

    getProvider()
        .then(getStatus)
        .then(updateStatus)
        .then(data => {
            return reply({ message: error['patchStatus']['200'][req.headers.lan] }).code(200);
        }).catch(e => {
            logger.error("provider update status API error =>", e)
            return reply({ message: e.message }).code(e.code);
        })

};

const responseCode = {
    status: {
        200: { message: error['patchStatus']['200'][error['lang']] },
        421: { message: error['patchStatus']['421'][error['lang']] },
        500: { message: error['genericErrMsg']['500'][error['lang']] }
    }
}//swagger response code

module.exports = {
    payloadValidator,
    APIHandler,
    responseCode
};