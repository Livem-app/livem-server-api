
'use strict';
const joi = require('joi');
const error = require('../error'); 
const util = require('util');
const Bcrypt = require('bcrypt');
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId
const events = require('../../../../models/events');
const provider = require('../../../../models/provider');
const configuration = require('../../../../configuration')

const payloadValidator = joi.object({
    oldPassword: joi.string().required().description('old password'),
    newPassword: joi.string().required().description('new password'),
}).required();//payload of provider update profile api

/**
 * @method PATCH /provider/password/me
 * @description change password based on old passowrd
 * @param {*} req 
 * @param {*} reply 
 * @property {string} oldPassword - current password
 * @property {string} newPassword - new passowrd
 */
const APIHandler = (req, reply) => {
    const dbErrResponse = { message: error['genericErrMsg']['500'][req.headers.lan], code: 500 };
    let dataToUpdate = {};

    const getProvider = function () {
        return new Promise((resolve, reject) => {
            provider.getProviderById(req.auth.credentials._id, (err, provider) => {
                return err ? reject(dbErrResponse) : resolve(provider);
            });
        });
    }//get provider
    let getPassword = function (data) {
        return new Promise((resolve, reject) => {
            if (data === null)
                reject({ message: error['patchPassword']['404'][req.headers.lan], code: 404 })
            let isPasswordValid = Bcrypt.compareSync(req.payload.oldPassword, data.password);
            if (isPasswordValid) {
                let password = Bcrypt.hashSync(req.payload.newPassword, parseInt(configuration.SALT_ROUND))
                provider.updateProfile(req.auth.credentials._id, { password: password }, (err, res) => {
                    return err ? reject(err) : resolve(data);
                })
            } else {
                return reply({ message: error['patchPassword']['401'][req.headers.lan] }).code(401);
            }
        });
    }//update password

    getProvider()
        .then(getPassword)
        .then(data => {
            return reply({ message: error['patchPassword']['200'][req.headers.lan] }).code(200);
        }).catch(e => {
            logger.error("Provider update password API error =>", e)
            return reply({ message: e.message }).code(e.code);
        })

};

const responseCode = {
    status: {
        200: { message: error['patchPassword']['200'][error['lang']] },
        404: { message: error['patchPassword']['404'][error['lang']] },
        404: { message: error['patchPassword']['401'][error['lang']] },
        500: { message: error['genericErrMsg']['500'][error['lang']] }
    }
}//swagger response code

module.exports = {
    payloadValidator,
    APIHandler,
    responseCode
};