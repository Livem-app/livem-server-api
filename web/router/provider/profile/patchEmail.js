'use strict';
const joi = require('joi'); 
const error = require('../error'); 
const logger = require('winston');  
const provider = require('../../../../models/provider'); 


const payloadValidator = joi.object({
    userType: joi.any().description('1- slave , 2- master').error(new Error('User type is incorrect Please enter valid type')),
    email: joi.any().description('email addresss').error(new Error('email is invalid PPlease enter correct')),
}).required();

let APIHandler = (req, reply) => {
    provider.read({ 'email': req.payload.email }, (err, res) => {
        if (err)
            return reply({ message: error['genericErrMsg']['500'][req.headers.lan] }).code(500);
        if (res != null) {
            return reply({ message: error['patchEmail']['412'][req.headers.lan] }).code(412);
        } else {
            provider.findOneAndUpdate(req.auth.credentials._id, { $set: { email: req.payload.email } }, (err, res) => {
                if (err)
                    return reply({ message: error['genericErrMsg']['500'][req.headers.lan] }).code(500);
                else {
                    return reply({ message: error['patchEmail']['200'][req.headers.lan] }).code(200);
                }
            });
        }
    });

};

const responseCode = {
    status: {
        200: { message: error['patchEmail']['200'][error['lang']] },
        412: { message: error['patchEmail']['412'][error['lang']] },
        500: { message: error['genericErrMsg']['500'][error['lang']] }
    }
}//swagger response code


module.exports = { payloadValidator, APIHandler, responseCode };