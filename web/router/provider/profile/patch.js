
'use strict';
const joi = require('joi');
const error = require('../error');
const Async = require('async');
const util = require('util');
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId
const events = require('../../../../models/events');
const provider = require('../../../../models/provider');
const address = require('../../../../models/address');


const payloadValidator = joi.object({
    firstName: joi.any().description('firstName'),
    lastName: joi.any().description('last name'),
    countryCode: joi.string().description('country code'),
    profilePic: joi.any().description('profile pic'),
    dob: joi.any().description('Date of birth format : YYYY-MM-DD'),
    about: joi.any().description('about'),
    link: joi.any().description('Youtube/Video link'),
    rules: joi.any().description('rules'),
    instruments: joi.any().description('instruments'),
    profileStatus: joi.number().min(0).max(1).description('profile status 0: in-active 1:active'),
    radius: joi.number().description('radius'),
    address: joi.string().description('address id'),
    musicGeners: joi.string().description('music geners'),
    events: joi.any().description('events id comma (,)seperated'),
    service: joi.array().description('[{"id":"SERVICE_ID","price" : "PRICE"}]')
}).required();//payload of provider update profile api

/**
 * @method PATCH /provider/profile/me
 * @description update profile data
 * @param {*} req 
 * @param {*} reply 
 */
const APIHandler = (req, reply) => {
    const dbErrResponse = { message: error['genericErrMsg']['500'][req.headers.lan], code: 500 };
    let dataToUpdate = {};
    let defualtLat = "";
    let defualtLon = "";
    const getProvider = (data) => {
        return new Promise((resolve, reject) => {
            provider.getProviderById(req.auth.credentials._id, (err, provider) => {
                return err ? reject(dbErrResponse) : resolve(provider);
            });
        });
    }//get provider details

    const getServices = (data) => {
        return new Promise((resolve, reject) => {
            if (data === null)
                reject({ message: error['patchProfile']['404'][req.headers.lan], code: 404 })
            var services = [];
            if (!(util.isNullOrUndefined(req.payload.service))) {
                var service = req.payload.service;
                try {
                    Async.forEach(data.services, function (serPro, callback) {
                        var match = false;
                        var priceNew;
                        Async.forEach(service, function (ser, callback1) {
                            if ((serPro.id).toString() == (ser.id).toString()) {
                                services.push({ id: ObjectID(serPro.id), name: serPro.name, unit: serPro.unit, price: parseInt(ser.price) });
                            }
                            callback1(null, services);
                        }, function (err, data) {
                            if (err)
                                return cb(null, true);
                            callback(null, true);
                        });

                    }, function (err, data) {
                        if (err)
                            reject(dbErrResponse)
                        dataToUpdate.services = services;
                        resolve(data);
                    });
                } catch (e) {
                    reject(dbErrResponse)
                }
            } else {
                resolve(data);
            }
        });
    }//get service category

    const getEvents = (data) => {
        return new Promise((resolve, reject) => {
            var event = [];
            if (!(util.isNullOrUndefined(req.payload.events))) {
                var selectEvent = (req.payload.events).split(',');
                events.readAll((err, result) => {
                    if (err)
                        reject(dbErrResponse)
                    if (result != null) {
                        Async.forEach(selectEvent, (proEvent, callback1) => {
                            Async.forEach(result, (e, callback) => {
                                if (proEvent == (e._id).toString()) {
                                    e.status = true;
                                    event.push(e);
                                }
                                callback(null, event);
                            }, (err, data) => {
                                if (err)
                                    return callback(err, null);
                                callback1(null, data);
                            });
                        }, (err, data) => {
                            if (err)
                                reject(dbErrResponse)
                            dataToUpdate.events = event;
                            resolve(data);
                        });
                    }
                });
            } else {
                resolve(data);
            }
        });
    }// get Events

    const getAddress = (data) => {
        return new Promise((resolve, reject) => {
            if (!(util.isNullOrUndefined(req.payload.address))) {
                address.read({ _id: new ObjectID(req.payload.address) }, (err, res) => {
                    if (err) {
                        return reject(dbErrResponse);
                    } else {
                        defualtLat = res.latitude;
                        defualtLon = res.longitude;
                        address.setDefult(req.payload.address, req.auth.credentials._id, (err, res) => { });
                        return resolve(res);
                    }
                });
            } else {
                resolve(data);
            }
        });
    }
    const updateData = (data) => {
        return new Promise((resolve, reject) => {
            if (typeof req.payload.dob != 'undefined')
                dataToUpdate.dob = req.payload.dob;
            if (typeof req.payload.firstName != 'undefined')
                dataToUpdate.firstName = req.payload.firstName;
            if (typeof req.payload.lastName != 'undefined')
                dataToUpdate.lastName = req.payload.lastName;
            if (typeof req.payload.about != 'undefined')
                dataToUpdate.about = req.payload.about;
            if (typeof req.payload.link != 'undefined')
                dataToUpdate.link = req.payload.link;
            if (typeof req.payload.rules != 'undefined')
                dataToUpdate.rules = req.payload.rules;
            if (typeof req.payload.instruments != 'undefined')
                dataToUpdate.instruments = req.payload.instruments;
            if (typeof req.payload.profileStatus != 'undefined') {
                dataToUpdate.profileStatus = req.payload.profileStatus;
                if (req.payload.profileStatus == 0) {
                    dataToUpdate.status = 4;
                }
            }

            if (typeof req.payload.radius != 'undefined')
                dataToUpdate.radius = req.payload.radius;
            if (typeof req.payload.address != 'undefined')
                dataToUpdate.address = req.payload.address;
            if (typeof req.payload.musicGeners != 'undefined')
                dataToUpdate.musicGeners = req.payload.musicGeners;
            if (typeof req.payload.profilePic != 'undefined')
                dataToUpdate.profilePic = req.payload.profilePic;
            if (defualtLat != '' && typeof defualtLon != '') {
                dataToUpdate.defualtLocation = {
                    longitude: parseFloat(defualtLon),
                    latitude: parseFloat(defualtLat)
                };
            }
            if (typeof req.payload.address != 'undefined') {
                dataToUpdate.defaultAddressId = req.payload.address;
            }
            if (Object.keys(dataToUpdate).length === 0) {
                reject({ message: error['patchProfile']['400'][req.headers.lan], code: 400 });
            } else { 
                provider.updateProfile(req.auth.credentials._id, dataToUpdate, (err, res) => {
                    return err ? reject(err) : resolve(data);
                })
            }
        });
    }//update data

    getProvider()
        .then(getServices)
        .then(getEvents)
        .then(getAddress)
        .then(updateData)
        .then(data => {
            return reply({ message: error['patchProfile']['200'][req.headers.lan] }).code(200);
        }).catch(e => {
            logger.error("Provider update profile API error =>", e)
            return reply({ message: e.message }).code(e.code);
        })

};

const responseCode = {
    status: {
        200: { message: error['patchProfile']['200']['1'] },
        400: { message: error['patchProfile']['400']['1'] },
        404: { message: error['patchProfile']['404']['1'] },
        500: { message: error['genericErrMsg']['500']['1'] }
    }
}//swagger response code

module.exports = {
    payloadValidator,
    APIHandler,
    responseCode
};