
'use strict';
const joi = require('joi');
const error = require('../error');
const util = require('util');
const moment = require('moment');
const Async = require('async');//asynchronous module 
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId
const Timestamp = require('mongodb').Timestamp;
const dispatcher = require('../../../commonModels/dispatcher');
const redis = require('../../../../models/redis');
const provider = require('../../../../models/provider');
const bookings = require('../../../../models/bookings');
const providerLocationLog = require('../../../../models/providerLocationLog');
const appConfig = require('../../../../models/appConfig');


const rabbitmqUtil = require("../../../../models/rabbitMq/payroll");
const rabbitMq = require("../../../../models/rabbitMq");

const payloadValidator = joi.object({
    latitude: joi.number().required().description('latitude').error(new Error('latitude is missing')),
    longitude: joi.number().required().description('longitude').error(new Error('longitude is missing')),
    batteryPercentage: joi.number().description('batteryPercentage').error(new Error('Battery Percentage is missing')),
    status: joi.number().required().integer().min(1).max(5).required().description('3- Online , 4- Offline').error(new Error('status is incorrect')),
    locationHeading: joi.string().description('locationHeading').error(new Error('location Heading is missing')),
    locationCheck: joi.number().description('locationCheck').error(new Error('Location publish 1:on 2:off')),
    appVersion: joi.string().description('App Version'),
    transit: joi.any().description('0- , 1- ').error(new Error('transit is incorrect')),
    bookingStr: joi.any().description('On-going Booking BookingId/Status ').error(new Error('bookingStr is incorrect')),
}).required();//payload of provider update profile api

/**
 * @method PATCH /provider/location
 * @description update provider loaction
 * @param {*} req 
 * @param {*} reply 
 */
const APIHandler = (req, reply) => {
    rabbitmqUtil.InsertQueuePayroll(rabbitMq.getChannelPayroll(), rabbitMq.QueuePayroll, { id: req.auth.credentials._id }, () => { });

    if (req.payload.bookingStr && req.payload.bookingStr != 'undefined' && (req.payload.bookingStr) != "") {
        var PublishData = req.payload.bookingStr.split(",");//might be in multiple bookings
        for (var key = 0; key < PublishData.length; key++) {

            var getOneElement = PublishData[key].split("|");
            var ltlg = req.payload.latitude + "," + req.payload.longitude;
            var logData = {};
            logData[getOneElement[1]] = ltlg;

            let data = {
                $set: { latitude: req.payload.latitude || '', longitude: req.payload.longitude || '' },
                $push: logData
            }
            providerLocationLog.updateByBookingId(parseFloat(getOneElement[0]), data, (err, res) => { });

        }

    }
    dispatcher.liveTrack({
        _id: req.auth.credentials._id,
        longitude: parseFloat(req.payload.longitude),
        latitude: parseFloat(req.payload.latitude)
    }, (err, res) => {
    });
    provider.getProviderById(req.auth.credentials._id, (err, res) => {
        if (err) {
            return reply({ message: error['genericErrMsg']['500'][req.headers.lan] }).code(500);
        } else {
            if (res.status == 9) {
                let updateQuery = {
                    loggedIn: false, accessCode: ''
                };
                statusUpdateTime: new Timestamp(1, moment().unix());
                provider.updateProfile(req.auth.credentials._id, updateQuery, (err, res) => {
                    if (err)
                        return reply({ message: error['genericErrMsg']['500'][req.headers.lan] }).code(500);
                    else {
                        dispatcher.providerStatus({ _id: req.auth.credentials._id }, (err, res) => { });
                        return reply({ message: error['patchLocation']['200'][req.headers.lan] }).code(200);
                    }

                });
            } else if (res.status == 4 || res.profileStatus == 0) {
                let updateQuery = {
                    "status": 4,
                    "statusMsg": "Offline",
                    statusUpdateTime: new Timestamp(1, moment().unix()),
                    location:
                        {
                            longitude: res.defualtLocation.longitude ? res.defualtLocation.longitude : 0,
                            latitude: res.defualtLocation.latitude ? res.defualtLocation.latitude : 0
                        },
                };
                provider.updateProfile(req.auth.credentials._id, updateQuery, (err, res) => {
                    if (err)
                        return reply({ message: error['genericErrMsg']['500'][req.headers.lan] }).code(500);
                    else {
                        dispatcher.providerStatus({ _id: req.auth.credentials._id }, (err, res) => { });
                        return reply({ message: error['patchLocation']['200'][req.headers.lan] }).code(200);
                    }

                });

            } else {
                let updateQuery = {
                    location: {
                        longitude: parseFloat(req.payload.longitude),
                        latitude: parseFloat(req.payload.latitude)
                    },
                    status: 3,
                    statusMsg: 'Online',
                    locationHeading: req.payload.locationHeading,
                    locationCheck: req.payload.locationCheck,
                    lastActive: moment().unix(),
                    statusUpdateTime: new Timestamp(1, moment().unix()),
                    "mobileDevices.batteryPercentage": req.payload.batteryPercentage,
                    "mobileDevices.appversion": req.payload.appVersion
                };

                appConfig.read((err, res) => {
                    redis.setex('presence_' + req.auth.credentials._id, res.proTimeOut ? res.proTimeOut : 60, req.auth.credentials._id);
                });


                dispatcher.providerStatus({ _id: req.auth.credentials._id }, (err, res) => {
                });

                if (req.payload.transit == 1 || req.payload.transit == 0) {
                    // liveTrack(req.auth.credentials._id, req.payload.latitude, req.payload.longitude, (err, cbData) => {
                    // }); 
                    provider.updateProfile(req.auth.credentials._id, updateQuery, (err, res) => {
                        if (err)
                            return reply({ message: error['genericErrMsg']['500'][req.headers.lan] }).code(500);
                        else
                            return reply({ message: error['patchLocation']['200'][req.headers.lan] }).code(200);
                    });
                } else {
                    return reply({ message: error['patchLocation']['200'][req.headers.lan] }).code(200);
                }
            }
        }
    });


};

const responseCode = {
    status: {
        200: { message: error['patchLocation']['200'][error['lang']] },
        500: { message: error['genericErrMsg']['500'][error['lang']] }
    }
}//swagger response code

module.exports = {
    payloadValidator,
    APIHandler,
    responseCode
};