
'use strict';
const joi = require('joi');
const error = require('../error');
const logger = require('winston');
const Async = require('async');
const _ = require('underscore-node');
const supportTxt = require('../../../../models/supportTxt');

const params = joi.object({
    userType: joi.number().integer().min(1).max(2).required().description('1- customer , 2- provider').error(new Error('User type is incorrect Please enter valid type')),
}).required();

/**
 * @method GET /customer/support/
 * @param {*} req 
 * @param {*} reply 
 */
const handler = (req, reply) => {
    const getSupport = () => {
        return new Promise((resolve, reject) => {
            supportTxt.readAll('2', (err, support) => {
                return err ? reject({ message: error['genericErrMsg']['500'][req.headers.lan], code: 500 })
                    : resolve(support);
            });
        });
    }//get support txt by usertype

    const responseData = (data) => {
        return new Promise((resolve, reject) => {
            var suportTxt = [];
            Async.forEach(data, (itemCat, callbackloopv) => {
                var sp = [];
                var lang = req.headers.lan - 1;
                if (itemCat.has_scat && itemCat.has_scat == true) {
                    Async.forEach(itemCat.sub_cat, (item, callbackloop) => {
                        sp.push({ 'Name': item.name[0], "desc": item.desc[0], 'link': "https://admin-test.livem.today/index.php?/utilities/getsubDescription/" + itemCat.cat_id + "/" + item.id.toString() + "/" + req.headers.lan });
                        callbackloop(null);
                    }, (loopErr) => {

                    });
                }
                suportTxt[itemCat.cat_id] = { 'Name': itemCat.name[0] || '', 'subcat': sp, 'desc': typeof itemCat.desc != "undefined" ? itemCat.desc[lang] : '', 'link': "https://admin-test.livem.today/index.php?/utilities/getDescription/" + itemCat.cat_id + "/" + req.headers.lan };
                callbackloopv(null);
            }, (loopErr) => {
                resolve(suportTxt);
            });
        });
    }//response data

    getSupport()
        .then(responseData)
        .then(data => {
            return reply({ message: error['genericErrMsg']['200'][req.headers.lan], data: _.without(data, null) }).code(200);
        }).catch(e => {
            logger.error("customer getProviderDetails API error =>", e)
            return reply({ message: e.message }).code(e.code);
        });


};

const responseCode = {
    status: {
        200: { message: error['genericErrMsg']['200'][error['lang']], data: joi.any() },
        500: { message: error['genericErrMsg']['500'][error['lang']] }
    }

}//swagger response code

module.exports = {
    params,
    handler,
    responseCode
};
