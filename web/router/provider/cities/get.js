
'use strict';
const joi = require('joi');
const error = require('../error');
const logger = require('winston');
const cities = require('../../../../models/cities');//cities collection model 

/**
 * @method GET /provider/city
 * @description This API used to fetch the different active city and select the city at the time of sign up
 * @param {*} req  
 * @param {*} reply 
 * @property {integer} lan 1:English
 * 
 */
const APIHandler = (req, reply) => {
    const getCity = function () {
        return new Promise((resolve, reject) => {
            let cityResult = [];
            cities.readAll((err, result) => {
                const dbErrResponse = { message: error['genericErrMsg']['500'][req.headers.lan], code: 500 };
                if (err)
                    reject(dbErrResponse)
                result.forEach(city => {
                    cityResult.push({ id: city._id, city: city.city });
                });
                resolve(cityResult);
            })
        });
    }//get all city array of object

    getCity()
        .then(data => {
            return reply({ message: error['getCity']['200'][req.headers.lan], data: data }).code(200);
        }).catch(e => {
            logger.error("Provider get cities API error =>", e)
            return reply({ message: e.message }).code(e.code);
        })
};

const responseCode = {
    status: {
        200: { message: error['getCity']['200'][error['lang']], data: joi.any() },
        500: { message: error['genericErrMsg']['500'][error['lang']] },
    }

}//swagger response code

module.exports = {
    APIHandler,
    responseCode
};