'use strict';

const entity = '/provider';
const get = require('./get');
const error = require('../error');
const headerValidator = require('../../../middleware/validator');

module.exports = [
    /**
     * @name GET /provider/city
     */
    {
        method: 'GET',
        path: entity + '/city',
        handler: get.APIHandler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['getCity'],
            notes: "get all city list  ",
            auth: false,
            response: get.responseCode,
            validate: {
                headers: headerValidator.headerLanValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },


];