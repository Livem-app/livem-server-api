'use strict';
const joi = require('joi');
const error = require('../error');
var Async = require('async');
const moment = require('moment');
const logger = require('winston');  
const ObjectID = require('mongodb').ObjectID;
const configuration = require('../../../../configuration'); 
const stripeMethods = require('../../../../library/stripe');
const provider = require('../../../../models/provider');


const payloadValidator = joi.object({
    account_number: joi.string().required().description('000123456').default('000123456789'),
    routing_number: joi.string().required().description('110000').default('110000000'),
    account_holder_name: joi.string().required().description('account holder name').default('Chethan'),
    country: joi.string().required().description('country').default('US'),
}).required();

let APIHandler = (req, reply) => {
    const dbErrResponse = { message: error['genericErrMsg']['500'][req.headers.lan], code: 500 };

    const checkUser = () => {
        return new Promise((resolve, reject) => {
            provider.read({ _id: new ObjectID(req.auth.credentials._id) }, (e, r) => {
                return e ? reject(dbErrResponse) : (r == null) ?
                    reject({ message: error['postStripeBank']['404'][req.headers.lan], code: 404 }) : resolve(r);
            });
        });
    }
    const addBank = (data) => {
        return new Promise((resolve, reject) => {
            stripeMethods.addBankAccount(data.stripeAccountId, req.payload, (err, banckAccount) => {
                return err ? reject(dbErrResponse) : resolve(banckAccount);
            });
        });

    }

    checkUser() 
        .then(addBank)
        .then(d => {
            return reply({ message: error['postStripeBank']['200'][req.headers.lan] }).code(200);
        }).catch(e => {
            logger.error("provider postStripeBank API error =>", e)
            return reply({ message: e.message }).code(e.code);
        })
};

const responseCode = {
    status: {
        200: { message: error['postStripeBank']['200'][error['lang']] },
        404: { message: error['postStripeBank']['404'][error['lang']] },
        500: { message: error['genericErrMsg']['500'][error['lang']] }
    }
}//swagger response code


module.exports = {
    payloadValidator,
    APIHandler,
    responseCode
};