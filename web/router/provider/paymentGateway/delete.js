'use strict';
const joi = require('joi');
const error = require('../error');
var Async = require('async');
const moment = require('moment');
const logger = require('winston'); 
const configuration = require('../../../../configuration'); 
const stripeMethods = require('../../../../library/stripe');
const provider = require('../../../../models/provider');
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId


const payloadValidator = joi.object({
    id: joi.string().required().description('000123456'),
}).required();

let APIHandler = (req, reply) => {
    const dbErrResponse = { message: error['genericErrMsg']['500'][req.headers.lan], code: 500 };

    const checkUser = () => {
        return new Promise((resolve, reject) => {
            provider.read({ _id: new ObjectID(req.auth.credentials._id) }, (e, r) => {
                return e ? reject(dbErrResponse) : (r == null) ?
                    reject({ message: error['deleteStripe']['404'][req.headers.lan], code: 404 }) : resolve(r);
            });
        });
    }
    const addBankDefault = (data) => {
        return new Promise((resolve, reject) => {
            stripeMethods.deleteBankAccount(data.stripeAccountId, req.payload.id, (err, confirmation) => {
                if (err) {
                    if (err.statusCode === 400)
                        return reject({ message: error['deleteStripe']['400'][req.headers.lan], code: 400 });
                    else if (err.statusCode === 404)
                        return reject({ message: error['deleteStripe']['404'][req.headers.lan], code: 404 });
                    else
                        return reject(dbErrResponse);
                } else
                    return resolve(confirmation);
            });
        });

    }

    checkUser()
        .then(addBankDefault)
        .then(d => {
            return reply({ message: error['deleteStripe']['200'][req.headers.lan], data: d }).code(200);
        }).catch(e => {
            logger.error("provider stripeDefualtBank API error =>", e)
            return reply({ message: e.message }).code(e.code);
        })
};

const responseCode = {
    status: {
        200: { message: error['deleteStripe']['200'][error['lang']], data: joi.any() },
        404: { message: error['deleteStripe']['404'][error['lang']] },
        400: { message: error['deleteStripe']['400'][error['lang']] },
        500: { message: error['genericErrMsg']['500'][error['lang']] }
    }
}//swagger response code


module.exports = {
    payloadValidator,
    APIHandler,
    responseCode
};