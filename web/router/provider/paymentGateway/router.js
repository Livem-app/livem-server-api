'use strict';

const entity = '/provider';
const get = require('./get');
const post = require('./post');
const error = require('../error');
const deleteById = require('./delete');
const postBank = require('./postBank');
const postDefault = require('./postDefault');
const headerValidator = require('../../../middleware/validator');

module.exports = [
    /**
     * @name POST /provider/stripe/me
     */
    {
        method: 'POST', // Methods Type
        path: entity + '/stripe/me',
        handler: post.APIHandler,
        config: {
            tags: ['api', entity],
            description: '',
            description: error['apiDescription']['postStripe'],  
            auth: 'providerJWT',
            response: post.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: post.payloadValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },

    /**
     * @name POST /provider/stripe/bank/me
    */
    {
        method: 'POST',
        path: entity + '/stripe/bank/me',
        handler: postBank.APIHandler,
        config: {
            tags: ['api', entity], 
            description: error['apiDescription']['postStripeBank'], 
            notes: "Add bank account to master\'s stripe account",
            auth: 'providerJWT',
            response: postBank.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: postBank.payloadValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },

    /**
     * @name GET /provider/stripe/me
     */
    {
        method: 'GET', // Methods Type
        path: entity + '/stripe/me',
        handler: get.APIHandler,
        config: {
            tags: ['api', entity], 
            description: error['apiDescription']['getStripe'], 
            notes: "Get stripe account details of a provider",
            auth: 'providerJWT',
            response: get.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },

    /**
     * @name POST /provider/stripe/bankdefault/me
    */
    {
        method: 'POST',
        path: entity + '/stripe/bankdefault/me',
        handler: postDefault.APIHandler,
        config: {
            tags: ['api', entity], 
            description: error['apiDescription']['postStripeDefault'], 
            notes: "Make a bank account default in provider\'s stripe account",
            auth: 'providerJWT',
            response: postDefault.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: postDefault.payloadValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
    /**
     * @name DELETE /provider/stripe/bank/me
    */
    {
        method: 'DELETE',
        path: entity + '/stripe/bank/me',
        handler: deleteById.APIHandler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['deleteStripe'],  
            notes: "Delete bank account from master\'s stripe account",
            auth: 'providerJWT',
            response: deleteById.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: deleteById.payloadValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
];