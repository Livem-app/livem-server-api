'use strict';
const joi = require('joi');
const error = require('../error');
const moment = require('moment');
const logger = require('winston');  
const ObjectID = require('mongodb').ObjectID; 
const provider = require('../../../../models/provider');
const configuration = require('../../../../configuration'); 
const stripeMethods = require('../../../../library/stripe');


const payloadValidator = joi.object({
    city: joi.string().required().description('city name'),
    country: joi.string().required().description('US'),
    line1: joi.string().required().description('address line 1'),
    postal_code: joi.string().required().description('post code'),
    state: joi.string().required().description('state'),

    day: joi.string().required().description('Day of your DOB'),
    month: joi.string().required().description('Month of Your DOB'),
    year: joi.string().required().description('Year of Your DOB'),

    first_name: joi.string().required().description('first Name'),
    last_name: joi.string().required().description('Last Name'),

    document: joi.string().required().description('Document Image'),
    personal_id_number: joi.string().required().description('9 digits American Id Number'),
    date: joi.string().required().description('your device date'),
    ip: joi.string().required().description('Nertwork ip address')
}).required();


function generateNewStripeAccountId(user, country, callback) {
    stripeMethods.createAccount({ type: 'custom', country: country, email: user.email }, (err, account) => {
        if (err) {
            return callback(err);
        }
        else {
            let condition = { $set: { stripeAccountId: account.id } };
            provider.findOneAndUpdate(user._id, condition, (err, doc) => {
                if (err)
                    return callback(err);
                else
                    return callback(null, account.id);
            });
        }
    });

}
let APIHandler = (req, reply) => {
    const dbErrResponse = { message: error['genericErrMsg']['500'][req.headers.lan], code: 500 };

    let checkUser = () => {
        return new Promise((resolve, reject) => {
            provider.read({ _id: new ObjectID(req.auth.credentials._id) }, (e, r) => {
                return e ? reject(dbErrResponse) : (r == null) ?
                    reject({ message: error['postStripe']['404'][req.headers.lan], code: 404 }) : resolve(r);
            });
        });
    }
    let retriveId = (data) => {
        return new Promise((resolve, reject) => {

            if (typeof data.stripeAccountId === 'undefined' || data.stripeAccountId === '' || data.stripeAccountId === null) {
                generateNewStripeAccountId(data, req.payload.country, (err, id) => {
                    return err ? reject(dbErrResponse) : resolve(id);
                });
            }//if the stripeAccountId is not present, generate & get the new stripeAccountId

            else {
                stripeMethods.retrieveAccount(data.stripeAccountId, (err, account) => {
                    if (err) {
                        if (err.statusCode === 404) {

                            generateNewStripeAccountId(data, req.payload.country, (err, id) => {
                                return err ? reject(dbErrResponse) : resolve(id);
                            });
                        }//if the id is not found, generate a new stripeAccountId for the master
                        else
                            return reject(dbErrResponse);
                    } else if (account.deleted === true) {
                        generateNewStripeAccountId(master, req.payload.country, (err, id) => {
                            return err ? reject(dbErrResponse) : resolve(id);
                        });
                    }//stripe sets deleted flag
                    else {
                        return resolve(data.stripeAccountId);
                    }

                });
            }
        });
    }

    const addBank = (data) => {
        return new Promise((resolve, reject) => {
            let dataAdd = {
                legal_entity: {
                    address: {
                        city: req.payload.city,
                        country: req.payload.country,
                        line1: req.payload.line1,
                        postal_code: req.payload.postal_code,
                        state: req.payload.state
                    },
                    dob: {
                        day: req.payload.day,
                        month: req.payload.month,
                        year: req.payload.year
                    },
                    first_name: req.payload.first_name,
                    last_name: req.payload.last_name
                }
            };

            stripeMethods.updateAccount(data, dataAdd, (err, account) => {
                return err ? reject(dbErrResponse) : resolve(account);
            });
        });
    }

    checkUser()
        .then(retriveId)
        .then(addBank)
        .then(d => {
            return reply({ message: error['postStripe']['200'][req.headers.lan] }).code(200);
        }).catch(e => {
            logger.error("customer postStripeCard API error =>", e)
            return reply({ message: e.message }).code(e.code);
        })
};

const responseCode = {
    status: {
        200: { message: error['postStripe']['200'][error['lang']] },
        404: { message: error['postStripe']['404'][error['lang']] },
        500: { message: error['genericErrMsg']['500'][error['lang']] }
    }
}//swagger response code


module.exports = {
    payloadValidator,
    APIHandler,
    responseCode
};