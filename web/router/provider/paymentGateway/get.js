'use strict';
const joi = require('joi');
const error = require('../error');
var Async = require('async');
const moment = require('moment');
const logger = require('winston'); 
const stripeMethods = require('../../../../library/stripe');
const provider = require('../../../../models/provider');
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId

let APIHandler = (req, reply) => {
    const dbErrResponse = { message: error['genericErrMsg']['500'][req.headers.lan], code: 500 };

    let checkUser = () => {
        return new Promise((resolve, reject) => {
            provider.read({ _id: new ObjectID(req.auth.credentials._id) }, (e, r) => {
                return e ? reject(dbErrResponse) : (r == null) ?
                    reject({ message: error['getStripe']['404'][req.headers.lan], code: 404 }) : resolve(r);
            });
        });
    }
    let retriveId = (data) => {
        return new Promise((resolve, reject) => {
            if (typeof data.stripeAccountId === 'undefined' || data.stripeAccountId === '') {
                return reject({ message: error['getStripe']['401'][req.headers.lan], code: 401 });
            } else
                stripeMethods.retrieveAccount(data.stripeAccountId, (err, account) => {
                    return err ? reject(err) : resolve(account);
                });
        });
    }


    checkUser()
        .then(retriveId)
        .then(d => {
            return reply({ message: error['getStripe']['200'][req.headers.lan], data: d }).code(200);
        }).catch(e => {
            logger.error("customer postStripeCard API error =>", e)
            return reply({ message: e.message }).code(e.code);
        })
};

const responseCode = {
    status: {
        200: { message: error['getStripe']['200'][error['lang']], data: joi.any() },
        401: { message: error['getStripe']['401'][error['lang']] },
        404: { message: error['getStripe']['404'][error['lang']] },
        500: { message: error['genericErrMsg']['500'][error['lang']] }
    }
}//swagger response code


module.exports = {
    APIHandler,
    responseCode
};