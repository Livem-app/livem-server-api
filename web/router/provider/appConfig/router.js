'use strict';

const get = require('./get');   
const error = require('../error');
const headerValidator = require('../../../middleware/validator'); 

const entity = '/provider'; 

module.exports = [
    
    /**
     * @name GET /app/config
     */
    {
        method: 'GET',
        path: entity + '/config',
        handler: get.handler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['getConfig'],
            notes: 'This API allows the user get configuration details.',
            auth: 'providerJWT',
            response: get.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
    
];