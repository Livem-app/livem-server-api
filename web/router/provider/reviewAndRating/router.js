'use strict';

const entity = '/provider'; 
const get =require('./get');
const post =require('./post');
const error = require('../error');
const headerValidator = require('../../../middleware/validator');

module.exports = [
    /**
     * @name GET /app/reviewAndRating/{pageNo}
     */
    {
        method: 'GET',
        path: entity + '/reviewAndRating/{pageNo}',
        handler: get.handler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['getReviewAndRating'],
            notes: "This API is get all the review and rating.",
            auth: 'providerJWT',
            response: get.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                params: get.params,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
    /**
     * @name POST /app/reviewAndRating
     */
    {
        method: 'POST',
        path: entity + '/reviewAndRating',
        handler: post.handler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['postReviewAndRating'],
            notes: "This API is  for the user to add review and rating perticuler booking.",
            auth: 'providerJWT',
            response: post.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: post.payload,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
];