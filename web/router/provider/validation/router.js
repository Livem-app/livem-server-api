'use strict';

const entity = '/provider';
const error = require('../error');
const postPhone = require('./postPhone');
const postEmail = require('./postEmail');
const headerValidator = require('../../../middleware/validator');

module.exports = [

    /**
    * @name POST /provider/phoneValidation
    */
    {
        method: 'POST',
        path: entity + '/phoneValidation',
        handler: postPhone.APIHandler,
        config: {
            tags: ['api', entity],
            description: error['apiDescription']['postPhoneValidation'],
            notes: "verify phone number API Error code ", // We use Joi plugin to validate request
            auth: false,
            response: postPhone.responseCode,
            validate: {
                headers: headerValidator.headerLanValidator,
                payload: postPhone.payloadValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }

        }

    },
    /**
     * @name POST /provider/emailValidation
    */
    {
        method: 'POST',
        path: entity + '/emailValidation',
        handler: postEmail.APIHandler,
        config: {// "tags" enable swagger to document API
            tags: ['api', entity],
            description: error['apiDescription']['postEmailValidation'],
            notes: "verify email ",
            auth: false,
            response: postEmail.responseCode,
            validate: {
                headers: headerValidator.headerLanValidator,
                payload: postEmail.payloadValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        },

    },



];