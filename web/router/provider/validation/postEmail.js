'use strict';
const joi = require('joi');
const error = require('../error');
var Async = require('async');
const logger = require('winston');
const provider = require('../../../../models/provider'); 

const payloadValidator = joi.object({
    email: joi.string().email().required().description('email').error(new Error('Email is missing or incorrect')),
}).required();//payload of provider signin api

/**
 * @method POST /provider/emailValidation
 * @description Email id already register or not
 * @param {*} req 
 * @param {*} reply 
 * @property {string} email -email address
 */
const APIHandler = (req, reply) => {
    var condition = { email: req.payload.email , status: { $ne: 0 }};
    provider.read(condition, (err, result) => {
        if (err)
            return reply({ message: error['genericErrMsg']['500'][req.headers.lan] }).code(500);
        if (result === null)
            return reply({ message: error['postEmailValidation']['200'][req.headers.lan] }).code(200);

        return reply({ message: error['postEmailValidation']['412'][req.headers.lan] }).code(412);
    });
};

const responseCode = {
    status: {
        200: { message: error['postEmailValidation']['200'][error['lang']] },
        412: { message: error['postEmailValidation']['412'][error['lang']] },
        500: { message: error['genericErrMsg']['500'][error['lang']] }
    }
}//swagger response code


module.exports = {
    payloadValidator,
    APIHandler,
    responseCode
};