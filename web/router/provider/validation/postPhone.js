'use strict';
const joi = require('joi');
const error = require('../error');
const Async = require('async');
const logger = require('winston');
const provider = require('../../../../models/provider'); 
const phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();//phone number validate


const payloadValidator = joi.object({
    countryCode: joi.string().required().description('country code').error(new Error('country code is missing')),
    mobile: joi.string().required().description('mobile number').error(new Error('mobile number is missing')),
}).required();//payload of provider signin api

/**
 * @method POST /provider/phoneValidation
 * @description phone number chack availabe or not
 * @param {*} req 
 * @param {*} reply 
 * @property {string} countryCode
 * @property {string} mobile
 */
let APIHandler = (req, reply) => {
    Async.waterfall([
        function (cb) {
            let phoneNumber = req.payload.countryCode + req.payload.mobile;
            try {
                let isValid = phoneUtil.parse(phoneNumber, null);
                if (phoneUtil.isValidNumber(isValid)) {
                    cb(null, true);
                } else {
                    return reply({ message: error['postPhoneValidation']['406'][req.headers.lan] }).code(406);
                }
            } catch (e) {
                return reply({ message: error['postPhoneValidation']['406'][req.headers.lan] }).code(406);
            }
        },
        function (d, cb) {
            if (typeof req.payload.phone === 'undefined' && req.payload.phone != '' && req.payload.phone != null)
                return reply({ message: error['postPhoneValidation']['400'][req.headers.lan] }).code(400);
            let condition = { 'phone.phone': req.payload.mobile, 'phone.countryCode': req.payload.countryCode, 'phone.isCurrentlyActive': true , status: { $ne: 0 }};
            provider.read(condition, (err, result) => {
                if (err)
                    return reply({ message: error['genericErrMsg']['500'][req.headers.lan] }).code(500);
                if (result === null) {
                    return reply({ message: error['postPhoneValidation']['200'][req.headers.lan] }).code(200);
                } else {
                    return reply({ message: error['postPhoneValidation']['412'][req.headers.lan] }).code(412);
                }
            });
        }
    ], (err, res) => {
        if (err)
            return reply({ message: error['genericErrMsg']['500'][req.headers.lan] }).code(500);
        return reply({ message: error['postPhoneValidation']['200'][req.headers.lan] }).code(200);
    });

};

const responseCode = {
    status: {
        200: { message: error['postPhoneValidation']['200'][error['lang']] },
        412: { message: error['postPhoneValidation']['412'][error['lang']] },
        400: { message: error['postPhoneValidation']['400'][error['lang']] },
        406: { message: error['postPhoneValidation']['406'][error['lang']] },
        500: { message: error['genericErrMsg']['500'][error['lang']] }
    }
}//swagger response code


module.exports = {
    payloadValidator,
    APIHandler,
    responseCode
};