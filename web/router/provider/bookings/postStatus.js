'use strict';
const joi = require('joi');
const Async = require('async');
const error = require('../error');
const logger = require('winston');
const moment = require('moment-timezone');
const ObjectID = require('mongodb').ObjectID;
const bookingMsg = require('../../../commonModels/notificationMsg/bookingMsg');
const fcm = require('../../../../library/fcm');
const redis = require('../../../../models/redis');
const mqtt_module = require('../../../../models/MQTT');
const bookings = require('../../../../models/bookings');
const provider = require('../../../../models/provider');
const webSocket = require('../../../../models/webSocket');
const appConfig = require('../../../../models/appConfig');
const customer = require('../../../../models/customer');
const assignedBookings = require('../../../../models/assignedBookings');
const unassignedBookings = require('../../../../models/unassignedBookings');
const dispatchedBookings = require('../../../../models/dispatchedBookings');
const email = require('../../../commonModels/email/customer');
const stripe = require('../../../commonModels/stripe');
// const transcation = require('../../../commonModels/wallet/transcation')
const rabbitmqUtil = require('../../../../models/rabbitMq/wallet');
const rabbitMq = require('../../../../models/rabbitMq');
const dailySchedules = require('../../../../models/dailySchedules');
const providerUpdateStatus = require('../../../commonModels/booking/provider');
const bookingAnalytics = require('../../../../models/bookingAnalytics');
const validatePromoCampaign = require('../../campaignAndreferral/validatePromoCampaign/post');
const campaignAndreferral = require('../../campaignAndreferral/promoCode/post');

const rabbitmqUtilPayroll = require("../../../../models/rabbitMq/payroll");

const payloadValidator = joi.object({
    bookingId: joi.number().required().integer().description('Booking Id').error(new Error("Booking Id is missing")),
    status: joi.number().required().integer().min(6).max(10).description('6- OntheWay , 7- Arrived, 8- started, 9-completed , 10- raise invoice').error(new Error('Booking status is missing')),
    latitude: joi.number().required().description('latitude').error(new Error('Latitude must be number')),
    longitude: joi.number().required().description('longitude').error(new Error('Longitude must be number')),
    signatureUrl: joi.any().description('signature Url').error(new Error('signatureUrl is missing')),
    distance: joi.number().default(0).description('distance').error(new Error('distance is missing')),
}).required();//payload of provider signin api


let APIHandler = (req, reply) => {
    const dbErrResponse = { message: error['genericErrMsg']['500'][req.headers.lan], code: 500 };
    let bookingData;
    let customerData;
    let providerData;
    var dataToUpdate = {};
    let totalJobTime = 0;
    const readBooking = (data) => {
        return new Promise((resolve, reject) => {
            assignedBookings.read({ bookingId: req.payload.bookingId }, (err, res) => {
                bookingData = res;
                if (err)
                    return reject(dbErrResponse);
                if (res === null)
                    return reject({ message: error['statusBooking']['404'][req.headers.lan], code: 404 });
                if (res.status != req.payload.status && res.status < req.payload.status)
                    resolve(res);
                else
                    return reject({ message: error['statusBooking']['400'][req.headers.lan], code: 400 });
            });
        });
    }//read booking

    const readCustomer = (data) => {
        return new Promise((resolve, reject) => {
            let con = { _id: ObjectID(bookingData.slaveId) };
            customer.read(con, (err, res) => {
                customerData = res;
                return err ? reject(err) : resolve(bookingData);
            });
        });
    }//read customer
    const readProvider = (data) => {
        return new Promise((resolve, reject) => {
            let con = { _id: ObjectID(bookingData.providerId) };
            provider.read(con, (err, res) => {
                providerData = res;
                return err ? reject(err) : resolve(data);
            });
        });
    }//read provider
    const sendBookingStatus = (data) => {
        return new Promise((resolve, reject) => {
            let proData = {
                providerId: req.auth.credentials._id,
                bookingId: req.payload.bookingId,
                status: req.payload.status,
                statusMsg: error['bookingStatus'][req.payload.status],
                statusUpdateTime: moment().unix()
            };
            webSocket.publish('dispatcher/jobStatus', proData, {}, (mqttErr, mqttRes) => {
            });
            webSocket.publish('admin/dispatchLog', proData, {}, (mqttErr, mqttRes) => {
            });
            provider.getProviderById(req.auth.credentials._id, (err, pro) => {

                var mqttRes = {
                    bookingId: req.payload.bookingId,
                    status: req.payload.status,
                    statusMsg: error['bookingStatus'][req.payload.status],
                    statusUpdateTime: moment().unix(),
                    proProfilePic: pro.profilePic || ''

                };
                if (customerData && customerData.fcmTopic != null) {
                    let request = {
                        fcmTopic: customerData.fcmTopic || "",
                        action: req.payload.status,
                        pushType: 1,
                        title: bookingMsg.notifyiTitle(req.payload.status, req.headers.lan),
                        msg: bookingMsg.notifyiMsg(req.payload.status, req.headers.lan, {
                            bookingDate: moment.unix(bookingData.bookingRequestedFor).tz("Asia/Kolkata").format('MMMM Do YYYY, h:mm a'),
                            providerName: pro.firstName + ' ' + pro.lastName
                        }),
                        data: mqttRes,
                        deviceType: customerData.mobileDevices.deviceType
                    }
                    fcm.notifyFcmTpic(request, (e, r) => { });
                }

                var msg = { errNum: 200, errFlag: 0, errMsg: 'Got Details.', data: mqttRes };
                mqtt_module.publish('jobStatus/' + data.slaveId, JSON.stringify(msg), { qos: 2 }, function (mqttErr, mqttRes) {

                });
                return resolve(data);
            });
        });
    }//send booking  status in mqtt to customer
    const checkOut = (data) => {
        return new Promise((resolve, reject) => {
            if (req.payload.status == 10) {
                if (bookingData.bookingType == 1) {
                    let updateQuery22 = {
                        query: { _id: new ObjectID(req.auth.credentials._id) },
                        data: { $set: { availableForNow: false } }
                    };
                    provider.updateOne(updateQuery22, (err, result) => {
                    });
                }
                bookingAnalyticsDataPush(bookingData);
                validatePromoCampaign.postRequestHandler({
                    bookingId: bookingData.bookingId,
                    userId: bookingData.slaveId,
                    customerName: bookingData.customerData.firstName,
                    cityId: bookingData.cityId,
                    zoneId: bookingData.cityId,
                    paymentMethod: bookingData.accounting.paymentMethod,
                    paymentMethodString: bookingData.accounting.paymentMethodText,
                    bookingTime: bookingData.bookingRequestedFor,
                    deliveryFee: 0,
                    cartValue: bookingData.accounting.total,
                    currency: bookingData.currency,
                    email: customerData.email,
                    cartId: ''

                }, (err, res) => {
                    console.log("validatePromoCampaign - errr", err);
                    console.log("validatePromoCampaign -res", res);
                })
                if (bookingData.claimData && bookingData.claimData.claimId != "") {
                    campaignAndreferral.claimCouponHandler({ claimId: bookingData.claimData.claimId, bookingId: bookingData.bookingId }, (err, res) => {
                        console.log("errr for unlock", err)
                        console.log("res for unlock", res)
                    })
                }

                let params = {
                    toName: customerData.firstName + " " + customerData.lastName,
                    to: customerData.email,
                    bookingData: bookingData,
                    bookingDate: moment.unix(bookingData.bookingRequestedFor).tz("Asia/Kolkata").format('MMMM Do YYYY, h:mm a'),
                    startTime: moment.unix(bookingData.bookingRequestedFor).tz("Asia/Kolkata").format('MMMM Do YYYY, h:mm a'),
                    endTime: moment.unix(bookingData.bookingRequestedFor).tz("Asia/Kolkata").format('MMMM Do YYYY, h:mm a'),
                    category: bookingData.category.name,
                    serviceName: bookingData.service.name + ' ' + bookingData.service.unit,
                    servicePrice: bookingData.service.price || 0,
                    currencySymbol: bookingData.currencySymbol || '$',
                    accounting: bookingData.accounting

                };

                email.invoice(params);//send invoice to customer

                //IF PAYMENT METHOD CARD
                if (bookingData.paymentMethod == 2) {
                    stripe.stripeTransaction.captureCharge(
                        bookingData.accounting.chargeId,
                        ""
                    ).then(res => {
                        let providerEarning = bookingData.accounting.providerEarning;
                        let appEarning = bookingData.accounting.appEarning;
                        let pgCommissionApp = bookingData.accounting.pgCommissionApp;
                        let pgCommissionProvider = bookingData.accounting.pgCommissionProvider;
                        let totalPgCommission = bookingData.accounting.totalPgCommission;
                        let total = bookingData.accounting.total;

                        pgCommissionApp = (total * (2.9 / 100)) + 0.3;
                        totalPgCommission = (total * (2.9 / 100)) + 0.3;

                        providerEarning = (total) * (100 - bookingData.accounting.appCommission) / 100;
                        appEarning = total - providerEarning - totalPgCommission;

                        dataToUpdate = {
                            'accounting.appEarning': parseFloat(appEarning),
                            'accounting.providerEarning': parseFloat(providerEarning),
                            'accounting.pgCommissionApp': parseFloat(pgCommissionApp),
                            'accounting.pgCommissionProvider': parseFloat(pgCommissionProvider),
                            'accounting.totalPgCommission': parseFloat(totalPgCommission),
                            'accounting.appEarningPgComm': parseFloat(appEarning + totalPgCommission),
                            'accounting.dues': - parseFloat(providerEarning),
                            'signatureUrl': req.payload.signatureUrl || '',
                            'status': req.payload.status,
                            'statusMsg': error['bookingStatus'][req.payload.status],
                            'bookingCompletedAt': moment().unix()
                        }
                        let dataArr = {
                            bookingId: bookingData.bookingId,
                            userId: bookingData.slaveId,
                            providerId: bookingData.providerId,
                            amount: bookingData.accounting.amount,
                            appEarning: appEarning,
                            chargeId: bookingData.accounting.chargeId,
                            pgComm: totalPgCommission,
                            providerEarning: providerEarning,
                            transcationType: 2,
                            currency: bookingData.currency,
                            currencySymbol: bookingData.currencySymbol
                        }
                        rabbitmqUtil.InsertQueue(
                            rabbitMq.getChannelWallet(),
                            rabbitMq.queueWallet,
                            dataArr,
                            (err, doc) => {

                            });
                        return resolve(data);
                    }).catch(e => {
                        logger.error("cancel booking capture amount error", e)
                        return reject(dbErrResponse);
                    });

                } else {
                    let providerEarning = bookingData.accounting.providerEarning;
                    let appEarning = bookingData.accounting.appEarning;
                    let pgCommissionApp = bookingData.accounting.pgCommissionApp;
                    let pgCommissionProvider = bookingData.accounting.pgCommissionProvider;
                    let totalPgCommission = bookingData.accounting.totalPgCommission;
                    let total = bookingData.accounting.total;


                    providerEarning = (total) * (100 - bookingData.accounting.appCommission) / 100;
                    appEarning = total - providerEarning - totalPgCommission;
                    dataToUpdate = {
                        'accounting.appEarning': parseFloat(appEarning),
                        'accounting.providerEarning': parseFloat(providerEarning),
                        'accounting.pgCommissionApp': parseFloat(pgCommissionApp),
                        'accounting.pgCommissionProvider': parseFloat(pgCommissionProvider),
                        'accounting.totalPgCommission': parseFloat(totalPgCommission),
                        'accounting.appEarningPgComm': parseFloat(appEarning + totalPgCommission),
                        'accounting.dues': parseFloat(appEarning),
                        'signatureUrl': req.payload.signatureUrl || '',
                        'status': req.payload.status,
                        'statusMsg': error['bookingStatus'][req.payload.status],
                        'bookingCompletedAt': moment().unix(),
                    }
                    // transcation.cashTransction({
                    //     bookingId: bookingData.bookingId,
                    //     userId: bookingData.slaveId,
                    //     providerId: bookingData.providerId,
                    //     amount: bookingData.accounting.amount,
                    //     appEarning: appEarning,
                    // }, (err, res) => { });
                    let dataArr = {
                        bookingId: bookingData.bookingId,
                        userId: bookingData.slaveId,
                        providerId: bookingData.providerId,
                        amount: bookingData.accounting.amount,
                        appEarning: appEarning,
                        chargeId: bookingData.accounting.chargeId,
                        pgComm: totalPgCommission,
                        providerEarning: providerEarning,
                        transcationType: 1,
                        currency: bookingData.currency,
                        currencySymbol: bookingData.currencySymbol
                    }
                    rabbitmqUtil.InsertQueue(
                        rabbitMq.getChannelWallet(),
                        rabbitMq.queueWallet,
                        dataArr,
                        (err, doc) => {

                        })
                    return resolve(data);
                }
            } else if (req.payload.status == 7) {
                dataToUpdate = {
                    'statusMsg': error['bookingStatus'][req.payload.status],
                    'status': req.payload.status,
                    'distance.arrived': req.payload.distance || 0
                }
                return resolve(data);
            } else if (req.payload.status == 9) {
                dataToUpdate = {
                    'statusMsg': error['bookingStatus'][req.payload.status],
                    'status': req.payload.status,
                    'accounting.totalJobTime': totalJobTime,
                    'distance.completed': req.payload.distance || 0
                }
                return resolve(data);
            } else {
                dataToUpdate = {
                    'statusMsg': error['bookingStatus'][req.payload.status],
                    'status': req.payload.status,
                }
                return resolve(data);
            }
        })
    }//complete booking to chard in stripe & transction to wallet
    const totalJobTimeInbooking = (data) => {
        return new Promise((resolve, reject) => {
            if (req.payload.status == 9) {
                let condition = [
                    {
                        $match: {
                            bookingId: req.payload.bookingId
                        }
                    },
                    { $unwind: '$jobStatusLogs' },

                    {
                        $match: {
                            bookingId: req.payload.bookingId,
                            "jobStatusLogs.status": 8
                        }
                    },
                    { $project: { _id: 0, 'time': '$jobStatusLogs.timeStamp', 'status': '$jobStatusLogs.status' } },
                ];
                assignedBookings.aggregate(condition, (err, res) => {
                    totalJobTime = (res[0].time) ? moment().unix() - res[0].time : 0;
                    console.log("totalJobTime", totalJobTime)
                    return resolve(data);
                });
            } else {
                return resolve(data);
            }
        });

    }
    const jobStatusUpdate = (data) => {
        return new Promise((resolve, reject) => {
            //update booking status in provider collection
            providerUpdateStatus.updateBookingStatus(req.auth.credentials._id, req.payload.bookingId, req.payload.status);
            let updateQuery = {
                query: { bookingId: req.payload.bookingId },
                data: {
                    $set: dataToUpdate,
                    $push: {
                        jobStatusLogs: {
                            status: req.payload.status,
                            statusMsg: error['bookingStatus'][req.payload.status],
                            stausUpdatedBy: 'Provider',
                            userId: req.auth.credentials._id || '',
                            timeStamp: moment().unix(),
                            latitude: parseFloat(req.payload.latitude) || '',
                            longitude: parseFloat(req.payload.longitude) || ''
                        }
                    }
                }
            };
            assignedBookings.findUpdate(updateQuery, (err, res) => {
                return err ? reject(err) : resolve(data);
            });

        });
    }//update booking status and accounting
    const removeSchedule = (data) => {
        return new Promise((resolve, reject) => {
            if (bookingData.bookingType == 2) {
                let bookArr = [];
                let condition = [
                    { '$match': { "schedule.booked": { "$elemMatch": { "bookingId": req.payload.bookingId } } } },
                    { '$unwind': '$schedule' },
                    { '$match': { "schedule.booked": { "$elemMatch": { "bookingId": req.payload.bookingId } } } },

                ];
                dailySchedules.readByAggregate(condition, (err, res) => {
                    let booked = res[0].schedule.booked;
                    if (booked.length != 0) {
                        Async.forEach(booked, function (book, callback1) {
                            if (book.bookingId == req.payload.bookingId) {
                                book.status = req.payload.status;
                                bookArr.push(book);
                                callback1(null, bookArr);
                            } else {
                                bookArr.push(book);
                                callback1(null, bookArr);
                            }

                        }, function (err, res) {
                            let con = {
                                query: {
                                    "schedule.booked": { "$elemMatch": { "bookingId": req.payload.bookingId } }
                                },
                                data: {
                                    '$set': {
                                        'schedule.$.booked': bookArr
                                    }
                                }
                            }
                            dailySchedules.findUpdate(con, (err, res) => {
                                return resolve(data);
                            })
                        });
                    } else {
                        return resolve(data);
                    }
                });
            } else {
                return resolve(data);
            }
        });

    }
    const updateCompleteBooking = (data) => {
        rabbitmqUtilPayroll.InsertQueuePayroll(rabbitMq.getChannelPayroll(), rabbitMq.QueuePayroll, { id: req.auth.credentials._id }, () => { });
        return new Promise((resolve, reject) => {
            if (req.payload.status == 10) {
                assignedBookings.findOneDelete({ bookingId: req.payload.bookingId }, (err, res) => {
                    if (err) {
                        return reject(dbErrResponse);
                    } else if (res === null) {
                        return reject({ message: error['statusBooking']['404'][req.headers.lan], code: 404 });
                    } else {
                        bookings.post(res.value, (err, result) => {
                            return err ? reject(err) : resolve(res.value);
                        });
                    }
                });
            } else {
                return resolve(data)
            }

        });
    }//update booking assignBooking to booking

    readBooking()
        .then(readCustomer)
        .then(readProvider)
        .then(sendBookingStatus)
        .then(totalJobTimeInbooking)
        .then(checkOut)
        .then(jobStatusUpdate)
        .then(removeSchedule)
        .then(updateCompleteBooking)
        .then(data => {
            return reply({ message: error['statusBooking']['200'][req.headers.lan] }).code(200);
        }).catch(e => {
            logger.error("provider statusBooking API error =>", e)
            return reply({ message: e.message }).code(e.code);
        });

};

const responseCode = {
    status: {
        500: { message: error['genericErrMsg']['500'][error['lang']] },
        400: { message: error['statusBooking']['400'][error['lang']] },
        404: { message: error['statusBooking']['404'][error['lang']] },
        200: {
            message: error['statusBooking']['200'][error['lang']]
        },
    }

}//swagger response code


module.exports = {
    payloadValidator,
    APIHandler,
    responseCode
};

const bookingAnalyticsDataPush = (data) => {
    bookingAnalytics.read({ userId: data.slaveId }, (err, res) => {
        if (err) {
        } else if (res === null) {
            bookingAnalytics.post({
                "userId": data.slaveId,
                "totalNumberOfBooking": 1,
                "totalBusinessAmount": data.accounting.total,
                "bookings": [
                    {
                        "bookingId": data.bookingId,
                        "providerId": data.providerId,
                        "amount": data.accounting.total,
                        "timestamp": moment().unix(),
                        "isoDate": new Date()
                    }
                ]
            }, (err, res) => {

            })
        } else {
            console.log(data.accounting)
            console.log(res.total)
            console.log(data.accounting.total)
            let updateQuery = {
                query: { "userId": data.slaveId },
                data: {
                    $set: {
                        "totalNumberOfBooking": res.totalNumberOfBooking + 1,
                        "totalBusinessAmount": res.totalBusinessAmount + data.accounting.total,
                    },
                    $push: {
                        bookings: {
                            "bookingId": data.bookingId,
                            "providerId": data.providerId,
                            "amount": data.accounting.total,
                            "timestamp": moment().unix(),
                            "isoDate": new Date()
                        }
                    }
                }
            };
            console.log(updateQuery)
            bookingAnalytics.findUpdate(updateQuery, (err, res) => {

            })
        }
    })
}//update booking assignBooking to booking