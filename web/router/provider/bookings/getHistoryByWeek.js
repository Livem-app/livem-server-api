'use strict';
const joi = require('joi');
const error = require('../error');
const logger = require('winston');
const Async = require('async');
const ObjectID = require('mongodb').ObjectID;
const moment = require('moment');//date-time 
const bookings = require('../../../../models/bookings');
const customer = require('../../../../models/customer');
const provider = require('../../../../models/provider');
const appConfig = require('../../../../models/appConfig');
const assignedBookings = require('../../../../models/assignedBookings');
const unassignedBookings = require('../../../../models/unassignedBookings');
const dispatchedBookings = require('../../../../models/dispatchedBookings');
let history = [];
let providerId;
function getDateRange(currentEndDate, startDate) {
    var dates = [];
    let diff = currentEndDate.diff(startDate, 'days');
    dates.push(moment(startDate).format('YYYY-MM-DD'));
    for (var i = 1; i <= diff; ++i) {
        dates.push(startDate.add(1, 'd').format('YYYY-MM-DD'));
    }

    return dates;
}//get all Date
function getData(data, cb) {
    let endDate = moment().endOf('day').unix();
    let startDate = moment.unix(data).startOf('day').unix();

    if (startDate < endDate) {
        let currentDate = moment.unix(startDate).add(6, 'd').unix();
        let currentEndDate = moment.unix(currentDate).endOf('day').unix();
        var a = moment.unix(currentEndDate);
        var b = moment.unix(startDate);
        let weekInterval = getDateRange(a, b);
        let noOfBooking = [0, 0, 0, 0, 0, 0, 0];
        Async.forEach(weekInterval, function (item, callbackloop) {
            let bokingStartDate = moment(item).startOf('day').unix();
            let bokingEndDate = moment(item).endOf('day').unix();

            bookings.bookingCount({
                providerId: providerId,
                'bookingRequestedFor': {
                    '$gte': bokingStartDate, '$lte': bokingEndDate
                }
            }, (err, res) => {
                let dayofweek = moment.unix(bokingStartDate).day();

                noOfBooking[dayofweek] = res;
                // noOfBooking.push(res);

                callbackloop(null, noOfBooking);
            });

        }, function (loopErr, res) {

            history.push({
                startDate: (startDate + 16400),
                sDate: moment.unix(startDate).format('YYYY-MM-DD'),
                endDate: (currentDate + 16400),
                eDate: moment.unix(currentDate).format('YYYY-MM-DD'),
                count: noOfBooking
            });
            startDate = moment.unix(startDate).add(7, 'd').unix();
            getData(startDate, (err, res) => {
                cb(err, res);
            });
        });
    } else {
        cb(null, "countData");
    }
}
let APIHandler = (req, reply) => {
    history = [];
    let activateOn;
    providerId = req.auth.credentials._id;
    console.log("providerId", providerId)
    const getProvider = (data) => {
        return new Promise((resolve, reject) => {
            provider.getProviderById(req.auth.credentials._id, (err, res) => {
                activateOn = res.activateOn;
                console.log("activateOn", activateOn)
                return err ? reject(err) : resolve(res.activateOn);
            });
        });
    }



    const historyBooking = (data) => {
        return new Promise((resolve, reject) => {
            let noOfDay = moment.unix(data).day();
            let startOfDay = moment.unix(data).subtract(noOfDay, 'd').unix();
            getData(startOfDay, (err, res) => {

                resolve(history);
            });
        });
    }

    getProvider().then(historyBooking)
        .then(data => {
            console.log("history", history)
            return reply({ message: error['getBookingHistory']['200'][req.headers.lan], data: history }).code(200);
        }).catch(e => {
            logger.error("provider getBooking API error =>", e)
            return reply({ message: e.message }).code(e.code);
        });
};

const responseCode = {
    status: {
        200: { message: error['getBookingHistory']['200']['1'], data: joi.any() },
        500: { message: error['genericErrMsg']['500']['1'] }
    }

}//swagger response code


module.exports = {
    APIHandler,
    responseCode
};