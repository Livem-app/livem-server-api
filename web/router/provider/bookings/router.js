'use strict';

const entity = '/provider';

const get = require('./get');
const getById = require('./getById');
const error = require('../error');
const patchAck = require('./patchAck');
const patchCancel = require('./patchCancel');
const getHistory = require('./getHistory');
const getHistoryByWeek = require('./getHistoryByWeek');
const patchTimer = require('./patchTimer');
const postStatus = require('./postStatus');
const postResponse = require('./postResponse');
const headerValidator = require('../../../middleware/validator');

module.exports = [
    /**
   * @name GET /provider/booking
   */
    {
        method: 'GET',
        path: entity + '/booking',
        handler: get.APIHandler,
        config: {
            tags: ['api', 'booking'],
            description: error['apiDescription']['getBooking'],
            notes: 'This Api is used to get all request or upcoming job.',
            auth: 'providerJWT',
            response: get.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                // payload: get.payloadValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
    {
        method: 'GET',
        path: entity + '/booking/{bookingId}',
        handler: getById.APIHandler,
        config: {
            tags: ['api', 'booking'],
            description: error['apiDescription']['getBookingById'],
            notes: 'This Api is used to get a perticuler booking details.',
            auth: 'providerJWT',
            response: getById.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                params: getById.paramsValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },


    /**
    * @name PATCH /provider/bookingAck
    */
    {
        method: 'PATCH',
        path: entity + '/bookingAck',
        handler: patchAck.APIHandler,
        config: {
            tags: ['api', 'booking'],
            description: error['apiDescription']['patchBookingAck'],
            notes: 'Booking Ack',
            auth: 'providerJWT',
            response: patchAck.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: patchAck.payloadValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },

    /**
    * @name PATCH /provider/bookingResponse
    */
    {
        method: 'PATCH',
        path: entity + '/bookingResponse',
        handler: postResponse.APIHandler,
        config: {
            tags: ['api', 'booking'],
            description: error['apiDescription']['patchBookingResponse'],
            notes: 'This API is used by the provider app to either accept or deny a booking.',
            auth: 'providerJWT',
            // response: postResponse.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: postResponse.payloadValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },

    /**
    * @name PATCH /provider/bookingStatus
    */
    {
        method: 'PATCH',
        path: entity + '/bookingStatus',
        handler: postStatus.APIHandler,
        config: {
            tags: ['api', 'booking'],
            description: error['apiDescription']['patchBookingStatus'],
            notes: 'this is used to modify the status of the booking based on the status either set by the provider or set by the dispatch or set by the admin.',
            auth: 'providerJWT',
            // response: postStatus.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: postStatus.payloadValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },

    /**
    * @name PATCH /provider/bookingTimer
    */
    {
        method: 'PATCH',
        path: entity + '/bookingTimer',
        handler: patchTimer.APIHandler,
        config: {
            tags: ['api', 'booking'],
            description: error['apiDescription']['patchBookingTimer'],
            notes: 'This Api is used Start Timer or Stop Timer,After booking start.',
            auth: 'providerJWT',
            // response: patchTimer.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: patchTimer.payloadValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },

    /**
    * @name PATCH /provider/cancelBooking
    */
    {
        method: 'PATCH',
        path: entity + '/cancelBooking',
        handler: patchCancel.APIHandler,
        config: {
            tags: ['api', 'booking'],
            description: error['apiDescription']['patchCancelBooking'],
            notes: 'This Api is used Start Timer or Stop Timer,After booking start.',
            auth: 'providerJWT',
            response: patchCancel.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: patchCancel.payloadValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
    
    /**
    * @name GET /provider/bookingHistory
    */
    {
        method: 'GET',
        path: entity + '/bookingHistory/{startDate}',
        handler: getHistory.APIHandler,
        config: {
            tags: ['api', 'booking'],
            description: error['apiDescription']['getBookingHistory'],
            notes: 'This Api is used to get all requests, upcoming and past Booking.',
            auth: 'providerJWT',
            response: getHistory.responseCode,
            validate: {
                params: getHistory.paramsValidator,
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
    {
        method: 'GET',
        path: entity + '/bookingHistoryByWeek',
        handler: getHistoryByWeek.APIHandler,
        config: {
            tags: ['api', 'booking'],
            description: error['apiDescription']['getBookingHistory'],
            notes: 'This Api is used to get all requests, upcoming and past Booking.',
            auth: 'providerJWT',
            // response: getHistory.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },

];