'use strict';
const joi = require('joi');
const error = require('../error');
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;
const moment = require('moment');//date-time 
const fcm = require('../../../../library/fcm');
const mqtt_module = require('../../../../models/MQTT');
const bookings = require('../../../../models/bookings');
const provider = require('../../../../models/provider');
const customer = require('../../../../models/customer');
const webSocket = require('../../../../models/webSocket');
const appConfig = require('../../../../models/appConfig');
const assignedBookings = require('../../../../models/assignedBookings');
const unassignedBookings = require('../../../../models/unassignedBookings');
const dispatchedBookings = require('../../../../models/dispatchedBookings');
const bookingMsg = require('../../../commonModels/notificationMsg/bookingMsg');

const payloadValidator = joi.object({
    bookingId: joi.number().description('bookingId').error(new Error('booking id is must be integer')),
    status: joi.number().required().integer().min(0).max(1).description('0- Stop , 1 - Start').error(new Error('status is missing')),
    second: joi.number().required().integer().description('Total Second After start job').error(new Error('second is missing')),
}).required();//payload of provider signin api


const APIHandler = (req, reply) => {
    let customerData;
    let providerData;
    const dbErrResponse = { message: error['genericErrMsg']['500'][req.headers.lan], code: 500 };
    const getAssignedBooking = (data) => {
        return new Promise((resolve, reject) => {
            let condition = { providerId: req.auth.credentials._id, bookingId: req.payload.bookingId };
            assignedBookings.read(condition, (err, res) => {
                if (err)
                    return reject(dbErrResponse)
                else if (res == null)
                    return reject({ message: error['timerBooking']['404'][req.headers.lan], code: 404 });
                else
                    resolve(res);
            });
        });
    }
    const readCustomer = (data) => {
        return new Promise((resolve, reject) => {
            let con = { _id: ObjectID(data.slaveId) };
            customer.read(con, (err, res) => {
                customerData = res;
                return err ? reject(err) : resolve(data);
            });
        });
    }
    const readProvider = (data) => {
        return new Promise((resolve, reject) => {
            let con = { _id: ObjectID(data.providerId) };
            provider.read(con, (err, res) => {
                providerData = res;
                return err ? reject(err) : resolve(data);
            });
        });
    }
    const updateTimer = (job) => {
        return new Promise((resolve, reject) => {
            let updateTime = {
                status: req.payload.status,
                second: req.payload.second
            }
            if (req.payload.status == 1) {
                updateTime.startTimeStamp = moment().unix();
            } else {
                if (job.bookingTimer.status == req.payload.status)
                    return reject({ message: error['timerBooking']['400'][req.headers.lan][req.payload.status], code: 400 });
                updateTime.startTimeStamp = job.bookingTimer.startTimeStamp;
            }
            var bookingTimer = {
                status: req.payload.status,
                second: req.payload.second,
                startTimeStamp: job.bookingTimer.startTimeStamp
            }
            let timerStatus = req.payload.status == 0 ? 14 : 8;
            var mqttRes = {
                bookingId: job.bookingId,
                status: job.status,
                statusMsg: error['bookingStatus'][timerStatus],
                statusUpdateTime: moment().unix(),
                bookingTimer: updateTime,
                proProfilePic: providerData.profilePic || ''
            };

            let request = {
                fcmTopic: customerData.fcmTopic,
                action: timerStatus,
                pushType: 1,
                title: bookingMsg.notifyiTitle(timerStatus, req.headers.lan),
                msg: bookingMsg.notifyiMsg(timerStatus, req.headers.lan, {
                    providerName: providerData.firstName + ' ' + providerData.lastName
                }),
                data: mqttRes,
                deviceType: customerData.mobileDevices.deviceType
            }
            console.log("request", request);
            fcm.notifyFcmTpic(request, (e, r) => { });
            var msg = { errNum: 200, errFlag: 0, errMsg: 'Got Details.', data: mqttRes };
            mqtt_module.publish('jobStatus/' + job.slaveId, JSON.stringify(msg), { qos: 2 }, function (mqttErr, mqttRes) {

            });

            var updateQuery = {
                query: { bookingId: job.bookingId },
                data: {
                    $set: { bookingTimer: updateTime }

                }
            };
            assignedBookings.findUpdate(updateQuery, (err, result) => {
                return resolve(job);
            });
        })
    }
    getAssignedBooking()
        .then(readCustomer)
        .then(readProvider)
        .then(updateTimer)
        .then(data => {
            return reply({ message: error['timerBooking']['200'][req.headers.lan] }).code(200);
        }).catch(e => {
            logger.error("provider statusBooking API error =>", e)
            return reply({ message: e.message }).code(e.code);
        });

};

const responseCode = {
    status: {
        500: { message: error['genericErrMsg']['500'][error['lang']] },
        400: { message: error['timerBooking']['400'][error['lang']] },
        404: { message: error['timerBooking']['404'][error['lang']] },
        200: {
            message: error['timerBooking']['200'][error['lang']]
        },
    }

}//swagger response code


module.exports = {
    payloadValidator,
    APIHandler,
    responseCode
};