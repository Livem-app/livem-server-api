'use strict';

const Async = require('async');
const joi = require('joi');
const error = require('../error');
const logger = require('winston');
const moment = require('moment-timezone');
const bookingMsg = require('../../../commonModels/notificationMsg/bookingMsg');
const fcm = require('../../../../library/fcm');
const ObjectID = require('mongodb').ObjectID;
const customer = require('../../../../models/customer');
const provider = require('../../../../models/provider');
const appConfig = require('../../../../models/appConfig');
const unassignedBookings = require('../../../../models/unassignedBookings');
const bookings = require('../../../../models/bookings');
const dispatchedBookings = require('../../../../models/dispatchedBookings');
const assignedBookings = require('../../../../models/assignedBookings');
const verificationCode = require('../../../../models/verificationCode');
const mqtt_module = require('../../../../models/MQTT');
const webSocket = require('../../../../models/webSocket');
const dailySchedules = require('../../../../models/dailySchedules');
const redis = require('../../../../models/redis');
const email = require('../../../commonModels/email/customer');
const paymentMethod = require('../../../commonModels/paymentMethod');
const stripe = require('../../../commonModels/stripe');
const providerUpdateStatus = require('../../../commonModels/booking/provider');

const campaignAndreferral = require('../../campaignAndreferral/promoCode/post');

const rabbitmqUtil = require("../../../../models/rabbitMq/payroll");
const rabbitMq = require("../../../../models/rabbitMq");

const payloadValidator = joi.object({
    bookingId: joi.number().required().integer().description('Booking Id').error(new Error("Booking Id is missing")),
    status: joi.number().required().integer().min(3).max(4).description('3- Accept , 4- Reject').error(new Error('Booking status is missing')),
    latitude: joi.number().required().description('latitude').error(new Error('Latitude must be number')),
    longitude: joi.number().required().description('longitude').error(new Error('Longitude must be number')),
    reminderId: joi.any().allow("").description("reminderId for schedule booking"),
}).required();//payload of provider signin api


let APIHandler = (req, reply) => {
    let customerData;
    let providerData;
    const readBooking = (data) => {
        return new Promise(function (resolve, reject) {
            unassignedBookings.findOneDelete({ bookingId: req.payload.bookingId, providerId: req.auth.credentials._id }, (err, res) => {
                if (err)
                    return reject({ message: error['genericErrMsg']['500'][req.headers.lan], code: 500 })
                if (res.value === null)
                    return reject({ message: error['responseBooking']['404'][req.headers.lan], code: 404 });
                resolve(res.value);
            });
        })
    }
    const readCustomer = (data) => {
        return new Promise(function (resolve, reject) {
            let con = { _id: ObjectID(data.slaveId) };
            customer.read(con, (err, res) => {
                customerData = res;
                return err ? reject(err) : resolve(data);
            });
        });
    }
    const readProvider = (data) => {
        return new Promise(function (resolve, reject) {
            let con = { _id: ObjectID(data.providerId) };
            provider.read(con, (err, res) => {
                providerData = res;
                return err ? reject(err) : resolve(data);
            });
        });
    }
    const removeSchedule = (data) => {
        return new Promise((resolve, reject) => {
            if (data.bookingType == 2) {
                if (req.payload.status == 4) {
                    let scheduleData;
                    dailySchedules.readByBookingId(req.payload.bookingId, (err, res) => {
                        if (err)
                            return resolve(data);
                        if (res) {
                            dailySchedules.cancelSchedule(req.payload.bookingId, (err, result) => {
                                let schedule = res.schedule;
                                schedule.forEach(element => {
                                    if (element["providerId"] == req.auth.credentials._id) {
                                        let scheArr = "";
                                        let bookArr = [];
                                        let book = element.booked;
                                        if (book.length != 0) {
                                            book.forEach(booked => {
                                                if (booked.bookingId != req.payload.bookingId) {
                                                    bookArr.push(booked);
                                                } else {
                                                    scheArr = element;
                                                }
                                            });
                                            scheArr.booked = bookArr;
                                            dailySchedules.addDailyScheduleInBook(res._id, scheArr, (err, res) => {
                                                return resolve(data);
                                            });
                                        }

                                    }
                                });
                            });
                        } else {
                            return resolve(data);
                        }
                    });
                } else {
                    let bookArr = [];
                    let condition = [
                        { '$match': { "schedule.booked": { "$elemMatch": { "bookingId": req.payload.bookingId } } } },
                        { '$unwind': '$schedule' },
                        { '$match': { "schedule.booked": { "$elemMatch": { "bookingId": req.payload.bookingId } } } },

                    ];
                    dailySchedules.readByAggregate(condition, (err, res) => {
                        let booked = res[0].schedule.booked;
                        if (booked.length != 0) {
                            Async.forEach(booked, function (book, callback1) {
                                if (book.bookingId == req.payload.bookingId) {
                                    book.status = 3;
                                    bookArr.push(book);
                                    callback1(null, bookArr);
                                } else {
                                    bookArr.push(book);
                                    callback1(null, bookArr);
                                }

                            }, function (err, res) {
                                let con = {
                                    query: {
                                        "schedule.booked": { "$elemMatch": { "bookingId": req.payload.bookingId } }
                                    },
                                    data: {
                                        '$set': {
                                            'schedule.$.booked': bookArr
                                        }
                                    }
                                }
                                dailySchedules.findUpdate(con, (err, res) => {
                                    return resolve(data);
                                })
                            });
                        } else {
                            return resolve(data);
                        }
                    });
                }
            } else {
                return resolve(data);
            }
        });

    }
    const updateBookingStatus = (data) => {
        return new Promise(function (resolve, reject) {
            if (req.payload.status == 3) {
                data.responseBookingStatus = 1;
                data.reminderId = req.payload.reminderId || "";
                assignedBookings.post(data, (err, result) => {
                    return err ? reject(err) : resolve(data);

                });
            } else {
                if (data.claimData && Object.keys(data.claimData).length !== 0 && data.claimData.claimId != "") {
                    campaignAndreferral.unlockCouponHandler({ claimId: data.claimData.claimId }, (err, res) => {
                        console.log("errr for unlock", err)
                        console.log("res for unlock", res)
                    })
                }
                rabbitmqUtil.InsertQueuePayroll(rabbitMq.getChannelPayroll(), rabbitMq.QueuePayroll, { id: req.auth.credentials._id }, () => { });
                data.responseBookingStatus = 0;
                data.bookingCompletedAt = moment().unix();
                bookings.post(data, (err, result) => {
                    return err ? reject(err) : resolve(data);

                });
            }
        });
    }
    const jobStatusUpdate = (data) => {
        return new Promise(function (resolve, reject) {
            //update booking status in provider collection
            providerUpdateStatus.updateBookingStatus(req.auth.credentials._id, req.payload.bookingId, req.payload.status);
            let updateQuery = {
                query: { bookingId: req.payload.bookingId },
                data: {
                    $set: { status: req.payload.status, statusMsg: error['bookingStatus'][req.payload.status], acceptReject: 0, },
                    $push: {
                        jobStatusLogs: {
                            status: req.payload.status,
                            statusMsg: error['bookingStatus'][req.payload.status],
                            stausUpdatedBy: 'Provider',
                            userId: req.auth.credentials._id || '',
                            timeStamp: moment().unix(),
                            latitude: parseFloat(req.payload.latitude) || '',
                            longitude: parseFloat(req.payload.longitude) || ''
                        }
                    }
                }
            };
            if (req.payload.status == 3) {
                let availableForNow = data.bookingType == 1 ? true : (providerData.availableForNow ? providerData.availableForNow : false);
                let updateQuery22 = {
                    query: { _id: new ObjectID(req.auth.credentials._id) },
                    data: { $set: { availableForNow: availableForNow } }
                };
                provider.updateOne(updateQuery22, (err, result) => {
                });

                assignedBookings.findUpdate(updateQuery, (err, res) => {
                    return err ? reject(err) : resolve(data);
                });
            } else {
                let updateQuery2 = {
                    query: { bookingId: req.payload.bookingId },
                    data: {
                        $set: {
                            "accounting.amount": 0,
                            "accounting.total": 0,
                            status: req.payload.status, statusMsg: error['bookingStatus'][req.payload.status], acceptReject: 0,
                        },
                        $push: {
                            jobStatusLogs: {
                                status: req.payload.status,
                                statusMsg: error['bookingStatus'][req.payload.status],
                                stausUpdatedBy: 'Provider',
                                userId: req.auth.credentials._id || '',
                                timeStamp: moment().unix(),
                                latitude: parseFloat(req.payload.latitude) || '',
                                longitude: parseFloat(req.payload.longitude) || ''
                            }
                        }
                    }
                };
                bookings.findUpdate(updateQuery2, (err, res) => {
                    return err ? reject(err) : resolve(data);
                });
            }

        });
    }
    const sendBookingResponse = (data) => {
        return new Promise(function (resolve, reject) {
            let mqttRes = {
                bookingId: req.payload.bookingId,
                status: req.payload.status,
                statusMsg: error['bookingStatus'][req.payload.status],
                statusUpdateTime: moment().unix(),
                proProfilePic: providerData.profilePic || '',
                providerName: providerData.firstName + ' ' + providerData.lastName,
                bookingDate: data.bookingRequestedFor

            };
            webSocket.publish('dispatcher/jobStatus', mqttRes, {}, function (mqttErr, mqttRes) {
            });
            webSocket.publish('admin/dispatchLog', mqttRes, {}, (mqttErr, mqttRes) => {
            });
            redis.delteSet('bid_' + req.payload.bookingId);
            let request = {
                fcmTopic: customerData.fcmTopic,
                action: req.payload.status,
                pushType: 1,
                title: bookingMsg.notifyiTitle(req.payload.status, req.headers.lan),
                msg: bookingMsg.notifyiMsg(req.payload.status, req.headers.lan, {
                    bookingDate: moment.unix(data.bookingRequestedFor).tz("Asia/Kolkata").format('MMMM Do YYYY, h:mm a'),
                    providerName: providerData.firstName + ' ' + providerData.lastName
                }),
                data: mqttRes,
                deviceType: customerData.mobileDevices.deviceType
            }
            fcm.notifyFcmTpic(request, (e, r) => { });
            var msg = { errNum: 200, errFlag: 0, errMsg: 'Got Details.', data: mqttRes };
            mqtt_module.publish('jobStatus/' + data.slaveId, JSON.stringify(msg), { qos: 2 }, function (mqttErr, mqttRes) {
                return mqttErr ? reject(mqttErr) : resolve(data);
            });
        });
    }

    const dispatcherLogUpdate = (data) => {
        return new Promise(function (resolve, reject) {
            let updateQuery = {
                query: { bookingId: req.payload.bookingId, providerId: req.auth.credentials._id },
                data: {
                    $set: {
                        providerResponse: moment().unix(),
                        status: req.payload.status,
                        statusMsg: error['bookingStatus'][req.payload.status]
                    }

                }
            };
            dispatchedBookings.findOneAndUpdate(updateQuery, (err, res) => {
                return err ? reject(err) : resolve(data);
            });
        });
    }


    const sendMail = (data) => {
        return new Promise((resolve, reject) => {
            if (req.payload.status == 3) {
                let params = {
                    to: customerData.email,
                    toName: customerData.firstName + ' ' + customerData.lastName,
                    providerName: providerData.firstName + ' ' + providerData.lastName,
                    bookingId: data.bookingId,
                    bookingDate: moment.unix(data.bookingRequestedFor).tz("Asia/Kolkata").format('MMMM Do YYYY, h:mm:ss a'),
                    bookingType: data.bookingType == 1 ? "Now" : 'scheduled',
                    address: data.addLine1,
                    bookingCategory: data.category.name,
                    serviceName: data.service.name + ' ' + data.service.unit,
                    servicePrice: data.service.price || 0,
                    discount: data.accounting.discount,
                    amount: data.accounting.amount
                };
                email.acceptBookingByProvider(params);
            } else {
                let params = {
                    to: customerData.email,
                    toName: customerData.firstName + ' ' + customerData.lastName,
                    providerName: providerData.firstName + ' ' + providerData.lastName,
                    bookingDate: moment.unix(data.bookingRequestedFor).tz("Asia/Kolkata").format('MMMM Do YYYY, h:mm:ss a'),
                }
                email.rejectBookingByProvider(params);
                // paymentMethod.refund({ bookingId: req.payload.bookingId }, (err, res) => { });//refund reject booking
            }
            return resolve(data);
        });
    }
    const stripeCheckOut = (data) => {
        return new Promise((resolve, reject) => {
            if (data.paymentMethod == 2 && req.payload.status != 3) {
                stripe.stripeTransaction.refundCharge(
                    data.accounting.chargeId,
                    ''
                ).then(res => {
                    return resolve(data);
                }).catch(e => {
                    logger.error("cancel booking by provider stripe error", e)
                    return reject(dbErrResponse);
                });

            } else {
                return resolve(data);
            }
        });
    }
    readBooking()
        .then(stripeCheckOut)
        .then(readCustomer)
        .then(readProvider)
        .then(removeSchedule)
        .then(updateBookingStatus)
        .then(jobStatusUpdate)
        .then(sendBookingResponse)
        .then(dispatcherLogUpdate)
        .then(sendMail)
        .then(data => {
            return reply({ message: error['responseBooking']['200'][req.headers.lan] }).code(200);
        }).catch(e => {
            logger.error("provider responseBooking API error =>", e)
            return reply({ message: e.message }).code(e.code);
        });

};

const responseCode = {
    status: {
        200: { message: error['responseBooking']['200'][error['lang']] },
        404: { message: error['responseBooking']['404'][error['lang']] },
        500: { message: error['genericErrMsg']['500'][error['lang']] }
    }
}//swagger response code


module.exports = {
    payloadValidator,
    APIHandler,
    responseCode
};