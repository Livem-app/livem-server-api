'use strict';
const joi = require('joi');
const logger = require('winston');
const error = require('../error');
const ObjectID = require('mongodb').ObjectID;
const moment = require('moment');//date-time 
const bookingMsg = require('../../../commonModels/notificationMsg/bookingMsg');
const mqtt_module = require('../../../../models/MQTT');
const fcm = require('../../../../library/fcm');
const bookings = require('../../../../models/bookings');
const customer = require('../../../../models/customer');
const provider = require('../../../../models/provider');
const webSocket = require('../../../../models/webSocket');
const appConfig = require('../../../../models/appConfig');
const assignedBookings = require('../../../../models/assignedBookings');
const unassignedBookings = require('../../../../models/unassignedBookings');
const dispatchedBookings = require('../../../../models/dispatchedBookings');
const dailySchedules = require('../../../../models/dailySchedules');
const cancellationReasons = require('../../../../models/cancellationReasons');
const email = require('../../../commonModels/email/provider');
const stripe = require('../../../commonModels/stripe');
const rabbitmqUtil = require("../../../../models/rabbitMq/payroll");
const rabbitMq = require("../../../../models/rabbitMq");
const providerUpdateStatus = require('../../../commonModels/booking/provider');
const campaignAndreferral = require('../../campaignAndreferral/promoCode/post');

const payloadValidator = joi.object({
    bookingId: joi.number().description('bookingId').error(new Error('booking id is must be integer')),
    resonId: joi.number().description('cancel resons id ').error(new Error('reson id is missing')),
}).required();//payload of provider signin api


const APIHandler = (req, reply) => {
    let customerData;
    let cancellationReason = "";
    const dbErrResponse = { message: error['genericErrMsg']['500'][req.headers.lan], code: 500 };
    const getAssignedBooking = (data) => {
        return new Promise((resolve, reject) => {
            let condition = { providerId: req.auth.credentials._id, bookingId: req.payload.bookingId };
            unassignedBookings.findOneDelete(condition, (err, res) => {
                if (err) {
                    return reject(dbErrResponse);
                }
                else if (res.value == null) {
                    assignedBookings.findOneDelete(condition, (err, res) => {
                        if (err) {
                            return reject(dbErrResponse);
                        }
                        else if (res.value == null) {
                            return reject({ message: error['pacthCancelBooking']['404'][req.headers.lan], code: 404 });
                        }
                        else {
                            bookings.post(res.value, (err, result) => {
                                return err ? reject(err) : resolve(res.value);

                            });
                        }
                    });
                }
                else {
                    bookings.post(res.value, (err, result) => {
                        return err ? reject(err) : resolve(res.value);
                    });
                }
            });
        });
    }

    const readCustomer = (data) => {
        return new Promise((resolve, reject) => {
            let con = { _id: ObjectID(data.slaveId) };
            customer.read(con, (err, res) => {
                customerData = res;
                return err ? reject(err) : resolve(data);
            });
        });
    }
    const removeSchedule = (data) => {
        return new Promise((resolve, reject) => {
            let scheduleData;
            dailySchedules.readByBookingId(req.payload.bookingId, (err, res) => {
                if (err)
                    return resolve(data);
                if (res) {
                    dailySchedules.cancelSchedule(req.payload.bookingId, (err, result) => {
                        let schedule = res.schedule;
                        schedule.forEach(element => {
                            if (element["providerId"] == data.providerId) {
                                let scheArr = "";
                                let bookArr = [];
                                let book = element.booked;
                                if (book.length != 0) {
                                    book.forEach(booked => {
                                        if (booked.bookingId != req.payload.bookingId) {
                                            bookArr.push(booked);
                                        } else {
                                            scheArr = element;
                                        }
                                    });
                                    scheArr.booked = bookArr;
                                    dailySchedules.addDailyScheduleInBook(res._id, scheArr, (err, res) => {
                                        return resolve(data);
                                    });
                                }

                            }
                        });
                    });
                } else {
                    return resolve(data);
                }
            });


        });
    }
    const getCancellationReson = (data) => {
        return new Promise((resolve, reject) => {
            cancellationReasons.read(
                {
                    res_id: parseInt(req.payload.resonId),
                    "res_for": "Driver"
                }, (err, res) => {
                    if (err) {
                        return reject(dbErrResponse);
                    } else {
                        cancellationReason = res.reasons[0]
                        return resolve(data);
                    }

                });
        });
    }
    const jobStatusUpdate = (data) => {
        return new Promise((resolve, reject) => {
            //update booking status in provider collection
            if (data.status >= 3 && data.bookingType == 1) {
                let updateQuery22 = {
                    query: { _id: new ObjectID(req.auth.credentials._id) },
                    data: { $set: { availableForNow: false } }
                };
                provider.updateOne(updateQuery22, (err, result) => {
                });
            }
            providerUpdateStatus.updateBookingStatus(req.auth.credentials._id, req.payload.bookingId, 11);
            let updateQuery = {
                query: { bookingId: req.payload.bookingId },
                data: {
                    $set: {
                        statusMsg: error['bookingStatus']["11"],
                        status: 11, resonId: req.payload.resonId, cancelBy: 2, cancellationReason: cancellationReason, responseBookingStatus: 0,
                        "accounting.amount": 0,
                        "accounting.total": 0,
                        "accounting.cancellationFee": 0,
                        'accounting.dues': 0,
                        bookingCompletedAt: moment().unix()
                    },
                    $push: {
                        jobStatusLogs: {
                            status: 11,
                            statusMsg: error['bookingStatus']["11"],
                            stausUpdatedBy: 'Provider',
                            userId: req.auth.credentials._id || '',
                            timeStamp: moment().unix(),
                            latitude: '',
                            longitude: ''
                        }
                    }
                }
            };
            bookings.findUpdate(updateQuery, (err, res) => {
                return err ? reject(err) : resolve(data);
            });

        });
    }
    const sendBookingResponse = (data) => {
        return new Promise(function (resolve, reject) {
            if (data.claimData && Object.keys(data.claimData).length !== 0 && data.claimData.claimId != "") {
                campaignAndreferral.unlockCouponHandler({ claimId: data.claimData.claimId }, (err, res) => {
                    console.log("errr for unlock", err)
                    console.log("res for unlock", res)
                })
            }
            provider.getProviderById(req.auth.credentials._id, (err, pro) => {
                var mqttRes = {
                    bookingId: req.payload.bookingId,
                    bookingType: data.bookingType,
                    reminderId: data.reminderId || "",
                    status: 11,
                    statusMsg: error['bookingStatus']["11"],
                    statusUpdateTime: moment().unix(),
                    proProfilePic: pro.profilePic || ''

                };
                let request = {
                    fcmTopic: customerData.fcmTopic,
                    action: 11,
                    pushType: 1,
                    title: bookingMsg.notifyiTitle(11, req.headers.lan),
                    msg: bookingMsg.notifyiMsg(11, req.headers.lan, {
                        bookingDate: moment.unix(data.bookingRequestedFor).tz("Asia/Kolkata").format('MMMM Do YYYY, h:mm a'),
                        providerName: pro.firstName + ' ' + pro.lastName
                    }),
                    data: mqttRes,
                    deviceType: customerData.mobileDevices.deviceType
                }
                fcm.notifyFcmTpic(request, (e, r) => {
                });

                var msg = { errNum: 200, errFlag: 0, errMsg: 'Got Details.', data: mqttRes };
                mqtt_module.publish('jobStatus/' + data.slaveId, JSON.stringify(msg), { qos: 2 }, function (mqttErr, mqttRes) {
                    return mqttErr ? reject(mqttErr) : resolve(data);
                });
            });

        });
    }
    const sendMail = (data) => {
        return new Promise((resolve, reject) => {
            cancellationReasons.read(
                {
                    res_id: parseInt(req.payload.resonId),
                    "res_for": "Driver"
                }, (err, res) => {
                    let params = {
                        toName: customerData.firstName + " " + customerData.lastName,
                        to: customerData.email,
                        bookingId: data.bookingId,
                        bookingDate: moment.unix(data.bookingRequestedFor).tz("Asia/Kolkata").format('MMMM Do YYYY, h:mm:ss a'),
                        resons: res.reasons[0],
                        providerName: data.providerData.firstName + ' ' + data.providerData.lastName

                    };
                    email.cancelBooking(params);
                    return resolve(data);
                });

        });
    }
    const stripeCheckOut = (data) => {
        rabbitmqUtil.InsertQueuePayroll(rabbitMq.getChannelPayroll(), rabbitMq.QueuePayroll, { id: req.auth.credentials._id }, () => { });
        return new Promise((resolve, reject) => {
            if (data.paymentMethod == 2) {
                stripe.stripeTransaction.refundCharge(
                    data.accounting.chargeId,
                    ''
                ).then(res => {
                    return resolve(data);
                }).catch(e => {
                    logger.error("cancel booking by provider stripe error", e)
                    return reject(dbErrResponse);
                });

            } else {
                return resolve(data);
            }
        });
    }
    getAssignedBooking()
        .then(getCancellationReson)
        .then(removeSchedule)
        .then(readCustomer)
        .then(jobStatusUpdate)
        .then(sendBookingResponse)
        .then(sendMail)
        .then(stripeCheckOut)
        .then(data => {
            // paymentMethod.refund({ bookingId: req.payload.bookingId }, (err, res) => { });//refun stripe charge
            return reply({ message: error['pacthCancelBooking']['200'][req.headers.lan] }).code(200);
        }).catch(e => {
            logger.error("provider statusBooking API error =>", e)
            return reply({ message: e.message }).code(e.code);
        });

};

const responseCode = {
    status: {
        500: { message: error['genericErrMsg']['500'][error['lang']] },
        404: { message: error['pacthCancelBooking']['404'][error['lang']] },
        200: {
            message: error['pacthCancelBooking']['200'][error['lang']]
        },
    }

}//swagger response code


module.exports = {
    payloadValidator,
    APIHandler,
    responseCode
};