'use strict';

module.exports.notifyiTitle = function (successCode, language, data) {
    var return_;
    switch (successCode) {
        case 1:
            return_ = 'Send booking';
            break;
        case 2:
            return_ = 'Booking ACK';
            break;
        case 3:
            return_ = 'Booking Accept';
            break;
        case 4:
            return_ = 'Booking Reject';
            break;
        case 5:
            return_ = 'Booking Ignore';
            break;
        case 6:
            return_ = 'Ontheway';
            break;

        case 7:
            return_ = 'Arrived';
            break;
        case 8:
            return_ = 'Booking Start';
            break;

        case 9:
            return_ = 'booking complete';
            break;
        case 10:
            return_ = 'Raise invoice';
            break;
        case 11:
            return_ = ' cancel by provider';
            break;
        case 12:
            return_ = ' cancel by customer';
            break;



        default:
            return_ = 'Unassigned.';

    }
    return return_;

}
module.exports.notifyiMsg = function (successCode, language, data) {
    var return_;
    switch (successCode) {
        case 1:
            return_ = 'Send booking';
            break;
        case 2:
            return_ = 'Booking ACK';
            break;
        case 3:
            return_ = 'Booking Accept';
            break;
        case 4:
            return_ = 'Booking Reject';
            break;
        case 5:
            return_ = 'Booking Ignore';
            break;
        case 6:
            return_ = 'Ontheway';
            break;

        case 7:
            return_ = 'Arrived';
            break;
        case 8:
            return_ = 'Booking Start';
            break;

        case 9:
            return_ = 'booking complete';
            break;
        case 10:
            return_ = 'Raise invoice';
            break;
        case 11:
            return_ = ' cancel by provider';
            break;
        case 12:
            return_ = ' cancel by customer';
            break;



        default:
            return_ = 'Unassigned.';


    }
    return return_;

}