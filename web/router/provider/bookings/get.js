'use strict';

const joi = require('joi');
const error = require('../error');
const logger = require('winston');
const Async = require('async');
const ObjectID = require('mongodb').ObjectID;
const moment = require('moment');//date-time  ;
const bookings = require('../../../../models/bookings');
const provider = require('../../../../models/provider');
const customer = require('../../../../models/customer');
const appConfig = require('../../../../models/appConfig');
const assignedBookings = require('../../../../models/assignedBookings');
const unassignedBookings = require('../../../../models/unassignedBookings');
const dispatchedBookings = require('../../../../models/dispatchedBookings');
const configuration = require('../../../../configuration');


Array.prototype.myFind = function (obj) {
    return this.filter(function (item) {
        for (var prop in obj)
            if (!(prop in item) || obj[prop] !== item[prop])
                return false;
        return true;
    });
};
const APIHandler = (req, reply) => {
    let request = [];
    let accepted = [];
    const requestBooking = (data) => {
        return new Promise(function (resolve, reject) {
            unassignedBookings.readAll({ providerId: req.auth.credentials._id }, (err, result) => {
                if (err) {
                    return reject(err);
                }
                else if (result === null)
                    return resolve(true);
                else {
                    Async.forEach(result, function (res, callbackloopv) {
                        provider.readByAggregate(res.latitude, res.longitude,
                            { _id: new ObjectID(req.auth.credentials._id) },
                            (err, pro) => {
                                if (err) {
                                    logger.error("get provider error : ", e);
                                } else {
                                    let distanceUnit = res.distanceMatrix == 0 ? 1000 : 1608;
                                    // let proDistance = pro[0].distance / distanceUnit;
                                    var bookingDetail = {
                                        bookingId: res.bookingId,
                                        customerId: res.slaveId,
                                        firstName: res.customerData.firstName || '',
                                        lastName: res.customerData.lastName || '',
                                        phone: res.customerData.phone,
                                        profilePic: res.customerData.profilePic || '',
                                        averageRating: res.customerData.averageRating || 0,
                                        bookingRequestedFor: res.bookingRequestedFor || '',
                                        bookingRequestedAt: res.bookingRequestedAt || '',
                                        bookingEndtime: res.bookingEndtime || "",
                                        eventStartTime: res.eventStartTime || "",
                                        currencySymbol: res.currencySymbol || '$',
                                        currency: res.currency || 'USD',
                                        distanceMatrix: res.distanceMatrix || 0,
                                        bookingExpireTime: res.expiryTimestamp || '',
                                        bookingType: res.bookingType || 1,
                                        addLine1: res.addLine1 || '',
                                        addLine2: res.addLine2 || '',
                                        city: res.city || '',
                                        state: res.state || '',
                                        country: res.country || "",
                                        placeId: res.placeId || "",
                                        pincode: res.pincode || "",
                                        latitude: res.latitude || "",
                                        longitude: res.longitude || "",
                                        typeofEvent: res.event.name || '',
                                        gigTime: res.service,
                                        distance: pro ? pro[0].distance : 0 || 0,//proDistance > 0 ? parseFloat((proDistance).toFixed(2)) : 0.00 || 0,
                                        paymentMethod: res.paymentMethod == 1 ? 'cash' : 'card',
                                        status: res.status,
                                        bookingTimer: res.bookingTimer || [],
                                        accounting: res.accounting || {},
                                        serverTime: moment().unix(),
                                        jobDiscription: res.jobDiscription || ""
                                    }
                                    request.push(bookingDetail);
                                    callbackloopv(null, request);
                                }
                            });

                    }, function (loopErr, res) {
                        resolve(request);
                    });
                }
            });

        });
    }
    const acceptedBooking = (data) => {
        return new Promise(function (resolve, reject) {
            assignedBookings.readAll({ providerId: req.auth.credentials._id }, (err, result) => {
                if (err) {
                    return reject(err);
                }
                else if (result === null)
                    return resolve(true);
                else {
                    Async.forEach(result, function (res, callbackloopv) {
                        provider.readByAggregate(res.latitude, res.longitude,
                            { _id: new ObjectID(req.auth.credentials._id) },
                            (err, pro) => {
                                console.log("--", pro);
                                if (err) {
                                    logger.error("get provider error : ", e);
                                } else {
                                    let distanceUnit = res.distanceMatrix == 0 ? 1000 : 1608;
                                    // let proDistance = pro[0].distance / distanceUnit;
                                    var bookingDetail = {
                                        bookingId: res.bookingId,
                                        customerId: res.slaveId,
                                        firstName: res.customerData.firstName || '',
                                        lastName: res.customerData.lastName || '',
                                        phone: res.customerData.phone,
                                        profilePic: res.customerData.profilePic || '',
                                        averageRating: res.customerData.averageRating || 0,
                                        bookingRequestedFor: res.bookingRequestedFor || '',
                                        bookingRequestedAt: res.bookingRequestedAt || '',
                                        bookingEndtime: res.bookingEndtime || "",
                                        eventStartTime: res.eventStartTime || "",
                                        currencySymbol: res.currencySymbol || '$',
                                        currency: res.currency || 'USD',
                                        distanceMatrix: res.distanceMatrix || 0,
                                        bookingExpireTime: res.expiryTimestamp || '',
                                        bookingType: res.bookingType || 1,
                                        addLine1: res.addLine1 || '',
                                        addLine2: res.addLine2 || '',
                                        city: res.city || '',
                                        state: res.state || '',
                                        country: res.country || "",
                                        placeId: res.placeId || "",
                                        pincode: res.pincode || "",
                                        latitude: res.latitude || "",
                                        longitude: res.longitude || "",
                                        typeofEvent: res.event.name || '',
                                        gigTime: res.service,
                                        distance: pro ? pro[0] ? pro[0].distance : 0 : 0 || 0,//proDistance > 0 ? parseFloat((proDistance).toFixed(2)) : 0.00 || 0,
                                        paymentMethod: res.paymentMethod == 1 ? 'cash' : 'card',
                                        status: res.status,
                                        bookingTimer: res.bookingTimer || [],
                                        accounting: res.accounting || {},
                                        serverTime: moment().unix(),
                                        jobDiscription: res.jobDiscription || ""
                                    }
                                    accepted.push(bookingDetail);
                                    callbackloopv(null, request);
                                }
                            });

                    }, function (loopErr, res) {
                        resolve(request);
                    });
                }
            });
        });
    }

    const getProviderStatus = (data) => {
        return new Promise((resolve, reject) => {
            provider.getProviderById(req.auth.credentials._id, (err, ProStatus) => {
                if (err)
                    return reply({ message: error['genericErrMsg']['500'][req.headers.lan] }).code(500);
                if (request.length != 0) {
                    request.sort(function (a, b) {
                        return parseInt(b.bookingId) - parseInt(a.bookingId);
                    });
                }
                if (accepted.length != 0) {
                    accepted.sort(function (a, b) {
                        return parseInt(b.bookingId) - parseInt(a.bookingId);
                    });
                }
                resolve({ request: request, accepted: accepted, status: (ProStatus.status == 3 ? 3 : 4), profileStatus: ProStatus.profileStatus });
            });
        });
    }
    requestBooking()
        .then(acceptedBooking)
        .then(getProviderStatus)
        .then(data => {
            return reply({ message: error['getBooking']['200'][req.headers.lan], data: data }).code(200);
        }).catch(e => {
            logger.error("provider getBooking API error =>", e)
            return reply({ message: e.message }).code(e.code);
        });
};

const responseCode = {
    status: {
        200: { message: error['getBooking']['200'][error['lang']], data: joi.any() },
        500: { message: error['genericErrMsg']['500'][error['lang']] }
    }

}//swagger response code


module.exports = {
    APIHandler,
    responseCode
};