'use strict';
const joi = require('joi');
const error = require('../error');
const logger = require('winston');
const Async = require('async');
const ObjectID = require('mongodb').ObjectID;
const moment = require('moment');//date-time 
const bookings = require('../../../../models/bookings');
const provider = require('../../../../models/provider');
const customer = require('../../../../models/customer');
const appConfig = require('../../../../models/appConfig');
const assignedBookings = require('../../../../models/assignedBookings');
const unassignedBookings = require('../../../../models/unassignedBookings');

const paramsValidator = joi.object({
    bookingId: joi.number().description('bookingId').error(new Error('booking id is must be integer')),
}).required();//params of provider schedule month api

let APIHandler = (req, reply) => {
    let history = [];
    const readBooking = (data) => {
        let con = { providerId: req.auth.credentials._id, bookingId: req.params.bookingId };
        return new Promise((resolve, reject) => { 
            bookings.read(con, (err, res) => {
                if (err) {
                    return reject({ message: error['genericErrMsg']['500'][req.headers.lan], code: 500 });
                } else if (res != null) {
                    return resolve(res);
                } else {
                    assignedBookings.read(con, (err, res) => {
                        if (err) {
                            return reject({ message: error['genericErrMsg']['500'][req.headers.lan], code: 500 });
                        } else if (res != null) {
                            return resolve(res);
                        } else {
                            unassignedBookings.read(con, (err, res) => {
                                if (err) {
                                    return reject({ message: error['genericErrMsg']['500'][req.headers.lan], code: 500 });
                                } else if (res != null) {
                                    return resolve(res);
                                } else {
                                    return reject({ message: error['genericErrMsg']['500'][req.headers.lan], code: 500 });
                                }
                            });
                        }
                    });
                }
            });

        });
    }

    readBooking()
        .then(data => {
            data.distance = 0;
            data.statusMsg = '';
            return reply({ message: error['getBookingById']['200'][req.headers.lan], data: data }).code(200);
        }).catch(e => {
            logger.error("provider getBooking API error =>", e)
            return reply({ message: e.message }).code(e.code);
        });
};

const responseCode = {
    status: {
        200: { message: error['getBookingById']['200']['1'], data: joi.any() },
        500: { message: error['genericErrMsg']['500']['1'] }
    }

}//swagger response code


module.exports = {
    paramsValidator,
    APIHandler,
    responseCode
};