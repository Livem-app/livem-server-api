'use strict';
const joi = require('joi');
const logger = require('winston');
const error = require('../error');
const ObjectID = require('mongodb').ObjectID;
const moment = require('moment');//date-time  
const provider = require('../../../../models/provider');
const unassignedBookings = require('../../../../models/unassignedBookings');
const dispatchedBookings = require('../../../../models/dispatchedBookings');
const mqtt_module = require('../../../../models/MQTT');
const webSocket = require('../../../../models/webSocket');
const providerUpdateStatus = require('../../../commonModels/booking/provider');

const payloadValidator = joi.object({
    bookingId: joi.number().required().integer().description('Booking Id').error(new Error("Booking Id is missing")),
    latitude: joi.number().required().description('latitude').error(new Error('Latitude must be number')),
    longitude: joi.number().required().description('longitude').error(new Error('Longitude must be number')),
}).required();//payload of provider signin api


let APIHandler = (req, reply) => {
    const getRequestedBooking = (data) => {
        return new Promise(function (resolve, reject) {
            unassignedBookings.read({ bookingId: req.payload.bookingId, providerId: req.auth.credentials._id }, (err, res) => {
                if (err)
                    return reply({ message: error['genericErrMsg']['500'][req.headers.lan] }).code(500);
                if (res === null)
                    return reply({ message: error['ackBooking']['404'][req.headers.lan] }).code(404);
                resolve(res);
            });
        })
    }
    const updateDispatch = (data) => {
        return new Promise(function (resolve, reject) {
            let updateQuery = {
                query: { bookingId: req.payload.bookingId, providerId: req.auth.credentials._id },
                data: {
                    $set: {
                        providerAck: moment().unix(),
                        status: 2,
                        statusMsg: ''
                    }

                }
            };
            dispatchedBookings.findOneAndUpdate(updateQuery, (err, data) => {
                return err ? reject(err) : resolve(data);
            });
        });
    }
    const jobStatusUpdate = (data) => {
        return new Promise(function (resolve, reject) {
            let updateQuery = {
                query: { bookingId: req.payload.bookingId },
                data: {
                    $set: { status: 2, statusMsg: error['bookingStatus']["2"], },
                    $push: {
                        jobStatusLogs: {
                            status: 2,
                            statusMsg: error['bookingStatus']["2"],
                            stausUpdatedBy: 'Provider',
                            userId: req.auth.credentials._id || '',
                            timeStamp: moment().unix(),
                            latitude: parseFloat(req.payload.latitude) || '',
                            longitude: parseFloat(req.payload.longitude) || ''
                        }
                    }
                }
            };
            unassignedBookings.findUpdate(updateQuery, (err, res) => {
                return err ? reject(err) : resolve(res);
            });

            //update booking status in provider collection
            providerUpdateStatus.updateBookingStatus(req.auth.credentials._id, req.payload.bookingId, 2);

            //send booking status in dispatcher
            webSocket.publish('dispatcher/jobStatus', updateQuery, {}, function (mqttErr, mqttRes) {
            });
        });
    }
    getRequestedBooking()
        .then(updateDispatch)
        .then(jobStatusUpdate)
        .then(data => {
            return reply({ message: error['ackBooking']['200'][req.headers.lan] }).code(200);
        }).catch(e => {
            logger.error("provider booking ACK API error =>", e)
            return reply({ message: e.message }).code(e.code);
        });
};

const responseCode = {
    status: {
        500: { message: error['genericErrMsg']['500'][error['lang']] },
        404: { message: error['ackBooking']['404'][error['lang']] },
        200: {
            message: error['ackBooking']['200'][error['lang']],
            data: joi.any()
        },
    }

}//swagger response code


module.exports = {
    payloadValidator,
    APIHandler,
    responseCode
};