'use strict';
const joi = require('joi');
const error = require('../error');
const logger = require('winston');
const Async = require('async');
const ObjectID = require('mongodb').ObjectID;
const moment = require('moment');//date-time 
const bookings = require('../../../../models/bookings');
const provider = require('../../../../models/provider');
const customer = require('../../../../models/customer');
const appConfig = require('../../../../models/appConfig');
const assignedBookings = require('../../../../models/assignedBookings');
const unassignedBookings = require('../../../../models/unassignedBookings');
const dispatchedBookings = require('../../../../models/dispatchedBookings');

const paramsValidator = joi.object({
    startDate: joi.required().description('YYYY-MM-DD').error(new Error('start date is missing')),
}).required();//params of provider schedule month api

let APIHandler = (req, reply) => {
    console.log("req", req.params);
    let history = [];
    const historyBooking = (data) => {
        return new Promise((resolve, reject) => {
            let bokingStartDate = moment(req.params.startDate).startOf('day').unix();
            let bokingEndDate = moment.unix(bokingStartDate).add(7, 'd').unix();
            bookings.readAll({
                providerId: req.auth.credentials._id,
                'bookingRequestedFor': {
                    '$gte': bokingStartDate, '$lte': bokingEndDate
                }
            }, (err, res) => {
                if (err)
                    return reject({ message: error['genericErrMsg']['500'][req.headers.lan], code: 500 });
                else {
                    Async.forEach(res, (res, callbackloopv) => {
                        var bookingDetail = {
                            bookingId: res.bookingId,
                            status: res.status,
                            statusMsg: error['bookingStatus'][res.status],
                            firstName: res.customerData.firstName || '',
                            lastName: res.customerData.lastName || '',
                            profilePic: res.customerData.profilePic || '',
                            bookingRequestedFor: res.bookingRequestedFor || '',
                            bookingRequestedAt: res.bookingRequestedAt || "",
                            eventStartTime: res.eventStartTime || "",
                            currencySymbol: res.currencySymbol || '$',
                            currency: res.currency || 'USD',
                            distanceMatrix: res.distanceMatrix || 0,
                            bookingExpireTime: res.expiryTimestamp || '',
                            bookingType: res.bookingType || 1,
                            addLine1: res.addLine1 || '',
                            addLine2: res.addLine2 || '',
                            city: res.city || '',
                            state: res.state || '',
                            country: res.country || "",
                            placeId: res.placeId || "",
                            pincode: res.pincode || "",
                            latitude: res.latitude || "",
                            longitude: res.longitude || "",
                            signatureUrl: res.signatureUrl || "",
                            event: res.event || [],
                            service: res.service || [],
                            amount: 0,
                            paymentMethod: res.paymentMethod || 0,
                            reviewByProvider: res.reviewByProvider || {},
                            reviewByCustomer: res.reviewByCustomer || {},
                            accounting: res.accounting,
                            cancellationReason: res.cancellationReason || "",
                            jobDiscription: res.jobDiscription || "",
                            serverTime: moment().unix(),

                        };
                        history.push(bookingDetail);

                        callbackloopv(null);

                    }, function (loopErr) {
                        resolve(history);
                    });
                }
            });

        });
    }

    historyBooking()
        .then(data => {
            console.log("response", data)
            return reply({ message: error['getBookingHistory']['200'][req.headers.lan], data: data }).code(200);
        }).catch(e => {
            logger.error("provider getBooking API error =>", e)
            return reply({ message: e.message }).code(e.code);
        });
};

const responseCode = {
    status: {
        200: { message: error['getBookingHistory']['200']['1'], data: joi.any() },
        500: { message: error['genericErrMsg']['500']['1'] }
    }

}//swagger response code


module.exports = {
    paramsValidator,
    APIHandler,
    responseCode
};