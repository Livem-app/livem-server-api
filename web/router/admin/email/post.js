
'use strict';
const joi = require('joi');
const Async = require('async');
const logger = require('winston');
const error = require('../error');
const ObjectID = require('mongodb').ObjectID;
const customer = require('../../../../models/customer');
const provider = require('../../../../models/provider');
const category = require('../../../../models/category');
const Auth = require('../../../middleware/authentication');
const emailProvider = require('../../../commonModels/email/provider');

const payload = joi.object({
    userId: joi.string().required().description('user Id').error(new Error('user is required')),
    userType: joi.number().required().integer().min(1).max(2).description('1- customer, 2- provider').error(new Error('user type invalid Please Enter Valid Type')),
    type: joi.number().required().integer().min(1).max(2).description('1- provider signup, 2- provider activation,').error(new Error('type invalid Please Enter Valid Type')),
}).required();


const handler = (req, reply) => {
    const getUserData = (data) => {
        return new Promise((resolve, reject) => {
            switch (req.payload.userType) {
                case 1:
                    customer.read({ _id: new ObjectID(req.payload.userId) }, (err, res) => {
                        return err ? reject(err) : resolve(res);
                    });
                    break;
                case 2:
                    provider.read({ _id: new ObjectID(req.payload.userId) }, (err, res) => {
                        return err ? reject(err) : resolve(res);
                    });
                    break;
            }
        });
    }
    const sendMail = (data) => {
        return new Promise((resolve, reject) => {
            switch (req.payload.type) {
                case 1:
                    emailProvider.signup({
                        toName: data.firstName,
                        to: data.email
                    });
                    resolve(data);
                    break;
                case 2:
                    category.read({ _id: new ObjectID(data.catlist[0].cid) }, (err, res) => {
                        emailProvider.welcome({
                            toName: data.firstName,
                            to: data.email,
                            categoryName: res.cat_name
                        });

                    });
                    resolve(data);
                    break;

            }

        });
    }
    getUserData()
        .then(sendMail)
        .then(d => {
            return reply({ message: error['genericErrMsg']['200'][req.headers.lan] }).code(200);
        }).catch(e => {
            logger.error("Provider VerifyPhoneNumber API error =>", e)
            return reply({ message: e.message }).code(e.code);
        })
};

const responseCode = {
    status: {
        500: { message: error['genericErrMsg']['500'][error['lang']] },
        200: { message: error['genericErrMsg']['200'][error['lang']] },
    }

}//swagger response code

module.exports = {
    payload,
    handler,
    responseCode
};