'use strict';

const entity = '/admin';
const error = require('../../error');
const get = require('./get');
const headerValidator = require('../../../../middleware/validator');

module.exports = [
    /**
    * @name  
    */
    {
        method: 'GET',
        path: entity + '/stripe/chargLogs/{startDate}/{endDate}',
        handler: get.handler,
        config: {
            tags: ['api', 'admin'],
            description: error['apiDescription']['getDispatchBooking'],
            auth: false,
            response: get.responseCode,
            validate: {
                params: get.params,
                headers: headerValidator.headerLanValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
];