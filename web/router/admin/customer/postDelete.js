
'use strict';
const joi = require('joi');
const Async = require('async');
const logger = require('winston');
const error = require('../error');
const customer = require('../../../../models/customer');
const zendesk = require('./../../../../models/zendesk');
const wallet = require('../../../commonModels/wallet');
const ObjectID = require('mongodb').ObjectID;

const payload = joi.object({
    ids: joi.array().required().description('Array of master ids')
}).required();


const handler = (req, reply) => {
    let notDeleteUser = 0;
    let userList = [];
    Async.each(req.payload.ids, (id, cb) => {
        try {
            customer.read({ _id: new ObjectID(id) }, (err, res) => {
                if (res && res.zendeskId && res.zendeskId != '') {
                    const url = zendesk.config.zd_api_url + '/search.json?query=requester:' + res.zendeskId;
                    zendesk.users.get(url, function (err, result) {
                        if (err) {
                            return reply(err);
                        } else {
                            let ticketId = [];
                            (result.results).forEach(element => {
                                ticketId.push(element.id);
                            });
                            var url = zendesk.config.zd_api_url + '/tickets/destroy_many.json?ids=' + ticketId;
                            zendesk.users.deleteZen(url, function (err, result) {
                                if (err) {
                                    return reply(err);
                                } else {
                                    var query = zendesk.config.zd_api_url + '/users/' + parseInt(res.zendeskId) + '.json'
                                    zendesk.users.deleteZen(query, function (err, result) {
                                        if (err) {
                                            cb(null);
                                        } else {
                                            if (result.error == 'RecordInvalid') {
                                                notDeleteUser += 1;
                                                userList.push(res);
                                                cb(null);
                                            } else {
                                                wallet.user.users.userDelete({
                                                    userType: 1,
                                                    userId: id
                                                }, (err, res) => {
                                                    if (err) {
                                                        cb(null);
                                                    } else {
                                                        customer.deleteById(id, (err, result) => {
                                                            if (err) {
                                                                return cb(err);
                                                            } else {
                                                                return cb(null, result);
                                                            }
                                                        });
                                                    }
                                                })
                                            }

                                        }
                                    });
                                }
                            });
                        }
                    });

                } else {
                    customer.deleteById(id, (err, result) => {
                        if (err) {
                            return cb(err);
                        } else {
                            return cb(null, result);
                        }
                    });
                }

            });

        } catch (e) {
            return cb(e);
        }
    }, (err, result) => {
        if (notDeleteUser != 0) {
            return reply({ errFlag: 1, userList: userList, notDeleteUser: notDeleteUser, message: error['genericErrMsg']['200'][req.headers.lan] }).code(200);
        } else {
            return reply({ errFlag: 0, notDeleteUser: notDeleteUser, message: error['genericErrMsg']['200'][req.headers.lan] }).code(200);
        }

    });
};

const responseCode = {
    // status: {
    //     500: { message: error['genericErrMsg']['500'][error['lang']] },
    //     200: { message: error['genericErrMsg']['200'][error['lang']], data: joi.any() },
    // }

}//swagger response code

module.exports = {
    payload,
    handler,
    responseCode
};