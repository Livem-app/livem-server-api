'use strict';

const entity = '/admin';
const error = require('../error');
const postBan = require('./postBan');
const postDelete = require('./postDelete');
const getReviewAndRating = require('./getReviewAndRating');
const headerValidator = require('../../../middleware/validator');

module.exports = [
    /**
    * @name  
    */
    {
        method: 'POST',
        path: entity + '/customer/ban',
        handler: postBan.handler,
        config: {
            tags: ['api', 'admin'],
            description: error['apiDescription']['postCustomerBan'],
            auth: false,
            response: postBan.responseCode,
            validate: {
                headers: headerValidator.headerLanValidator,
                payload: postBan.payload,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
    {
        method: 'GET',
        path: entity + '/customer/reviewAndRating/{customerId}/{startDate}/{endDate}',
        handler: getReviewAndRating.handler,
        config: {
            tags: ['api', 'admin'],
            description: error['apiDescription']['getAllBooking'],
            auth: false,
            // response: getReviewAndRating.responseCode,
            validate: {
                params: getReviewAndRating.params,
                headers: headerValidator.headerLanValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
    {
        method: 'POST',
        path: entity + '/customer/delete',
        handler: postDelete.handler,
        config: {
            tags: ['api', 'admin'],
            description: error['apiDescription']['getAllBooking'],
            auth: false,
            // response: postDelete.responseCode,
            validate: {
                headers: headerValidator.headerLanValidator,
                payload: postDelete.payload,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
];