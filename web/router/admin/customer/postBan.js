
'use strict';
const joi = require('joi');
const Async = require('async');
const moment = require('moment');//date-time
const logger = require('winston');
const error = require('../error');
const ObjectID = require('mongodb').ObjectID;
const Timestamp = require('mongodb').Timestamp;
const fcm = require('../../../../library/fcm');
const customer = require('../../../../models/customer');
const Auth = require('../../../middleware/authentication');
const email = require('../../../commonModels/email/customer');

const payload = joi.object({
    ids: joi.array().required().description('Array of master ids'),
    status: joi.number().integer().min(0).max(1).description('0- active , 1 - Ban').error(new Error('status is missing')),
}).required();


const handler = (req, reply) => {
    Async.each(req.payload.ids, (id, cb) => {
        console.log(id);
        try {
            let status = req.payload.status == 0 ? 3 : 4;
            let statusMsg = status == 4 ? 'Banned' : 'Active';
            let setData = {
                $set: {
                    status: status,
                    statusMsg: statusMsg,
                    statusUpdateTime: new Timestamp(1, moment().unix()), 
                    loggedIn: false,
                    accessCode: '',
                }
            };
            customer.findOneAndUpdate(id, setData, (err, result) => {
                if (err) {
                    return cb(err);
                } else {
                    if (status == 4) {
                        let con = { _id: ObjectID(id) };
                        customer.read(con, (err, res) => {
                            let request = {
                                fcmTopic: res.fcmTopic,
                                action: 23,
                                pushType: 3,
                                title: "Customer Banned",
                                msg: "Your profile has been banned by our customer management team",
                                data: [],
                                deviceType: res.mobileDevices.deviceType
                            }
                            fcm.notifyFcmTpic(request, (e, r) => { });
                            let params = {
                                toName: res.firstName + " " + res.lastName,
                                to: res.email,

                            };
                            email.deActiveByAdmin(params);
                        });
                    }
                    return cb(null, result);
                }
            });
        } catch (e) {

            return cb(e);
        }
    }, (err, result) => {
        if (err) {
            console.log("err", err)
            return reply({ message: error['genericErrMsg']['500'][req.headers.lan] }).code(500);
        } else {
            return reply({ message: error['genericErrMsg']['200'][req.headers.lan] }).code(200);
        }
    });
};

const responseCode = {
    status: {
        500: { message: error['genericErrMsg']['500'][error['lang']] },
        200: { message: error['genericErrMsg']['200'][error['lang']] },
    }

}//swagger response code

module.exports = {
    payload,
    handler,
    responseCode
};