
// 'use strict';

const joi = require('joi');
const Async = require('async');
const error = require('../../error');
const moment = require('moment');//date-time
const logger = require('winston');
const ObjectId = require('mongodb').ObjectID;
const provider = require('../../../../../models/provider');
const operator = require('../../../../../models/operator');

const params = joi.object({
    providerId: joi.string().description('provider Id').error(new Error('providerId is missing'))
}).required();

const handler = (req, reply) => {
    let providerArr = [];
    let condition = {};
    condition = { "_id": new ObjectId(req.params.providerId) }
    Async.series([
        function (callback) { 
            provider.read(condition, (err, provider) => {
                if (err) {
                    return callback(err);
                } else if (provider === null) {
                    return callback(null, provider);
                } else {
                    var operat = [];
                    var prov = {
                        email: provider.email,
                        firstName: provider.firstName,
                        lastName: provider.lastName,
                        profilePic: provider.profilePic,
                        phone: provider.phone,
                        Type: (provider.accountType == 1) ? 'Independent Contractor' : (provider.accountType == 2) ? 'Operator' : '',
                        operator: operat
                    }
                    if (provider.companyId && provider.companyId != '') {
                        var cond = { "_id": new ObjectId(provider.companyId) }
                        operator.read(cond, (err, res1) => {

                            operat.push({ name: res1.operatorName });

                            return callback(null, provider);
                        });

                    } else {
                        operat.push({ name: "" });
                        providerArr.push({
                            prov
                        })
                        return callback(null, provider);
                    }
                    var prov = {
                        email: provider.email,
                        firstName: provider.firstName,
                        lastName: provider.lastName,
                        profilePic: provider.profilePic,
                        phone: provider.phone,
                        Type: (provider.accountType == 1) ? 'Independent Contractor' : 'Operator',
                        operator: operat
                    }

                    providerArr.push({
                        prov
                    });
                }
            });
        }

    ], function (err, result) {
        if (err) {
            logger.error("Admin All provider API error =>", err)
            return reply({ message: error['genericErrMsg']['500'][req.headers.lan] }).code(500);
        } else {
            return reply({ message: error['genericErrMsg']['200'][req.headers.lan], data: providerArr }).code(200);
        }

    });
};

const responseCode = {
    status: {
        // 500: { message: error['genericErrMsg']['500'][error['lang']] },
        // 200: {
        //     message: error['getProvider']['200'][error['lang']],
        //     data: joi.any()
        // },
    }

}//swagger response code

module.exports = {
    params,
    handler,
    responseCode
};