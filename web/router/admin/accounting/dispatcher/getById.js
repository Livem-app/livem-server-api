
'use strict';

const joi = require('joi');
const Async = require('async');
const error = require('../../error');
const moment = require('moment');//date-time
const logger = require('winston');
const bookings = require('../../../../../models/dispatchedBookings');
const appConfig = require('../../../../../models/appConfig');
const ObjectID = require('mongodb').ObjectID;
 

const params = joi.object({ 
    bookingId: joi.string().description('booking Ids').error(new Error('bookingId is missing'))
}).required();

const handler = (req, reply) => { 
    let bookingArr = [];
    let condition = {};
    var arr = req.params.bookingId.split(",").map(function (val) { return Number(val); });
    var newArr = [];
    for (var i = 0; i < arr.length; i++) {
        newArr[i] = parseInt(arr[i]);
    }   
    condition = {"bookingId" : {$in: newArr}}
    var aconfig = []
        appConfig.read((err, conf) => {
        if (err) {
            return callback(err);
        } else {
            aconfig.push({ currencySymbol: conf.currencySymbol });
            
        }
    });
    
    Async.series([
        function (callback) {
            bookings.readAllBooking(condition, (err, res) => {
                if (err) {
                    return callback(err);
                } else if (res === null) {
                    return callback(null, res);
                } else { 
    
                    res.forEach(booking => {
                            

                        bookingArr.push({
                            dispatcherId: booking.dispatcherId,
                            dispatchTime: booking.dispatchedByServerAt,
                            expiryTime: booking.expiryTimestamp,
                            promoCode: booking.promoCode,
                            ackTime: booking.providerAck,
                            status: booking.statusMsg,
                            bookingTable: "bookings",
                        });
                    });
                    return callback(null, res);
                }
            });
        }        
    ], function (err, result) {
        if (err) {
            logger.error("Admin All booking API error =>", err)
            return reply({ message: error['genericErrMsg']['500'][req.headers.lan] }).code(500);
        } else {
            if (bookingArr.length != 0) {
                bookingArr.sort((a, b) => {
                    return (b.bookingId) - (a.bookingId);
                });
            }
            return reply({ message: error['genericErrMsg']['200'][req.headers.lan], data: bookingArr }).code(200);
        }
    });
};

const responseCode = {
    status: {
        // 500: { message: error['genericErrMsg']['500'][error['lang']] },
        // 200: {
        //     message: error['getProvider']['200'][error['lang']],
        //     data: joi.any()
        // },
    }

}//swagger response code

module.exports = {
    params,
    handler,
    responseCode
};