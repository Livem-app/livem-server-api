
'use strict';

const joi = require('joi');
const Async = require('async');
const error = require('../../error');
const moment = require('moment');//date-time
const logger = require('winston');
const ObjectId = require('mongodb').ObjectID;
const customer = require('../../../../../models/customer'); 

const params = joi.object({
    // status: joi.number().integer().description('status').error(new Error('status is missing')),
    customerId: joi.string().description('customer Id').error(new Error('customerId is missing'))
}).required();

const handler = (req, reply) => { 
    let customerArr = [];
    let condition = {};
    

    condition = {"_id" : new ObjectId(req.params.customerId)}

    
    Async.series([
        function (callback) { 
            customer.readAll(condition, (err, res) => {
                if (err) {
                    return callback(err);
                } else if (res === null) {
                    return callback(null, res);
                } else {
                    res.forEach(customer => {
                        customerArr.push({
                            email: customer.email,
                            firstName: customer.firstName,
                            lastName: customer.lastName,
                            profilePic: customer.profilePic,
                            phone: customer.phone,
                        });
                    });
                    return callback(null, res);
                }
            });
        }
        
    ], function (err, result) {
        if (err) {
            logger.error("Admin All customer API error =>", err)
            return reply({ message: error['genericErrMsg']['500'][req.headers.lan] }).code(500);
        } else { 
            return reply({ message: error['genericErrMsg']['200'][req.headers.lan], data: customerArr }).code(200);
        }

    });


};

const responseCode = {
    status: {
        // 500: { message: error['genericErrMsg']['500'][error['lang']] },
        // 200: {
        //     message: error['getcustomer']['200'][error['lang']],
        //     data: joi.any()
        // },
    }

}//swagger response code

module.exports = {
    params,
    handler,
    responseCode
};