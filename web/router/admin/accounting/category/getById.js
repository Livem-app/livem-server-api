
'use strict';

const joi = require('joi');
const Async = require('async');
const error = require('../../error');
const moment = require('moment');//date-time
const logger = require('winston');
const ObjectId = require('mongodb').ObjectID;
const category = require('../../../../../models/category');
const busGr = require('../../../../../models/businessGroup');

const params = joi.object({
    categoryId: joi.string().description('category Id').error(new Error('categoryId is missing'))
}).required();

const handler = (req, reply) => {
    let categoryArr = [];
    let condition = {};
    condition = { "_id": new ObjectId(req.params.categoryId) }
    Async.series([
        function (callback) {
            category.read(condition, (err, category) => {
                if (err) {
                    return callback(err);
                } else if (category === null) {
                    return callback(null, category);
                } else {
                    var busGrp = [];
                    var catg = {
                        name: category.cat_name,
                        serviceType: category.service_type,
                        billingModel: category.billing_model,
                        busGroup: busGrp
                    }
                    if (category.bussinessGroup && category.bussinessGroup != '') {
                        return callback(null, category);
                    } else {
                        busGrp.push({ name: "" });
                        categoryArr.push({
                            catg
                        })

                        return callback(null, category);

                    }
                    var catg = {
                        name: category.cat_name,
                        serviceType: category.service_type,
                        billingModel: category.billing_model,
                        busGroup: busGrp
                    }

                    categoryArr.push({
                        catg
                    })
                }
            });
        }

    ], function (err, result) {
        if (err) {
            logger.error("Admin All customer API error =>", err)
            return reply({ message: error['genericErrMsg']['500'][req.headers.lan] }).code(500);
        } else {
            return reply({ message: error['genericErrMsg']['200'][req.headers.lan], data: categoryArr }).code(200);
        }

    });


};

const responseCode = {
    status: {
        // 500: { message: error['genericErrMsg']['500'][error['lang']] },
        // 200: {
        //     message: error['getcustomer']['200'][error['lang']],
        //     data: joi.any()
        // },
    }

}//swagger response code

module.exports = {
    params,
    handler,
    responseCode
};