'use strict';

const entity = '/admin';
const error = require('../error');
const getAll = require('./get');
const getById = require('./getById');
const getProvider = require('./provider/getById')
const getCustomer = require('./customer/getById')
const getDispatcher = require('./dispatcher/getById')
const getCategory  = require('./category/getById')

const headerValidator = require('../../../middleware/validator');

module.exports = [
    /**
    * @name  
    */
    {
        method: 'GET',
        path: entity + '/accounting/{startDate}/{endDate}/{paymentType}',
        handler: getAll.handler,
        config: {
            tags: ['api', 'admin'],
            description: error['apiDescription']['getAccounting'],
            auth: false,
            response: getAll.responseCode,
            validate: {
                params: getAll.params,
                headers: headerValidator.headerLanValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },

    /**
    * @name  
    */

    {
        method: 'GET',
        path: entity + '/accounting/{bookingId}',
        handler: getById.handler,
        config: {
            tags: ['api', 'admin'],
            description: error['apiDescription']['getAccountingById'],
            auth: false,
            response: getById.responseCode,
            validate: {
                params: getById.params,
                headers: headerValidator.headerLanValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },


     /**
    * @name  
    */
    
    {
        method: 'GET',
        path: entity + '/accounting/provider/{providerId}',
        handler: getProvider.handler,
        config: {
            tags: ['api', 'admin'],
            description: error['apiDescription']['getProviderById'],
            auth: false,
            response: getProvider.responseCode,
            validate: {
                params: getProvider.params,
                headers: headerValidator.headerLanValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },


    /**
    * @name  
    */
    
    {
        method: 'GET',
        path: entity + '/accounting/customer/{customerId}',
        handler: getCustomer.handler,
        config: {
            tags: ['api', 'admin'],
            description: error['apiDescription']['getCustomerById'],
            auth: false,
            response: getCustomer.responseCode,
            validate: {
                params: getCustomer.params,
                headers: headerValidator.headerLanValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
    
    /**
    * @name  
    */
    
    {
        method: 'GET',
        path: entity + '/accounting/dispatcher/{bookingId}',
        handler: getDispatcher.handler,
        config: {
            tags: ['api', 'admin'],
            description: error['apiDescription']['getCustomerById'],
            auth: false,
            response: getDispatcher.responseCode,
            validate: {
                params: getDispatcher.params,
                headers: headerValidator.headerLanValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
    
    /**
    * @name  
    */
    
    {
        method: 'GET',
        path: entity + '/accounting/category/{categoryId}',
        handler: getCategory.handler,
        config: {
            tags: ['api', 'admin'],
            description: error['apiDescription']['getCategoryById'],
            auth: false,
            response: getCategory.responseCode,
            validate: {
                params: getCategory.params,
                headers: headerValidator.headerLanValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
    
    
    
];