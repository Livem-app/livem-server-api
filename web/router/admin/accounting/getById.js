
'use strict';

const joi = require('joi');
const Async = require('async');
const error = require('../error');
const moment = require('moment');//date-time
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;
const bookings = require('../../../../models/bookings');
const customer = require('../../../../models/customer');
const assignedBookings = require('../../../../models/assignedBookings');
const unassignedBookings = require('../../../../models/unassignedBookings');

const params = joi.object({
    bookingId: joi.number().min(1).description('booking Id').error(new Error('bookingId is missing'))
}).required();

const handler = (req, reply) => {
    let customerData = '';
    const getBooking = (data) => {
        return new Promise((resolve, reject) => {
            let con = { bookingId: req.params.bookingId };
            unassignedBookings.read(con, (err, res) => {
                if (err) {
                    return reject({ message: error['genericErrMsg']['500'][req.headers.lan], data: 500 });
                } else if (res == null) {
                    assignedBookings.read(con, (err, res) => {
                        if (err) {
                            return reject({ message: error['genericErrMsg']['500'][req.headers.lan], data: 500 });
                        } else if (res == null) {
                            bookings.read(con, (err, res) => {
                                if (err) {
                                    return reject({ message: error['genericErrMsg']['500'][req.headers.lan], data: 500 });
                                } else if (res == null) {
                                    return reject({ message: error['getBookingDetails']['404'][req.headers.lan], data: 404 });
                                } else {
                                    resolve(res);
                                }
                            });
                        } else {
                            resolve(res);
                        }
                    });
                } else {
                    resolve(res);
                }
            });
        });
    }//get booking
    const readCustomer = (data) => {
        return new Promise((resolve, reject) => {
            let con = { _id: new ObjectID(data.slaveId) };
            customer.read(con, (err, res) => {
                customerData = res;
                return err ? reject(err) : resolve(data);
            });
        });
    }//read customer
    const responseData = (ures) => {
        let bookingArr = [];
        return new Promise((resolve, reject) => {
            bookingArr.push({
                bookingId: ures.bookingId,
                slaveId: ures.slaveId,
                providerId: ures.providerId,
                promoCode: ures.promoCode,
                bookingRequestedAt: ures.bookingRequestedAt,
                bookingRequestedFor: ures.bookingRequestedFor,
                expiryTimestamp: ures.expiryTimestamp,
                bookingType: (ures.bookingType == 1) ? 'Now' : 'Later',
                paymentMethod: (ures.paymentMethod == 1) ? 'Cash' : (ures.paymentMethod == 2) ? 'Card' : 'Wallet',
                status: ures.status,
                statusMsg: error['bookingStatus'][ures.status],
                customerData: ures.customerData,
                providerData: ures.providerData,
                accounting: ures.accounting,
                category: ures.category.name,
                categoryId: ures.category.id,
                address: ures.addLine1,
                bookingAddress: ures.addLine1 + ' ' + ures.addLine2,
                bookingCity: ures.city,
                bookingState: ures.state,
                services: ures.service,
                bookingCountry: ures.country,
                bookingPin: ures.pincode,
                currencySymbol: ures.currencySymbol,
                currency: ures.currency,
                lat: ures.latitude,
                long: ures.longitude,
                jobLog: ures.jobStatusLogs,
                signatureUrl: ures.signatureUrl || '',
                dispatcherLog: ures.jobDispatchLogs,
                custEmail: customerData ? customerData.email : '' || '',
                bookingTable: "bookings",
            });
            return resolve(bookingArr);
        });
    }

    getBooking()
        .then(readCustomer)
        .then(responseData)
        .then(data => {
            return reply({ message: error['genericErrMsg']['200'][req.headers.lan], data: data }).code(200);
        })
        .catch(e => {
            logger.error("Admin All booking API error =>", e)
            return reply({ message: error['genericErrMsg']['500'][req.headers.lan] }).code(500);
        });
};

const responseCode = {
    status: {
        // 500: { message: error['genericErrMsg']['500'][error['lang']] },
        // 200: {
        //     message: error['getProvider']['200'][error['lang']],
        //     data: joi.any()
        // },
    }

}//swagger response code

module.exports = {
    params,
    handler,
    responseCode
};