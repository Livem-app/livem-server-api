'use strict';

const entity = '/admin';
const get = require('./get');
const error = require('../error');
const postForgot = require('./postForgot');
const postReset = require('./postReset');

const headerValidator = require('../../../middleware/validator');

module.exports = [

    {
        method: 'GET',
        path: entity + '/hashPassword/{password}',
        handler: get.handler,
        config: {
            tags: ['api', 'admin'],
            description: error['apiDescription']['getHashPassword'],
            auth: false,
            response: get.responseCode,
            validate: {
                headers: headerValidator.headerLanValidator,
                params: get.params,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
    {
        method: 'POST',
        path: entity + '/forgotPassword',
        handler: postForgot.handler,
        config: {
            tags: ['api', 'admin'],
            description: error['apiDescription']['postForgotPassword'],
            auth: false,
            // response: postForgot.responseCode,
            validate: {
                headers: headerValidator.headerLanValidator,
                payload: postForgot.payload,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
    {
        method: 'POST',
        path: entity + '/resetPassword',
        handler: postReset.handler,
        config: {
            tags: ['api', 'admin'],
            description: error['apiDescription']['postResetPassword'],
            auth: false,
            // response: postReset.responseCode,
            validate: {
                headers: headerValidator.headerLanValidator,
                payload: postReset.payload,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
];