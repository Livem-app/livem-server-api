
'use strict';

const joi = require('joi');
const Async = require('async');
const error = require('../error');
const moment = require('moment');//date-time
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;
var Bcrypt = require('bcrypt');
const configuration = require('../../../../configuration');


const params = joi.object({
    password: joi.string().description('Password').error(new Error('Password')),
}).required();


const handler = (req, reply) => {
    let password = Bcrypt.hashSync(req.params.password, parseInt(configuration.SALT_ROUND));
    return reply({ message: error['genericErrMsg']['200'][req.headers.lan], data: password }).code(200);
};

const responseCode = {
    status: {
        // 500: { message: error['genericErrMsg']['500'][error['lang']] },
        // 200: {
        //     message: error['getProvider']['200'][error['lang']],
        //     data: joi.any()
        // },
    }

}//swagger response code

module.exports = {
    params,
    handler,
    responseCode
};