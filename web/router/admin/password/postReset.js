
'use strict';
const joi = require('joi');
const Async = require('async');
const jwt = require('jsonwebtoken');
const logger = require('winston');
var md5 = require('md5');
const moment = require('moment-timezone');
var Bcrypt = require('bcrypt');
const ObjectID = require('mongodb').ObjectID;
const error = require('../error');
const adminUsers = require('../../../../models/adminUsers');
const verificationCode = require('../../../../models/verificationCode');
const provider = require('../../../../models/provider');
const customer = require('../../../../models/customer');
const Auth = require('../../../middleware/authentication');
const configuration = require('../../../../configuration');
const emailForCustomer = require('../../../commonModels/email/customer');
const emailForProvider = require('../../../commonModels/email/provider');



const payload = joi.object({
    token: joi.string().required().description('Token'),
    password: joi.string().required().description('Password')
}).required();


const handler = (req, reply) => {
    let token = req.payload.token;
    let collectionName = '';
    Async.waterfall([
        function (cb) {
            jwt.verify(token, configuration.JWT_KEY, function (err, decoded) {
                if (err) {
                    return reply({ errNum: 200, errMsg: 'Link is expired', errFlag: 1 });
                } else {
                    return cb(null, decoded);
                }


            });
        }, //verify if the token is valid

        function (decoded, cb) {
            verificationCode.read({ userId: (decoded.id), status: true }, (err, doc) => {
                if (err)
                    return cb(err);
                else if (doc === null)
                    return cb({ errNum: 400, errMsg: 'Link expired!', errFlag: 1 });
                else if (doc.lastToken === token)
                    return cb({ errNum: 400, errMsg: 'Link expired!', errFlag: 1 });
                else
                    return cb(null, decoded, doc);
            });
        }, //verify if the reset password request is a valid & link cannot be used multiple times

        function (decoded, doc, cb) {
            if (parseInt(decoded.type) == 1) {
                customer.read({ _id: new ObjectID(doc.userId) }, (err, user) => {
                    if (err)
                        return cb(err);
                    else if (doc === null)
                        return cb({ errNum: 400, errMsg: 'Link expired!', errFlag: 1 });
                    else
                        return cb(null, decoded, doc, user);
                });
            } else {
                provider.read({ _id: new ObjectID(doc.userId) }, (err, user) => {
                    if (err)
                        return cb(err);
                    else if (doc === null)
                        return cb({ errNum: 400, errMsg: 'Link expired!', errFlag: 1 });
                    else
                        return cb(null, decoded, doc, user);
                });
            }
        }, //check if the user is valid

        function (decoded, doc, user, cb) {
            let password = Bcrypt.hashSync(req.payload.password, parseInt(configuration.SALT_ROUND));

            let queryObject = { $set: { password: password } };

            if (parseInt(decoded.type) == 1) {
                customer.findOneAndUpdate(doc.userId, queryObject, (err, users) => {
                    if (err) {
                        return cb(err);
                    } else if (doc === null) {
                        return cb({ errNum: 400, errMsg: 'Link expired!', errFlag: 1 });
                    } else {
                        let params = {
                            toName: user.firstName + " " + user.lastName,
                            to: user.email,
                            DateOfChange: moment.tz("Asia/Kolkata").format('MMMM Do YYYY, h:mm:ss a')
                        }; 
                        emailForCustomer.passwordUpdatedForCustomer(params)
                        return cb(null, doc);
                    }
                });
            } else {
                provider.findOneAndUpdate(doc.userId, queryObject, (err, users) => {
                    if (err) {
                        return cb(err);
                    } else if (doc === null) {
                        return cb({ errNum: 400, errMsg: 'Link expired!', errFlag: 1 });
                    } else {
                        let params = {
                            toName: user.firstName + " " + user.lastName,
                            to: user.email,
                            DateOfChange: moment.tz("Asia/Kolkata").format('MMMM Do YYYY, h:mm:ss a')
                        }; 
                        emailForProvider.passwordUpdatedForProvider(params)
                        return cb(null, doc);
                    }
                });
            }
        }, //update the hashed password

        function (doc, cb) {
            let queryObject = {
                query: { _id: new ObjectID(doc._id), status: true },
                data: {
                    $set: {
                        status: false,
                        verified: true,
                        lastToken: token
                    }
                }
            };
            verificationCode.updateAfterVerify(queryObject, (err, res) => {
                if (err)
                    return cb(err);
                return cb(null, doc);
            });
        }, //update the lastToken used
    ], (err, result) => {
        if (err)
            return reply(err);
        return reply({ errNum: 200, errMsg: 'Password updated successfuly', errFlag: 0 });
    });

};

const responseCode = {
    // status: {
    //     500: { message: error['genericErrMsg']['500'][error['lang']] },
    //     200: { message: error['genericErrMsg']['200'][error['lang']] },
    // }

}//swagger response code

module.exports = {
    payload,
    handler,
    responseCode
};