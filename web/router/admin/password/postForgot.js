
'use strict';
const joi = require('joi');
const Async = require('async');
const logger = require('winston');
const error = require('../error');
const adminUsers = require('../../../../models/adminUsers');
const Auth = require('../../../middleware/authentication');

const payload = joi.object({
    email: joi.string().required().description('email id').error(new Error('email is required'))
}).required();


const handler = (req, reply) => {
    var condition = {
        'email': req.payload.email
    }
    adminUsers.read({
        'email': req.payload.email
    }, (err, res) => {
        if (err) {
            return reply({ message: error['genericErrMsg']['500'][req.headers.lan] }).code(500);
        } else if (res === null) {
            return reply({ message: error['genericErrMsg']['404'][req.headers.lan] }).code(404);
        } else {
            // var params = {
            //     to: result.email,
            //     id: result._id,
            //     toName: result.name
            // };
            // email.adminForgotpassword(params);
            return reply({ message: error['postForgotPassword']['200'][req.headers.lan] }).code(200);
        }
    });

};

const responseCode = {
    status: {
        500: { message: error['genericErrMsg']['500'][error['lang']] },
        200: { message: error['postForgotPassword']['200'][error['lang']] },
    }

}//swagger response code

module.exports = {
    payload,
    handler,
    responseCode
};