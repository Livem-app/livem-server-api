'use strict';

const entity = '/admin';
const error = require('../error');
const post = require('./post'); 
const headerValidator = require('../../../middleware/validator');

module.exports = [
    /**
    * @name  
    */
    {
        method: 'POST',
        path: entity + '/godsview/login',
        handler: post.handler,
        config: {
            tags: ['api', 'admin'],
            description: "Godsview Login api",
            auth: false,
            // response: post.responseCode,
            validate: {
                headers: headerValidator.headerLanValidator,
                payload: post.payload,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
];