
'use strict';

const joi = require('joi');
const Async = require('async');
const error = require('../error');
const moment = require('moment');//date-time
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;
const provider = require('../../../../models/provider');

const params = joi.object({
    providerId: joi.string().description('providerId'),
    startDate: joi.number().allow('').description('start date timeStamp').error(new Error('start date is missing')),
    endDate: joi.number().allow('').description('start date timeStamp').error(new Error('end date is missing')),
}).required();

const handler = (req, reply) => {
    let reviewArr = [];
    let condition = {}; 
    if (req.params.startDate != "0" || req.params.startDate != 0 || req.params.startDate != "" || req.params.endDate != "") {
        condition = [
            { '$unwind': '$reviews' },
            {
                '$match': {
                    "reviews.reviewAt":
                    {
                        "$gte": req.params.startDate, "$lte": req.params.endDate
                    }, _id: ObjectID(req.params.providerId)
                }
            }

        ];
    } else {
        condition = [
            { '$unwind': '$reviews' },
            { '$match': { _id: ObjectID(req.params.providerId) } }
        ];
    }
    Async.series([
        function (callback) { 
            provider.readByAggregateMethod(condition, (err, res) => {
                if (err) {
                    return callback(err);
                } else if (res === null) {
                    return callback(null, res);
                } else {
                    res.forEach(rev => { 
                        reviewArr.push({
                            bookingId: rev.reviews.bookingId,
                            review: rev.reviews.review,
                            rating: rev.reviews.rating,
                            reviewAt: rev.reviews.reviewAt,
                            firstName: rev.reviews.firstName,
                            lastName: rev.reviews.lastName,
                            profilePic: rev.reviews.profilePic,
                            userId: rev.reviews.userId
                        });
                    });
                    return callback(null, res);
                }
            });
            // bookings.readAll(condition, (err, res) => {
            //     if (err) {
            //         return callback(err);
            //     } else if (res === null) {
            //         return callback(null, res);
            //     } else {
            //         res.forEach(booking => {
            //             bookingArr.push({
            //                 bookingId: booking.bookingId,
            //                 slaveId: booking.slaveId,
            //                 providerId: booking.providerId,
            //                 bookingRequestedAt: booking.bookingRequestedAt,
            //                 bookingRequestedFor: booking.bookingRequestedFor,
            //                 expiryTimestamp: booking.expiryTimestamp,
            //                 bookingType: (booking.bookingType == 1) ? 'Now' : 'Later',
            //                 paymentMethod: (booking.paymentMethod == 1) ? 'Cash' : (booking.paymentMethod == 2) ? 'Card' : 'Wallet',
            //                 status: booking.status,
            //                 statusMsg: error['bookingStatus'][booking.status],
            //                 customerData: booking.customerData,
            //                 providerData: booking.providerData,
            //                 accounting: booking.accounting,
            //                 category: booking.category.name,
            //                 address: booking.addLine1,
            //                 bookingTable: "bookings",
            //             });
            //         });
            //         return callback(null, res);
            //     }
            // });
        },

    ], function (err, result) {
        if (err) {
            logger.error("Admin All booking API error =>", err)
            return reply({ message: error['genericErrMsg']['500'][req.headers.lan] }).code(500);
        } else {
            // if (bookingArr.length != 0) {
            //     bookingArr.sort((a, b) => {
            //         return (b.bookingId) - (a.bookingId);
            //     });
            // }
            return reply({ message: error['genericErrMsg']['200'][req.headers.lan], data: reviewArr }).code(200);
        }

    });


};

const responseCode = {
    status: {
        // 500: { message: error['genericErrMsg']['500'][error['lang']] },
        // 200: {
        //     message: error['getProvider']['200'][error['lang']],
        //     data: joi.any()
        // },
    }

}//swagger response code

module.exports = {
    params,
    handler,
    responseCode
};