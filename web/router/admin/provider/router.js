'use strict';

const entity = '/admin';
const postLogout = require('./postLogout');
const postSignUp = require('./postSignUp');
const postBan = require('./postBan');
const getReviewAndRating = require('./getReviewAndRating');
const postMakeOffline = require('./postMakeOffline');
const getView = require('./getView');
const getShiftLogs = require('./getShiftLogs');
const error = require('../error');
const postDelete = require('./postDelete');
const headerValidator = require('../../../middleware/validator');

module.exports = [
    /**
    * @name  
    */
    {
        method: 'POST',
        path: entity + '/provider/logout',
        handler: postLogout.handler,
        config: {
            tags: ['api', 'admin'],
            description: error['apiDescription']['postLogout'],
            auth: false,
            response: postLogout.responseCode,
            validate: {
                headers: headerValidator.headerLanValidator,
                payload: postLogout.payload,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
    /**
    * @name  
    */
    {
        method: 'POST',
        path: entity + '/provider/ban',
        handler: postBan.handler,
        config: {
            tags: ['api', 'admin'],
            description: error['apiDescription']['postBan'],
            auth: false,
            response: postBan.responseCode,
            validate: {
                headers: headerValidator.headerLanValidator,
                payload: postBan.payload,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },

    /**
   * @name  
   */
    {
        method: 'POST',
        path: entity + '/provider/makeOffline',
        handler: postMakeOffline.handler,
        config: {
            tags: ['api', 'admin'],
            description: error['apiDescription']['postMakeOffline'],
            auth: false,
            response: postMakeOffline.responseCode,
            validate: {
                headers: headerValidator.headerLanValidator,
                payload: postMakeOffline.payload,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
    {
        method: 'GET',
        path: entity + '/provider/reviewAndRating/{providerId}/{startDate}/{endDate}',
        handler: getReviewAndRating.handler,
        config: {
            tags: ['api', 'admin'],
            description: error['apiDescription']['getAllBooking'],
            auth: false,
            // response: getReviewAndRating.responseCode,
            validate: {
                params: getReviewAndRating.params,
                headers: headerValidator.headerLanValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
    {
        method: 'GET',
        path: entity + '/provider/view/{providerId}/{startDate}/{endDate}',
        handler: getView.handler,
        config: {
            tags: ['api', 'admin'],
            description: "get Provider view customer list",
            auth: false,
            // response: getReviewAndRating.responseCode,
            validate: {
                params: getView.params,
                headers: headerValidator.headerLanValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
    {
        method: 'GET',
        path: entity + '/provider/shiftLogs/{providerId}/{startDate}/{endDate}',
        handler: getShiftLogs.handler,
        config: {
            tags: ['api', 'admin'],
            description: "get Provider daily shift logs",
            auth: false,
            // response: getShiftLogs.responseCode,
            validate: {
                params: getShiftLogs.params,
                headers: headerValidator.headerLanValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
    {
        method: 'POST',
        path: entity + '/provider/delete',
        handler: postDelete.handler,
        config: {
            tags: ['api', 'admin'],
            description: error['apiDescription']['postLogout'],
            auth: false,
            // response: postDelete.responseCode,
            validate: {
                headers: headerValidator.headerLanValidator,
                payload: postDelete.payload,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
    {
        method: 'POST',
        path: entity + '/provider/signUp',
        handler: postSignUp.handler,
        config: {
            tags: ['api', 'admin'],
            description: error['apiDescription']['postLogout'],
            auth: false,
            // response: postSignUp.responseCode,
            validate: {
                headers: headerValidator.headerLanValidator,
                payload: postSignUp.payload,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },

];