
'use strict';
const joi = require('joi');
const Async = require('async');
const logger = require('winston');
const error = require('../error');
const moment = require('moment');//date-time
const email = require('../../../commonModels/email/provider');
const provider = require('../../../../models/provider');
const appConfig = require('../../../../models/appConfig');
const cities = require('../../../../models/cities');
const events = require('../../../../models/events');
const zendesk = require('./../../../../models/zendesk');
const wallet = require('../../../commonModels/wallet');
const userList = require('../../../commonModels/userList');
const Timestamp = require('mongodb').Timestamp;
const ObjectID = require('mongodb').ObjectID;

const payload = joi.object({
    id: joi.string().required().description('Array of provider ids')
}).required();


const handler = (req, reply) => {
    let zendeskId = "";
    let currencySymbol = '$';
    let currency = 'USD';
    let radius = 5;
    let event = [];
    const checkProvider = (data) => {
        return new Promise((resolve, reject) => {
            provider.getProviderById(req.payload.id, (e, r) => {
                return e ? reject(dbErrResponse) : (r == null) ?
                    reject({ message: error['VerifyPhoneNumber']['404'][req.headers.lan], code: 404 }) : resolve(r);
            });
        });
    }//check provider valid or not
    const getCity = (data) => {
        return new Promise((resolve, reject) => {
            cities.read({ _id: new ObjectID(data.cityId) }, (err, res) => {
                if (err) {
                    return reject(dbErrResponse);
                } else {
                    currencySymbol = res.currencySymbol || '$';
                    currency = res.currency || 'USD';
                    return resolve(data);
                }
            });
        });
    }
    const getAppConfig = (data) => {
        return new Promise((resolve, reject) => {
            appConfig.read((err, res) => {
                if (err) {
                    return reject(dbErrResponse);
                } else {
                    radius = res.locateProvider || 0;
                    return resolve(data);
                }
            });
        });
    }
    const getEvents = (data) => {
        return new Promise((resolve, reject) => {
            events.readAll((err, result) => {
                if (err)
                    reject(dbErrResponse)
                if (result != null) {
                    Async.forEach(result, (e, callback) => {
                        e.status = true;
                        event.push(e);
                        callback(null, event);
                    }, (err, res) => {
                        if (err)
                            return reject(dbErrResponse);
                        else {
                            return resolve(data);
                        }
                    });
                } else {
                    return resolve(data);
                }
            });
        });
    }// get Events
    const zendeskCreateUsaer = (data) => {
        return new Promise((resolve, reject) => {
            var url = zendesk.config.zd_api_url + '/users.json';
            var dataArr = { "user": { "name": data.firstName, "email": data.email, "role": 'end-user' } };
            zendesk.users.post(dataArr, url, function (err, result) {
                if (err) {
                    return reject(dbErrResponse);
                } else {
                    zendeskId = result.user ? result.user.id : "";
                    return resolve(data);
                }
            });
        });
    }//creat zendesk user
    const createUserForWallet = (data) => {
        return new Promise((resolve, reject) => {
            userList.createUser(
                req.payload.id,
                data.firstName,
                data.lastName,
                data.profilePic,
                data.phone[0].countryCode,
                data.phone[0].phone,
                2,
                '',
                1
            )//insert userlist table use for simple chat-module
            let dataArr = {
                userType: 2,
                userId: req.payload.id,
            };
            wallet.user.users.userCreate(dataArr, (err, walletId) => {
                if (err)
                    return reject(dbErrResponse);

                if (walletId == '')
                    return reject({ message: error['postSignUp']['504'][req.headers.lan], code: 504 });
                provider.findOneAndUpdate(req.payload.id,
                    {
                        $set: {
                            walletId: new ObjectID(walletId),
                            zendeskId: zendeskId,
                            createTs: new Timestamp(1, data.createdDt), 
                            createDate: new Date(),
                            currencySymbol: currencySymbol || '$',
                            currency: currency || 'USD',
                            radius: radius,
                            events: event || [],
                        }
                    },
                    (err, res) => {
                        return err ? reject(dbErrResponse) : resolve(data);
                    });
            });
        });
    }
    checkProvider()
        .then(getAppConfig)
        .then(getCity)
        .then(getEvents)
        .then(zendeskCreateUsaer)
        .then(createUserForWallet)
        .then(d => {
            let responseData = {
                'firstName': d.firstName ? d.firstName : "",
                'lastName': d.lastName ? d.lastName : "",
                'profilePic': d.profilePic ? d.profilePic : "",
                'status': 1,
                'email': d.email,
            };
            let params = {
                toName: d.firstName,
                to: d.email
            };
            email.signup(params);
            // email.welcomeMasterEmail(params);//send mail to provider
            return reply({ message: error['genericErrMsg']['200'][req.headers.lan] }).code(200);
        }).catch(e => {
            logger.error("Provider VerifyPhoneNumber API error =>", e)
            return reply({ message: e.message }).code(e.code);
        })


};

const responseCode = {
    // status: {
    //     500: { message: error['genericErrMsg']['500'][error['lang']] },
    //     200: { message: error['genericErrMsg']['200'][error['lang']], data: joi.any() },
    // }

}//swagger response code

module.exports = {
    payload,
    handler,
    responseCode
};