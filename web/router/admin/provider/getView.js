
'use strict';

const joi = require('joi');
const Async = require('async');
const error = require('../error');
const moment = require('moment');//date-time
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;
const customer = require('../../../../models/customer');
const providerProfileViews = require('../../../../models/providerProfileViews');

const params = joi.object({
    providerId: joi.string().description('providerId'),
    startDate: joi.number().allow('').description('start date timeStamp').error(new Error('start date is missing')),
    endDate: joi.number().allow('').description('start date timeStamp').error(new Error('end date is missing')),
}).required();

const handler = (req, reply) => {
    let reviewArr = [];
    let condition = {}; 
    if (req.params.startDate != "0" || req.params.startDate != 0 || req.params.startDate != "" || req.params.endDate != "") {
        condition = [
            { '$unwind': '$views' },
            {
                '$match': {
                    "views.viewedOn":
                        {
                            "$gte": req.params.startDate, "$lte": req.params.endDate
                        }, proId: new ObjectID(req.params.providerId)
                }
            }

        ];
    } else {
        condition = [
            { '$unwind': '$views' },
            { '$match': { proId: new ObjectID(req.params.providerId) } }
        ];
    }
    let customerData = [];
    Async.series([
        function (callback) {
 
            providerProfileViews.read(condition, (err, res) => {
                if (err) {
                    return callback(err);
                } else if (res === null) {
                    return callback(null, res);
                } else {
                    Async.forEach(res, (view, callbackloopv) => { 
                        customer.read({ _id: new ObjectID(view.views.viewedBy) }, (err, customer) => {
                            let dataArr = {
                                'firstName': customer.firstName ? customer.firstName : '',
                                'lastName': customer.lastName ? customer.lastName : '',
                                'email': customer.email ? customer.email : "",
                                'profilePic': customer.profilePic ? customer.profilePic : "",
                                'viewedOn': view.views.viewedOn
                            } 
                            customer.phone.forEach(function (e) {
                                if (e.isCurrentlyActive == true) {
                                    dataArr.countryCode = e.countryCode;
                                    dataArr.phone = e.phone;
                                    customerData.push(dataArr);
                                    return callbackloopv(null)
                                }
                            });
                        });

                    }, (loopErr, ress) => {
                        return callback(null, customerData);
                    });

                }
            });

        },

    ], function (err, result) {
        if (err) {
            logger.error("Admin All booking API error =>", err)
            return reply({ message: error['genericErrMsg']['500'][req.headers.lan] }).code(500);
        } else {
            // if (bookingArr.length != 0) {
            //     bookingArr.sort((a, b) => {
            //         return (b.bookingId) - (a.bookingId);
            //     });
            // }
            return reply({ message: error['genericErrMsg']['200'][req.headers.lan], data: customerData }).code(200);
        }

    });


};

const responseCode = {
    status: {
        // 500: { message: error['genericErrMsg']['500'][error['lang']] },
        // 200: {
        //     message: error['getProvider']['200'][error['lang']],
        //     data: joi.any()
        // },
    }

}//swagger response code

module.exports = {
    params,
    handler,
    responseCode
};