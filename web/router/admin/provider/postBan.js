
'use strict';
const joi = require('joi');
const moment = require('moment');//date-time
const Async = require('async');
const logger = require('winston');
const error = require('../error');
const fcm = require('../../../../library/fcm');
const redis = require('../../../../models/redis');
const provider = require('../../../../models/provider');
const Auth = require('../../../middleware/authentication');
const dispatcher = require('../../../commonModels/dispatcher');
const Timestamp = require('mongodb').Timestamp;
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId

const payload = joi.object({
    ids: joi.array().required().description('Array of master ids')
}).required();


const handler = (req, reply) => {
    Async.each(req.payload.ids, (id, cb) => {
        try {
            let setData = {
                $set: {
                    status: 9,
                    statusMsg: 'Banned',
                    loggedIn: '',
                    statusUpdateTime: new Timestamp(1, moment().unix()),
                    statusUpdateDate: new Date(),
                }
            };
            provider.findOneAndUpdate(id, setData, (err, result) => {
                if (err) {
                    return cb(err);
                } else {
                    dispatcher.providerStatus({ _id: id }, (err, res) => { });
                    return cb(null, result);
                }
            });
            provider.getProviderById(id, (err, pro) => {
                let request = {
                    fcmTopic: pro.fcmTopic,
                    action: 22,
                    pushType: 1,
                    title: "Provider banned",
                    msg: "Your profile has been banned by our provider management team",
                    data: [],
                    deviceType: pro.mobileDevices.deviceType
                }
                fcm.notifyFcmTpic(request, (e, r) => { });
            });
            redis.delteSet('presence_' + id);
        } catch (e) {
            return cb(e);
        }
    }, (err, result) => {
        if (err) {
            return reply({ message: error['genericErrMsg']['500'][req.headers.lan] }).code(500);
        } else {
            return reply({ message: error['genericErrMsg']['200'][req.headers.lan] }).code(200);
        }
    });
};

const responseCode = {
    status: {
        500: { message: error['genericErrMsg']['500'][error['lang']] },
        200: { message: error['genericErrMsg']['200'][error['lang']], data: joi.any() },
    }

}//swagger response code

module.exports = {
    payload,
    handler,
    responseCode
};