
'use strict';

const joi = require('joi');
const Async = require('async');
const error = require('../error');
const moment = require('moment');//date-time
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;
const providerDailyOnlineLogs = require('../../../../models/providerDailyOnlineLogs');

const params = joi.object({
    providerId: joi.string().description('providerId'),
    startDate: joi.any().allow('').description('start date timeStamp').error(new Error('start date is missing')),
    endDate: joi.any().allow('').description('start date timeStamp').error(new Error('end date is missing')),
}).required();

const handler = (req, reply) => { 
    let queryObj = { providerId: new ObjectID(req.params.providerId) };

    if (typeof req.params.startDate != 'undefined' && typeof req.params.endDate != 'undefined'
        && req.params.startDate != 0 && req.params.endDate != 0) {
        queryObj['timestamp'] = {
            $gte: moment(req.params.startDate).startOf('day').unix(), $lte: moment(req.params.endDate).endOf('day').unix()
        };
    }
    else if (typeof req.params.startDate != 'undefined' && req.params.startDate != 0) {
        // queryObj.q['timestamp'] = moment(parseInt(req.payload.from)).startOf('day').unix();
        queryObj['timestamp'] = moment(req.params.startDate).startOf('day').unix();
    }
    else {
        ;//do not include anything in condition
    } 
    providerDailyOnlineLogs.readAll(queryObj, (err, res) => {
        if (err) {
            logger.error("Admin All shift log API error =>", err)
            return reply({ message: error['genericErrMsg']['500'][req.headers.lan] }).code(500);
        } else {
            return reply({ message: error['genericErrMsg']['200'][req.headers.lan], data: res }).code(200);
        }
    })

};

const responseCode = {
    status: {
        // 500: { message: error['genericErrMsg']['500'][error['lang']] },
        // 200: {
        //     message: error['getProvider']['200'][error['lang']],
        //     data: joi.any()
        // },
    }

}//swagger response code

module.exports = {
    params,
    handler,
    responseCode
};