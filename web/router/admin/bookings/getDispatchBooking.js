
'use strict';

const joi = require('joi');
const error = require('../error');
const moment = require('moment');//date-time
const logger = require('winston');
const dispatchedBookings = require('../../../../models/dispatchedBookings');

const params = joi.object({
    cityId: joi.any().allow("").description('cityId').error(new Error('cityId is missing')),
    startDate: joi.number().allow('').description('start date timeStamp').error(new Error('start date is missing')),
    endDate: joi.number().allow('').description('start date timeStamp').error(new Error('end date is missing')),
}).required();

const handler = (req, reply) => {
    let condition = {};

    if (req.params.cityId != 0 && req.params.cityId != "") {
        condition.cityId = req.params.cityId;
    }
    if (req.params.startDate != "0" || req.params.startDate != 0 || req.params.startDate != "" || req.params.endDate != "") {
        condition.dispatchedByServerAt = {
            '$gte': req.params.startDate, '$lte': req.params.endDate
        }
    }
    dispatchedBookings.readAllBooking(condition, (err, res) => {
        if (err) {
            return reply({ message: error['genericErrMsg']['500'][req.headers.lan] }).code(500);
        } else {
            if (res.length != 0) {
                res.sort((a, b) => {
                    return (b.bookingId) - (a.bookingId);
                });
                return reply({ message: error['genericErrMsg']['200'][req.headers.lan], data: res }).code(200);
            } else {
                return reply({ message: error['genericErrMsg']['200'][req.headers.lan], data: res }).code(200);
            }

        }
    });
};

const responseCode = {
    status: {
        // 500: { message: error['genericErrMsg']['500'][error['lang']] },
        // 200: {
        //     message: error['getProvider']['200'][error['lang']],
        //     data: joi.any()
        // },
    }

}//swagger response code

module.exports = {
    params,
    handler,
    responseCode
};