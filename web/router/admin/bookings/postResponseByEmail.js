
'use strict';

const joi = require('joi');
const Async = require('async');
const error = require('../error');
const moment = require('moment-timezone');
const logger = require('winston');
const jwt = require('jsonwebtoken');
const customer = require('../../../../models/customer');
const provider = require('../../../../models/provider');
const bookings = require('../../../../models/bookings');
const configuration = require('../../../../configuration');
const assignedBookings = require('../../../../models/assignedBookings');
const unassignedBookings = require('../../../../models/unassignedBookings');
const dispatchedBookings = require('../../../../models/dispatchedBookings');
const mqtt_module = require('../../../../models/MQTT');
const webSocket = require('../../../../models/webSocket');
const redis = require('../../../../models/redis');
const fcm = require('../../../../library/fcm');
const ObjectID = require('mongodb').ObjectID;
const email = require('../../../commonModels/email/customer');
const stripe = require('../../../commonModels/stripe');

const payload = joi.object({
    token: joi.any().required().description('Token').error(new Error('token is missing'))
}).required();

const handler = (req, reply) => {
    let decodeData;
    let customerData;
    let providerData;
    let bookingData;
    const decodeToken = (data) => {
        return new Promise((resolve, reject) => {
            try {
                jwt.verify(req.payload.token, configuration.JWT_KEY, (err, decoded) => {
                    return err ? reject(err) : resolve(decoded);
                });
            } catch (e) {
                return reject(err)
            }

        });
    }
    const checkBooking = (data) => {
        decodeData = data;
        return new Promise((resolve, reject) => {
            unassignedBookings.findOneDelete({ bookingId: parseFloat(data.bookingId), providerId: data.providerId }, (err, res) => {
                if (err) {
                    return reject({ message: error['genericErrMsg']['500'][req.headers.lan], code: 500 })
                } else if (res.value === null) {
                    assignedBookings.read({ bookingId: parseFloat(data.bookingId), providerId: data.providerId }, (err, bookingData) => {
                        if (err) {
                            return reject({ message: error['genericErrMsg']['500'][req.headers.lan], code: 500 })
                        } else if (bookingData === null) {
                            bookings.read({ bookingId: parseFloat(data.bookingId), providerId: data.providerId }, (err, bookingData) => {
                                if (err) {
                                    return reject({ message: error['genericErrMsg']['500'][req.headers.lan], code: 500 })
                                } else if (bookingData === null) {
                                    return reject({ message: error['genericErrMsg']['500'][req.headers.lan], code: 500 })
                                } else {
                                    let dataArr = {
                                        toName: bookingData.providerData.firstName + " " + bookingData.providerData.lastName,
                                        providerId: data.providerId,
                                        bookingId: bookingData.bookingId,
                                        bookingDate: moment.unix(bookingData.bookingRequestedFor).tz("Asia/Kolkata").format('MMMM Do YYYY, h:mm:ss a'),
                                        bookingType: bookingData.bookingType == 1 ? "Now" : 'scheduled',
                                        address: bookingData.addLine1,
                                        bookingCategory: bookingData.category.name,
                                        serviceName: bookingData.service.name + ' ' + bookingData.service.unit,
                                        servicePrice: bookingData.service.price || 0,
                                        discount: bookingData.accounting.discount || 0,
                                        amount: bookingData.accounting.amount || 0,
                                        acceptReject: bookingData.acceptReject,
                                        status: bookingData.status,
                                        currencySymbol: bookingData.currencySymbol
                                    }
                                    return reply({ message: error['genericErrMsg']['200'][req.headers.lan], errorFlag: 1, data: dataArr }).code(200);
                                }
                            })
                        } else {
                            let dataArr = {
                                toName: bookingData.providerData.firstName + " " + bookingData.providerData.lastName,
                                providerId: data.providerId,
                                bookingId: bookingData.bookingId,
                                bookingDate: moment.unix(bookingData.bookingRequestedFor).tz("Asia/Kolkata").format('MMMM Do YYYY, h:mm:ss a'),
                                bookingType: bookingData.bookingType == 1 ? "Now" : 'scheduled',
                                address: bookingData.addLine1,
                                bookingCategory: bookingData.category.name,
                                serviceName: bookingData.service.name + ' ' + bookingData.service.unit,
                                servicePrice: bookingData.service.price || 0,
                                discount: bookingData.accounting.discount || 0,
                                amount: bookingData.accounting.amount || 0,
                                acceptReject: bookingData.acceptReject,
                                status: bookingData.status,
                                currencySymbol: bookingData.currencySymbol
                            }
                            return reply({ message: error['genericErrMsg']['200'][req.headers.lan], errorFlag: 1, data: dataArr }).code(200);
                        }
                    })

                }
                else {
                    bookingData = res.value;
                    resolve(res.value);
                }

            });
        });
    }
    const stripeCheckOut = (data) => {
        return new Promise((resolve, reject) => {
            if (data.paymentMethod == 2 && parseInt(decodeData.status) != 3) {
                stripe.stripeTransaction.refundCharge(
                    data.accounting.chargeId,
                    ''
                ).then(res => {
                    return resolve(data);
                }).catch(e => {
                    logger.error("reject booking by provider stripe error", e)
                    return reject(dbErrResponse);
                });

            } else {
                return resolve(data);
            }
        });
    }
    const readCustomer = (data) => {
        return new Promise(function (resolve, reject) {
            let con = { _id: ObjectID(data.slaveId) };
            customer.read(con, (err, res) => {
                customerData = res;
                return err ? reject(err) : resolve(data);
            });
        });
    }
    const readProvider = (data) => {
        return new Promise(function (resolve, reject) {
            let con = { _id: ObjectID(data.providerId) };
            provider.read(con, (err, res) => {
                providerData = res;
                return err ? reject(err) : resolve(data);
            });
        });
    }
    const updateBookingStatus = (data) => {
        return new Promise(function (resolve, reject) {
            if (parseInt(decodeData.status) == 3) {
                assignedBookings.post(data, (err, result) => {
                    return err ? reject(err) : resolve(data);

                });
            } else {
                bookings.post(data, (err, result) => {
                    return err ? reject(err) : resolve(data);

                });
            }
        });
    }

    const jobStatusUpdate = (data) => {
        return new Promise(function (resolve, reject) {
            let updateQuery = {
                query: { bookingId: decodeData.bookingId },
                data: {
                    $set: { status: parseInt(decodeData.status), statusMsg: error['bookingStatus'][decodeData.status], acceptReject: 1 },
                    $push: {
                        jobStatusLogs: {
                            status: parseInt(decodeData.status),
                            statusMsg: error['bookingStatus'][decodeData.status],
                            stausUpdatedBy: 'Provider',
                            userId: decodeData.providerId || '',
                            timeStamp: moment().unix(),
                            latitude: '',
                            longitude: ''
                        }
                    }
                }
            };
            if (parseInt(decodeData.status) == 3) {
                assignedBookings.findUpdate(updateQuery, (err, res) => {
                    return err ? reject(err) : resolve(data);
                });
            } else {
                bookings.findUpdate(updateQuery, (err, res) => {
                    return err ? reject(err) : resolve(data);
                });
            }

        });
    }

    const sendBookingResponse = (data) => {
        let statusMsgg = parseInt(decodeData.status) == 3 ? 31 : 41;
        return new Promise(function (resolve, reject) {
            let mqttRes = {
                bookingId: decodeData.bookingId,
                status: parseInt(decodeData.status),
                statusMsg: error['bookingStatus'][decodeData.status],
                statusUpdateTime: moment().unix(),
                proProfilePic: providerData.profilePic || '',
                bookingType: data.bookingType,
                bookingRequestedAt: data.bookingRequestedAt,
                bookingRequestedFor: data.bookingRequestedFor,
                bookingEndtime: data.bookingEndtime || '',
                addLine1: data.addLine1,
                latitude: data.latitude,
                longitude: data.longitude


            };
            let mqttResPro = {
                bookingId: decodeData.bookingId,
                status: parseInt(decodeData.status),
                statusMsg: error['bookingStatus'][statusMsgg],
                statusUpdateTime: moment().unix(),
                proProfilePic: providerData.profilePic || '',
                bookingType: data.bookingType,
                bookingRequestedAt: data.bookingRequestedAt,
                bookingRequestedFor: data.bookingRequestedFor,
                bookingEndtime: data.bookingEndtime || '',
                addLine1: data.addLine1,
                latitude: data.latitude,
                longitude: data.longitude


            };
            webSocket.publish('dispatcher/jobStatus', mqttRes, {}, function (mqttErr, mqttRes) {
            });
            webSocket.publish('admin/dispatchLog', mqttRes, {}, (mqttErr, mqttRes) => {
            });
            redis.delteSet('bid_' + decodeData.bookingId);
            let request = {
                fcmTopic: customerData.fcmTopic,
                action: parseInt(decodeData.status),
                pushType: 1,
                title: error['bookingStatus'][decodeData.status],
                msg: error['bookingStatus'][decodeData.status],
                data: mqttRes,
                deviceType: customerData.mobileDevices.deviceType
            }
            fcm.notifyFcmTpic(request, (e, r) => { });
            let requestToPro = {
                fcmTopic: providerData.fcmTopic,
                action: 18,
                pushType: 1,
                title: error['bookingStatus'][statusMsgg],
                msg: error['bookingStatus'][statusMsgg],
                data: mqttResPro,
                deviceType: providerData.mobileDevices.deviceType
            }
            fcm.notifyFcmTpic(requestToPro, (e, r) => { });
            var msg = { errNum: 200, errFlag: 0, errMsg: 'Got Details.', data: mqttRes };
            mqtt_module.publish('jobStatus/' + providerData._id, JSON.stringify(msg), { qos: 2 }, function (mqttErr, mqttRes) {
                // return mqttErr ? reject(mqttErr) : resolve(data);
            });
            mqtt_module.publish('jobStatus/' + customerData._id, JSON.stringify(msg), { qos: 2 }, function (mqttErr, mqttRes) {
                return mqttErr ? reject(mqttErr) : resolve(data);
            });

        });
    }
    const dispatcherLogUpdate = (data) => {
        return new Promise((resolve, reject) => {
            let updateQuery = {
                query: { bookingId: parseFloat(decodeData.bookingId) },
                data: {
                    $set: {
                        providerAck: moment().unix(),
                        status: parseInt(decodeData.status),
                        statusMsg: error['bookingStatus'][(decodeData.status)]
                    }

                }
            };
            dispatchedBookings.findOneAndUpdate(updateQuery, (err, data) => {
                return err ? reject(err) : resolve(data);
            });
        });
    }

    const pushBookingInProvider = (data) => {
        return new Promise(function (resolve, reject) {
            let queryObject = {
                $push: {
                    assignedBooking: {
                        bookingId: parseFloat(decodeData.bookingId),
                        status: parseInt(decodeData.status)
                    }
                }
            };
            provider.findOneAndUpdate(providerData._id, queryObject, (err, result) => {
                return err ? reject(err) : resolve(data);
            });
        });
    }

    const sendMailToCustomer = (data) => {
        return new Promise((resolve, reject) => {
            if (parseInt(decodeData.status) == 3) {
                let params = {
                    to: customerData.email,
                    toName: customerData.firstName + ' ' + customerData.lastName,
                    providerName: providerData.firstName + '' + providerData.lastName,
                    bookingId: bookingData.bookingId,
                    bookingDate: moment.unix(bookingData.bookingRequestedFor).format('MMMM Do YYYY, h:mm:ss a'),
                    bookingType: bookingData.bookingType == 1 ? "Now" : 'scheduled',
                    address: bookingData.addLine1,
                    bookingCategory: bookingData.category.name,
                    serviceName: bookingData.service.name + ' ' + bookingData.service.unit,
                    servicePrice: bookingData.service.price || 0,
                    discount: bookingData.accounting.discount,
                    amount: bookingData.accounting.amount
                };
                email.acceptBookingByProvider(params);
            } else {
                let params = {
                    to: customerData.email,
                    toName: customerData.firstName + ' ' + customerData.lastName,
                    providerName: providerData.firstName + '' + providerData.lastName,
                    bookingDate: moment.unix(bookingData.bookingRequestedFor).format('MMMM Do YYYY, h:mm:ss a'),
                }
                email.rejectBookingByProvider(params);
            }
            return resolve(data);
        });
    }

    decodeToken()
        .then(checkBooking)
        .then(stripeCheckOut)
        .then(readCustomer)
        .then(readProvider)
        .then(updateBookingStatus)
        .then(jobStatusUpdate)
        .then(sendBookingResponse)
        .then(dispatcherLogUpdate)
        .then(pushBookingInProvider)
        .then(sendMailToCustomer)
        .then(data => {
            let dataArr = {
                toName: providerData.firstName + " " + providerData.lastName,
                to: providerData.email,
                providerId: providerData._id,
                bookingId: bookingData.bookingId,
                bookingDate: moment.unix(bookingData.bookingRequestedFor).tz("Asia/Kolkata").format('MMMM Do YYYY, h:mm:ss a'),
                bookingType: bookingData.bookingType == 1 ? "Now" : 'scheduled',
                address: bookingData.addLine1,
                bookingCategory: bookingData.category.name,
                serviceName: bookingData.service.name + ' ' + bookingData.service.unit,
                servicePrice: bookingData.service.price || 0,
                discount: bookingData.accounting.discount || 0,
                amount: bookingData.accounting.amount || 0,
                acceptReject: 1,
                currencySymbol: bookingData.currencySymbol
            }
            return reply({ message: error['genericErrMsg']['200'][req.headers.lan], data: dataArr }).code(200);
        })
        .catch(e => {
            return reply({ message: error['genericErrMsg']['500'][req.headers.lan] }).code(500);
        })

};

const responseCode = {
    status: {
        // 500: { message: error['genericErrMsg']['500'][error['lang']] },
        // 200: {
        //     message: error['getProvider']['200'][error['lang']],
        //     data: joi.any()
        // },
    }

}//swagger response code

module.exports = {
    payload,
    handler,
    responseCode
};