'use strict';

const entity = '/admin';
const error = require('../error');
const getDispatchBooking = require('./getDispatchBooking');
const getAll = require('./getAll');
const getOngoing = require('./getOngoing');
const getExpired = require('./getExpired');
const getComplete = require('./getComplete');
const getUnAssigned = require('./getUnAssigned');
const getCancelled = require('./getCancelled');
const postResponseByEmail = require('./postResponseByEmail');
const headerValidator = require('../../../middleware/validator');

module.exports = [
    /**
    * @name  
    */
    {
        method: 'GET',
        path: entity + '/booking/dispatch/{cityId}/{startDate}/{endDate}',
        handler: getDispatchBooking.handler,
        config: {
            tags: ['api', 'admin'],
            description: error['apiDescription']['getDispatchBooking'],
            auth: false,
            response: getDispatchBooking.responseCode,
            validate: {
                params: getDispatchBooking.params,
                headers: headerValidator.headerLanValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
    /**
    * @name  
    */
    {
        method: 'GET',
        path: entity + '/booking/all/{cityId}/{status}/{startDate}/{endDate}',
        handler: getAll.handler,
        config: {
            tags: ['api', 'admin'],
            description: error['apiDescription']['getAllBooking'],
            auth: false,
            response: getAll.responseCode,
            validate: {
                params: getAll.params,
                headers: headerValidator.headerLanValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },

    /**
    * @name  
    */
    {
        method: 'GET',
        path: entity + '/booking/ongoing/{cityId}/{startDate}/{endDate}',
        handler: getOngoing.handler,
        config: {
            tags: ['api', 'admin'],
            description: error['apiDescription']['getAllBooking'],
            auth: false,
            response: getOngoing.responseCode,
            validate: {
                params: getOngoing.params,
                headers: headerValidator.headerLanValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
    {
        method: 'GET',
        path: entity + '/booking/unAssigned/{cityId}/{startDate}/{endDate}',
        handler: getUnAssigned.handler,
        config: {
            tags: ['api', 'admin'],
            description: error['apiDescription']['getAllBooking'],
            auth: false,
            // response: getUnAssigned.responseCode,
            validate: {
                params: getUnAssigned.params,
                headers: headerValidator.headerLanValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
    /**
    * @name  
    */
    {
        method: 'GET',
        path: entity + '/booking/complete/{cityId}/{startDate}/{endDate}',
        handler: getComplete.handler,
        config: {
            tags: ['api', 'admin'],
            description: error['apiDescription']['getAllBooking'],
            auth: false,
            response: getComplete.responseCode,
            validate: {
                params: getComplete.params,
                headers: headerValidator.headerLanValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
    {
        method: 'GET',
        path: entity + '/booking/expired/{cityId}/{startDate}/{endDate}',
        handler: getExpired.handler,
        config: {
            tags: ['api', 'admin'],
            description: error['apiDescription']['getAllBooking'],
            auth: false,
            response: getExpired.responseCode,
            validate: {
                params: getExpired.params,
                headers: headerValidator.headerLanValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
    {
        method: 'GET',
        path: entity + '/booking/cancelled/{cityId}/{status}/{startDate}/{endDate}',
        handler: getCancelled.handler,
        config: {
            tags: ['api', 'admin'],
            description: error['apiDescription']['getAllBooking'],
            auth: false,
            response: getCancelled.responseCode,
            validate: {
                params: getCancelled.params,
                headers: headerValidator.headerLanValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
    {
        method: 'POST',
        path: entity + '/email/reponseBooking',
        handler: postResponseByEmail.handler,
        config: {
            tags: ['api', 'admin'],
            description: error['apiDescription']['getAllBooking'],
            auth: false,
            response: postResponseByEmail.responseCode,
            validate: {
                payload: postResponseByEmail.payload,
                headers: headerValidator.headerLanValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    }
];