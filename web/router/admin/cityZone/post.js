
'use strict';

const Joi = require('joi');
const logger = require('winston');
const moment = require('moment');

const error = require('../error');
const redis = require('../../../../models/redis');

// const payload = Joi.object({
//     updateType: Joi.string().required().description('citiesZone = 1')
// }).required();

const APIHandler = (req, reply) => { 
    redis.setex('citiesZone', 1, moment().unix(), function (err, result) {
        

    });
    return reply({ message: error['genericErrMsg']['200'][req.headers.lan] }).code(200);
};

const responseCode = {
    status: {
        500: { message: error['genericErrMsg']['500'][error['lang']] },
        200: { message: error['genericErrMsg']['200'][error['lang']] },
    }
}

module.exports = { APIHandler, responseCode };