
'use strict';

const entity = "/admin";
const postCityZone = require('./post');
let get = require("./get");

module.exports = [
    /**
    * This API called by admin when citiesZone Modified or added.
    */
    {
        method: 'POST',
        path: entity + '/zoneUpdated',
        handler: postCityZone.APIHandler,
        config: {
            tags: ['api', 'admin'],
            description: 'This API called by admin when citiesZone Modified or added.',
            notes: 'This API called by admin when citiesZone Modified or added.',
            auth: false,
            response: postCityZone.responseCode,
            validate: {
                // payload: postCityZone.payload,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
    // {
    //     method: 'GET',
    //     path: '/admin/city',
    //     config: {
    //         tags: ['api', 'admin'],
    //         description: 'Get all cities',
    //         notes: 'Get all city list',
    //         auth: false,
    //         validate: {
    //             /** @memberof headerValidator */
    //             // headers: headerValidator.language,
    //             /** @memberof headerValidator */
    //             // failAction: headerValidator.customError
    //         },
    //         // response: {
    //         //     status: {
    //         //         200: { message: error['getData']['200']['0'] , data:Joi.any()},
    //         //         500: { message: error['genericErrMsg']['500']['0'] },
    //         //         404: { message: error['getData']['404']['0'] }
    //         //     }
    //         // }
    //     },
    //     /** @memberof get */
    //     handler: get.handler

    // },
    // {
    //     method: 'GET',
    //     path: '/admin/cityDetailsByCityIds/{cityIds}',
    //     config: {
    //         tags: ['api', 'admin'],
    //         description: 'Get city details by city id',
    //         notes: 'Get city details by city Id',
    //         auth: false,
    //         validate:
    //         // 
    //         {
    //             /**@memberOf get**/
    //             params: get.cityDetailsByCityIdsValidator,
    //             /** @memberof headerValidator */
    //             // headers: headerValidator.language,
    //             /** @memberof headerValidator */
    //             // failAction: headerValidator.customError,
    //         },
    //         // response: {
    //         //     status: {
    //         //         200: { message: error['getData']['200']['0'] , data:Joi.any()},
    //         //         500: { message: Joi.any().default(error['genericErrMsg']['500']['0']) },
    //         //         404: { message: Joi.any().default(error['getData']['404']['0']) }
    //         //     }
    //         // }
    //     },
    //     /** @memberof get */
    //     handler: get.cityDetailsByCityIdsHandler

    // }
]