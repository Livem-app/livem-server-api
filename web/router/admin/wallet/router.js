'use strict';
var Joi = require('joi');
const entity = '/admin';
const error = require('../error');
const getAllUser = require('./getAllUser');
const getStatement = require('./getStatement');
const postStatementFilter = require('./postStatementFilter');
const postStatementExport = require('./postStatementExport');
const headerValidator = require('../../../middleware/validator');
const wallet = require('../../../../worker/wallet/wallet')
var async = require("async");
module.exports = [


    {
        method: 'POST',
        path: '/wallet/allUser',
        config: {
            tags: ['api', 'admin'],
            description: 'Get all wallet users',
            notes: 'Get all wallet users',
            auth: false,
        },
        handler: getAllUser.handler
    },
    {
        method: 'POST',
        path: '/wallet/statement',
        config: {
            tags: ['api', 'admin'],
            description: 'Get wallet statement for perticular user',
            notes: 'Get wallet statement for perticular user',
            auth: false,
        },
        handler: getStatement.handler
    },
    {
        method: 'POST',
        path: '/wallet/statementFilter',
        config: {
            tags: ['api', 'admin'],
            description: 'Get wallet statement for perticular user by filter',
            notes: 'Get wallet statement for perticular user by filter',
            auth: false,
        },
        handler: postStatementFilter.handler
    },
    {
        method: 'POST',
        path: '/wallet/statementExport',
        config: {
            tags: ['api', 'admin'],
            description: 'Get wallet statement for perticular user by filter export',
            notes: 'Get wallet statement for perticular user by filter export',
            auth: false,
        },
        handler: postStatementExport.handler
    },
    {
        method: 'POST', // Methods Type
        path: '/wallet/walletTransction', // Url
        config: {// "tags" enable swagger to document API 
            tags: ['api', 'slave'],
            description: 'wallet Transction for all entity',
            notes: "wallet Transction for all entity", // We use Joi plugin to validate request 
            auth: false,
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form'
                },
                responses: {
                    '200': {
                        'description': 'Success',
                        'schema': Joi.object({
                            equals: Joi.number(),
                        }).label('Result')
                    },
                    '400': {
                        'description': 'Bad Request',
                        'schema': Joi.object({
                            equals: Joi.number(),
                        }).label('Result')
                    }
                }
            },
            validate: {
                payload: {
                    userId: Joi.string().required().description('user id separated by ,'),
                    amount: Joi.any().required().description('amount'),
                    userType: Joi.string().required().description('1-MASTER, 2-SLAVE, 3-OPERATOR , 4-APP, 5-PG'),
                    trigger: Joi.string().description('ADMIN/PROMO/REFREL/TRIP/WALLET_RECHARGE'),
                    currency: Joi.string().description('Currency'),
                    txnType: Joi.string().description('Transaction Type(1-CREDIT, 2-DEBIT)'),
                    comment: Joi.string().description('add your Comment'),
                    paymentType: Joi.string().description('CASH/CARD/WALLET'),
                    initiatedBy: Joi.string().description('ADMIN_USERNAME/CUSTOMER')
                }
            }
        },
        handler: function (req, reply) { // request handler method
            var userId_arr = req.payload.userId.split(",");
            var trx_res = [];
            async.forEach(userId_arr, function (item, callback) {
                req.payload.userId = item;
                req.payload.userType = (req.payload.userType == 1) ? 2 : 1;
                wallet.walletTransction(req.payload, function (err, data) {
                    if (err)
                        trx_res.push({ "id": item, "data": "", "err": err });
                    else
                        trx_res.push({ "id": item, "data": data, "err": "" });
                    callback();
                });
            }, function (err) {
                reply({ 'flag': 0, 'data': trx_res });
            });
        }
    },
    // {
    //     method: 'GET',
    //     path: '/wallet/configrationChanged',
    //     config: {
    //         tags: ['api', 'admin'],
    //         description: 'when configration changed',
    //         notes: 'when configration changed',
    //         auth: false
    //     },
    //     handler: postConfigrationChanged.configrationChanged
    // },
    {
        method: 'POST',
        path: '/wallet/walletConfiguration',
        config: {
            tags: ['api', 'admin'],
            description: 'wallet credit line changed',
            notes: 'wallet credit line changed',
            auth: false,
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form'
                }
            },
            validate: {
                payload: {
                    userType: Joi.any().required().description('1 - customer, 2 - provider'),
                    userId: Joi.string().required().description('user id'),
                    status: Joi.any().required().description('status 50-both , 51-soflimit 52-hardlimit 53-enable  54- wallet disable')
                }
            }
        },
        handler: wallet.walletConfiguration
    },

];