
'use strict';

const joi = require('joi');
const Async = require('async');
const error = require('../error');
const moment = require('moment');//date-time
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;
const wallet = require('../../../../models/wallet')
const cities = require('../../../../models/cities')

const handler = (req, reply) => {
    // based on user type
    let collection = "";
    switch (parseInt(req.payload.userType)) {
        case 1:
            collection = "provider";
            break;
        case 2:
            collection = "slaves";
            break;
        case 3:
            collection = "operators";
            break;
        default:
            collection = "";
            break;
    }
    if (collection == "")
        return reply({ errNum: 500, errMsg: 'Server Error', errFlag: 1 });
    let condition = {};
    // based on tab selection
    switch (parseInt(req.payload.tabType)) {
        case 2:
            condition = { 'walletSoftLimitHit': true };
            break;
        case 3:
            condition = { 'walletHardLimitHit': true };
            break;
    }

    var regexValue = req.payload.sSearch;
    if (req.payload.sSearch != 'undefined' && req.payload.sSearch != '') {
        switch (parseInt(req.payload.userType)) {
            case 1:
                Object.assign(condition, { '$or': [{ 'firstName': new RegExp(regexValue, 'i') }, { 'lastName': new RegExp(regexValue, 'i') }, { 'email': new RegExp(regexValue, 'i') }, { 'phone.countryCode': new RegExp(regexValue, 'i') }, { 'phone.phone': new RegExp(regexValue, 'i') }, { 'walletAmount': new RegExp(regexValue, 'i') }] })
                break;
            case 2:
                Object.assign(condition, { '$or': [{ 'firstName': new RegExp(regexValue, 'i') }, { 'email': new RegExp(regexValue, 'i') }, { 'phone.countryCode': new RegExp(regexValue, 'i') }, { 'phone.phone': new RegExp(regexValue, 'i') }] })
                break;
            case 3:
                Object.assign(condition, { '$or': [{ 'email': new RegExp(regexValue, 'i') }, { 'mobile': new RegExp(regexValue, 'i') }] })
                break;
            default:
                break;
        }
    }

    wallet.Count(collection, condition, (err, count) => {

        if (err)
            return reply({ errNum: 500, errMsg: 'Server Error', errFlag: 1 });
        if (count === 0)
            return reply({ iTotalRecords: 0, iTotalDisplayRecords: 0, aaData: [] });
        let queryObj = {
            q: condition,
            p: {},
            s: { _id: -1 },
            skip: parseInt(req.payload.iDisplayStart) || 0,
            limit: parseInt(req.payload.iDisplayLength) || 20
        }
        wallet.SELECT(collection, queryObj, (err, docs) => {
            let provider = [];
            if (err)
                return reply({ errNum: 500, errMsg: 'Server Error', errFlag: 1 });
            if (req.payload.userType == 1) {
                Async.forEach(docs, function (pro, callbackloopp) {
                    if (pro.cityId) {
                        cities.read({ _id: new ObjectID(pro.cityId) }, (err, res) => {
                            pro.currencySymbol = res.currencySymbol || '$';
                            pro.currency = res.currency || 'USD';
                            provider.push(pro);
                            callbackloopp(null);
                        })
                    } else {
                        provider.push(pro);
                        callbackloopp(null);
                    }

                }, (err, res) => {
                    return reply({
                        iTotalRecords: count,
                        iTotalDisplayRecords: count,
                        aaData: provider
                    });
                });
            } else {
                return reply({
                    iTotalRecords: count,
                    iTotalDisplayRecords: count,
                    aaData: docs
                });
            }

        });
    });
};

const responseCode = {
    status: {
        // 500: { message: error['genericErrMsg']['500'][error['lang']] },
        // 200: {
        //     message: error['getProvider']['200'][error['lang']],
        //     data: joi.any()
        // },
    }

}//swagger response code

module.exports = {
    handler,
};