
'use strict';

const joi = require('joi');
const Async = require('async');
const error = require('../error');
const moment = require('moment');//date-time
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;
const wallet = require('../../../../models/wallet')
const cities = require('../../../../models/cities')
const handler = (req, reply) => {
    let collection = "";
    switch (parseInt(req.payload.userType)) {
        case 1:
            collection = "walletProvider";
            break;
        case 2:
            collection = "walletCustomer";
            break;
        case 3:
            collection = "walletOperator";
            break;
        case 4:
            collection = "walletApp";
            break;
        case 5:
            collection = "walletPg";
            break;
        default:
            collection = "";
            break;
    }

    if (collection == "")
        return reply({ errNum: 500, errMsg: 'Server Error', errFlag: 1 });
    let condition = {};
    switch (parseInt(req.payload.userType)) {
        case 1:
        case 2:
        case 3:
            condition = { userId: req.payload.userId };
            break;
        default:
            condition = {};
            break;
    }

    if (!(typeof req.payload.searchByPayment == 'undefined' || req.payload.searchByPayment == null || req.payload.searchByPayment == '' || req.payload.searchByPayment == '0')) {
        Object.assign(condition, { 'txnType': req.payload.searchByPayment })
    }
    if (!(typeof req.payload.searchByTrigger == 'undefined' || req.payload.searchByTrigger == null || req.payload.searchByTrigger == '' || req.payload.searchByTrigger == '0')) {
        Object.assign(condition, { 'trigger': req.payload.searchByTrigger })
    }
    if (!(typeof req.payload.searchByStartDate == 'undefined' || req.payload.searchByStartDate == 'undefined--undefined' || req.payload.searchByStartDate == null || req.payload.searchByStartDate == '' || req.payload.searchByStartDate == '0') && !(typeof req.payload.searchByEndDate == 'undefined' || req.payload.searchByEndDate == 'undefined--undefined' || req.payload.searchByEndDate == null || req.payload.searchByEndDate == '' || req.payload.searchByEndDate == '0')) {
        var serverStartTime = moment(req.payload.searchByStartDate + ' 00:00:01').unix();
        var serverEndTime = moment(req.payload.searchByEndDate + ' 23:59:59').unix();
        var dateCond = { '$gte': serverStartTime, '$lte': serverEndTime };
        Object.assign(condition, { 'timestamp': dateCond })
    }

    //    if (req.payload.sSearch != 'undefined' && req.payload.sSearch != '')
    //        condition = {}

    wallet.Count(collection, condition, (err, count) => {

        if (err)
            return reply({ errNum: 500, errMsg: 'Server Error', errFlag: 1 });
        if (count === 0)
            return reply({ iTotalRecords: 0, iTotalDisplayRecords: 0, aaData: [] });
        let queryObj = {
            q: condition,
            p: {},
            s: { _id: -1 },
            skip: parseInt(req.payload.iDisplayStart) || 0,
            limit: parseInt(req.payload.iDisplayLength) || 20
        }
        wallet.SELECT(collection, queryObj, (err, docs) => {

            if (err)
                return reply({ errNum: 500, errMsg: 'Server Error', errFlag: 1 });
            if (req.payload.userType == 1) {
                Async.forEach(docs, function (pro, callbackloopp) {
                    if (pro.cityId) {
                        cities.read({ _id: new ObjectID(pro.cityId) }, (err, res) => {
                            pro.currencySymbol = res.currencySymbol || '$';
                            pro.currency = res.currency || 'USD';
                            provider.push(pro);
                            callbackloopp(null);
                        })
                    } else {
                        provider.push(pro);
                        callbackloopp(null);
                    }

                }, (err, res) => {
                    return reply({
                        iTotalRecords: count,
                        iTotalDisplayRecords: count,
                        aaData: provider
                    });
                });
            } else {
                return reply({
                    iTotalRecords: count,
                    iTotalDisplayRecords: count,
                    aaData: docs
                });
            } 
        });
    });
};

const responseCode = {
    status: {
        // 500: { message: error['genericErrMsg']['500'][error['lang']] },
        // 200: {
        //     message: error['getProvider']['200'][error['lang']],
        //     data: joi.any()
        // },
    }

}//swagger response code

module.exports = {
    handler,
};