
'use strict';

const joi = require('joi');
const Async = require('async');
const error = require('../error');
const moment = require('moment');//date-time
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;
const wallet = require('../../../../models/wallet')

const handler = (req, reply) => {
    let collection = "";
    switch (parseInt(req.payload.userType)) {
        case 1:
            collection = "walletProvider";
            break;
        case 2:
            collection = "walletCustomer";
            break;
        case 3:
            collection = "walletOperator";
            break;
        case 4:
            collection = "walletApp";
            break;
        case 5:
            collection = "walletPg";
            break;
        default:
            collection = "";
            break;
    }

    if (collection == "")
        return reply({ errNum: 500, errMsg: 'Server Error', errFlag: 1 });
    let condition = {};
    switch (parseInt(req.payload.userType)) {
        case 1:
        case 2:
        case 3:
            condition = { userId: req.payload.userId };
            break;
        default:
            condition = {};
            break;
    }

    var regexValue = req.payload.sSearch;
    if (req.payload.sSearch != 'undefined' && req.payload.sSearch != '') {
        //        switch (parseInt(req.payload.userType)) {
        //            case 1:
        //                Object.assign(condition, {'$or': [{'firstName': new RegExp(regexValue, 'i')}, {'lastName': new RegExp(regexValue, 'i')}, {'email': new RegExp(regexValue, 'i')}, {'mobile': new RegExp(regexValue, 'i')}]})
        //                break;
        //            case 2:
        //                Object.assign(condition, {'$or': [{'name': new RegExp(regexValue, 'i')}, {'email': new RegExp(regexValue, 'i')}, {'phone': new RegExp(regexValue, 'i')}]})
        //                break;
        //            case 3:
        //                Object.assign(condition, {'$or': [{'operatorName': new RegExp(regexValue, 'i')}, {'email': new RegExp(regexValue, 'i')}, {'mobile': new RegExp(regexValue, 'i')}]})
        //                break;
        //            default:
        //                break;
        //        }
        Object.assign(condition, { '$or': [{ 'txnType': new RegExp(regexValue, 'i') }, { 'trigger': new RegExp(regexValue, 'i') }, { 'comment': new RegExp(regexValue, 'i') }] })

    }

    wallet.Count(collection, condition, (err, count) => {

        if (err)
            return reply({ errNum: 500, errMsg: 'Server Error', errFlag: 1 });
        if (count === 0)
            return reply({ iTotalRecords: 0, iTotalDisplayRecords: 0, aaData: [] });
        let queryObj = {
            q: condition,
            p: {},
            s: { _id: -1 },
            skip: parseInt(req.payload.iDisplayStart) || 0,
            limit: parseInt(req.payload.iDisplayLength) || 20
        }
        wallet.SELECT(collection, queryObj, (err, docs) => {

            if (err)
                return reply({ errNum: 500, errMsg: 'Server Error', errFlag: 1 });
            return reply({
                iTotalRecords: count,
                iTotalDisplayRecords: count,
                aaData: docs
            });
        });
    });
};

const responseCode = {
    status: {
        // 500: { message: error['genericErrMsg']['500'][error['lang']] },
        // 200: {
        //     message: error['getProvider']['200'][error['lang']],
        //     data: joi.any()
        // },
    }

}//swagger response code

module.exports = {
    handler,
};