'use strict';
const joi = require('joi');
const error = require('../../error');
const Async = require('async');
const logger = require('winston');
const moment = require('moment');//date-time
const schedule = require('../../../../../models/schedule');
const address = require('../../../../../models/address');
const dailySchedules = require('../../../../../models/dailySchedules');
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId

process.env.TZ = 'UTC/GMT';

const payloadValidator = joi.object({
    ids: joi.array().required().description('Array of master ids'),
    startTime: joi.string().required().description('start time in 24 hours format : HH:MM').error(new Error('start time is missing')),
    endTime: joi.string().required().description('end time in 24 hours format : HH:MM').error(new Error('end time is missing')),
    price: joi.number().integer().default(0).description('slot price').error(new Error('slot price is missing')),
    startDate: joi.string().allow('').description('end date format : YYYY-MM-DD').error(new Error("Enter start date")),
    endDate: joi.string().allow('').description('end date format : YYYY-MM-DD').error(new Error('end date is missing')),
    deviceTime: joi.string().required().description('end date format : YYYY-MM-DD HH:MM:SS').error(new Error('current devidce date & time is missing')),
    repeatDay: joi.number().required().integer().min(1).max(4).description('if select type = 2 then this field is mandatory \n Repeat Day of slot: \n 1:Every-Day, \n 2:Weak-Day, \n 3:Week-End \n 4: Select Days').error(new Error('Repeat day is missing')),
    days: joi.array().allow('').description('if you select repeate day 4 then its mandatory \n Format: ["Mon","Tue","Wed","Thu","Fri","Sat","Sun"]').error(new Error('days is missing')),
    duration: joi.number().required().integer().min(1).max(5).description('if select type = 2 then this field is mandatory \n Repeat Day of slot: \n 1:Every-Day, \n 2:Weak-Day, \n 3:Week-End \n 4: Select Days').error(new Error('Repeat day is missing')),

}).required();//payload of provider signin api

function getDateRange(startDate, endDate, dateFormat) {
    var dates = [],
        end = moment(endDate),
        diff = endDate.diff(startDate, 'days');
    if (!startDate.isValid() || !endDate.isValid() || diff < 0) {
        return;
    }
    dates.push(moment(startDate).format(dateFormat));
    for (var i = 0; i < diff; i++) {
        dates.push(startDate.add(1, 'd').format(dateFormat));
    }

    return dates;
}//get all Date
/**
 @param {} req 
 @param {} reply 
 */
const APIHandler = (req, reply) => { 
    Async.each(req.payload.ids, (providerId, cb) => {
        if(req.payload.duration==1)
        {   
            req.payload.startDate = moment().add(1, 'day').format('YYYY-MM-DD');
            req.payload.endDate = moment().endOf("month").format("YYYY-MM-DD");
        }
        else if(req.payload.duration==2)
        {
            req.payload.startDate = moment().add(1, 'day').format('YYYY-MM-DD');
            req.payload.endDate = moment(moment().add(1,'month').format('YYYY-MM-DD')).endOf('month').format('YYYY-MM-DD');
        }
        else if(req.payload.duration==3)
        {
            req.payload.startDate = moment().add(1, 'day').format('YYYY-MM-DD');
            req.payload.endDate = moment(moment().add(2,'month').format('YYYY-MM-DD')).endOf('month').format('YYYY-MM-DD');
        }
        else if(req.payload.duration==4)
        {
            req.payload.startDate = moment().add(1, 'day').format('YYYY-MM-DD');
            req.payload.endDate = moment(moment().add(3,'month').format('YYYY-MM-DD')).endOf('month').format('YYYY-MM-DD');
        }

        try {
            if (req.payload.endTime == '00:00')
                req.payload.endTime = "23:59";

            let actStartDate = req.payload.startDate;
            let actEndDate = req.payload.endDate;
            let actStartTime = req.payload.startTime;
            let actEndTime = req.payload.endTime;
            let dateTimeSt = moment(req.payload.startDate + ' ' + req.payload.startTime).unix();

            let reqStartDateTime = moment(req.payload.startDate + ' ' + req.payload.startTime).unix();
            let minuteSt = moment.unix(reqStartDateTime).minute();
            let hourSt = moment.unix(reqStartDateTime).hour();
            let checkStartTime = parseFloat(hourSt + '.' + minuteSt);

            let reqEndDateTime = moment(req.payload.endDate + ' ' + req.payload.endTime).unix();
            let minuteEnd = moment.unix(reqEndDateTime).minute();
            let hourEnd = moment.unix(reqEndDateTime).hour();
            let checkEndTime = parseFloat(hourEnd + '.' + minuteEnd);

            let serverTime = (moment().unix());
            let deviceTime = moment(req.payload.deviceTime, 'YYYY-MM-DD HH:mm:ss').unix();
            let timeZone = serverTime - deviceTime;

            let reqArr = JSON.parse(JSON.stringify(req.payload));

            let scheduleStartDateTime = moment(req.payload.startDate + ' ' + req.payload.startTime).unix();
            let scheduleEndDateTime = moment(req.payload.endDate + ' ' + req.payload.endTime).unix();
            let scheduleEndDateTimeEndOfDay = moment(req.payload.endDate + ' 23:59').unix();

            let strtDateTime = scheduleStartDateTime + timeZone;
            let endDateTime = scheduleEndDateTime + timeZone;
            let endDateTimeEndOfDay = scheduleEndDateTimeEndOfDay + timeZone;

            req.payload.startDate = moment.unix(strtDateTime).format('YYYY-MM-DD');
            req.payload.endDate = moment.unix(endDateTime).format('YYYY-MM-DD')
            req.payload.startTime = moment.unix(strtDateTime).format('HH:mm');
            req.payload.endTime = moment.unix(endDateTime).format('HH:mm');

            if (checkStartTime >= checkEndTime || checkStartTime == checkEndTime) {

                reqArr.timeZone = timeZone;
                reqArr.actStartDate = actStartDate;
                reqArr.actEndDate = actEndDate;
                reqArr.actStartTime = '00:00';

                reqArr.actEndTime = actEndTime;
                addOneMoreDay(req, reqArr, providerId, () => { });

                actEndTime = '23:59';
                scheduleEndDateTime = scheduleEndDateTimeEndOfDay;
                endDateTime = endDateTimeEndOfDay;
                req.payload.endDate = moment.unix(endDateTime).format('YYYY-MM-DD');
                req.payload.endTime = moment.unix(endDateTime).format('HH:mm');
            }

            var scheduleId = new ObjectID();
            var scheduleCreate = false;
            Async.series([

                function (callback) {
                    var dayArr = [];
                    switch (req.payload.repeatDay) {
                        case 1:
                            dayArr = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
                            req.payload.days = dayArr;
                            break;
                        case 2:
                            dayArr = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri'];
                            req.payload.days = dayArr;
                            break;
                        case 3:
                            dayArr = ['Sat', 'Sun'];
                            req.payload.days = dayArr;
                            break;
                        case 4:
                            dayArr = req.payload.days;
                        default:
                            if (req.payload.days == null || req.payload.days == undefined) {
                                return callback(Status.status(55, req.headers.lan));
                            }
                    }
                    var now = moment(actStartDate);
                    var then = moment(actEndDate);
                    var intervalDate = getDateRange(now, then, 'YYYY-MM-DD');

                    let minute = moment.unix(strtDateTime).minute();

                    let hour = moment.unix(strtDateTime).hour();
                    let plus = parseFloat(hour + '.' + minute);
                    let minuteEnd = moment.unix(endDateTime).minute();
                    let hourEnd = moment.unix(endDateTime).hour();
                    let endPlusTime = parseFloat(hourEnd + '.' + minuteEnd); 
                    schedule.checkAvailableSchedule(providerId, moment(req.payload.startDate).unix(), plus, endPlusTime, (err, cbData) => {
                        if (err)
                            return cb(err);
                        if (cbData.length != 0)
                            return cb(null);
                        Async.forEach(intervalDate, function (item, callbackloop) {

                            var d = moment(item).format("ddd");
                            var dayMatch = dayArr.includes(d);
                            address.readByCond({ user_Id: new ObjectID(providerId), defaultAddress: 1 }, (err, addressData) => {
                                if (addressData) {
                                    let locationLog = {
                                        longitude: parseFloat(addressData.longitude),
                                        latitude: parseFloat(addressData.latitude)
                                    }
                                    if (dayMatch) {

                                        var startD = moment(item + ' ' + actStartTime).unix() + timeZone;
                                        var endD = moment(item + ' ' + actEndTime).unix() + timeZone;
                                        let cond = [
                                            { '$unwind': '$schedule' },
                                            {
                                                '$match': {
                                                    "schedule.providerId": new ObjectID(providerId),
                                                    'schedule.startTime': { '$lte': startD },
                                                    'schedule.endTime': { '$gte': endD }
                                                }
                                            }

                                        ]
                                        dailySchedules.readByAggregate(cond, (err, cbData) => {
                                            if (err)
                                                return cb(err);
                                            else if (cbData.length != 0)
                                                return cb(null);
                                            else {
                                                req.providerId = providerId;
                                                req.payload.addresssId = addressData._id;
                                                dailySchedules.addDailySchedule(req, item, scheduleId, startD, endD, locationLog, (err, cbData) => {
                                                    if (err)
                                                        return cb(err);
                                                    if (cbData) {
                                                        scheduleCreate = true;
                                                    }
                                                    callbackloop(null);
                                                });
                                            }
                                        });

                                    } else {
                                        callbackloop(null);
                                    }
                                }
                            });

                        }, function (loopErr) {
                            return callback(null, true);
                        });
                    });
                }//update the new details
            ],
                function (err, data) {

                    if (err)
                        return cb(err);
                    if (!scheduleCreate)
                        return cb(null);
                    address.readByCond({ user_Id: new ObjectID(providerId), defaultAddress: 1 }, (err, addressData) => {
                        if (addressData) {
                            let minuteStart = moment.unix(strtDateTime).minute();
                            let hourStart = moment.unix(strtDateTime).hour();
                            let startPlus = parseFloat(hourStart + '.' + minuteStart);
                            let minute = moment.unix(endDateTime).minute();
                            let hour = moment.unix(endDateTime).hour();
                            let endPlus = parseFloat(hour + '.' + minute);
                            var userdata = {
                                _id: scheduleId,
                                status: true,
                                createdDate: moment().unix(),
                                providerId: new ObjectID(providerId),
                                price: req.payload.price ? req.payload.price : '',
                                
                                startDate: moment(req.payload.startDate).unix(),
                                endDate: moment(req.payload.endDate).unix(),
                                startTime: req.payload.startTime,
                                endTime: req.payload.endTime,
                                addresssId: addressData._id,
                                days: req.payload.days ? req.payload.days : '',
                                start: strtDateTime,
                                end: endDateTime,
                                startTimeInt: startPlus,
                                endTimeInt: endPlus,
                                location: {
                                    longitude: parseFloat(addressData.longitude),
                                    latitude: parseFloat(addressData.latitude)
                                },
                            };
                            schedule.postSchedule(userdata, (err, response) => {
                                if (err)
                                    return cb(err);
                                return cb(null);
                            });
                        } else {
                            return cb(null);
                        }
                    });

                });
        } catch (e) {
            return cb(e);
        }
    }, (err, result) => {
        if (err) {
            return reply({ message: error['genericErrMsg']['500'][req.headers.lan] }).code(500);
        } else {
            return reply({ message: error['genericErrMsg']['200'][req.headers.lan] }).code(200);
        }
    });

};

const responseCode = {
    // status: {
    //     200: { message: error['postSchedule']['200'][error['lang']] },
    //     401: { message: error['postSchedule']['401'][error['lang']] },
    //     500: { message: error['genericErrMsg']['500'][error['lang']] }
    // }
}//swagger response code


const addOneMoreDay = (req, reqArr, providerId, reply) => { 
    let scheduleStartDateTime = moment(reqArr.startDate + ' ' + reqArr.actStartTime).add(1, 'd').unix();
    let scheduleEndDateTime = moment(reqArr.endDate + ' ' + reqArr.endTime).add(1, 'd').unix();

    let strtDateTime = scheduleStartDateTime + reqArr.timeZone;
    let endDateTime = scheduleEndDateTime + reqArr.timeZone;

    reqArr.startDate = moment.unix(strtDateTime).format('YYYY-MM-DD');
    reqArr.endDate = moment.unix(endDateTime).format('YYYY-MM-DD')
    reqArr.startTime = moment.unix(strtDateTime).format('HH:mm');
    reqArr.endTime = moment.unix(endDateTime).format('HH:mm');

    var scheduleId = new ObjectID();
    var scheduleCreate = false;
    Async.series([

        function (callback) {
            var dayArr = [];
            switch (reqArr.repeatDay) {
                case 1:
                    dayArr = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
                    reqArr.days = dayArr;
                    break;
                case 2:
                    dayArr = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri'];
                    reqArr.days = dayArr;
                    break;
                case 3:
                    dayArr = ['Sat', 'Sun'];
                    reqArr.days = dayArr;
                    break;
                case 4:
                    dayArr = reqArr.days;
                default:
                    if (reqArr.days == null || reqArr.days == undefined) {
                        return callback(Status.status(55, req.headers.lan));
                    }
            }
            var now = moment(reqArr.actStartDate).add(1, 'd');
            var then = moment(reqArr.actEndDate).add(1, 'd');
            var intervalDate = getDateRange(now, then, 'YYYY-MM-DD');

            let minute = moment.unix(strtDateTime).minute();

            let hour = moment.unix(strtDateTime).hour();
            let plus = parseFloat(hour + '.' + minute);
            let minuteEnd = moment.unix(endDateTime).minute();
            let hourEnd = moment.unix(endDateTime).hour();
            let endPlusTime = parseFloat(hourEnd + '.' + minuteEnd);
            schedule.checkAvailableSchedule(providerId, moment(req.payload.startDate).unix(), plus, endPlusTime, (err, cbData) => {
                if (err)
                    return reply({ message: error['genericErrMsg']['500'][req.headers.lan] });
                if (cbData.length != 0)
                    return reply({ message: error['postSchedule']['401'][req.headers.lan] });
                Async.forEach(intervalDate, function (item, callbackloop) {
                    var d = moment(item).format("ddd");
                    var dayMatch = dayArr.includes(d);
                    address.readByCond({ user_Id: new ObjectID(providerId), defaultAddress: 1 }, (err, addressData) => {
                        if (addressData) {
                            let locationLog = {
                                longitude: parseFloat(addressData.longitude),
                                latitude: parseFloat(addressData.latitude)
                            }
                            if (dayMatch) {
                                var startD = moment(item + ' ' + reqArr.actStartTime).unix() + reqArr.timeZone;
                                var endD = moment(item + ' ' + reqArr.actEndTime).unix() + reqArr.timeZone;
                                let cond = [
                                    { '$unwind': '$schedule' },
                                    {
                                        '$match': {
                                            "schedule.providerId": new ObjectID(providerId),
                                            'schedule.startTime': { '$lte': startD },
                                            'schedule.endTime': { '$gte': endD }
                                        }
                                    }

                                ]
                                dailySchedules.readByAggregate(cond, (err, cbData) => {
                                    if (err)
                                        return reply({ message: error['genericErrMsg']['500'][req.headers.lan] }).code(500);
                                    else if (cbData.length != 0)
                                        return reply({ message: error['postSchedule']['401'][req.headers.lan] }).code(401);
                                    else {
                                        req.providerId = providerId;
                                        req.payload.addresssId = addressData._id;
                                        dailySchedules.addDailySchedule(req, item, scheduleId, startD, endD, locationLog, (err, cbData) => {
                                            if (err)
                                                return reply({ message: error['genericErrMsg']['500'][req.headers.lan] }).code(500);
                                            if (cbData) {
                                                scheduleCreate = true;
                                            }
                                            callbackloop(null);
                                        });
                                    }
                                });
                            } else {
                                callbackloop(null);
                            }
                        }
                    });

                }, function (loopErr) {
                    return callback(null, true);
                });
            });
        }//update the new details
    ],
        function (err, data) {
            if (err)
                return reply({ message: error['genericErrMsg']['500'][req.headers.lan] });
            if (!scheduleCreate)
                return reply({ message: error['postSchedule']['401'][req.headers.lan] });
            address.readByCond({ user_Id: new ObjectID(providerId), defaultAddress: 1 }, (err, addressData) => {
                if (addressData) {
                    let minuteStart = moment.unix(strtDateTime).minute();
                    let hourStart = moment.unix(strtDateTime).hour();
                    let startPlus = parseFloat(hourStart + '.' + minuteStart);
                    let minute = moment.unix(endDateTime).minute();
                    let hour = moment.unix(endDateTime).hour();
                    let endPlus = parseFloat(hour + '.' + minute);
                    var userdata = {
                        _id: scheduleId,
                        status: true,
                        createdDate: moment().unix(),
                        providerId: new ObjectID(providerId),
                        price: reqArr.price ? reqArr.price : '',
                        
                        startDate: moment(reqArr.startDate).unix(),
                        endDate: moment(reqArr.endDate).unix(),
                        startTime: reqArr.startTime,
                        endTime: reqArr.endTime,
                        addresssId: addressData._id,
                        days: reqArr.days ? reqArr.days : '',
                        start: strtDateTime,
                        end: endDateTime,
                        startTimeInt: startPlus,
                        endTimeInt: endPlus,
                        location: {
                            longitude: parseFloat(addressData.longitude),
                            latitude: parseFloat(addressData.latitude)
                        },
                    };
                    schedule.postSchedule(userdata, (err, response) => {
                        if (err)
                            return reply({ message: error['genericErrMsg']['500'][req.headers.lan] });
                        return reply({ message: error['postSchedule']['200'][req.headers.lan] });
                    });
                } else {
                    return reply({ message: error['postSchedule']['401'][req.headers.lan] });
                }
            });

        });
}

module.exports = { payloadValidator, APIHandler, responseCode };
