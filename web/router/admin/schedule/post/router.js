    'use strict';

const entity = '/admin';

const addSchdule = require('./addSchdule');

// const errorMsg = require('../../../../../locales');
const errorMsg = require('../../error');
const headerValidator = require('../../../../middleware/validator');

module.exports = [
    /**
    * @name GET /schdule/getSchduleByDate/{cityId}/{catid}/{schduleType}/{date}
    */
    {
        method: 'POST',
        path: entity + '/schdule',
        handler: addSchdule.APIHandler,
        config: {
            tags: ['api', entity],
            description: errorMsg['apiDescription']['adminScheduleGet'],
            notes: 'get all schedule of provider by date',
            auth: false,
            // response: getByLatLong.responseCode,
            validate: {
                headers: headerValidator.headerLanValidator,
                payload: addSchdule.payloadValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },


];