'use strict'

const joi = require('joi');
const Async = require('async');
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;
// const errorMsg = require('../../../../../locales');
const category = require('../../../../../models/category');
const provider = require('../../../../../models/provider');
const dailySchedules = require('../../../../../models/dailySchedules');
const error = require('../../error');

const params = joi.object({
    cityId: joi.any().allow("").description('cityId').error(new Error('cityId is missing')),
    catid: joi.any().allow("").description('catId').error(new Error('catId is missing')),
    schduleType: joi.number().default(1).description('schduleType 1:available,2:not- available').error(new Error('schduleType must be number')),
    date: joi.number().required().description('date').error(new Error('date is missing')),
}).required();

/**
 * @method GET /admin/schedule/{scheduleType}/{date}
 * @param {*} req 
 * @param {*} reply 
 */

const handler = (req, reply) => {
    const dbErrResponse = { message: error['genericErrMsg']['500']['1'], code: 500 };

    const getProvider = () => {
        return new Promise((resolve, reject) => {
            let proCon = {};
            if (req.params.catid != 0 && req.params.catid != "0") {
                proCon = { status: { "$in": [2, 3, 4, 5, 7] }, deactive: { "$ne": true }, 'catlist.cid': new ObjectID(req.params.catid) }
            } else {
                proCon = { status: { "$in": [2, 3, 4, 5, 7] }, deactive: { "$ne": true } }
            }
            if (req.params.cityId != 0 && req.params.cityId != "0") {
                proCon.cityId = req.params.cityId
            }

            console.log("proco", proCon)
            provider.readAll(proCon, (err, res) => {
                return err ? reject(dbErrResponse) : (res === null) ? reject(res) : resolve(res);
            });
        });
    }//get all provider
    let responseDataforAvailable = [];
    let responseDataforNotAvailable = [];
    const getDailyschedule = (providerdata) => {

        let query = [];
        return new Promise((resolve, reject) => {
            Async.forEach(providerdata, (pdata, callback) => {

                query = [
                    { $unwind: "$schedule" },
                    { $match: { date: req.params.date, "schedule.providerId": new ObjectID(pdata._id) } }

                ];
                dailySchedules.readByAggregate(query, (err, dailydata) => {
                    if (err) {
                        return callback(err);
                    } else if (dailydata.length > 0 ) {
                        getCategoryName(pdata)
                            .then(data => {
                                pdata.catNameArr = data;
                                pdata.dailySchedules = dailydata;
                                responseDataforAvailable.push(pdata)
                                callback(null, responseDataforAvailable)
                            }).catch(e => {
                                callback(null)
                            });
                    } else if (dailydata.length == 0) {
                        getCategoryName(pdata)
                            .then(data => {
                                pdata.catNameArr = data;
                                pdata.dailySchedules = dailydata;
                                responseDataforNotAvailable.push(pdata)
                                callback(null, responseDataforNotAvailable)
                            }).catch(e => {
                                callback(null)
                            });
                    } else {
                        return callback(null)
                    }
                });
            }, (err, res) => {
                return err ? reject(err)
                    : (req.params.schduleType == 2) ? resolve(responseDataforNotAvailable)
                        : resolve(responseDataforAvailable);
            });
        });
    }//get dailyschedule based on provider and response data


    const getCategoryName = (providerData) => {
        let catnamearr = [];
        return new Promise((resolve, reject) => {
            if (providerData.catlist) {
                Async.forEach(providerData.catlist, (catid, callback) => {
                    if (catid.cid) {
                        category.read(catid.cid, (err, catres) => {
                            if (err) {
                                return callback(err);
                            } else if (catres === null) {
                                return callback(null)
                            } else {
                                catnamearr.push(catres.cat_name);
                                callback(null, catnamearr)
                            }
                        });
                    }
                    else {
                        callback(null);
                    }

                }, (err, res) => {
                    return err ? reject(err) : resolve(catnamearr);
                });
            }
            else {
                return reslove(catnamearr);
            }

        });
    }//return get category name 





    getProvider()
        .then(getDailyschedule)
        
        .then(data => {
            return reply({ message: error['adminScheduleGet'][req.headers.lan], data: data, created: responseDataforAvailable.length, notCreated: responseDataforNotAvailable.length }).code(200);
        }).catch(e => {
            logger.error("Admin DialySchedule API error =>", e)
            return reply({ message: e.message }).code(e.code);
        });
}
module.exports = {
    params,
    handler
};