
'use strict';

const joi = require('joi');
const Async = require('async');
const error = require('../error');
const moment = require('moment');//date-time
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;
const adminPushNotifications = require('../../../../models/adminPushNotifications');
const fcm = require('../../../../library/fcm');


const payload = joi.object({
    topics: joi.string().required().description('array of topics'),
    title: joi.string().required().description('title of the push notification'),
    message: joi.string().required().description('message of the push notification'),
    type: joi.string().required().description('type - individual, city, zone'),
    cities: joi.string().description('city names'),
    userType: joi.number().description('customer, driver')
}).required();

const logAdminPushNotification = (data, callback) => {
    adminPushNotifications.post(data, (err, doc) => {
        if (err)
            return callback({ errNum: 500, errMsg: 'Database error', errFlag: 1 });

        return callback({ errFlag: 0, errMsg: 'Success', errNum: 200 });
    });
};
const handler = (req, reply) => {
    let topics = req.payload.topics.split(',');
    if (!Array.isArray(topics)) return reply({ errNum: 500, errMsg: 'Server Error', errFlag: 1 });//return if the topic is empty
    topics.forEach(topic => {
        let request = {
            fcmTopic: topic,
            action: 111,
            pushType: 7,
            title: req.payload.title,
            msg: req.payload.message,
            data: [],
            deviceType: 1
        }
        fcm.notifyFcmTpic(request, (e, r) => {
            let data = {
                title: req.payload.title,
                message: req.payload.message,
                type: req.payload.type,
                userType: req.payload.userType,
                topic: topic,
                time: moment().unix()
            }
            logAdminPushNotification(data, (err, res) => {
                
            })
        });

    });
    return reply({ errNum: 200, errMsg: 'Success', errFlag: 1 });


};

const responseCode = {
    status: {
        // 500: { message: error['genericErrMsg']['500'][error['lang']] },
        // 200: {
        //     message: error['getProvider']['200'][error['lang']],
        //     data: joi.any()
        // },
    }

}//swagger response code

module.exports = {
    payload,
    handler,
    responseCode
};