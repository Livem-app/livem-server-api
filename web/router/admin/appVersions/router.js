'use strict';

const entity = '';
const error = require('../error');
const get = require('./get'); 
const post = require('./post'); 
const patch = require('./patch'); 
const patchMandatory = require('./patchMandatory'); 
const headerValidator = require('../../../middleware/validator');

module.exports = [
    /**
    * @name  
    */
    {
        method: 'GET',
        path: entity + '/app/versions/{type}',
        handler: get.handler,
        config: {
            tags: ['api', 'admin'],
            description: "get All App Versions",
            auth: false,
            // response: post.responseCode,
            validate: {
                headers: headerValidator.headerLanValidator,
                params: get.params,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
    {
        method: 'POST',
        path: entity + '/app/versions',
        handler: post.handler,
        config: {
            tags: ['api', 'admin'],
            description: "This Api to add a new app & its versions",
            auth: false,
            // response: post.responseCode,
            validate: {
                headers: headerValidator.headerLanValidator,
                payload: post.payload,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
    {
        method: 'PATCH',
        path: entity + '/app/versions',
        handler: patch.handler,
        config: {
            tags: ['api', 'admin'],
            description: "This Api to update the new app version",
            auth: false,
            // response: post.responseCode,
            validate: {
                headers: headerValidator.headerLanValidator,
                payload: patch.payload,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
    {
        method: 'PATCH',
        path: entity + '/app/versions/mandatory',
        handler: patchMandatory.handler,
        config: {
            tags: ['api', 'admin'],
            description: "make the current version mandatory or not",
            auth: false,
            // response: post.responseCode,
            validate: {
                headers: headerValidator.headerLanValidator,
                payload: patchMandatory.payload,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
];