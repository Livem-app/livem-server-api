
'use strict';

const joi = require('joi');
const Async = require('async');
const error = require('../error');
const moment = require('moment');//date-time
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;
const appVersions = require('../../../../models/appVersions')


const payload = joi.object({
    type: joi.number().min(1).description('1-android, 2-ios, 1-driver, 2-customer'),
    version: joi.string().description('version number 1.0.0'),
    mandatory: joi.string().description('mandatory update to be enabled'),
}).required();


const handler = (req, reply) => {
    let mandatory = (req.payload.mandatory == 'true') ? true : false;

    let queryObj = { $set: { mandatory: mandatory, 'versions.$.mandatory': mandatory } }

    appVersions.update({ type: parseInt(req.payload.type), 'versions.version': req.payload.version }, queryObj, (err, doc) => {
        if (err)
            return reply({ errNum: 500, errMsg: 'Database error', errFlag: 1 });

        return reply({ errNum: 200, errFlag: 0, errMsg: 'Success' });
    });

};

const responseCode = {
    status: {
        // 500: { message: error['genericErrMsg']['500'][error['lang']] },
        // 200: {
        //     message: error['getProvider']['200'][error['lang']],
        //     data: joi.any()
        // },
    }

}//swagger response code

module.exports = {
    payload,
    handler,
    responseCode
};