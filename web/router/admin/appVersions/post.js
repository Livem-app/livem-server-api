
'use strict';

const joi = require('joi');
const Async = require('async');
const error = require('../error');
const moment = require('moment');//date-time
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID; 
const appVersions = require('../../../../models/appVersions')


const payload = joi.object({
    type: joi.number().min(1).description('1-android, 2-ios, 1-driver, 2-customer'),
    typeName: joi.string().description('name of the app'),
    version: joi.string().description('version number 1.0.0'),
    mandatory: joi.string().description('mandatory update to be enabled'),
}).required();


const handler = (req, reply) => {
    let data = {
        type: req.payload.type,
        typeName: req.payload.typeName,
        latestVersion: req.payload.version,
        mandatory: (req.payload.mandatory == 'true') ? true : false,
        versions: [{
            version: req.payload.version,
            timestamp: moment().unix(),
            mandatory: (req.payload.mandatory == 'true') ? true : false
        }],
        timestamp: moment().unix()
    };
    appVersions.read({ type: parseInt(req.payload.type) }, (err, doc) => {
        if (err)
            return reply({ errNum: 500, errMsg: 'Database error', errFlag: 1 });

        if (doc != null)
            return reply({ errNum: 400, errFlag: 1, errMsg: 'App version with this type already exists, please select a new type' });
        else {
            appVersions.post(data, (err, doc) => {
                if (err) return reply({ errNum: 500, errMsg: 'Database error', errFlag: 1 });

                return reply({ errNum: 200, errMsg: 'Success', errFlag: 0 });
            });
        }
    });
    
};

const responseCode = {
    status: {
        // 500: { message: error['genericErrMsg']['500'][error['lang']] },
        // 200: {
        //     message: error['getProvider']['200'][error['lang']],
        //     data: joi.any()
        // },
    }

}//swagger response code

module.exports = {
    payload,
    handler,
    responseCode
};