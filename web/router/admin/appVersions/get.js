
'use strict';

const joi = require('joi');
const Async = require('async');
const error = require('../error');
const moment = require('moment');//date-time
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;
var Bcrypt = require('bcrypt');
const configuration = require('../../../../configuration');
const appVersions =require('../../../../models/appVersions')


const params = joi.object({
    type: joi.number().required().min(1).description('1-android, 2-ios, 1-driver, 2-customer'),
}).required();


const handler = (req, reply) => {
    appVersions.read( { type: parseInt(req.params.type) }, (err, doc) => {
        if (err)
        return reply({ message: error['genericErrMsg']['500'][req.headers.lan] }).code(500);

        if (doc === null)
            return reply({ message: error['genericErrMsg']['200'][req.headers.lan], aaData: [] }).code(200);
        else
            return reply({ aaData: [doc] }).code(200);
    });
};

const responseCode = {
    status: {
        // 500: { message: error['genericErrMsg']['500'][error['lang']] },
        // 200: {
        //     message: error['getProvider']['200'][error['lang']],
        //     data: joi.any()
        // },
    }

}//swagger response code

module.exports = {
    params,
    handler,
    responseCode
};