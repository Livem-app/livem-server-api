
'use strict';

const joi = require('joi');
const Async = require('async');
const error = require('../error');
const moment = require('moment');//date-time
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;
const paymentCycles = require('../../../../models/paymentCycles')

const handler = (req, reply) => {
    paymentCycles.read({ name: req.params.cycle }, (err, doc) => {
        if (err)
            return reply(err).code(500);

        let data = {
            name: doc.name || '',
            type: doc.type || '',
            paid: (Array.isArray(doc.transfered)) ? doc.transfered.length : 0,
            issues: (Array.isArray(doc.issues)) ? doc.issues.length : 0,
            collectCash: (Array.isArray(doc.collectCash)) ? doc.collectCash.length : 0,
            time: doc.lastUpdate || Moment().unix()
        }

        return reply({ data: [data] }).code(200);
    })
};


module.exports = {
    handler,
};