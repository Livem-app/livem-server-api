
'use strict';

const joi = require('joi');
const Async = require('async');
const error = require('../error');
const moment = require('moment');//date-time
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;
const paymentCycles = require('../../../../models/paymentCycles')

const handler = (req, reply) => {
    getPaymentCycles(req.payload || {})
        .then(data => { return reply(data) })
        // .catch(err => { return reply(Status.status(3)) })
        .catch(err => { return reply(err) })
};


/**
 * Method to get the payment cycles
 */
const getPaymentCycles = (params) => {

    return new Promise((resolve, reject) => {

        let condition = {};

        if (typeof params.sSearch != 'undefined' && params.sSearch != '')
            condition['name'] = { $regex: new RegExp('^' + params.sSearch, 'i') };

        if (typeof params.startDate != 'undefined' && typeof params.endDate != 'undefined') {
            condition['startDate'] = { $gte: parseInt(params.startDate) };
            condition['endDate'] = { $lte: parseInt(params.endDate) };
        }
        paymentCycles.count(condition, (err, count) => {
            if (err) return reject(err);

            if (count === 0)
                return resolve({ iTotalRecords: 0, iTotalDisplayRecords: 0, aaData: [] });

            paymentCycles.readAllBySort({
                q: condition,
                p: {
                    _id: 1,
                    endDate: 1,
                    name: 1,
                    startDate: 1,
                    operators: 1,
                    drivers: 1,
                    dues: 1,
                    status: 1,
                    type: 1
                },
                s: { startDate: -1 },
                skip: parseInt(params.iDisplayStart) || 0,
                limit: parseInt(params.iDisplayLength) || 20
            }, (err, docs) => {
                if (err) return reject(err);

                return resolve({
                    iTotalRecords: count,
                    iTotalDisplayRecords: count,
                    aaData: docs
                });
            })
        })
    })
}


module.exports = {
    handler,
};