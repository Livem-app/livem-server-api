
'use strict';

const joi = require('joi');
const Async = require('async');
const error = require('../error');
const moment = require('moment');//date-time
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;
const payroll = require('../../../../models/payroll')
const appConfig = require('../../../../models/appConfig')
const provider = require('../../../../models/provider')

const handler = (req, reply) => {
    let id = new ObjectID(req.payload.id);
    let collectionName = (parseInt(req.payload.payrollType) === 1) ? 'payroll' : 'payrollWeekly';
    payroll.readPayroll({
        q: { did: id },
        s: { fromDate: -1 },
        skip: parseInt(req.payload.skip) + 1,
        limit: 1
    }, (err, docs) => {
        if (err)
            return reply({ errNum: 500, errFlag: 0, errMsg: "internal server error" });

        return reply({ errNum: 200, errFlag: 0, errMsg: "success", data: docs })
    })
};



module.exports = {
    handler,
};