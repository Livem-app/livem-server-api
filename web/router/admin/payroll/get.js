
'use strict';

const joi = require('joi');
const Async = require('async');
const error = require('../error');
const moment = require('moment');//date-time
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;
const payroll = require('../../../../models/payroll')

const handler = (req, reply) => {
    let params = {
        did: new ObjectID(req.params.id),
        fromDate: moment().startOf('day').unix(),
        toDate: moment().endOf('day').unix()
    }
 

    payroll.readAll(params, (err, data) => {
        if (err)
            return reply(err);

        return reply({ data: data }).code(200);
    });
};


module.exports = {
    handler,
};