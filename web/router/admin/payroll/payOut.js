
'use strict';

const joi = require('joi');
const Async = require('async');
const error = require('../error');
const moment = require('moment');//date-time
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;
const paymentCycles = require('../../../../models/paymentCycles')
const payroll = require('../../../../models/payroll')
const payoutFeeds = require('../../../../models/payoutFeeds')
const stripeConnectAccount = require('../../../../models/stripeConnectAccount')
const stripe = require('../../../commonModels/stripe')
const rabbitmqUtil = require('../../../../models/rabbitMq/wallet');
const rabbitMq = require('../../../../models/rabbitMq');

const handler = (req, reply) => {
    paymentCycles.read({ name: req.payload.cycleName }, (err, doc) => {
        if (err)
            return reply({ message: error['genericErrMsg']['500'][req.headers.lan] }).code(500);

        if (doc === null)
            return reply({ errNum: 400, errMsg: 'Invalid payment cycle', errFlag: 1 });

        if (doc.status === 'Running')
            return reply({ errNum: 400, errMsg: 'Payment cycle is already in process', errFlag: 1 });

        if (doc.status === 'Settled')
            return reply({ errNum: 400, errMsg: 'Payment cycle already settled', errFlag: 1 });

        let params = req.payload;

        params.startDate = doc.startDate;
        params.endDate = doc.endDate;
        console.log("processs start")
        runPayout(params, () => { return 1; });

        return reply({ errNum: 200, errMsg: 'Payment processing began', errFlag: 1 });
    })
};


/**
 * Method to process the payout
 * @param {*} params - cycleName, startDate, endDate of the cycle
 * @param {*} cb
 */
const runPayout = (params, cb) => {
    console.log("run payout")
    updatePaymentCycleStatus({ name: params.cycleName, status: 'Running' })
        .then(data => {
            return new Promise((resolve, reject) => {
                processPayments({
                    payrollType: 1,
                    cycleName: params.cycleName,
                    startDate: params.startDate,
                    endDate: params.endDate
                }).then(data => {
                    console.log("data ddddd");
                    return resolve('done')
                }).catch(err => {
                    console.log("data ddddd");
                    return resolve('done')
                });
            });
        })
        .then(data => {
            console.log("stripe settled error");
            return updatePaymentCycleStatus({ name: params.cycleName, status: 'Settled' })
        })
        .then(data => { return cb('updated payment cycle') })
        .catch(err => { return cb(err) })
}


/**
 * Method to update the payment cycle status
 * @param {*} params
 */
const updatePaymentCycleStatus = (params) => {
    console.log("update payment cycle status")
    return new Promise((resolve, reject) => {
        paymentCycles.findUpdate({
            query: { name: params.name },
            data: { $set: { status: params.status, lastUpdate: moment().unix() } }
        }, (err, doc) => {
            if (err)
                return reject(err);

            return resolve('done');
        })
    })
}

/**
 * Method to process the payment and log the payment status
 * @param {*} params - cycleName
 */
const processPayments = (params) => {
    console.log("process payment")
    return new Promise((resolve, reject) => {
        paymentCycles.read({ name: params.cycleName }, (err, data) => {
            if (err)
                return reject({ errMsg: 'Database error' });

            if (data === null)
                return reject({ errMsg: 'Cycle name not found' });
            else {
                console.log("else 2")
                if (typeof data.pendingDrivers != 'undefined' && data.pendingDrivers.length > 0) {
                    console.log("data");
                    let masterDetails = data.pendingDrivers[0];
                    console.log("masterDetails", masterDetails);
                    if (parseFloat(masterDetails.dues) < 0) {
                        console.log("duew", masterDetails.dues)
                        doStripePayout(params.payrollType, params.cycleName, params.startDate, params.endDate, masterDetails)
                            .then(data => {
                                console.log("data ddddd");
                                return resolve('done')
                            }).catch(err => {
                                console.log("datadddddddddd");
                                return resolve('done')
                            });
                    }
                    else {
                        console.log("else duew c", masterDetails.dues)
                        updateCashCollection({
                            cycleName: params.cycleName,
                            id: masterDetails.id,
                            dues: masterDetails.dues,
                            payrollType: params.payrollType,

                            name: masterDetails.name || '',
                            phone: masterDetails.phone || '',
                            email: masterDetails.email || ''
                        }).then(d => {
                            console.log("resolve    in  update cash")
                            return resolve(params);
                        }).catch(e => {
                            console.log("error in  update cash")
                            return reject(e)
                        });
                    }
                }
                else {
                    console.log("elese");
                    return reject(data);
                }
            }

        })

    }).then(data => {
        console.log("processsssssssssssssssssss")
        return processPayments(params)
    }).then(data => {
        return ('done')
    }).catch(err => {
        // return
        console.log("reject")
        return (err);
    })
}

/**
 * Method to get the payment cycle document to process payments
 * @param {*} name - name of the cycle
 */
const getPaymentCycleToProcessPayments = (name) => {
    console.log("getPaymentCycleToProcessPayments")
    return new Promise((resolve, reject) => {
        paymentCycles.read({ name }, (err, doc) => {
            if (err)
                return reject({ errMsg: 'Database error' });

            if (doc === null)
                return reject({ errMsg: 'Cycle name not found' });

            return resolve(doc);
        })
    })
}


/**
 * Method to update the cash to be collected by the master
 * @param {*} params - payrollType, cycleName, id, dues
 */
const updateCashCollection = (params) => {
    console.log("updateCashCollection")
    console.log(params);

    return new Promise((resolve, reject) => {

        let query = {
            query: { name: params.cycleName },
            data: {
                $addToSet: {
                    collectCash: {
                        id: new ObjectID(params.id),
                        dues: params.dues,
                        time: moment().unix(),

                        name: params.name || '',
                        phone: params.phone || '',
                        email: params.email || ''
                    }
                },
                $pull: { pendingDrivers: { id: new ObjectID(params.id) } }
            }
        }
        paymentCycles.findUpdate(query, (err, doc) => {
            if (err)
                return reject({ errMsg: 'Database error' });


            let collectionName = (parseInt(params.payrollType) === 1) ? 'payroll' : 'payrollWeekly';
            payroll.findUpdate(
                {
                    query: {
                        did: new ObjectID(params.id),
                        cycleName: params.cycleName
                    },
                    data: {
                        $set: {
                            paymentStatus: 'Collect Cash',
                            status: 3,
                            settlementTime: moment().unix()
                        }
                    }
                },
                (err, status) => {
                    if (err)
                        return reject({ errMsg: 'Database error' });

                    return resolve({ errMsg: 'Cash collection updated' });
                })
        })
    })
}

/**
 * Method to log the stripe transfer id
 * @param {*} params
 */
const logStripeTransferId = (params) => {
    console.log("logStripeTransferId")
    return new Promise((resolve, reject) => {

        let query = {
            query: { name: params.cycleName },
            data: {
                $addToSet:
                    {
                        transfered:
                            {
                                id: new ObjectID(params.id),
                                amount: params.amount,
                                txnId: params.txnId,
                                time: params.created,

                                name: params.name || '',
                                phone: params.phone || '',
                                email: params.email || ''
                            }
                    },
                $pull: { pendingDrivers: { id: new ObjectID(params.id) } }
            }
        }
        paymentCycles.findUpdate(query, (err, doc) => {
            if (err)
                return reject({ errMsg: 'DB error: Unable to log stripe transfer id' });

            // let collectionName = (parseInt(params.payrollType) === 1) ? 'payroll' : 'payrollWeekly';
            payroll.findUpdate(
                {
                    query: { did: new ObjectID(params.id), cycleName: params.cycleName },
                    data: {
                        $set: {
                            paymentStatus: 'Settled',
                            status: 1,
                            txnId: params.txnId,
                            paidAmount: params.amount,
                            settlementTime: moment().unix()
                        }
                    }
                },
                (err, status) => {
                    if (err)
                        return reject({ errMsg: 'Database error' });

                    return resolve(params);
                })
        })
    })
}

/**
 * Method to log the stripe transfer issues
 * @param {*} params
 */
const logStripeIssues = (params) => {
    console.log("logStripeIssues")
    return new Promise((resolve, reject) => {

        let query = {
            query: { name: params.cycleName },
            data: {
                $addToSet:
                    {
                        issues:
                            {
                                id: new ObjectID(params.id),
                                amount: params.amount,
                                message: params.message,
                                time: moment().unix(),

                                name: params.name || '',
                                phone: params.phone || '',
                                email: params.email || ''
                            }
                    },
                $pull: { pendingDrivers: { id: new ObjectID(params.id) } }
            }
        }
        paymentCycles.findUpdate(query, (err, doc) => {
            if (err)
                return reject({ errMsg: 'Database error' });

            // let collectionName = (parseInt(params.payrollType) === 1) ? 'payroll' : 'payrollWeekly';
            payroll.findUpdate(
                {
                    query: { did: new ObjectID(params.id), cycleName: params.cycleName },
                    data: {
                        $set: {
                            paymentStatus: 'Issues',
                            status: 2,
                            message: params.message,
                            settlementTime: moment().unix()
                        }
                    }
                },
                (err, status) => {
                    if (err)
                        return reject({ errMsg: 'Database error' });

                    return resolve({ errMsg: 'issue logged' });
                })
        })
    })
}

/**
 * Method to make the stripe payout
 * @param {*} cycleName - payment cycle name
 * @param {*} params
 */
const doStripePayout = (payrollType, cycleName, startDate, endDate, params) => {
    console.log("doStripePayout")
    return new Promise((resolve, reject) => {

        getStripeAccountId(params.id)
            // .then(id => { return verifyStripeAccount(id) })//verify the stripe account id
            .then(id => {
                console.log("id", id);
                return stripeTransfer({
                    id: params.id,
                    connectedStripeAccountId: id,
                    amount: Math.abs(parseInt(parseFloat(params.dues))),//amount to be transfer in cents
                    currency: 'usd'
                })
            })//create the stripe transfer
            .then(transfer => {
                console.log("do stripe transer done")
                return logStripeTransferId({
                    cycleName,
                    id: params.id,
                    amount: (transfer.amount / 100),//cents to floating values(dollars)
                    txnId: transfer.id,
                    created: transfer.created,
                    payrollType,

                    name: params.name || '',
                    phone: params.phone || '',
                    email: params.email || '',

                    startDate,
                    endDate
                })
            })//log the transfer details
            .then(data => {
                console.log("ddddddd", data)
                let dataArr = {
                    cycleName: cycleName,
                    created: data.created,

                    providerId: params.id,
                    amount: data.amount,
                    txnId: data.txnId,
                    transcationType: 3,//payout
                    name: data.name || '',
                    phone: data.phone || '',
                    email: data.email || '',
                    startDate: startDate,
                    endDate: endDate
                }
                insertInPayOutFeeds(dataArr); 
                rabbitmqUtil.InsertQueue(
                    rabbitMq.getChannelWallet(),
                    rabbitMq.queueWallet,
                    dataArr,
                    (err, doc) => {

                    });
                return resolve('done');
            })//update the wallet amount 
            .catch(err => {

                console.log('error');
                console.log(err);


                logStripeIssues({
                    cycleName,
                    id: params.id,
                    amount: Math.abs(parseInt(parseFloat(params.dues) * 100)),
                    message: err.errMsg,
                    payrollType,
                    name: params.name || '',
                    phone: params.phone || '',
                    email: params.email || ''
                })
                    .then(data => {
                        console.log("-------------- done ")
                        return resolve('done')
                    })
                    .catch(err => {
                        console.log("-----2--------- done ")
                        return resolve('done')
                    })
            })
    })
}

const getStripeAccountId = (id) => {
    console.log("getStripeAccountId")
    return new Promise((resolve, reject) => {
        stripeConnectAccount.getAccountById(id, (err, doc) => {
            if (err)
                return reject({ errMsg: 'Database error' });

            if (doc === null)
                return reject({ errMsg: 'Master not found' });

            if (typeof doc.stripeId === undefined || doc.stripeId === '' || doc.stripeId === null)
                return reject({ errMsg: 'No stripe id found' });

            return resolve(doc.stripeId);
        })
    })
}
/**
 * Method to transfer the amount to the stripe connect account
 * @param {*} params - stripe id, amount to be transfered
 */
const stripeTransfer = (params) => {
    return new Promise((resolve, reject) => {
        stripe.stripeTransaction.transferToConnectAccount(params.id, params.amount, 'usd')
            .then(data => {
                return resolve(data);
            }).catch(err => {
                if (err)
                    return reject({ errMsg: err.message });
            })
    })
}
const insertInPayOutFeeds = (params) => {
    let dataObj = {
        cycleName: params.cycleName,
        amount: params.amount,
        transferDate: params.created,
        transferId: params.txnId,
        providerId: new ObjectID(params.providerId),
        name: params.name,
        phone: params.phone,
        email: params.email,
        startDate: params.startDate,
        endDate: params.endDate
    }
    console.log("-----------------", dataObj)
    payoutFeeds.insert(dataObj, (err, res) => { 
    })

}//debit from apps wallet

module.exports = {
    handler,
};