
'use strict';

const joi = require('joi');
const Async = require('async');
const error = require('../error');
const moment = require('moment');//date-time
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;
const payroll = require('../../../../models/payroll')
const bookings = require('../../../../models/bookings')
const provider = require('../../../../models/provider')

const handler = (req, reply) => {
    bookings.read({ bookingId: parseFloat(req.params.id) }, (err, doc) => {
        if (err)
            return reply({ errNum: 500, errFlag: 0, errMsg: "internal server error" });

        if (doc === null)
            return reply({ errNum: 400, errMsg: 'Booking not available', errFlag: 1 });

        // let distanceMetric = doc.distanceMatrix == 0 ? 'Km' : 'Miles' || 'Miles';
        let currencySymbol = doc.currencySymbol || '$'; 
        let data = {

            bid: doc.bookingId,
            // tripDistance: doc.receivers[0].DriverTripDistance + ' ' + distanceMetric,
            // deadHead: doc.receivers[0].DriverPickupDistance + ' ' + distanceMetric,
            // tripTime: doc.accouting.time || 0,
            // loadingWaitingTime: ((parseInt(doc.receivers[0].DriverLoadedStartedTime) - parseInt(doc.receivers[0].DriverArrivedTime))) || 0,
            // unloadingWaitingTime: ((parseInt(doc.receivers[0].DriverDropedTime) - parseInt(doc.receivers[0].DriverReachedDropLocationTime))),
            // baseFee: currencySymbol + ' ' + (doc.accouting.baseFare || 0),
            // distanceFee: currencySymbol + ' ' + (doc.accouting.distFare || 0),
            // timeFee: currencySymbol + ' ' + (doc.accounting.timeFare || 0),
            // waitingFee: currencySymbol + ' ' + (doc.accounting.watingFee || 0),
            // handlingCharges: currencySymbol + ' ' + (doc.accounting.handlingFee || 0),
            // tollFee: currencySymbol + ' ' + (doc.accouting.tollFee || 0),
            // adjustments: currencySymbol + ' ' + (doc.accounting.adjustments || 0),
            // adjustmentsNotes: doc.accouting.adjustmentsNotes || '',
            // adjustmentsDate: doc.accouting.adjustmentsDate || '',
            total: currencySymbol + ' ' + (doc.accounting.total || 0),
            paymentMethod: doc.accounting.paymentMethodText,
            // cashCollected: currencySymbol + ' ' + (doc.accounting.cashCollected || 0),
            appCommission: currencySymbol + ' ' + parseFloat(doc.accounting.appEarningPgComm || 0).toFixed(2),
            driverEarnings: currencySymbol + ' ' + parseFloat(doc.accounting.providerEarning || 0).toFixed(2),
            cancelationFee: currencySymbol + ' ' + parseFloat(doc.accounting.cancellationFee || 0).toFixed(2),

            customerName: doc.customerData.firstName || '',
            customerNumber: doc.customerData.phone || '',

            // receiverName: doc.receivers[0].name || '',
            // receiverNumber: doc.receivers[0].mobile || '',

            // helpers: doc.helpers || 0
        }

        return reply({ errNum: 200, errFlag: 0, errMsg: "success", data: data })
    })
};



module.exports = {
    handler,
};