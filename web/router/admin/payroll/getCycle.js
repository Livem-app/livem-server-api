
'use strict';

const joi = require('joi');
const Async = require('async');
const error = require('../error');
const moment = require('moment');//date-time
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;
const payroll = require('../../../../models/payroll')

const handler = (req, reply) => {
    let collectionName = (parseInt(req.params.type) === 1) ? 'payroll' : 'payrollWeekly';

    let condition = {
        cycleName: req.params.cycle
    }

    if (typeof req.params.status != 'undefined')
        condition['status'] = parseInt(req.params.status);
    payroll.readAll(condition, (err, docs) => {
        if (err)
            return reply(err);

        return reply({ data: docs }).code(200);
    })
};


module.exports = {
    handler,
};