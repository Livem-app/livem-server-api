'use strict';

const entity = '/admin';

var Joi = require('joi');
var async = require("async");
const error = require('../error');
const get = require('./get');
const getPaymentCycle = require('./getPaymentCycle');
const getCycle = require('./getCycle');
const getReports = require('./getReports')
const payOut = require('./payOut')
const summary = require('./summary')
const summaryPrevious = require('./summaryPrevious')
const summaryBooking = require('./summaryBooking')
const acceptanceRate = require('./acceptanceRate')
const getBooking = require('./getBooking')
const paymentCycles = require('../../../commonModels/paymentCycles');


module.exports = [
    {
        method: 'GET',
        path: '/payroll/{id}',
        config: {
            tags: ['api', 'admin'],
            description: 'Get the masters payroll data',
            notes: 'Get the masters payroll data',
            auth: false,
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form'
                }
            },
            validate: {
                params: {
                    id: Joi.string().required().description('masters id')
                }
            }
        },
        handler: get.handler
    },

    {
        method: 'POST',
        path: '/payroll/cycles',
        config: {
            tags: ['api', 'admin'],
            description: 'Get the payroll or payment cycles',
            notes: 'Get the payroll or payment cycles',
            auth: false,
        },
        handler: getPaymentCycle.handler
    },

    {
        method: 'GET',
        path: '/payroll/drivers/{cycle}/{type}',
        config: {
            tags: ['api', 'admin'],
            description: 'Get the payroll or payment cycles',
            notes: 'Get the payroll or payment cycles',
            auth: false,
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form'
                }
            },
            validate: {
                params: {
                    cycle: Joi.string().required().description('cycle name'),
                    type: Joi.number().required().description('1-daily, 2-weekly')
                }
            }
        },
        handler: getCycle.handler
    },

    {
        method: 'GET',
        path: '/payroll/drivers/{cycle}/{type}/{status}',
        config: {
            tags: ['api', 'admin'],
            description: 'Get the payroll or payment cycles',
            notes: 'Get the payroll or payment cycles',
            auth: false,
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form'
                }
            },
            validate: {
                params: {
                    cycle: Joi.string().required().description('cycle name'),
                    type: Joi.number().required().description('1-daily, 2-weekly'),
                    status: Joi.number().description('1-paid, 2-bank issues, 3-collect cash')
                }
            }
        },
        handler: getCycle.handler
    },
    {
        method: 'GET',
        path: '/payroll/bookings/{id}',
        config: {
            tags: ['api', 'admin'],
            description: 'Get the payroll or payment cycles',
            notes: 'Get the payroll or payment cycles',
            auth: false,
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form'
                }
            },
            validate: {
                params: {
                    id: Joi.number().required().description('booking id')
                }
            }
        },
        handler: getBooking.handler
    },

    {
        method: 'GET',
        path: '/payroll/reports/{cycle}',
        config: {
            tags: ['api', 'admin'],
            description: 'Get the reports of a cycle',
            notes: 'Get the reports of a cycle',
            auth: false,
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form'
                }
            },
            validate: {
                params: {
                    cycle: Joi.string().required().description('cycle id or name')
                }
            }
        },
        handler: getReports.handler
    },

    {
        method: 'POST',
        path: '/payroll/settle',
        config: {
            tags: ['api', 'admin'],
            description: 'API to settle the payments',
            notes: 'API to settle the payments',
            auth: false,
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form'
                }
            },
            validate: {
                payload: {
                    cycleName: Joi.string().required().description('cycle id or name'),
                    payrollType: Joi.number().required().description('1-daily, 2-weekly')
                }
            }
        },
        handler: payOut.handler
    },

    {
        method: 'POST',
        path: '/payroll/runtodayscycle',
        config: {
            tags: ['api', 'admin'],
            description: 'API to run todays cycle',
            notes: 'API to run todays cycle',
            auth: false
        },
        handler: paymentCycles.runDailyCycle
    },
    {
        method: 'POST',
        path: '/provider/summary',
        config: {
            tags: ['api', 'admin'],
            description: 'Get the master\'s summary',
            notes: "Get the master's summary",
            // auth: 'masterJWT',
            auth: false,
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form'
                }
            },
            validate: {
                // payload: {
                //     type: Joi.number().required().description('0-Today, 1-Current Cycle, >=2 - Previous Cycles with pagination')
                // },
                payload: {
                    id: Joi.string().required().description('id')
                },
                // headers: headerAuthValidator
            }
        },
        handler: summary.handler
    },
    {
        method: 'POST',
        path: '/provider/summaryPrevious',
        config: {
            tags: ['api', 'admin'],
            description: 'Get the master\'s summary',
            notes: "Get the master's summary",
            // auth: 'masterJWT',
            auth: false,
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form'
                }
            },
            validate: {
                // payload: {
                //     type: Joi.number().required().description('0-Today, 1-Current Cycle, >=2 - Previous Cycles with pagination')
                // },
                payload: {
                    id: Joi.string().required().description('id'),
                    payrollType: Joi.number().required().description('1-daily, 2-weekly'),
                    skip: Joi.number().required().description('number of documents to skip')
                },
                // headers: headerAuthValidator
            }
        },
        handler: summaryPrevious.handler
    },
    {
        method: 'POST',
        path: '/provider/summary/bookings',
        config: {
            tags: ['api', 'admin'],
            description: 'Get the master\'s summary bookings',
            notes: "Get the master's summary bookings",
            // auth: 'masterJWT',
            auth: false,
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form'
                }
            },
            validate: {
                // payload: {
                //     type: Joi.number().required().description('0-Today, 1-Current Cycle, >=2 - Previous Cycles with pagination')
                // },
                payload: {
                    id: Joi.string().required().description('id'),
                    fromDate: Joi.number().required().description('from date'),
                    toDate: Joi.number().required().description('to date')
                },
                // headers: headerAuthValidator
            }
        },
        handler: summaryBooking.handler
    },
    {
        method: 'POST',
        path: '/provider/summary/acceptanceRate',
        config: {
            tags: ['api', 'admin'],
            description: 'Get the master\'s acceptance rate',
            notes: "Get the master\'s acceptance rate",
            // auth: 'masterJWT',
            auth: false,
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form'
                }
            },
            validate: {
                payload: {
                    payrollType: Joi.number().required().description('1-daily, 2-weekly'),
                    cycleName: Joi.string().required().description('cycle name'),
                    id: Joi.string().required().description('master id')
                },
                // headers: headerAuthValidator
            }
        },
        handler: acceptanceRate.handler
    },

];