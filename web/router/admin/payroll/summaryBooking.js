
'use strict';

const joi = require('joi');
const Async = require('async');
const error = require('../error');
const moment = require('moment');//date-time
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;
const payroll = require('../../../../models/payroll')
const bookings = require('../../../../models/bookings')
const provider = require('../../../../models/provider')

const handler = (req, reply) => {

    let query = [
        {
            $match: {
                providerId: req.payload.id,
                status: { $in: [11,12, 10] },
                bookingRequestedFor: { $gte: parseInt(req.payload.fromDate), $lte: parseInt(req.payload.toDate) }
            }
        },
        { $sort: { bookingCompletedAt: -1 } },
        {
            $project: {
                bookingId: 1,
                currencySymbol: 1,
                amount: '$accounting.providerEarning',
                status: '$statusMsg',
                paymentTypeText: '$accounting.paymentMethodText',
            }
        },
    ];
    bookings.aggregate(query, (err, docs) => {
        if (err)
            return reply({ errNum: 500, errFlag: 0, errMsg: "internal server error" });

        return reply({ errNum: 200, errFlag: 0, errMsg: "success", data: docs })
    });
};



module.exports = {
    handler,
};