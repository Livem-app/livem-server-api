
'use strict';

const joi = require('joi');
const Async = require('async');
const error = require('../error');
const moment = require('moment');//date-time
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;
const payroll = require('../../../../models/payroll')
const appConfig = require('../../../../models/appConfig')
const provider = require('../../../../models/provider')
const cities = require('../../../../models/cities')

const handler = (req, reply) => {
    getMasterSummary({ id: req.payload.id }, (data) => { return reply(data) });
};


/**
 * Method to get the summary of the master
 */
const getMasterSummary = (params, callback) => {

    let id = new ObjectID(params.id);

    let response = [];
    let moreDocs = 0;
    let distanceMetric = 'Miles';
    let currencySymbol = '$';

    let updatePayroll = false;//update the payroll data if the current days data is not available asynchronously

    Async.waterfall([

        function (cb) {
            provider.getProviderById(id, (err, doc) => {
                if (err)
                    return cb(err);

                if (doc === null)
                    return cb({ errNum: 400, errMsg: 'Session exipred, please login', errFlag: 1 });
                else {
                    cities.read({ _id: new ObjectID(doc.cityId) }, (err, res) => {
                        if (err) {
                            return cb(err);
                        } else {
                            currencySymbol = res.currencySymbol;
                            distanceMetric = (res.distanceMatrix == 0) ? 'Km' : 'Miles';
                            return cb(null, parseInt(doc.payrollType || 1));
                        }
                    });
                }

            })
        },//get the payrollType of the master

        function (payrollType, cb) {

            let data = {
                did: id,
                bookings: 0,
                earnings: 0,
                rating: 0,
                acceptanceRate: 0,
                tripDistance: 0,
                deadHead: 0,
                onlineTime: 0,
                waitingTime: 0,
                paymentStatus: 'Not Settled',
                fromDate: moment().startOf('day').unix(),
                toDate: moment().endOf('day').unix(),
                payrollType
            }
            payroll.read({
                did: id,
                fromDate: moment().startOf('day').unix(),
                toDate: moment().endOf('day').unix()
            }, (err, doc) => {
                if (err)
                    return cb(err);

                if (doc != null)
                    data = doc;
                else
                    updatePayroll = true;//update the payroll since the current days document is not available

                response.push(data);

                return cb(null, data, (doc === null) ? false : true, payrollType);
            })
        },//get the current days summary

        function (todaySummary, todaySummaryAvailable, payrollType, cb) {

            switch (payrollType) {

                case 1:
                    response.push(todaySummary);

                    let skip = (todaySummaryAvailable) ? 1 : 0;
                    let limit = 1;
                    payroll.readPayroll({
                        q: { did: id },
                        s: { fromDate: -1 },
                        skip: skip,
                        limit: limit
                    }, (err, docs) => {
                        if (err)
                            return cb(status.status(3));
                        console.log("doc --", docs)
                        payroll.count({ did: id }, (err, count) => {
                            if (err)
                                return cb(status.status(3));

                            moreDocs = count - skip;

                            return cb(null, response.concat(docs));
                        })
                    });
                    break;

                // case 2:
                //     Mongo.SELECT('payrollWeekly', {
                //         q: { did: id },
                //         s: { fromDate: -1 },
                //         skip: 0,
                //         limit: 2
                //     }, (err, docs) => {
                //         if (err)
                //             return cb(status.status(3));

                //         Mongo.count('payroll', { did: id }, (err, count) => {
                //             if (err)
                //                 return cb(status.status(3));

                //             moreDocs = count;

                //             return cb(null, response.concat(docs));
                //         });
                //     });
                //     break;

                default:
                    return cb(null, response);
            }
        }//get the further days summary based on the payroll type
    ], (err, result) => {
        if (err){
            console.log(err)
            return callback({ errNum: 500, errFlag: 0, errMsg: "internal server error" });    
        }
        

        // if (updatePayroll)
        //     // updatePayrollTesting({ id: params.id }, () => { return 1; });
        //     PayrollMethods.updatePayrollData({ id: params.id }, () => { return 1; });
        return callback({ errNum: 200, errFlag: 0, errMsg: "success", data: { summary: result, count: moreDocs, distanceMetric, currencySymbol, reload: updatePayroll } })

    })
}
module.exports = {
    handler,
};