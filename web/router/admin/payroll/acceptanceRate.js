
'use strict';

const joi = require('joi');
const Async = require('async');
const error = require('../error');
const moment = require('moment');//date-time
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;
const payroll = require('../../../../models/payroll')
const bookings = require('../../../../models/bookings')
const provider = require('../../../../models/provider')

const handler = (req, reply) => {
    let collectionName = (parseInt(req.payload.payrollType) === 1) ? 'payroll' : 'payrollWeekly';
    payroll.read(
        {
            cycleName: req.payload.cycleName,
            did: new ObjectID(req.payload.id),
        },
        (err, doc) => {
            if (err)
                return reply({ errNum: 500, errFlag: 0, errMsg: "internal server error" });
            let data = {
                received: 0,
                accepted: 0,
                ignored: 0,
                rejected: 0,
                completed: 0,
                cancelled: 0,
                cancelledByCustomer: 0,
                acceptanceRate: 0,
                fromDate: 0,
                toDate: 0
            }

            if (doc != null)
                data = {
                    received: doc.received || 0,
                    accepted: doc.accepted || 0,
                    ignored: doc.ignored || 0,
                    rejected: doc.rejected || 0,
                    completed: doc.completedBookings || 0,
                    cancelled: doc.cancelled || 0,
                    cancelledByCustomer: doc.cancelledByCustomer || 0,
                    acceptanceRate: doc.acceptanceRate || 0,
                    fromDate: doc.fromDate || 0,
                    toDate: doc.toDate || 0
                }

            return reply({ errNum: 200, errFlag: 0, errMsg: "success", data: data })
        })
};



module.exports = {
    handler,
};