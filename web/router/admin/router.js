'use strict';

const provider = require('./provider');

const password = require('./password');

const customer = require('./customer');

const logs = require('./logs');

const bookings = require('./bookings');

const email = require('./email');
const schedule = require('./schedule');

const appVersions = require('./appVersions');

const sendPush = require('./sendPush');
const godsview = require('./godsview');
const cityZone = require('./cityZone');
const stripe = require('./stripe');

const wallet = require('./wallet');
const payroll = require('./payroll');

const accounting = require('./accounting');

module.exports = [].concat(
    logs,
    schedule,
    email,
    provider,
    customer,
    password,
    bookings,
    sendPush,
    appVersions,
    godsview,
    cityZone,
    stripe,
    wallet,
    payroll,
    accounting
);