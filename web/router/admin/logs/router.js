'use strict';

const entity = '/admin';
const getSms = require('./getSms');
const getEmail = require('./getEmail');
const error = require('../error');

const headerValidator = require('../../../middleware/validator');

module.exports = [
    /**
    * @name  
    */
    {
        method: 'GET',
        path: entity + '/smsLog/{startDate}/{endDate}',
        handler: getSms.handler,
        config: {
            tags: ['api', 'admin'],
            description: error['apiDescription']['getSms'],
            auth: false,
            response: getSms.responseCode,
            validate: {
                params: getSms.params,
                headers: headerValidator.headerLanValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
    /**
   * @name  
   */
    {
        method: 'GET',
        path: entity + '/emailLog/{startDate}/{endDate}',
        handler: getEmail.handler,
        config: {
            tags: ['api', 'admin'],
            description: error['apiDescription']['getEmail'],
            auth: false,
            response: getEmail.responseCode,
            validate: {
                params: getEmail.params,
                headers: headerValidator.headerLanValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    }
];