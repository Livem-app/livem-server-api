
const auth = require('./authentication');
const swagger = require('./swagger');
const good = require('./good');

module.exports = { good, swagger, auth };