'use strict';

const JWT = require('jsonwebtoken');
const HAPI_AUTH_JWT = require('./hapi-auth-jwt2');
const moment = require('moment');//date-time
const Config = process.env;
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId
const customer = require('../../models/customer');
const provider = require('../../models/provider');
const dispatchers = require('../../models/dispatchers');

let AUTH = module.exports = {};

AUTH.HAPI_AUTH_JWT = HAPI_AUTH_JWT;

/**
 * Method to generate a JWT token
 * @param {*} data - the claims to be included in the jwt
 * @param {*} type - the subject, which will differentiate the tokens (master, slave or admin)
 */
AUTH.SignJWT = (data, type, expTime) => {

    data.accessCode = Math.floor(Math.random() * 8999 + 1000);//4 digit random access code
    updateAccessCode(type, data, () => {
    });//asynchronously update the access code in respective databae documents
    return JWT.sign(
        data,
        'key',
        {
            expiresIn: expTime,
            subject: type
        });
};

/**
 * Method to update the accessCode asynchronously
 * @param {*} type - to identify the collection name to update
 * @param {*} data - the accessCode to update
 * @param {*} cb - callback
 */
function updateAccessCode(type, data, cb) {
    var queryObj = { $set: { accessCode: data.accessCode } };
    switch (type) {
        case 'customer':
            customer.findOneAndUpdate(data._id, queryObj, (e, r) => {
                return cb(e, r);
            });
            break;
        case 'provider':
            provider.findOneAndUpdate(data._id, queryObj, (e, r) => {
                return cb(e, r);
            });
            break;
        case 'dispatcher':
            dispatchers.UpdateById(data._id, queryObj, (e, r) => {
                return cb(e, r);
            });
            break;
        default:
            return cb();
    }
}

/**
 * Method to validate the accessCode
 * First check for the accessCode in the redis cache if found, & is true return true
 * If accessCode is not found in the cache, search in the mongoDb, if found & is true return true
 * Cache the id & accessCode
 * @param {*} id 
 * @param {*} type 
 * @param {*} accessCode 
 * @param {*} cb 
 */
function validateAccessCode(id, type, accessCode, cb) {
    var condition = { _id: new ObjectID(id), 'accessCode': accessCode }
    if (typeof accessCode === 'undefined')
        return cb(false);
    switch (type) {
        case 'customer':
            customer.read(condition, (err, result) => {
                return err ? cb(false) : (result === null) ? cb(false) : cb(true);
            });
            break;
        case 'provider':
            provider.read(condition, (err, result) => {
                return err ? cb(false) : (result === null) ? cb(false) : cb(true);
            });
            break;
        case 'dispatcher':
            dispatchers.read(condition, (err, result) => {
                return err ? cb(false) : (result === null) ? cb(false) : cb(true);
            });
            break;

        default:
            return cb(false);
    }

}
/**
 * Method to validate the slaves' JWT
 * @param {*} decoded - decoded token
 * @param {*} cb - callback
 */
const ValidateJWT = (decoded, req, cb) => {
    validateAccessCode(decoded._id, decoded.sub, decoded.accessCode, (allow) => {
        let isValid = (decoded.key == 'acc' && allow) ? true : false; 
        return cb(null, isValid);
    });
};

/**
 * Method to validate the refresh ' JWT
 * @param {*} decoded - decoded token
 * @param {*} cb - callback
 */
const ValidateRefJWT = (decoded, req, cb) => {
    validateAccessCode(decoded._id, decoded.sub, decoded.accessCode, (allow) => {

        let isValid = (decoded.key == 'ref' && allow) ? true : false;

        return cb(null, isValid);

    });
};
const tokenError = (context) => { 
    if (context.errorType == 'TokenExpiredError' && context.attributes.key == 'acc') {
        let authToken = AUTH.SignJWT({ _id: context.attributes._id, key: 'ref', deviceId: context.attributes.deviceId }, context.attributes.sub, '60000');//sign a new JWT
        context.errorType = 477;
        context.message = context.message;
        context.refToken = authToken;
    } else {
        context.errorType = 499;
        context.message = "Your session has been expired as you have logged in some other device.";
    }
    return context;
}

const refTokenError = (context) => {

    context.errorType = 499;
    context.message = context.message;

    return context;
}

AUTH.providerJwt =
    {
        key: 'key',
        validateFunc: ValidateJWT,
        verifyOptions: { algorithms: ['HS256'] },
        errorFunc: function (context) {
            return tokenError(context);
        }
    }
AUTH.customerJwt =
    {
        key: 'key',
        validateFunc: ValidateJWT,
        verifyOptions: { algorithms: ['HS256'] },
        errorFunc: function (context) { 
            return tokenError(context);
        }
    }
AUTH.dispatcherJWT =
    {
        key: 'key',
        validateFunc: ValidateJWT,
        verifyOptions: { algorithms: ['HS256'] },
        errorFunc: function (context) {
            return tokenError(context);
        }
    }
AUTH.refreshJwt =
    {
        key: 'key',
        validateFunc: ValidateRefJWT,
        verifyOptions: { algorithms: ['HS256'] },
        errorFunc: function (context) {
            return refTokenError(context);
        }
    }


module.exports = AUTH;
