
const joi = require('joi');

const headerAuthValidator = joi.object({
    'authorization': joi.string().required()
    .description("authorization code,Eg. Key").error(new Error('authorization is missing')),
    lan: joi.string().default(1).description("Language(English-1),Eg. 1").example("1").error(new Error('lang is missing')),

}).options({ allowUnknown: true })

const headerLanValidator = joi.object({
    lan: joi.string().default(1).description("Language(English-1),Eg. 1").example("1").error(new Error('lang is missing')),
}).options({ allowUnknown: true })

module.exports = {
    headerAuthValidator,
    headerLanValidator
};