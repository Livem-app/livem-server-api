'use strict'

const common = require('./components/common');
const logger = require('./components/logger');
const server = require('./components/server');
const mongodb = require('./components/mongodb');
const stripe = require('./components/stripe');
const twilio = require('./components/twilio');
const mailgun = require('./components/mailgun');
const fcm = require('./components/fcm');
const mqtt = require('./components/mqtt');  
const rabbitMq =require('./components/rabbitMq');

module.exports = Object.assign({},rabbitMq, common, logger, server, mongodb, twilio, stripe, mailgun, fcm, mqtt)
