var email_themes ={
    'headerlogo':'https://s3.amazonaws.com/livemapplication/invoice/1q.png',
    'footerlogo':'https://s3.amazonaws.com/livemapplication/invoice/ww.png',
    'facebook':'https://s3.amazonaws.com/livemapplication/invoice/fb.png',
    'twitter':'https://s3.amazonaws.com/livemapplication/invoice/tw.png',
    'linkedin':'https://s3.amazonaws.com/livemapplication/invoice/in.png',
    'youtube':'https://s3.amazonaws.com/livemapplication/invoice/yo.png',
	'profile':'https://s3.amazonaws.com/livemapplication/invoice/profile.png',
	'cash':'https://s3.amazonaws.com/livemapplication/invoice/cash.png',
	'provideinvoicelogo':'https://s3.amazonaws.com/livemapplication/invoice/total.png',
}