'use strict';
const smsLog = require('../../models/smsLog');

module.exports = [
{
    method: 'POST',
    path: '/webhooks/twilio',
    config: {// "tags" enable swagger to document API
        tags: ['api', 'test'],
        description: 'This API is to validate the phone number at the time of sign up, if this particular phone number is already registered or not',
        notes: "verify phone number API Error code \n\n 500 - Internal server error, \n\n 400 - Phone number already registered, \n\n 200 - Success", // We use Joi plugin to validate request

    },
    handler: function (req, reply) {
        var condition = {
            query: {msgId: req.payload.SmsSid},
            data: {
                $set: {
                    status: req.payload.SmsStatus,
                    ErrorCode: req.payload.ErrorCode
                }
            }
        };
        smsLog.updateByMsgId(condition, (err, result) => {
        });//update message status by webhook
        return reply(1).code(200);
        
    }
}
];