'use strict'

const joi = require('joi')
let moment = require('moment');//date-time
const config = require('../../config')
const smsLog = require('../../models/smsLog')
const configuration = require('../../configuration');
const client = require('twilio')(
    config.twilio.TWILIO_ACCOUNT_SID,
    config.twilio.TWILIO_AUTH_TOKEN
);
const envVarsSchema = joi.object({
    to: joi.string().required(),
    body: joi.string().required(),
    from: joi.string().default(config.twilio.CELL_PHONE_NUMBER),
    statusCallback: configuration.API_URL + '/webhooks/twilio'//twillo call back in this api
}).unknown().required()

/**
 * sendSms this method  used to send message usign twilio 
 * store messgae log  in database
 * @param {Object} params - the params like:from,to,body and statusCallback
 * @param {Object} callback - sucesss either error 
 * @returns {Object} object - return messgae body or error.
 */
function sendSms(params, callback) {
    try {
        params.from = config.twilio.CELL_PHONE_NUMBER;
        params.statusCallback = configuration.API_URL + '/webhooks/twilio';
        console.log('parans',params)
        const twilioConf = joi.attempt(params, envVarsSchema)//Joi validation
        client.messages.create(twilioConf, function (err, message) {
            if (typeof message != 'undefined') {
                let userdata = {
                    msgId: message.sid,
                    createDate: moment().unix(),
                    status: message.status,
                    trigger: params.trigger,
                    msg: message.body,
                    to: message.to
                };
                smsLog.Insert(userdata, (err, response) => {
                });//insert  log in database
                callback(null, message);
            }
            else {
                let userdata = {
                    msgId: '',
                    createDate: moment().unix(),
                    status: 'failed',
                    trigger: params.trigger,
                    msg: params.body,
                    to: params.to
                };
                smsLog.Insert(userdata, (err, response) => {
                });//insert  log in database
                callback(err);
            }
        });
    } catch (e) {
        callback(e);
    }

}

module.exports = { sendSms };