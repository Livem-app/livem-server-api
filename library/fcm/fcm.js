const FCM = require('fcm-push');
const config = require('../../config');
const fcm = new FCM(config.fcm.FCM_SERVER_KEY);

const notifyFcmTpic = (request, callback) => {
    let payload;
    let resData = {
        action: request.action || 1,
        pushType: request.pushType || 1,
        title: request.title || '',
        msg: request.msg || '',
        data: request.data || []
    }
    if (request.deviceType == 1) {
        payload = {
            to: '/topics/' + request.fcmTopic, // required fill with device token or topics
            collapse_key: 'your_collapse_key',
            priority: 'high',
            "delay_while_idle": true,
            "dry_run": false,
            "time_to_live": 3600,
            "content_available": true,
            badge: "1",
            data: resData

        };
        payload.notification = {
            title: request.title,
            body: request.msg,
            sound: "default"
        };
    } else if (request.deviceType == 2) {
        payload = {
            to: '/topics/' + request.fcmTopic, // required fill with device token or topics
            collapse_key: 'your_collapse_key',
            priority: 'high',
            "delay_while_idle": true,
            "dry_run": false,
            "time_to_live": 3600,
            "content_available": true,
            badge: "1",
            data: resData

        };
    } else {
        return callback(false);
    }

    //promise style
    fcm.send(payload)
        .then(function (response) { 
            return callback(true);
        })
        .catch(function (err) { 
            return callback(false);
        });
}


const notifyForTimeOut = (request, callback) => {
 
    let payload;
    var resData = {
        action: request.action || 1,
        pushType: request.pushType || 1,
        title: request.title || '',
        msg: request.msg || '',
        data: request.data || []
    }
    if (request.deviceType == 1) {
        payload = {
            to: '/topics/' + request.fcmTopic, // required fill with device token or topics    
            "mutable_content": true,
            "data": {
                a: 506,
                b: 'hello'
            },
            "notification":
            {
                "body": " ",
                "title": "",
                "sound": "",
                // "a": "506"       
            }
        };

    } else {
        payload = {
            to: '/topics/' + request.fcmTopic, // required fill with device token or topics
            collapse_key: 'your_collapse_key',
            priority: 'high',
            "delay_while_idle": true,
            "dry_run": false,
            "time_to_live": 3600,
            "content_available": true,
            badge: "1",
            data: resData

        };
    }
    //promise style
    fcm.send(payload)
        .then(function (response) { 
            return callback(true);
        })
        .catch(function (err) { 
            return callback(false);
        });
}


 
module.exports = {
    notifyFcmTpic,
    notifyForTimeOut, 
}