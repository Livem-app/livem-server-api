'use strict';
const emailLog = require('../../models/emailLog')
// Mailgun webhook api 
module.exports = [
    {
        method: 'POST',
        path: '/webhooks/mailgun',
        config: {// "tags" enable swagger to document API
            tags: ['api', 'test'],
            description: 'This API is to validate the phone number at the time of sign up, if this particular phone number is already registered or not',
            notes: "verify phone number API Error code \n\n 500 - Internal server error, \n\n 400 - Phone number already registered, \n\n 200 - Success", // We use Joi plugin to validate request

        },
        handler: function (req, reply) { 
            if (req.payload) {
                let data = {
                    msgId: req.payload['Message-Id'],
                    event: req.payload.event
                }
                emailLog.updateByMsgId(data, (err, result) => {
                });//update mail status in db
            }
            reply(1);
        }
    }
];