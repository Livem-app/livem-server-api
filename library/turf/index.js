'use strict'

const Joi = require('joi');
const logger = require('winston');
const moment = require('moment');
const async = require('async');
var jsonfile = require('jsonfile');
var turf = require('turf');

const cities = require('../../models/cities');
const redis = require('../../models/redis');
const subscriberTurf = redis.subscriber;
subscriberTurf.psubscribe('__key*__:*')
subscriberTurf.on("pmessage", function (pattern, channel, message) {
    if (channel == '__keyevent@0__:expired') {
        redisEventListner(pattern, channel, message);
    }
});

var file = 'cityZone.json';
let cityZone = [];

exports.readCityZone = (callback) => {
    jsonfile.readFile(file, function (err, obj) {
        if (err)
            return callback(err);
        if (typeof obj != 'undefined') {
            cityZone = obj;
        }
        logger.info(`Turf cityZone DONE -------------------------`);
        return callback();
    });
}

function updateCityZone() {
    cities.readAll((err, citiesRes) => {
        jsonfile.writeFile(file, citiesRes, function (err, obj) {
            if (err)
                logger.error("write file Error : ", err);
            else {
                logger.log(`cityZone file modified Successfully-------------------------`);
                jsonfile.readFile(file, function (err, obj) {
                    if (err)
                        logger.error(`cityZone file error -------------------------`);
                    if (typeof obj != 'undefined') {
                        cityZone = obj;
                        logger.log(`cityZone file read Successfully-------------------------`);
                    }
                });
            }
        });
    });
}


exports.isWithinPolygons = (latitude, longitude, callback) => {
    let point = {
        "type": "Feature",
        "id": " dsfasfasfsaf",
        "geometry": {
            "type": "Point",
            "coordinates": [longitude, latitude]
        }
    };
    let dataObj = null;
    let polygon = {
        "type": "Feature",
        "geometry": {
            "type": "Polygon",
            "coordinates": []
        }
    };
    for (var key = 0; key < cityZone.length; key++) {
        polygon.geometry = cityZone[key].polygons;
        if (turf.inside(point, polygon)) {
            dataObj = cityZone[key];
            break;
        }
    }
    return callback(null, dataObj);
}

function redisEventListner(pattern, channel, message) {
    switch (channel) {
        case '__keyevent@0__:expired':
            switch (message) {
                case 'citiesZone':
                    updateCityZone();
                    break;
            }
            break;
    }
}