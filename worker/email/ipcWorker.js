const logger = require('winston');
const ipc = require('node-ipc');

ipc.config.id = 'normalworker';
ipc.config.retry = 1000;
ipc.config.silent = true;

const sendMail = require('../../library/mailgun');

logger.debug("start time" + new Date());
logger.debug('Process Started.......' + process.pid + " ");


ipc.connectTo('rabbitmqserver', () => {
    ipc.of.rabbitmqserver.on('connect', () => {
        ipc.of.rabbitmqserver.emit('process.connected', {});
        logger.debug("process.connected");
    });


    ipc.of.rabbitmqserver.on('process.start', (data) => {
        logger.debug("process.start");

        let param = {
            to: data.email,
            subject: data.subject,
            html: data.body,
            trigger:data.trigger
        }
        
        sendMail.sendMail(data, (err, res) => { });

        ipc.of.rabbitmqserver.emit('Ep1CompletedExecution', { "data": "done" });
    });

    ipc.of.rabbitmqserver.on('Ep1shutdown', (data) => {
       logger.debug('Got shutdown command, exiting...' + process.pid);

        process.exit();
    })
});



