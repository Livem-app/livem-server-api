'use strict'

let worker = require('./worker');
let ipcWorker = require('./ipcWorker');

module.exports = [].concat(worker, ipcWorker);