const logger = require('winston'); 
const rabbitMq = require('../../models/rabbitMq');
const sendMail = require('../../library/mailgun');
const emailLog = require('../../models/emailLog');
const db = require('../../models/mongodb');


rabbitMq.connect(() => {
    db.connect(() => { 
        prepareConsumer(rabbitMq.getChannelEmail(), rabbitMq.queueEmail, rabbitMq.get());
    });//create a connection to mongodb
});

function prepareConsumer(channelEmail, queueEmail, amqpConn) {
    channelEmail.assertQueue(queueEmail, { durable: false }
        , function (err, queue) {
            if (queue.messageCount > 0) {
                channelEmail.consume(queueEmail, function (msg) {

                    var data = JSON.parse(msg.content.toString());

                    sendMail.sendMail(data, (err, res) => {
                        if (res) {
                            emailLog.Insert(res, (err, response) => {
                            });
                        } 
                    });
                }, { noAck: true }, function (err, ok) {
                });
            }
            else {
            }
        });
}
