const logger = require('winston');
const ipc = require('node-ipc');
const rabbitMq = require('../../models/rabbitMq');
const transcation = require('../../web/commonModels/wallet/transcation');
const db = require('../../models/mongodb');
//create a connection to mongodb



logger.debug('step 4: child Process Started.......' + process.pid + " ");

rabbitMq.connect(() => {
    db.connect(() => {
        prepareConsumer(rabbitMq.getChannelWallet(), rabbitMq.queueWallet, rabbitMq.get());
    });

});

function prepareConsumer(channelWallet, queueWallet, amqpConn) {
    console.log("data.transcationType")
    channelWallet.assertQueue(queueWallet, { durable: false }
        , function (err, queue) {

            channelWallet.consume(queueWallet, function (msg) {


                var data = JSON.parse(msg.content.toString());
                console.log("data.transcationType", data.transcationType)
                if (data.transcationType == 1) {
                    transcation.cashTransction(data, (err, res) => {
                    })
                } else if (data.transcationType == 2) {
                    transcation.cardTransction(data, (err, res) => {
                    });
                } else if (data.transcationType == 3) {
                    transcation.bankPayout(data, (err, res) => {
                    });
                } else {
                    console.log("worker transction type wrong");
                }
                // wallet.walletTransction(data, (err, res) => {
                //     console.log('result', res);
                // });


                //   console.log("consume end"+new Date());
            }, { noAck: true }, function (err, ok) {



            });

        });

}

