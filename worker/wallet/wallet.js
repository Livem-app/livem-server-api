
'use strict';

const Joi = require('joi');
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;
var moment = require('moment');
var Async = require("async");
const Timestamp = require('mongodb').Timestamp;
const wallet = require('../../models/wallet');
const provider = require('../../models/provider');
const appConfig = require('../../models/appConfig');
const fcm = require('../../library/fcm')

/**
 * Method to update the users document with the wallet status
 * @param {*} params - userType, userId, amount
 */
const UpdateWalletStatus = (params, callback) => {

    var collection = '';
    var limits = ['x', 'y'];
    switch (parseInt(params.userType)) {
        case 2:
            collection = 'provider';
            limits = ['softLimitDriver', 'hardLimitDriver'];
            break;

        case 1:
            collection = 'slaves';
            limits = ['softLimitShipper', 'hardLimitShipper'];
            break;

        case 3:
            collection = 'operators';
            limits = ['softLimitOperator', 'hardLimitOperator'];
            break;

        default:
            return callback({ errMsg: 'Invalid userType', errNum: 400, errFlag: 1 });
    }

    let querObj = { $set: { walletAmount: params.amount } }

    params.collection = collection;
    params.limits = limits;
    provider.findOneAndUpdate(params.userId, querObj, (err, result) => {
        if (err)
            return callback(err);

        checkIfLimitHit(params, () => {
        });

        // if (parseInt(params.userType) === 1) {
        //     // Master.updatePayroll({ id: params.userId }, (err, status) => { });
        //     PayrollMethod.updatePayrollData({ id: params.userId }, (err, status) => { });
        // }
        // else if (parseInt(params.userType) === 2) {
        //     console.log('here in update customer rating');
        //     updateCustomerRating({ id: params.userId }).then(data => { return });//update the slave rating
        // }
        // else {
        //     ;//do nothing
        // }

        return callback(null, 'done');
    });
}


/**
 * Method to check if the soft or hardlimit is hit
 * @param {*} params - userId, type
 * @param {*} callback
 */
function checkIfLimitHit(params, callback) {

    let softLimitHit = false;
    let hardLimitHit = false;

    var pubnubChn = "";
    var userType = "";
    var statementData = params.walletData;
    let userData = "";

    let notifyUser = false;
    let deviceType = "";

    let userSoftLimit = 0;
    let userHardLimit = 0;

    params.amount = parseFloat(params.amount).toFixed(2);
    params.walletData.closingBal = parseFloat(params.walletData.closingBal).toFixed(2);
    params.walletData.openingBal = parseFloat(params.walletData.openingBal).toFixed(2);
    console.log("limit check")
    Async.waterfall([
        function (cb) {
            appConfig.readOne({ walletSettings: { $exists: true } }, (err, config) => {
                if (err)
                    return cb(err);

                if (config) {
                    softLimitHit = (parseFloat(params.amount) >= (parseFloat(config.walletSettings[params.limits[0]] || 0))) ? true : false;
                    hardLimitHit = (parseFloat(params.amount) >= (parseFloat(config.walletSettings[params.limits[1]] || 0))) ? true : false;

                    userSoftLimit = parseFloat(config.walletSettings[params.limits[0]] || 0);
                    userHardLimit = parseFloat(config.walletSettings[params.limits[1]] || 0);
                }

                provider.getProviderById(params.userId, (err, doc) => {
                    if (err)
                        return cb(err);

                    if (doc === null)
                        return callback({ errNum: 400, errMsg: 'not found', errFlag: 1 });

                    userData = doc;

                    if (doc.setCreditEnable)
                        notifyUser = true;//notify the user only if the credit line is enabled

                    // switch (parseInt(params.userType)) {
                    //     case 1:
                    //         pubnubChn = (typeof userData.listner != 'undefined') ? userData.listner : "";
                    //         userType = 1;
                    //         deviceType = userData.deviceType || 1;
                    //         break;
                    //     case 2:
                    //         pubnubChn = (typeof userData.deviceId != 'undefined') ? "slave_" + userData.deviceId : "";
                    //         userType = 2;
                    //         deviceType = userData.deviceType || 1;
                    //         break;
                    // }

                    // if (notifyUser && typeof params.isWalletConfigrationChanged == 'undefined') {
                    //     var message = { a: 49, 'msg': "your final wallet amount is " + params.amount, walletData: params.walletData };
                    //     if (!(typeof userData.pushTopic === 'undefined' && userData.pushTopic === '' && userData.pushTopic === null))
                    //         walletPushAndPubnub(pubnubChn, message, userType, deviceType, userData.pushToken, () => {
                    //         });
                    // }

                    if ((doc != null) && (typeof doc.walletSoftLimit != 'undefined') && (typeof doc.walletHardLimit != 'undefined')) {
                        softLimitHit = (parseFloat(params.amount) >= (parseFloat(doc.walletSoftLimit))) ? true : false;
                        hardLimitHit = (parseFloat(params.amount) >= (parseFloat(doc.walletHardLimit))) ? true : false;

                        userSoftLimit = parseFloat(doc.walletSoftLimit);
                        userHardLimit = parseFloat(doc.walletHardLimit);
                    }
                    else {
                        softLimitHit = false;
                        hardLimitHit = true;
                        userSoftLimit = 0;
                        userHardLimit = 0;
                    }//if their is no value defined for limits use these as defaults

                    return cb(null, 'done');
                });
            })
        }
    ], (err, result) => {
        if (err)
            return callback(err);
        params.status = 0;
        if (softLimitHit) {
            console.log("limit check softLimitHit")
            params.status = 1;
            provider.findOneAndUpdate(params.userId, {
                $set: { walletSoftLimitHit: true, walletHardLimitHit: false }
            }, (err, result) => {
                softLimit(params, () => {
                });

            });
            provider.getProviderById(params.userId, (err, res) => {
                if (notifyUser) {
                    console.log("send push for soft limit hit")
                    sendPush('You have reached the Soft Limit', 'You have reached the Soft Limit', res, { walletBalance: params.amount })
                }
            });

            // if (notifyUser) {
            //     var message = { a: 45, 'msg': "You have reached the Soft Limit", walletBalance: params.amount };
            //     walletPushAndPubnub(pubnubChn, message, userType, deviceType, userData.pushToken, () => {
            //     });
            //     var keysToReplace = {
            //         'softLimit': userSoftLimit,
            //         'walletBalance': params.amount,
            //         'currency': statementData.currency,
            //         'username': userData.name
            //     }
            //     walletEmail('walletSoftLimit.html', userData.email, 'Credit Soft Limit reached', keysToReplace, () => {
            //     });
            // }//notify user only if the wallet is enabled
        }

        if (hardLimitHit) {
            console.log("hrad lini thit")
            params.status = 1;
            provider.findOneAndUpdate(params.userId, {
                $set: { walletHardLimitHit: true, walletSoftLimitHit: false }
            }, (err, result) => {
                hardLimit(params, () => {
                });

            });
            provider.getProviderById(params.userId, (err, res) => {
                if (notifyUser) {
                    sendPush('You have reached the Hard Limit', 'You have reached the Hard Limit', res, { walletBalance: params.amount })
                }
            });

            // if (notifyUser) {
            //     var message = { a: 46, 'msg': "You have reached the Hard Limit", walletBalance: params.amount };
            //     walletPushAndPubnub(pubnubChn, message, userType, deviceType, userData.pushToken, () => {
            //     });
            //     var keysToReplace = {
            //         'hardLimit': userHardLimit,
            //         'walletBalance': params.amount,
            //         'currency': statementData.currency,
            //         'username': userData.name
            //     }
            //     walletEmail('walletHardLimit.html', userData.email, 'Credit Hard Limit reached', keysToReplace, () => {
            //     });
            // }
        }

        if (softLimitHit == false && (typeof userData.walletSoftLimitHit != 'undefined') && userData.walletSoftLimitHit == true) {
            provider.findOneAndUpdate(params.userId, {
                $set: { walletSoftLimitHit: false, walletHardLimitHit: false }
            }, (err, res) => {
                softLimit(params, () => {
                });
            })

        }
        if (hardLimitHit == false && (typeof userData.walletHardLimitHit != 'undefined') && userData.walletHardLimitHit == true) {
            provider.findOneAndUpdate(params.userId, {
                $set: { walletHardLimitHit: false, walletSoftLimitHit: false }
            }, (err, res) => {
                hardLimit(params, () => {
                });
            })
        }
        return callback(null, 'done');
    });
}
// const status = require('./walletStatusMessage');
/* 
 *  
 Params = {
 userId,                User Id for User(master,slave,operator)     REQUIRED
 trigger,               ADMIN/TRIP/PROMO/REFREL                     REQUIRED
 comment,               description of transction                   REQUIRED
 currency,              currency                                    REQUIRED
 txnType,               transction type(1-Credit, 2-Debit)          REQUIRED
 amount,                transction amount                           REQUIRED
 paymentType,           CARD/CASH/WALLET                            REQUIRED
 tripId,                Booking ID                                  OPTIONAL
 paymentTxtId,          PAYMENT GATEWAY TRANSCTION ID               OPTIONAL
 initiatedBy            Transction done by(ADMIN NAME)              OPTIONAL
 bookingType            Booking Type(RIDE/DELIVERY/SERVICE)              OPTIONAL
 userType               USER TYPE(1-MASTER, 2-SLAVE, 3-OPERATOR , 4-APP, 5-PG)              REQUIRED
 cashCollected          cashCollected                               OPTIONAL
 }
 */

module.exports.walletTransction = function (params, callback) {
    console.log("wallet", params)
    params.paymentTxtId = (typeof params.paymentTxtId == "undefined" || params.paymentTxtId == "") ? "N/A" : params.paymentTxtId;
    params.initiatedBy = (typeof params.initiatedBy == "undefined" || params.initiatedBy == "") ? "N/A" : params.initiatedBy;
    params.tripId = (typeof params.tripId == "undefined" || params.tripId == "") ? "N/A" : params.tripId;
    params.bookingType = (typeof params.bookingType == "undefined" || params.bookingType == "") ? "N/A" : params.bookingType;
    params.cashCollected = (typeof params.cashCollected == "undefined" || params.cashCollected == "") ? "0" : params.cashCollected;
    var userCollection = "";
    var walletCollection = "";

    switch (parseInt(params.userType)) {
        case 1:
            walletCollection = "walletCustomer";
            userCollection = "walletCustomerUser";
            break;
        case 2:
            walletCollection = "walletProvider";
            userCollection = "walletProviderUser";
            break;
        case 3:
            walletCollection = "walletOperator";
            userCollection = "walletOperatorUser";
            break;
        case 4:
            walletCollection = "walletApp";
            userCollection = "";
            break;
        case 5:
            walletCollection = "walletPg";
            userCollection = "";
            break;
        default:
            walletCollection = "";
            userCollection = "";
            break;
    }
    if (walletCollection == "")
        callback("Invalid User Type");

    var txnId = walletCollection.substring(0, 3).toUpperCase() + "-" + moment().unix() + "-" + (Math.random() * (Math.floor(999) - Math.ceil(111)) + 111);
    var userId = params.userId;
    var txnType = "";

    switch (parseInt(params.txnType)) {
        case 1:
            txnType = "CREDIT";
            params.amount = Math.abs(parseFloat(params.amount));
            break;
        case 2:
            txnType = "DEBIT";
            params.amount = Math.abs(parseFloat(params.amount)) * -1;
            break;
        case 3:
            txnType = "DEBIT FOR COLLECTION";
            params.amount = Math.abs(parseFloat(params.amount)) * -1;
            break;
        default:
            callback("Invalid Transaction Type");
            break;
    }

    wallet.SelectWIthLimitSortSkip(walletCollection, { 'userId': userId }, { '_id': -1 }, 1, 0, function (err, waletData) {
        var closingBal = 0;
        if (err) {
            callback(err);
        } else {
            if (waletData.length === 0)
                closingBal = 0;
            else
                closingBal = waletData[0].closingBal || 0;
            var final_closingBal = parseFloat(closingBal) + parseFloat(params.amount);
            if ((params.paymentType == "CARD" && params.trigger == "TRIP" && params.userType == 1) ||
                (params.paymentType == "CASH" && params.trigger == "TRIP" && params.userType == 1))
                final_closingBal = closingBal;
            var walletData = {
                txnId: txnId,
                userId: userId,
                txnType: txnType,
                trigger: params.trigger,
                comment: params.comment,
                currency: params.currency || 'USD',
                currencySymbol: params.currencySymbol || '$',
                openingBal: parseFloat((closingBal || 0).toFixed(2)),
                amount: parseFloat((params.amount || 0).toFixed(2)),
                closingBal: parseFloat((final_closingBal || 0).toFixed(2)),

                cashCollected: parseFloat(params.cashCollected),
                paymentType: params.paymentType,
                timestamp: parseInt(moment().unix()),
                transctionTime: new Timestamp(1, moment().unix()),
                transctionDate: new Date(),
                tripId: params.tripId,
                bookingType: params.bookingType,
                paymentTxnId: params.paymentTxtId,
                intiatedBy: params.initiatedBy
            }
            wallet.Insert(walletCollection, walletData, function (err, result) {
                if (err)
                    callback(err);
                else {
                    if (userCollection != "") {

                        wallet.FINDONEANDUPDATE(
                            userCollection,
                            {
                                query: { 'userId': userId },
                                data: { $set: { "wallet_balance": parseFloat((final_closingBal || 0).toFixed(2)) } },
                                options: { upsert: true }
                            },
                            function (err, userCollectionData) {
                                if (err)
                                    callback(err);
                            }); //create a new document if it does not exists & update the balance
                        if (params.userType == 2) {
                            UpdateWalletStatus({
                                userType: params.userType,
                                amount: parseFloat((final_closingBal || 0).toFixed(2)),
                                walletData: walletData,
                                userId: userId
                            }, (err, result) => {
                                if (err)
                                    console.log(err);
                            }); //notify a function in transaction.js to update the wallet balance
                        }


                        wallet.SelectWIthLimitSortSkip(walletCollection, {}, { '_id': -1 }, 1, 0, function (err, waletData) {
                            var walletId = "";
                            if (err) {
                                callback(err);
                            } else {
                                if (waletData.length > 0) {
                                    walletId = waletData[0]._id.toString();
                                    wallet.UpdatePush(
                                        userCollection,
                                        { 'userId': userId },
                                        { "txn": walletId },
                                        function (err, userCollectionData) {
                                            if (err)
                                                callback(err);
                                        });
                                }
                            }
                        });
                    }
                    callback(null, txnId);
                }
            });
        }
    });
}

module.exports.authorizeAmount = function (params, callback) {
    var wallet_balance = 0;
    wallet.SelectOne('walletSlaveUser', { 'userId': params.userId }, function (err, userData) {
        if (err) {
            callback(err);
        } else {
            if (userData) {
                var amtWithHardLimit = userData.wallet_balance + params.hardLimit || 0;
                if (typeof userData.wallet_balance != "undefined")
                    wallet_balance = userData.wallet_balance;
                if (params.amount > amtWithHardLimit) {
                    var data = {
                        wallet_balance: wallet_balance,
                        errorFlag: 1,
                        chargeId: ""
                    };
                    callback(null, data);
                } else {
                    var finalClosingBalance = parseFloat(wallet_balance) - parseFloat(params.amount);
                    wallet.Update(
                        'walletSlaveUser',
                        { 'userId': params.userId },
                        { "wallet_balance": parseFloat(finalClosingBalance) },
                        function (err, userCollectionData) {
                            if (err)
                                callback(err);
                            var walletChargesData = {
                                userId: params.userId,
                                amount: params.amount,
                                bid: params.bid || 'N/A',
                                captured: params.capture || false,
                                refund: false,
                            };
                            wallet.Insert('walletCharges', walletChargesData, function (err, result) {
                                if (err)
                                    callback(err);
                                else {
                                    wallet.SelectWIthLimitSortSkip('walletCharges', {}, { '_id': -1 }, 1, 0, function (err, walletChargesData) {
                                        if (err) {
                                            callback(err);
                                        } else {
                                            var data = {
                                                wallet_balance: wallet_balance,
                                                errorFlag: 0,
                                                chargeId: walletChargesData[0]._id.toString(),
                                            };
                                            callback(null, data);
                                        }
                                    });
                                }
                            });
                        });
                }
            } else {
                var data = {
                    wallet_balance: wallet_balance,
                    errorFlag: 1,
                    chargeId: ""
                };
                callback(null, data);
            }
        }
    });
}



module.exports.captureAmount = function (params, callback) {
    wallet.SelectOne('walletCharges', { '_id': new ObjectID(params.chargeId) }, function (err, walletChargesData) {
        if (err) {
            callback(err);
        } else {
            if (walletChargesData) {
                wallet.Update(
                    'walletCharges',
                    { '_id': new ObjectID(params.chargeId) },
                    { "captured": true },
                    function (err, userCollectionData) {
                        if (err)
                            callback(err);
                        var data = {
                            errorFlag: 0,
                            chargeId: params.chargeId
                        };
                        callback(null, data);
                    });
            } else {
                var data = {
                    errorFlag: 1,
                    chargeId: params.chargeId
                };
                callback(null, data);
            }
        }
    });
}



module.exports.refundAmount = function (params, callback) {
    wallet.SelectOne('walletCharges', { '_id': new ObjectID(params.chargeId) }, function (err, walletChargesData) {
        if (err) {
            callback(err);
        } else {
            if (walletChargesData) {
                wallet.Update(
                    'walletCharges',
                    { '_id': params.chargeId },
                    { "refund": true },
                    function (err, userCollectionData) {
                        if (err)
                            callback(err);
                        wallet.SelectOne('walletSlaveUser', { 'userId': walletChargesData.userId }, function (err, userData) {
                            if (err) {
                                callback(err);
                            } else {
                                if (userData) {
                                    var finalClosingBalance = userData.wallet_balance + walletChargesData.amount;
                                    wallet.Update(
                                        'walletSlaveUser',
                                        { 'userId': walletChargesData.userId },
                                        { "wallet_balance": parseFloat(finalClosingBalance) },
                                        function (err, userCollectionData) {
                                            if (err)
                                                callback(err);
                                            var data = {
                                                errorFlag: 0,
                                                wallet_balance: finalClosingBalance
                                            };
                                            callback(null, data);
                                        });
                                } else {
                                    var data = {
                                        errorFlag: 1
                                    };
                                    callback(null, data);
                                }
                            }
                        });
                    });
            } else {
                var data = {
                    errorFlag: 1,
                    chargeId: params.chargeId
                };
                callback(null, data);
            }
        }
    });
}

/**
 * Method to get the charge details
 * @param {*} bid - booking, userId
 */
module.exports.getChargeDetails = (params, callback) => {

    wallet.SelectOne('walletCharges', { 'bid': parseInt(params.bid) }, (err, charge) => {
        if (err) return callback(err);

        return callback(err, charge);
    });
}


/*  
 Params = {
 userId,                User Id for User(master,slave,operator)     REQUIRED
 userType               USER TYPE(1-MASTER, 2-SLAVE, 3-OPERATOR , 4-APP, 5-PG)              REQUIRED
 }
 */
module.exports.userCreate = function (params, callback) {
    var userCollection = "";
    switch (parseInt(params.userType)) {
        case 1:
            userCollection = "walletProviderUser";
            break;
        case 2:
            userCollection = "walletCustomerUser";
            break;
        case 3:
            userCollection = "walletOperatorUser";
            break;
        default:
            callback("Invalid User Type");
            break;
    }
    var userData = {
        userId: params.userId,
        softLimitStatus: 0,
        hardLimitStatus: 0,
        wallet_balance: 0
    }
    wallet.Insert(userCollection, userData, function (err, result) {
        if (err)
            callback(err);
        else {
            wallet.SelectWIthLimitSortSkip(userCollection, {}, { '_id': -1 }, 1, 0, function (err, waletData) {
                var walletId = "";
                if (err) {
                    callback(err);
                } else {
                    if (waletData.length > 0)
                        walletId = waletData[0]._id.toString();
                    callback(null, walletId);
                }
            });
        }
    });
}

/* 
 Params = {
 userId,                User Id for User(master,slave,operator)     REQUIRED
 userType               USER TYPE(1-MASTER, 2-SLAVE, 3-OPERATOR , 4-APP, 5-PG)              REQUIRED
 }
 */
module.exports.userDelete = function (params, callback) {
    var userCollection = "";
    switch (parseInt(params.userType)) {
        case 1:
            userCollection = "walletMasterUser";
            break;
        case 2:
            userCollection = "walletSlaveUser";
            break;
        case 3:
            userCollection = "walletOperatorUser";
            break;
        default:
            callback("Invalid User Type");
            break;
    }
    wallet.Delete(userCollection, { userId: params.userId }, function (err, result) {
        if (err)
            callback(err);
        else {
            callback(null, params.userId);
        }
    });
}

/* 
 *  
 Params = {
 userId,                User Id for User(master,slave,operator)     REQUIRED
 userType               USER TYPE(1-MASTER, 2-SLAVE, 3-OPERATOR , 4-APP, 5-PG)              REQUIRED
 }
 */
module.exports.accountBalance = function (params, callback) {
    var userCollection = "";
    switch (parseInt(params.userType)) {
        case 1:
            userCollection = "walletMasterUser";
            break;
        case 2:
            userCollection = "walletSlaveUser";
            break;
        case 3:
            userCollection = "walletOperatorUser";
            break;
        default:
            userCollection = "";
            callback("Invalid User Id");
            break;
    }

    wallet.SelectOne(userCollection, { 'userId': params.userId }, function (err, waletData) {
        if (err) {
            callback(err);
        } else {
            if (waletData) {
                waletData.wallet_balance = (typeof waletData.wallet_balance == "undefined" || waletData.wallet_balance == "") ? 0 : waletData.wallet_balance;
                callback(null, waletData.wallet_balance);
            } else
                callback(err);
        }
    });
}


/*  
 Params = {
 userId,                User Id for User(master,slave,operator)     REQUIRED
 userType               USER TYPE(1-MASTER, 2-SLAVE, 3-OPERATOR)              REQUIRED
 status                 soft limit status(1-SET SOFT LIMIT, 0-UNSET SOFT LIMIT)              REQUIRED
 }
 */
const softLimit = function (params, callback) {
    var userCollection = "";
    switch (parseInt(params.userType)) {
        case 1:
            userCollection = "walletCustomerUser";
            break;
        case 2:
            userCollection = "walletProviderUser";
            break;
        case 3:
            userCollection = "walletOperatorUser";
            break;
        default:
            callback("Invalid User Type");
            break;
    }
    wallet.Update(
        userCollection,
        { 'userId': params.userId },
        { "softLimitStatus": parseInt(params.status) },
        function (err, userCollectionData) {
            if (err)
                callback(err);
            callback(null, params.userId);
        });
}

/*  
 Params = {
 userId,                User Id for User(master,slave,operator)     REQUIRED
 userType               USER TYPE(1-MASTER, 2-SLAVE, 3-OPERATOR)              REQUIRED
 status                 hard limit status(1-SET SOFT LIMIT, 0-UNSET SOFT LIMIT)              REQUIRED
 }
 */
const hardLimit = function (params, callback) {
    var userCollection = "";
    switch (parseInt(params.userType)) {
        case 1:
            userCollection = "walletCustomerUser";
            break;
        case 2:
            userCollection = "walletProviderUser";
            break;
        case 3:
            userCollection = "walletOperatorUser";
            break;
        default:
            callback("Invalid User Type");
            break;
    }
    wallet.Update(
        userCollection,
        { 'userId': params.userId },
        { "hardLimitStatus": parseInt(params.status) },
        function (err, userCollectionData) {
            if (err)
                callback(err);
            callback(null, params.userId);
        });
}


/*  
 Params = {
 userId,                User Id for User(master,slave,operator)     REQUIRED
 userType               USER TYPE(1-MASTER, 2-SLAVE, 3-OPERATOR)    REQUIRED
 pageIndex              page index 0 by default                     REQUIRED
 }
 */
module.exports.transction = function (params, callback) {

    var walletCollection = "";
    switch (parseInt(params.userType)) {
        case 1:
            walletCollection = "walletMaster";
            break;
        case 2:
            walletCollection = "walletSlave";
            break;
        case 3:
            walletCollection = "walletOperator";
            break;
        default:
            walletCollection = "";
            break;
    }
    if (walletCollection == "")
        return callback("invalid User Type");
    var limit = 10;
    var skip = 10 * params.pageIndex;
    var debitCond = { 'txnType': 'DEBIT', 'userId': params.userId };
    var creditCond = { 'txnType': 'CREDIT', 'userId': params.userId };
    var creditDebitCond = { 'userId': params.userId };
    var txnType = "";
    async.series([
        function (callback) {
            wallet.SelectWIthLimitSortSkip(walletCollection, debitCond, { '_id': -1 }, limit, skip, function (err, transction) {
                if (err) {
                    callback(err);
                } else {
                    var debitArr = [];
                    for (var key = 0; key < transction.length; key++) {
                        txnType = transction[key].txnType;
                        var tra = {
                            'txnId': transction[key].txnId,
                            'trigger': transction[key].trigger,
                            'txnType': txnType,
                            'comment': transction[key].comment,
                            'currency': transction[key].currency,
                            'openingBal': transction[key].openingBal,
                            'amount': transction[key].amount,
                            'closingBal': transction[key].closingBal,
                            'paymentType': transction[key].paymentType,
                            'timestamp': transction[key].timestamp,
                            'paymentTxnId': transction[key].paymentTxnId,
                            'intiatedBy': transction[key].intiatedBy,
                            'tripId': transction[key].tripId || ''
                        }
                        debitArr.push(tra);
                    }
                    callback(null, debitArr);
                }
            });
        },
        function (callback) {
            wallet.SelectWIthLimitSortSkip(walletCollection, creditCond, { '_id': -1 }, limit, skip, function (err, transction) {
                if (err) {
                    callback(err);
                } else {
                    var creditArr = [];
                    for (var key = 0; key < transction.length; key++) {
                        txnType = transction[key].txnType;
                        var tra = {
                            'txnId': transction[key].txnId,
                            'trigger': transction[key].trigger,
                            'txnType': txnType,
                            'comment': transction[key].comment,
                            'currency': transction[key].currency,
                            'openingBal': transction[key].openingBal,
                            'amount': transction[key].amount,
                            'closingBal': transction[key].closingBal,
                            'paymentType': transction[key].paymentType,
                            'timestamp': transction[key].timestamp,
                            'paymentTxnId': transction[key].paymentTxnId,
                            'intiatedBy': transction[key].intiatedBy,
                            'tripId': transction[key].tripId || ''
                        }
                        creditArr.push(tra);
                    }
                    callback(null, creditArr);
                }
            });
        },
        function (callback) {
            wallet.SelectWIthLimitSortSkip(walletCollection, creditDebitCond, { '_id': -1 }, limit, skip, function (err, transction) {
                if (err) {
                    callback(err);
                } else {
                    var creditDebitArr = [];
                    for (var key = 0; key < transction.length; key++) {
                        txnType = transction[key].txnType;
                        var tra = {
                            'txnId': transction[key].txnId,
                            'trigger': transction[key].trigger,
                            'txnType': txnType,
                            'comment': transction[key].comment,
                            'currency': transction[key].currency,
                            'openingBal': transction[key].openingBal,
                            'amount': transction[key].amount,
                            'closingBal': transction[key].closingBal,
                            'paymentType': transction[key].paymentType,
                            'timestamp': transction[key].timestamp,
                            'paymentTxnId': transction[key].paymentTxnId,
                            'intiatedBy': transction[key].intiatedBy,
                            'tripId': transction[key].tripId || ''
                        }
                        creditDebitArr.push(tra);
                    }
                    callback(null, creditDebitArr);
                }
            });
        }
    ], function (err, result) {
        if (err)
            return callback("something goes wrong");
        return callback(null, {
            debitArr: result[0],
            creditArr: result[1],
            creditDebitArr: result[2]
        });
    });
}



/**
 * API - to get all the wallet users
 */
module.exports.allUser = (req, reply) => {

    // based on user type
    let collection = "";
    switch (parseInt(req.payload.userType)) {
        case 1:
            collection = "masters";
            break;
        case 2:
            collection = "slaves";
            break;
        case 3:
            collection = "operators";
            break;
        default:
            collection = "";
            break;
    }
    if (collection == "")
        return reply(status.status(3));
    let condition = {};
    // based on tab selection
    switch (parseInt(req.payload.tabType)) {
        case 2:
            condition = { 'walletSoftLimitHit': true };
            break;
        case 3:
            condition = { 'walletHardLimitHit': true };
            break;
    }

    var regexValue = req.payload.sSearch;
    if (req.payload.sSearch != 'undefined' && req.payload.sSearch != '') {
        switch (parseInt(req.payload.userType)) {
            case 1:
                Object.assign(condition, { '$or': [{ 'firstName': new RegExp(regexValue, 'i') }, { 'lastName': new RegExp(regexValue, 'i') }, { 'email': new RegExp(regexValue, 'i') }, { 'mobile': new RegExp(regexValue, 'i') }] })
                break;
            case 2:
                Object.assign(condition, { '$or': [{ 'name': new RegExp(regexValue, 'i') }, { 'email': new RegExp(regexValue, 'i') }, { 'phone': new RegExp(regexValue, 'i') }] })
                break;
            case 3:
                Object.assign(condition, { '$or': [{ 'operatorName': new RegExp(regexValue, 'i') }, { 'email': new RegExp(regexValue, 'i') }, { 'mobile': new RegExp(regexValue, 'i') }] })
                break;
            default:
                break;
        }
    }

    wallet.Count(collection, condition, (err, count) => {
        if (err)
            return reply(status.status(3));
        if (count === 0)
            return reply({ iTotalRecords: 0, iTotalDisplayRecords: 0, aaData: [] });
        let queryObj = {
            q: condition,
            p: {},
            s: { _id: -1 },
            skip: parseInt(req.payload.iDisplayStart) || 0,
            limit: parseInt(req.payload.iDisplayLength) || 20
        }
        wallet.SELECT(collection, queryObj, (err, docs) => {

            if (err)
                return reply(status.status(3));
            return reply({
                iTotalRecords: count,
                iTotalDisplayRecords: count,
                aaData: docs
            });
        });
    });
};


/**
 * API - to get statement detail for perticular users
 */
module.exports.statement = (req, reply) => {

    let collection = "";
    switch (parseInt(req.payload.userType)) {
        case 1:
            collection = "walletMaster";
            break;
        case 2:
            collection = "walletSlave";
            break;
        case 3:
            collection = "walletOperator";
            break;
        case 4:
            collection = "walletApp";
            break;
        case 5:
            collection = "walletPg";
            break;
        default:
            collection = "";
            break;
    }

    if (collection == "")
        return reply(status.status(3));
    let condition = {};
    switch (parseInt(req.payload.userType)) {
        case 1:
        case 2:
        case 3:
            condition = { userId: req.payload.userId };
            break;
        default:
            condition = {};
            break;
    }

    var regexValue = req.payload.sSearch;
    if (req.payload.sSearch != 'undefined' && req.payload.sSearch != '') {
        Object.assign(condition, { '$or': [{ 'txnType': new RegExp(regexValue, 'i') }, { 'trigger': new RegExp(regexValue, 'i') }, { 'comment': new RegExp(regexValue, 'i') }] })

    }

    wallet.Count(collection, condition, (err, count) => {

        if (err)
            return reply(status.status(3));
        if (count === 0)
            return reply({ iTotalRecords: 0, iTotalDisplayRecords: 0, aaData: [] });
        let queryObj = {
            q: condition,
            p: {},
            s: { _id: -1 },
            skip: parseInt(req.payload.iDisplayStart) || 0,
            limit: parseInt(req.payload.iDisplayLength) || 20
        }
        wallet.SELECT(collection, queryObj, (err, docs) => {

            if (err)
                return reply(status.status(3));
            return reply({
                iTotalRecords: count,
                iTotalDisplayRecords: count,
                aaData: docs
            });
        });
    });
};


/**
 * API - to get statement detail for perticular users
 */
module.exports.statementFilter = (req, reply) => {

    let collection = "";
    switch (parseInt(req.payload.userType)) {
        case 1:
            collection = "walletMaster";
            break;
        case 2:
            collection = "walletSlave";
            break;
        case 3:
            collection = "walletOperator";
            break;
        case 4:
            collection = "walletApp";
            break;
        case 5:
            collection = "walletPg";
            break;
        default:
            collection = "";
            break;
    }

    if (collection == "")
        return reply(status.status(3));
    let condition = {};
    switch (parseInt(req.payload.userType)) {
        case 1:
        case 2:
        case 3:
            condition = { userId: req.payload.userId };
            break;
        default:
            condition = {};
            break;
    }

    if (!(typeof req.payload.searchByPayment == 'undefined' || req.payload.searchByPayment == null || req.payload.searchByPayment == '' || req.payload.searchByPayment == '0')) {
        Object.assign(condition, { 'txnType': req.payload.searchByPayment })
    }
    if (!(typeof req.payload.searchByTrigger == 'undefined' || req.payload.searchByTrigger == null || req.payload.searchByTrigger == '' || req.payload.searchByTrigger == '0')) {
        Object.assign(condition, { 'trigger': req.payload.searchByTrigger })
    }
    if (!(typeof req.payload.searchByStartDate == 'undefined' || req.payload.searchByStartDate == 'undefined--undefined' || req.payload.searchByStartDate == null || req.payload.searchByStartDate == '' || req.payload.searchByStartDate == '0') && !(typeof req.payload.searchByEndDate == 'undefined' || req.payload.searchByEndDate == 'undefined--undefined' || req.payload.searchByEndDate == null || req.payload.searchByEndDate == '' || req.payload.searchByEndDate == '0')) {
        var serverStartTime = moment(req.payload.searchByStartDate + ' 00:00:01').unix();
        var serverEndTime = moment(req.payload.searchByEndDate + ' 23:59:59').unix();
        var dateCond = { '$gte': serverStartTime, '$lte': serverEndTime };
        Object.assign(condition, { 'timestamp': dateCond })
    }

    wallet.Count(collection, condition, (err, count) => {

        if (err)
            return reply(status.status(3));
        if (count === 0)
            return reply({ iTotalRecords: 0, iTotalDisplayRecords: 0, aaData: [] });
        let queryObj = {
            q: condition,
            p: {},
            s: { _id: -1 },
            skip: parseInt(req.payload.iDisplayStart) || 0,
            limit: parseInt(req.payload.iDisplayLength) || 20
        }
        wallet.SELECT(collection, queryObj, (err, docs) => {

            if (err)
                return reply(status.status(3));
            return reply({
                iTotalRecords: count,
                iTotalDisplayRecords: count,
                aaData: docs
            });
        });
    });
};


/**
 * API - to get statement detail for perticular users
 */

module.exports.statementExport = (req, reply) => {

    let collection = "";
    switch (parseInt(req.payload.userType)) {
        case 1:
            collection = "walletMaster";
            break;
        case 2:
            collection = "walletSlave";
            break;
        case 3:
            collection = "walletOperator";
            break;
        case 4:
            collection = "walletApp";
            break;
        case 5:
            collection = "walletPg";
            break;
        default:
            collection = "";
            break;
    }

    if (collection == "")
        return reply(status.status(3));
    let condition = {};
    switch (parseInt(req.payload.userType)) {
        case 1:
        case 2:
        case 3:
            condition = { userId: req.payload.userId };
            break;
        default:
            condition = {};
            break;
    }

    if (!(typeof req.payload.searchByPayment == 'undefined' || req.payload.searchByPayment == null || req.payload.searchByPayment == '' || req.payload.searchByPayment == '0')) {
        Object.assign(condition, { 'txnType': req.payload.searchByPayment })
    }
    if (!(typeof req.payload.searchByTrigger == 'undefined' || req.payload.searchByTrigger == null || req.payload.searchByTrigger == '' || req.payload.searchByTrigger == '0')) {
        Object.assign(condition, { 'trigger': req.payload.searchByTrigger })
    }
    if (!(typeof req.payload.searchByStartDate == 'undefined' || req.payload.searchByStartDate == 'undefined--undefined' || req.payload.searchByStartDate == null || req.payload.searchByStartDate == '' || req.payload.searchByStartDate == '0') && !(typeof req.payload.searchByEndDate == 'undefined' || req.payload.searchByEndDate == 'undefined--undefined' || req.payload.searchByEndDate == null || req.payload.searchByEndDate == '' || req.payload.searchByEndDate == '0')) {
        var serverStartTime = moment(req.payload.searchByStartDate + ' 00:00:01').unix();
        var serverEndTime = moment(req.payload.searchByEndDate + ' 23:59:59').unix();
        var dateCond = { '$gte': serverStartTime, '$lte': serverEndTime };
        Object.assign(condition, { 'timestamp': dateCond })
    }

    var stData = {
        'condition': condition,
        'collection': collection
    };
    wallet.Insert('statementReport', stData, function (err, result) { });
    wallet.Count(collection, condition, (err, count) => {

        if (err)
            return reply(status.status(3));
        if (count === 0)
            return reply({ iTotalRecords: 0, iTotalDisplayRecords: 0, aaData: [] });
        let queryObj = {
            q: condition,
            p: {},
            s: { _id: -1 }
        }
        wallet.SELECT(collection, queryObj, (err, docs) => {

            if (err)
                return reply(status.status(3));
            return reply({
                iTotalRecords: count,
                iTotalDisplayRecords: count,
                aaData: docs
            });
        });
    });
};

module.exports.walletConfiguration = (request, reply) => {
    walletConfigurationTrasction(request.payload, (err, result) => {
    });
    return reply({ errNum: 200, errFlag: 0, errMsg: "success" });
};



const walletConfigurationTrasction = (params, callback) => {
    var userType = "";
    var pubnubChn = "";
    var userSoftLimit = "";
    var userHardLimit = "";
    var deviceType = "";

    var userCollection = "";
    var limits = ['x', 'y'];
    switch (parseInt(params.userType)) {
        case 2:
            userCollection = 'provider';
            limits = ['softLimitDriver', 'hardLimitDriver'];
            break;

        case 1:
            userCollection = 'slaves';
            limits = ['softLimitShipper', 'hardLimitShipper'];
            break;
        case 3:
            userCollection = 'operators';
            limits = ['softLimitOperator', 'hardLimitOperator'];
            break;
        default:
            return callback({ errMsg: 'Invalid userType', errNum: 400, errFlag: 1 });
            break;
    }
    appConfig.readOne({ walletSettings: { $exists: true } }, (err, config) => {
        if (err || config === null)
            return callback(err);

        userSoftLimit = parseFloat(config.walletSettings[limits[0]] || 0);
        userHardLimit = parseFloat(config.walletSettings[limits[1]] || 0);
        wallet.SelectOne(userCollection, { _id: new ObjectID(params.userId) }, (err, userData) => {
            if (err)
                return callback(err);
            else {
                if (userData) {

                    if ((typeof userData.walletSoftLimit != 'undefined') && (typeof userData.walletHardLimit != 'undefined')) {
                        userSoftLimit = parseFloat(userData.walletSoftLimit);
                        userHardLimit = parseFloat(userData.walletHardLimit);
                    }

                    switch (parseInt(params.userType)) {
                        case 1:
                            // pubnubChn = (typeof userData.listner != 'undefined') ? userData.listner : "";
                            userType = 1;
                            deviceType = userData.deviceType || 1;
                            break;
                        case 2:
                            // pubnubChn = (typeof userData.deviceId != 'undefined') ? "slave_" + userData.deviceId : "";
                            userType = 2;
                            deviceType = userData.deviceType || 1;
                            break;
                    }

                    userData.walletAmount = ((typeof userData.walletAmount == 'undefined') ? 0 : parseFloat(userData.walletAmount).toFixed(2));

                    switch (parseInt(params.status)) {
                        case 50:
                        case 51:
                        case 52:
                            var walletData = {
                                txnId: "",
                                userId: params.userId,
                                txnType: "",
                                trigger: "",
                                comment: "",
                                currency: "USD",
                                openingBal: userData.walletAmount,
                                amount: userData.walletAmount,
                                closingBal: userData.walletAmount,
                                paymentType: "",
                                timestamp: "",
                                tripId: "",
                                bookingType: "",
                                paymentTxnId: "",
                                intiatedBy: ""
                            }
                            console.log('change wallet configration userData.walletAmount');
                            console.log(userData.walletAmount);
                            UpdateWalletStatus({
                                userType: params.userType,
                                amount: userData.walletAmount,
                                walletData: walletData,
                                userId: params.userId,
                                isWalletConfigrationChanged: true
                            }, (err, result) => {
                                if (err)
                                    console.log(err);
                            });
                            break;
                        default:
                            break;
                    }

                    switch (parseInt(params.status)) {
                        case 50:
                            // var message = { a: params.status, 'msg': "soft limit and hard limit changed", walletAmount: userData.walletAmount, userSoftLimit: userSoftLimit, userHardLimit: userHardLimit };
                            // walletPushAndPubnub(pubnubChn, message, userType, deviceType, userData.pushTopic, () => {
                            // });
                            sendPushById(params.userId, 'soft limit and hard limit changed', 'soft limit and hard limit changed')
                            break;

                        case 51:
                            // var message = { a: params.status, 'msg': "soft limit changed", walletAmount: userData.walletAmount, userSoftLimit: userSoftLimit, userHardLimit: userHardLimit };
                            // walletPushAndPubnub(pubnubChn, message, userType, deviceType, userData.pushTopic, () => {
                            // });
                            sendPushById(params.userId, 'soft limit changed', 'soft limit changed')
                            break;

                        case 52:
                            // var message = { a: params.status, 'msg': "hard limit changed", walletAmount: userData.walletAmount, userSoftLimit: userSoftLimit, userHardLimit: userHardLimit };
                            // walletPushAndPubnub(pubnubChn, message, userType, deviceType, userData.pushTopic, () => {
                            // });
                            sendPushById(params.userId, 'hard limit changed', 'hard limit changed')
                            break;

                        case 53:
                            // var message = { a: params.status, 'msg': "wallet enabled", walletAmount: userData.walletAmount, userSoftLimit: userSoftLimit, userHardLimit: userHardLimit };
                            // walletPushAndPubnub(pubnubChn, message, userType, deviceType, userData.pushTopic, () => {
                            // });
                            sendPushById(params.userId, 'wallet enabled', 'wallet enabled')
                            break;

                        case 54:
                            // var message = { a: params.status, 'msg': "wallet disabled", walletAmount: userData.walletAmount, userSoftLimit: userSoftLimit, userHardLimit: userHardLimit };
                            // walletPushAndPubnub(pubnubChn, message, userType, deviceType, userData.pushTopic, () => {
                            // });
                            sendPushById(params.userId, 'wallet disabled', 'wallet disabled')
                            break;

                        default:
                            break;
                    }
                } else {
                    return callback(err);
                }
            }

        });
    });
    return callback(null, 'done');
}

const sendPush = (title, msg, providerData, data) => {
    console.log("push send for rachde limit 888888888888888888888888888888888888888888888888888")
    let request = {
        fcmTopic: providerData.fcmTopic,
        action: 30,
        pushType: 6,
        title: title,
        msg: msg,
        data: data,
        deviceType: 1
    }
    console.log("request0000000000000", request)
    fcm.notifyFcmTpic(request, (e, r) => { });
}
const sendPushById = (id, title, msg) => {
    provider.getProviderById(id, (err, res) => {
        console.log("push send for rachde limit 888888888888888888888888888888888888888888888888888")
        let request = {
            fcmTopic: res.fcmTopic,
            action: 30,
            pushType: 6,
            title: title,
            msg: msg,
            data: [],
            deviceType: 1
        }
        console.log("request0000000000000", request)
        fcm.notifyFcmTpic(request, (e, r) => { });
    });

}