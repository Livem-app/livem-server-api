const logger = require('winston');
const config = require("../../config");
const rabbitMq = require('../../models/rebitMq');
const mqtt = require('../../library/mqtt');
const Joi = require("joi");
const async = require("async");
const Promise = require('promise');
const ObjectID = require("mongodb").ObjectID

var db = require('./../../models/mongodb');

const zandesk = require('./../../models/zendesk');
const salesforce = require('./../../models/salesforce');
const ticketCrud = require('../../web/routes/events/ticketCrud');
const zendeskEvent = require('./../../models/zendeskEvent');
const salesforceUtility = require('../../web/routes/events/salesforceUtility');

const cluster = require('cluster');


logger.default.transports.console.colorize = true
logger.default.transports.console.timestamp = true
logger.default.transports.console.prettyPrint = config.env === 'development'
logger.level = config.logger.level


rabbitMq.connect(() => {
    db.connect(() => { 
        prepareConsumer(rabbitMq.getChannelInsert(), rabbitMq.queueInsert, rabbitMq.get());
    });//create a connection to mongodb
});



function prepareConsumer(channel, queue_arg, amqpConn) {

    channel.assertQueue(queue_arg, { durable: false }
        , function (err, queue) {
            channel.consume(queue_arg, function (msg) {
                try {
                    var request = JSON.parse(msg.content.toString());
                    var ticketId = request.payload.ticketId;
                    var groupId = request.payload.groupId;
                    var bodyMsg = request.payload.comment;

                } catch (error) {
                    logger.error("try cat error ", error)
                }
            }, { noAck: true }, function (err, ok) {
                logger.silly("no have message");
            });
        });
}
