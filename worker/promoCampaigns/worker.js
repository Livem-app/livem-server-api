'use strict';

const logger = require('winston');
const rabbitMq = require('../../models/rabbitMq');
const promoCampaigns = require('../handlers/promoCampaigns');
const db = require('../../models/mongodb');

logger.info('step 3 : Child Process Started For validating campaign.......' + process.pid + " ");

/**
 * Connecting RabbitMQ and MongoDB 
 * Calling Prepare Consumer
 */
rabbitMq.connect(() => {
    db.connect(() => {
        prepareConsumerValidateCampaign(rabbitMq.getPromoCampaignChannel(), rabbitMq.queuePromoCampaign, rabbitMq.get());
    });
});

/**
 * Preparing Consumer for Consuming Booking from New Booking Queue
  @param {} channelNewBooking New Booking Channel
  @param {} queueNewBooking  New Booking Queue
  @param {} amqpConn RabbitMQ connection
 */
function prepareConsumerValidateCampaign(channelPromoCampaign, queuePromoCampaign, amqpConn) {
    
    channelPromoCampaign.assertQueue(queuePromoCampaign, { durable: false }
        , function (err, queue) {
            logger.info(err);
            
            logger.error(queue)
            
            if (typeof queue.messageCount != 'undefined' && queue.messageCount > 0) {
                
                channelPromoCampaign.consume(queuePromoCampaign, function (msg) {                    
                
                    var data = JSON.parse(msg.content.toString());
                
                    logger.info("Received new validation request : " + data);
                
                    promoCampaigns.postRequestHandler(data, function (err, response) {
                
                    });
                }, { noAck: true }, function (err, ok) {
                    //To check if need to exit worker
                    // exitWorker(channelPromoCampaign, amqpConn, queueNewBooking);
                });
            }
            else {
                logger.info("New request Queue Is Empty");
                //No need of this worker
                // exitWorker(channelPromoCampaign, amqpConn, queueNewBooking);
            }
        });
}