const logger = require('winston');
const rabbitMq = require('../../models/rabbitMq');
const customer = require('../../web/commonModels/customer');
const db = require('../../models/mongodb');


rabbitMq.connect(() => {
    db.connect(() => {
        prepareConsumer(rabbitMq.getChannelProvider(), rabbitMq.queueProvider, rabbitMq.get());
    });//create a connection to mongodb

});



function prepareConsumer(channelProvider, queueProvider, amqpConn) {

    channelProvider.assertQueue(queueProvider, { durable: false }
        , function (err, queue) { 
            channelProvider.consume(queueProvider, function (msg) {
                logger.debug("consume started" + new Date());
                var data = JSON.parse(msg.content.toString());
                customer.getProviderhandler(data, (err, res) => {
                });
            }, { noAck: true }, function (err, ok) {

            });

        });

}

