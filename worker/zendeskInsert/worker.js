const logger = require('winston');
const config = require("../../config") 
const rabbitMq = require('../../models/rabbitMq');
// const mqtt = require('../../library/mqtt');
const Joi = require("joi");
const async = require("async");
const Promise = require('promise');
const ObjectID = require("mongodb").ObjectID

// var db = require('./../../models/mongodb');
// db.connect(function (err) {
//     if (err) {
//         logger.info('Unable to connect to Mongo.')
//         process.exit(1)
//     } else {

//     }
// })


const zandesk = require('./../../models/zendesk');
// const salesforce = require('./../../models/salesforce');
// const ticketCrud = require('../../web/routes/events/ticketCrud');
// const zendeskEvent = require('./../../models/zendeskEvent');
// const salesforceUtility = require('../../web/routes/events/salesforceUtility');

const cluster = require('cluster');

 

logger.silly('step 4: child Process Started.......' + process.pid + " ");

rabbitMq.connect(() => {
    prepareConsumer(rabbitMq.getChannelTicket(), rabbitMq.ticketQueue, rabbitMq.get());
});



function prepareConsumer(channel, queue_arg, amqpConn) {

    channel.assertQueue(queue_arg, { durable: false }
        , function (err, queue) {
            logger.silly("RabbitMQ Message count", queue.messageCount + " " + queue.consumerCount);

            channel.consume(queue_arg, function (msg) {

                logger.silly("consume started" + new Date());
                logger.silly("msg   ", msg.content.toString());



                // var _id = data._id;
                //mqtt.publish("searchResult/" + _id, JSON.stringify({ data: dataToSend }), { qos: 2 }, () => { });

                try {
                    var request = JSON.parse(msg.content.toString());
                    logger.silly("In worker ticket: ", request);
                    logger.silly('request.payload', request.payload);
                    zandesk.operations.createSingleTicket(request.payload, function (err, result) {
                        if (err) {
                            return reply(err);
                        } else {
                            return reply(result);
                        }
                    })

                } catch (error) {
                    logger.error("try cat error ", error)
                }


                //   console.log("consume end"+new Date());
            }, { noAck: true }, function (err, ok) {
                //To check if need to exit worker
                // exitWorker(channel, amqpConn, queue_arg);
                logger.silly("no have message");
            });
        });
} 