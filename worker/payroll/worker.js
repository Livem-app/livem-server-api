
'use strict';

const logger = require('winston');
const moment = require('moment');
const rabbitMq = require('../../models/rabbitMq');
const db = require('../../models/mongodb');

const payroll = require('../../web/commonModels/payroll');
const paymentCycles = require('../../web/commonModels/paymentCycles');

rabbitMq.connect(() => {
    db.connect(() => {
        prepareConsumerNewPayroll(rabbitMq.getChannelPayroll(), rabbitMq.QueuePayroll, rabbitMq.get());
    });
});

function prepareConsumerNewPayroll(channelPayroll, queuePayroll, amqpConn) {
    channelPayroll.assertQueue(queuePayroll, { durable: false }
        , function (err, queue) {
            if (typeof queue.messageCount != 'undefined' && queue.messageCount > 0) {
                channelPayroll.consume(queuePayroll, function (msg) {
                    var data = JSON.parse(msg.content.toString());
                    logger.info("New payroll Logging Started.", data);
                    payroll.updatePayrollData(data, (err, res) => {
                    })
                    paymentCycles.runDailyCycle(data, (err, res) => { })

                }, { noAck: true }, function (err, ok) {
                    //To check if need to exit worker
                    // exitWorker(channelPayroll, amqpConn, queuePayroll);
                });
            }
            else {
                logger.info("New payroll Event Queue Is Empty");
            }
        });
}


