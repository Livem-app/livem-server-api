'use strict';

const logger = require('winston');
const rabbitMq = require('../../models/rabbitMq');
const referralCampaign = require('../handlers/referralCampaign');
const db = require('../../models/mongodb');

logger.info('step 3 : Child Process Started For validating referral campaign.......' + process.pid + " ");

/**
 * Connecting RabbitMQ and MongoDB 
 * Calling Prepare Consumer
 */
rabbitMq.connect(() => {
    db.connect(() => {
        prepareConsumerValidateReferralCampaign(rabbitMq.getReferralCampaignChannel(), rabbitMq.queueReferralCampaign, rabbitMq.get());
    });
});

/**
 * Preparing Consumer for Consuming referral campaign check request
  @param {} 
  @param {} 
  @param {} 
 */
function prepareConsumerValidateReferralCampaign(channelReferralCampaign, queueReferralCampaign, amqpConn) {
    channelReferralCampaign.assertQueue(queueReferralCampaign, {
        durable: false
    }, function(err, queue) {
        
        // logger.info(queue.messageCount);
        if (typeof queue.messageCount != 'undefined' && queue.messageCount > 0) {
            channelReferralCampaign.consume(queueReferralCampaign, function(msg) {
                var data = JSON.parse(msg.content.toString());
                logger.info("Received new referral campaign validation request : ");
                referralCampaign.referralCodeHandler(data, function(err, response) {});
            }, {
                noAck: true
            }, function(err, ok) {
                //To check if need to exit worker
                // exitWorker(channelReferralCampaign, amqpConn, queueNewBooking);
            });
        } else {
            logger.info("New request Queue Is Empty");
            //No need of this worker
            // exitWorker(channelReferralCampaign, amqpConn, queueNewBooking);
        }
    });
}