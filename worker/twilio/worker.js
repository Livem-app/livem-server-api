const logger = require('winston');
const rabbitMq = require('../../models/rabbitMq');
const SMS = require("../../library/twilio");
const smsLog = require('../../models/smsLog');
const db = require('../../models/mongodb');



rabbitMq.connect(() => {
    db.connect(() => {
        prepareConsumer(rabbitMq.getChannelSms(), rabbitMq.queueSms, rabbitMq.get());
    });//create a connection to mongodb

});



function prepareConsumer(channel, queue_arg, amqpConn) {

    channel.assertQueue(queue_arg, { durable: false }
        , function (err, queue) {
            if (queue.messageCount > 0) {
                channel.consume(queue_arg, function (msg) {
                    var data = JSON.parse(msg.content.toString()); 
                    SMS.sendSms(data, function (err, result) {
                    });

                }, { noAck: true }, function (err, ok) {

                });
            }
        });

}