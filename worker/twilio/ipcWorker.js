const logger = require('winston');
const ipc = require('node-ipc');

ipc.config.id = 'normalworker';
ipc.config.retry = 1000;
ipc.config.silent = true;

const sendSms = require('../../library/twilio');

logger.debug("start time" + new Date());
logger.debug('Process Started.......' + process.pid + " ");


ipc.connectTo('smsserver', () => {
    ipc.of.smsserver.on('connect', () => {
        ipc.of.smsserver.emit('process.connected', {});
       logger.debug("process.connected");
    });


    ipc.of.smsserver.on('process.start', (data) => {
        logger.debug("process.start");


        let param = {
            to: data.countryCode + data.phoneNumber,
            body: data.body
        }

        sendSms.sendSms(param, (err, res) => { });

        ipc.of.smsserver.emit('Ep1CompletedExecution', { "data": "done" });
    });

    ipc.of.smsserver.on('Ep1shutdown', (data) => {
        logger.debug('Got shutdown command, exiting...' + process.pid);

        process.exit();
    })
});



