'use strict'

const tablename = 'events';
const db = require('../mongodb')
const logger = require('winston');


const readAll = (callback) => {
    db.get().collection(tablename)
        .find({}).toArray((err, result) => {
            return callback(null, result);
        });
}
const read = (data, callback) => {
    db.get().collection(tablename)
        .findOne(data, ((err, result) => {
            return callback(err, result);
        }));

}
module.exports = {
    read,
    readAll
};
