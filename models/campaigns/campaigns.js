'use strict'

const tablename = 'campaigns';
const db = require('../mongodb')
const logger = require('winston');
const moment = require('moment');//date-time
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId

/**
 * @function readAll
 * @param {*} callback 
 */
const readAll = (callback) => {
    db.get().collection(tablename)
        .find({}).toArray((err, result) => {
            return callback(null, result);
        });
}

const read = (data, callback) => {
    db.get().collection(tablename)
        .findOne(data, ((err, result) => {
            return callback(err, result);
        }));

}
const insert = (data, callback) => {
    db.get().collection(tablename)
        .insert(data, ((err, result) => {
            return callback(err, result);
        }));

}
module.exports = {
    read,
    readAll,
    insert
};

