'use strict'

const db = require('../mongodb')
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;

const tablename = 'address';

/**
 * @function insert
 * @param {*} data 
 * @param {*} callback 
 */
const insert = (data, callback) => {
    db.get().collection(tablename)
        .findOneAndUpdate(
        { taggedAs: data.taggedAs, user_Id: new ObjectID(data.user_Id) },
        {
            $set: data
        },
        { upsert: true },
        ((err, result) => {
            let addressId = (result.value == null) ? result.lastErrorObject.upserted : result.value._id;
            return callback(err, addressId);
        }));

}



/**
 * @function deleteById
 * @param {*} id -addressid
 * @param {*} callback 
 */
const deleteById = (id, callback) => {
    db.get().collection(tablename)
        .remove(
        { _id: new ObjectID(id) },
        ((err, result) => {
            return callback(err, result);
        }));

}

/**
 * @function updateById
 * @param {*} userId -userid 
 * @param {*} data 
 * @param {*} callback 
 */
const updateById = (userId, data, callback) => {
    db.get().collection(tablename)
        .findOneAndUpdate(
        { _id: new ObjectID(userId) },
        {
            $set: data
        },
        ((err, result) => {
            return callback(err, result);
        }));

}

/**
 * @function readByUserId
 * @param {*} id -address id
 * @param {*} callback 
 */
const readByUserId = (id, callback) => {
    db.get().collection(tablename)
        .find({ user_Id: new ObjectID(id) }).toArray((err, result) => {
            return callback(err, result);
        });

}

const read = (data, callback) => {
    db.get().collection(tablename)
        .findOne(data, ((err, result) => {
            return callback(err, result);
        }));

}
const readByCond = (data, callback) => {
    db.get().collection(tablename)
        .findOne(data, ((err, result) => {
            return callback(err, result);
        }));

}

const setDefult = (addressId, userId, callback) => {
    db.get().collection(tablename)
        .update(
        { user_Id: new ObjectID(userId) },
        {
            $set: { defaultAddress: 0 }
        },
        { multi: true }, ((err, result) => {
            db.get().collection(tablename)
                .findOneAndUpdate(
                { _id: new ObjectID(addressId) },
                {
                    $set: {
                        defaultAddress: 1
                    }
                },
                ((err, result) => {
                    return callback(err, result);
                }));
        }));


}

module.exports = {
    read,
    insert,
    setDefult,
    deleteById,
    updateById,
    readByUserId,
    readByCond
};

