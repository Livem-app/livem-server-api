'use strict'

const tablename = 'providerDailyOnlineLogs';
const db = require('../mongodb')
const logger = require('winston');
const moment = require('moment');//date-time
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId

function getByDay(providerId, callback) {
    db.get().collection(tablename)
        .findOne({
            date: moment().startOf('day').unix(),
            'logs.proId': providerId
        }, ((err, result) => {
            return callback(err, result);
        }));

}

function updateByProviderId(queryObj, callback) {
    db.get().collection(tablename)
        .findOneAndUpdate(
        queryObj.query,
        queryObj.data,
        { upsert: true },
        ((err, result) => {
            return callback(err, result);
        }));
}
const read = (data, callback) => {
    db.get().collection(tablename)
        .findOne(data, ((err, result) => {
            return callback(err, result);
        }));

}
const readAll = (data, callback) => {
    db.get().collection(tablename)
        .find(data).toArray((err, result) => {
            return callback(null, result);
        });
}
const aggregate = (condition, callback) => {
    db.get().collection(tablename)
        .aggregate(condition, ((err, result) => { // aggregate method
            return callback(err, result);
        }));
}
module.exports = {
    updateByProviderId,
    getByDay,
    read,
    readAll,
    aggregate
};
