const mqtt = require('mqtt');
const logger = require('winston')
const config = require('../../config');

// get ip address
var os = require('os');
var interfaces = os.networkInterfaces();
var addresses = [];
for (var k in interfaces) {
    for (var k2 in interfaces[k]) {
        var address = interfaces[k][k2];
        if (address.family === 'IPv4' && !address.internal) {
            addresses.push(address.address);
        }
    }
}

var MqttClientId = "MQTT_CLIENT_" + Math.random();
/**
 * options:
 *  - clientId
 *  - username
 *  - password
 *  - keepalive: number
 *  - clean:
 *  - will: 
 */
var mqtt_options = {
    clientId: MqttClientId,
    keepalive: 20,
    clean: false
};

const client = mqtt.connect(config.mqtt.MQTT_URL, mqtt_options);

client.on('connect', function () { // When connected
    logger.info("\n Web-socket connected");
});
client.on('error', function () {
    logger.error("Web-Socket connection ERROR")
    client.end()
});
/**
 * options: object
 *  - qos (integer): 0 > fire and forget
 *          1 > guaranteed delivery
 *          2 > guaranteed delivery with awk
 */
function websocket_publish(topic, message, options, callback) {

    try {
        client.publish(topic, JSON.stringify(message), options, function () { });
        callback(null, { err: 0, message: 'Publish.' })
    } catch (exec) {
        callback({ err: 1, message: exec.message })
    }

}


exports.publish = websocket_publish 
