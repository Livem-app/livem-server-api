'use strict'

const db = require('../mongodb');
const tablename = 'cities';

/**
 * @method readAll
 * @description Read all Cities
 * @param {*} callback 
 */
const readAll = (callback) => {
    db.get().collection(tablename)
        .find({ "isDeleted": false }).toArray((err, result) => {
            return callback(err, result);
        });
}
const inActiveCities = (data, callback) => {
    db.get().collection('inActiveCities')
        .findOneAndUpdate({ city: data.city },
        data,
        { upsert: true },
        ((err, result) => {
            return callback(err, result);
        }));
}
/**
 * @method read
 * @description read city by condition
 * @param {*} data 
 * @param {*} callback 
 */
const read = (data, callback) => {
    db.get().collection(tablename)
        .findOne(data, ((err, result) => {
            return callback(err, result);
        }));

}
const readBylatLong = (lat, long, callback) => {
    let condition = [
        {
            $geoNear: {
                near: {
                    longitude: long,
                    latitude: lat
                },
                spherical: true,
                distanceField: "distance",
                maxDistance: 50000 / 6378137,
                distanceMultiplier: 6378.1,
                // query: { status: 3 }
            }
        },
        { $sort: { _id: 1 } }
    ];
    db.get().collection(tablename)
        .aggregate(condition, (function (err, result) { // aggregate method
            return callback(err, result);
        }));
}


/** 
* @function
* @name isExists 
* @param {object} params - data coming from controller
*/
const isExists = (params, callback) => {
    db.get().collection(tablename)
        .findOne({
            _id: params._id
        }, (err, result) => {
            return callback(err, result);
        });
}
/** 
* @function
* @name selectCity 
* @param {object} params - data coming from controller
*/
const selectCity = (params, callback) => {
    db.get().collection(tablename)
        .findOne({
            polygons: {
                $geoIntersects: {
                    $geometry: {
                        type: 'Point',
                        coordinates: [params.long, params.lat]
                    }
                }
            }
        },
            { title: 1 }, ((err, city) => {
                return callback(err, city);
            }));
}
/** 
* @function
* @name getAll 
* @param {object} params - data coming from controller
*/
const getAll = (params, callback) => {
    db.get().collection(tablename).find({ "cities.isDeleted": false })
        .toArray((err, result) => { // normal select method
            return callback(err, result);
        });
}

const cityDetailsByCityId = (cityIds, callBack) => {


    db.get().collection(tablename)
        .find({
            "_id": { $in: cityIds }
        }).toArray((err, result) => {
            return callBack(err, result);
        });
}

const getAllCities = (params, callback) => {
    db.get().collection(tablename).find(params)
        .toArray((err, result) => {
            return callback(err, result);
        });
}
module.exports = {
    readBylatLong,
    readAll,
    read,
    inActiveCities,
    isExists,
    selectCity,
    getAll,
    cityDetailsByCityId,
    getAllCities
};

