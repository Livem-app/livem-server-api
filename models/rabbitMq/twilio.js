const path = require('path'); 
const fork = require('child_process').fork;
let logger = require('winston');
let amqp = require('./rabbitMq');
const cluster = require('cluster');
var cpus = {};
 

function InsertQueue(channel, queue, data, callback) {

    logger.info("step 2: inserting message into queue");

    if (channel) {
        channel.assertQueue(queue, { durable: false }, function (err, queueList) {

            let messageCount = queueList.messageCount;
            let consumerCount = queueList.consumerCount;

            logger.info("Message count", messageCount);
            logger.info("Consumer count", consumerCount);

            channel.sendToQueue(queue, Buffer.from(JSON.stringify(data)));
            logger.info("cluster.worker.id : ", cluster.worker.id)
            logger.info("cpus : ", cpus);
            if (!cpus[cluster.worker.id]) {
                logger.info("in IF");
                cpus[cluster.worker.id] = true;
                startIPCServerForRabbitMQWorker(data);
            } else {
                logger.info("in ELSE");
            }

        });
    }
    else {
        logger.error("channal not found...");
    }
}


function startIPCServerForRabbitMQWorker(data) {

    logger.info("step 3: forking chaild process");

    var file = path.join(__dirname, '../../worker/twilio/worker.js');
    var child_process = fork(file);
 

}


module.exports = { InsertQueue };