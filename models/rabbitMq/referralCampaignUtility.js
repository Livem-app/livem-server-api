'use strict'

const path = require('path');
const fork = require('child_process').fork;
let logger = require('winston');

let amqp = require('./rabbitMq');

const cluster = require('cluster');
var cpus = {};
var cpusRetry = {};

/**
 * insert booking data in rabbitMq
  @param {} channel - RabbitMq Channel
  @param {} queue  - RabbitMq queue
  @param {} data - data to insert in queue
  @param {} callback - callback
 */

function InsertReferralQueue(channel, queue, data, callback) {
    // logger.info("channel----------- ", channel)
    // logger.info("queue----------------- ", queue)

    

    logger.info("step 1: New referral campaign request inserted : Process ID " + process.pid);
    if (channel) {
        logger.info("pass1");
        channel.assertQueue(queue, { durable: false }, function (err, queueList) {
            logger.info("pass2")
            // logger.info("insert data2")
            // logger.info(data)
            // let messageCount = queueList.messageCount;
            // let consumerCount = queueList.consumerCount;
            channel.sendToQueue(queue, Buffer.from(JSON.stringify(data)));

            // channel.sendToQueue(queue, new Buffer(JSON.stringify(data)), {}, function(err, ok) {
                
            //     logger.info("error utility")
            //     logger.info(err);
            //     logger.info("ok");
            //     logger.info(ok);


            //     if (err !== null) {
            //         console.warn(' [*] Message nacked');
            //     } else {
            //         console.log(' [*] Message acked');
            //     }
            // });

            if (!cpus[cluster.worker.id]) {
                cpus[cluster.worker.id] = true;
                startIPCServerForRabbitMQWorker();
            }
        });
    }
    else {
        logger.info("RabbitMq Channel is not  ready....");
    }
}

/**
 * start process for consuming from queue
 */
function startIPCServerForRabbitMQWorker() {
    
    logger.info("step 2 : forking new referral campaign Child process" + process.pid);
    var file = path.join(__dirname, '../../worker/referralCampaignsWorker/worker');
    var child_process = fork(file);
}

module.exports = { InsertReferralQueue };