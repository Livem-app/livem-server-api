const path = require('path');
const fork = require('child_process').fork;
let logger = require('winston');
let amqp = require('./rabbitMq');
let emailLog = require('../emailLog');
const cluster = require('cluster');

var cpus = {};

function InsertQueue(channel, queue, data, callback) {
    if (channel) {
        channel.assertQueue(queue, { durable: false }, function (err, queueList) {

            let messageCount = queueList.messageCount;
            let consumerCount = queueList.consumerCount;


            channel.sendToQueue(queue, Buffer.from(JSON.stringify(data)));
            if (!cpus[cluster.worker.id]) {
                cpus[cluster.worker.id] = true;
                startIPCServerForRabbitMQWorker();
            } else {
                logger.info("in ELSE");
            }
        });
    }
    else {
        logger.info("in ELSE");

    }


}


function startIPCServerForRabbitMQWorker() { 
    var file = path.join(__dirname, '../../worker/email/worker.js');
    var child_process = fork(file); 
    logger.debug("pid forked ........ ", child_process.pid);

}




module.exports = { InsertQueue };