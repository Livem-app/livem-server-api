
'use strict'

const path = require('path');
const fork = require('child_process').fork;
let logger = require('winston');

let amqp = require('./rabbitMq');

const cluster = require('cluster');
var cpus = {};

/**
 * insert booking data in rabbitMq
 * @param {*} channel - RabbitMq Channel
 * @param {*} queue  - RabbitMq queue
 * @param {*} data - data to insert in queue
 * @param {*} callback - callback
 */

function InsertQueuePayroll(channel, queue, data, callback) {

    logger.info("step 1: New Payroll Event In Queue : Process ID " + process.pid);

    if (channel) {
        channel.assertQueue(queue, { durable: false }, function (err, queueList) {

            let messageCount = queueList.messageCount;
            let consumerCount = queueList.consumerCount;

            logger.info("New payroll count : ", messageCount);
            logger.info("New payroll Consumer count : ", consumerCount);
            channel.sendToQueue(queue, Buffer.from(JSON.stringify(data)));
            if (!cpus[cluster.worker.id]) {
                cpus[cluster.worker.id] = true;
                startIPCServerForRabbitMQWorker();
            }
        });
    }
    else {
        logger.info("RabbitMq Channel is not  ready....");
    }
}

/**
 * start process for consuming from queue
 */
function startIPCServerForRabbitMQWorker() {

    logger.info("step 2 : forking New payroll Child process" + process.pid);
    var file = path.join(__dirname, '../../worker/payroll/worker.js');
    var child_process = fork(file);
}

module.exports = { InsertQueuePayroll };