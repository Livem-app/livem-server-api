const path = require('path'); 
const fork = require('child_process').fork;
let logger = require('winston');
let amqp = require('./rabbitMq');
// const mqtt = require('../../library/mqtt');

//const userList = require('../userList');
//const userListType = require('../userListType');

const cluster = require('cluster');
var cpus = {};
 



function InsertQueue(channel, queue, data, callback) {

    logger.silly("step 2: inserting message into queue");

    if (channel) {
        channel.assertQueue(queue, { durable: false }, function (err, queueList) {

            let messageCount = queueList.messageCount;
            let consumerCount = queueList.consumerCount;

            logger.silly("Message count", messageCount);
            logger.silly("Consumer count", consumerCount);

            // if (messageCount == 0 || ((messageCount > amqp.thresholdLatLong) && (messageCount / (amqp.thresholdLatLong * consumerCount) > 1))) {
            // Note: on Node 6 Buffer.from(msg) should be used

            channel.sendToQueue(queue, Buffer.from(JSON.stringify(data)));
            logger.silly("cluster.worker.id : ", cluster.worker.id)
            // logger.silly("cpus : ", cpus);
            if (!cpus[cluster.worker.id]) {
                logger.silly("in IF");
                cpus[cluster.worker.id] = true;
                startIPCServerForRabbitMQWorker(data);
            } else {
                logger.silly("in ELSE");
            }
            // }
            // else {
            //     // Note: on Node 6 Buffer.from(msg) should be used
            //     channel.sendToQueue(queue, Buffer.from(JSON.stringify(data)));
            //     // startIPCServerForRabbitMQWorker(data);
            // }

        });
    } else {
        logger.error("channal not found...",channel);
    }

}


function startIPCServerForRabbitMQWorker(data) {

    

    logger.silly("step 3: forking chaild process");
    // if (!cpus[cluster.worker.id]) {

    var file = path.join(__dirname, '../../worker/zendeskInsert/worker.js');
    var child_process = fork(file);
    // }

    logger.silly("cpus : ", cpus)
    logger.silly("pid forked ........ ", child_process.pid);

    
}

module.exports = { InsertQueue };