'use strict'
/** 
 * This const requires the mongo url
 * @const
 * @requires module:config 
 */
const config = require('../../config');
const logger = require('winston');

const fork = require('child_process').fork;

//For the RabbitMQ connection
const amqp = require('amqplib/callback_api');


//For the chhanel corresponding to the high traffic apis
let channelEmail = null;
let channelTicket = null;
let channelSms = null;
let channelWallet = null;
let channelInsert = null;

let channelProvider = null;

let channelStripeEvent = null;

let channelPayroll = null;

let channelPromoCampaign = null;
let channelReferralCampagn = null;
//For the queues corresponding to the high traffic apis
let queueEmail = 'QueueEmail';
let queueTicket = 'QueueTicket';
let queueSms = 'QueueSms';
let queueWallet = 'QueueWallet';
let queueInsert = 'QueueInsert';
let ticketQueue = 'ticketQueue';

let queueProvider = 'QueueProvider';

let QueueStripeEvent = 'QueueStripeEvent';

let QueuePayroll = 'QueuePayroll';


let queuePromoCampaign = "promoCampaign";
let queueReferralCampaign = "referralCampaign";
//For the threshold of messages that can be executed by one worker corresponding to the high traffic api call
let thresholdEmail = 100;
let thresholdTicket = 100;
let thresholdSms = 100;
let thresholdWalle = 100;

let thresholdInsert = 100;
const ticketholdInsert = 100;

let thresholdProvider = 100;

let thresholdStripeEvent = 100;

let thresholdPayroll = 100;

let thresoldPromoCampaign = 100;
let thresoldReferralCampaign = 100;
//set up a connection
/**
 * @amqpConn will hold the connection and channels will be set up in the connection.
 */
var state = { amqpConn: null };


/**
 * Method to connect to the mongodb
 * @param {*} url
 * @returns connection object
 */
exports.connect = (callback) => {
    if (state.amqpConn) return callback();
    amqp.connect(config.rabbitMq.RABBITMQURL + "?heartbeat=60", (err, conn) => {
console.log('===>>>',config.rabbitMq.RABBITMQURL)
        if (err) {
            logger.error("[AMQP]", err.message);
            //return setTimeout(connectRabbitMQServer, 1000);
            return callback(err);
        }
        conn.on("error", (err) => {
            if (err.message !== "Connection closing") {
                logger.error("[AMQP] conn error", err.message);
                return callback(err);
            }
        });
        conn.on("close", () => {
            // logger.info("[AMQP] reconnecting");
            //return setTimeout(connectRabbitMQServer, 1000);
        });
        logger.info("[AMQP] connected");
        state.amqpConn = conn;


        preparePublisher();
        return callback();

    });

}

/**
 * Method to get the connection object of the mongodb
 * @returns db object
 */
exports.get = () => {
    return state.amqpConn;
}


function preparePublisher() {


    channelEmail = state.amqpConn.createChannel((err, ch) => {

        if (closeOnErr(err)) return;

        ch.on("error", (err) => {
            logger.error("[AMQP] channel error", err.message);
        });

        ch.on("close", () => {
            logger.info("[AMQP] channel closed");
        });

    });

    channelInsert = state.amqpConn.createChannel((err, ch) => {

        if (closeOnErr(err)) return;

        ch.on("error", (err) => {
            logger.error("[AMQP] channel error", err.message);
        });

        ch.on("close", () => {
            logger.info("[AMQP] channel closed");
        });

    });

    channelTicket = state.amqpConn.createChannel((err, ch) => {

        if (closeOnErr(err)) return;

        ch.on("error", (err) => {
            logger.error("[AMQP] channel error", err.message);
        });

        ch.on("close", () => {
            logger.info("[AMQP] channel closed");
        });

    });

    channelSms = state.amqpConn.createChannel((err, ch) => {

        if (closeOnErr(err)) return;

        ch.on("error", (err) => {
            logger.error("[AMQP] channel error", err.message);
        });

        ch.on("close", () => {
            logger.info("[AMQP] channel closed");
        });

    });

    channelWallet = state.amqpConn.createChannel((err, ch) => {

        if (closeOnErr(err)) return;

        ch.on("error", (err) => {
            logger.error("[AMQP] channel error", err.message);
        });

        ch.on("close", () => {
            logger.info("[AMQP] channel closed");
        });

    });

    channelProvider = state.amqpConn.createChannel((err, ch) => {

        if (closeOnErr(err)) return;

        ch.on("error", (err) => {
            logger.error("[AMQP] channel error", err.message);
        });

        ch.on("close", () => {
            logger.info("[AMQP] channel closed");
        });

    });

    channelStripeEvent = state.amqpConn.createChannel((err, ch) => {
        if (closeOnErr(err)) return;
        ch.on("error", (err) => {
            logger.error("[AMQP] channel error", err.message);
        });
        ch.on("close", () => {
            logger.info("[AMQP] channel closed");
        });
    });

    channelPayroll = state.amqpConn.createChannel((err, ch) => {
        if (closeOnErr(err)) return;
        ch.on("error", (err) => {
            logger.error("[AMQP] channel error", err.message);
        });
        ch.on("close", () => {
            logger.info("[AMQP] channel closed");
        });
    });
    channelPromoCampaign = state.amqpConn.createChannel((err, ch) => {

        if (closeOnErr(err)) return;

        ch.on("error", (err) => {
            logger.error("[AMQP] channel error", err.message);
        });

        ch.on("close", () => {
            logger.info("[AMQP] channel closed");
        });

    });

    channelReferralCampagn = state.amqpConn.createChannel((err, ch) => {

        if (closeOnErr(err)) return;

        ch.on("error", (err) => {
            logger.error("[AMQP] channel error", err.message);
        });

        ch.on("close", () => {
            logger.info("[AMQP] channel closed");
        });

    });
}

function closeOnErr(err) {

    if (!err) return false;

    logger.error("[AMQP] error", err);
    state.amqpConn.close();
    return true;
}


exports.getChannelEmail = () => {
    return channelEmail;
}

exports.getChannelTicket = () => {
    return channelTicket;
}


exports.getChannelSms = () => {
    return channelSms;
}

exports.getChannelWallet = () => {
    return channelWallet;
}
exports.getChannelInsert = () => {
    return channelInsert;
}

exports.getChannelProvider = () => {
    return channelProvider;
}


exports.getChannelStripeEvent = () => {
    return channelStripeEvent;
}

exports.getChannelPayroll = () => {
    return channelPayroll;
}

exports.getPromoCampaignChannel = () => {
    return channelPromoCampaign;
}
exports.getReferralCampaignChannel = () => {
    return channelReferralCampagn;
}

exports.QueueStripeEvent = QueueStripeEvent;
exports.thresholdStripeEvent = thresholdStripeEvent;

exports.queueEmail = queueEmail;
exports.thresholdEmail = thresholdEmail;
exports.queueTicket = queueTicket;
exports.thresholdTicket = thresholdTicket;
exports.queueSms = queueSms;
exports.thresholdSms = thresholdSms;
exports.queueWallet = queueWallet;
exports.thresholdWalle = thresholdWalle;

exports.queueInsert = queueInsert;
exports.thresholdInsert = thresholdInsert;
exports.ticketholdInsert = ticketholdInsert;
exports.ticketQueue = ticketQueue;

exports.queueProvider = queueProvider;
exports.thresholdProvider = thresholdProvider;

exports.QueuePayroll = QueuePayroll;
exports.thresholdPayroll = thresholdPayroll;


// Referral campaign

exports.queueReferralCampaign = queueReferralCampaign;
exports.thresoldReferralCampaign = thresoldReferralCampaign;

// Promo campaign
exports.queuePromoCampaign = queuePromoCampaign;
exports.thresoldPromoCampaign = thresoldPromoCampaign;

