'use strict'

const tablename = 'verificationCode';
const db = require('../mongodb')
const logger = require('winston');
const moment = require('moment');//date-time
var ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId

function read(data, callback) {
    db.get().collection(tablename)
        .findOne(data, ((err, result) => {
            return callback(err, result);
        }));

}
function readAll(userType, callback) {
    db.get().collection(tablename)
        .find({ usertype: userType }).toArray((err, result) => {
            return callback(null, result);
        });
}
function updateByPhone(phone, callback) {
    db.get().collection(tablename)
        .update(
        { givenInput: phone },
        {
            $set: { status: false }
        },
        { multi: true }, ((err, result) => {
            return callback(err, result);
        }));

}

function updateByDeviceId(data, callback) {
    logger.info("updateByDeviceId", data);
    db.get().collection(tablename)
        .findOneAndUpdate(
        { deviceId: data.deviceId },
        {
            $set: {
                userId: new ObjectID(data.userId),
                userType: data.userType, //1 - slave ,2 - master
                deviceId: data.deviceId,
                deviceType: parseInt(data.deviceType),
                appversion: data.appVersion || '',
                deviceOsVersion: data.deviceOsVersion || '',
                devMake: data.deviceMake || '',
                batteryPercentage: data.batteryPercentage || '',
                locationHeading: data.locationHeading || '',
                devModel: data.deviceModel || '',
                deviceTime: new Date(data.deviceTime) || '',
                serverTime: moment().unix(),
                lastLogin: moment().unix(),
                lastActive: moment().unix(),
                loggedIn: true
            }
        },
        { upsert: true }, ((err, result) => {
            return callback(err, result);
        }));

}
function updateAfterVerify(con, callback) {
    db.get().collection(tablename)
        .findOneAndUpdate(
        con.query, con.data, ((err, result) => {
            return callback(err, result);
        }));

}
function post(data, callback) {
    db.get().collection(tablename)
        .insert(data, ((err, result) => {
            return callback(err, result);
        }));

}
function getByProviderId(id, callback) {
    let con = {
        userId: id,
        status: true
    }
    db.get().collection(tablename)
        .findOne(con, ((err, result) => {
            return callback(err, result);
        }));

}
function updateById(id, callback) {
    db.get().collection(tablename)
        .update(
        { userId: id },
        {
            $set: { status: false }
        },
        { multi: true }, ((err, result) => {
            return callback(err, result);
        }));

}

function readAllData(data, callback) {
    db.get().collection(tablename)
        .find(data).toArray((err, result) => {
            return callback(null, result);
        });
}


module.exports = {
    read,
    readAll,
    readAllData,
    updateByPhone,
    post,
    getByProviderId,
    updateAfterVerify,
    updateById
};
