'use strict'

const db = require('../mongodb')
const logger = require('winston');
const moment = require('moment');//date-time
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId

const tablename = 'subCategory';

/**
 * get all subcategory by category id
 * @param {*} id Category ID
 * @param {*} callback 
 */
const readByCategory = (id, callback) => {
    db.get().collection(tablename)
        .find({"cat_id" : id}).toArray((err, result) => {
            return callback(err, result);
        });
}
module.exports = {
    readByCategory
};

