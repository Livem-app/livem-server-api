'use strict'

const tablename = 'appVersions';
const db = require('../mongodb')
const logger = require('winston');

/**
 * @function read
 * @description Find one config data
 * @param {*} callback 
 */
const read = (data, callback) => {
    db.get().collection(tablename)
        .findOne(data, ((err, result) => {
            return callback(err, result);
        }));

}
const post = (data, callback) => {
    db.get().collection(tablename)
        .insert(data, ((err, result) => {
            return callback(err, result);
        }));

}
const update = (queryObj, data, callback) => {
    db.get().collection(tablename)
        .findOneAndUpdate(
        queryObj,
        data,
        ((err, result) => {
            return callback(err, result);
        }));

}
module.exports = {
    read,
    post,
    update
};
