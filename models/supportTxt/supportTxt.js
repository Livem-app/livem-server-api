'use strict'

const db = require('../mongodb')
const logger = require('winston');
const moment = require('moment');//date-time
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId

const tablename = 'supportTxt';

function readAll(userType, callback) {
    db.get().collection(tablename)
        .find({ usertype: userType }).toArray((err, result) => {
            return callback(null, result);
        });
}
module.exports = {
    readAll
};

