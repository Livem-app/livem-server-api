'use strict'

const tablename = 'bookings';
const db = require('../mongodb')
const logger = require('winston');
const moment = require('moment');//date-time
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId

const post = (data, callback) => {
    db.get().collection(tablename)
        .insert(data, ((err, result) => {
            return callback(err, result);
        }));

}

const bookingCount = (data, callback) => {
    db.get().collection(tablename)
        .count(data, ((err, result) => {
            return callback(err, result);
        }));
}

const read = (data, callback) => {
    db.get().collection(tablename)
        .findOne(data, ((err, result) => {
            return callback(err, result);
        }));

}
const findOneAndUpdate = (bookingId, data, callback) => {
    db.get().collection(tablename)
        .findOneAndUpdate(
        { bookingId: bookingId },
        data,
        ((err, result) => {
            return callback(err, result);
        }));
}
const findUpdate = (obj, callback) => {
    db.get().collection(tablename)
        .findOneAndUpdate(
        obj.query,
        obj.data,
        ((err, result) => {
            return callback(err, result);
        }));
}
const readAll = (data, callback) => {
    db.get().collection(tablename)
        .find(data).sort({ bookingId: -1 }).toArray((err, result) => {
            return callback(null, result);
        });
}

const aggregate = (condition, callback) => {
    db.get().collection(tablename)
        .aggregate(condition, ((err, result) => { // aggregate method
            return callback(err, result);
        }));
}
module.exports = {
    post,
    read,
    readAll,
    aggregate,
    bookingCount,
    findUpdate,
    findOneAndUpdate
};

