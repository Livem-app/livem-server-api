'use strict'

const tablename = 'appConfig';
const db = require('../mongodb')
const logger = require('winston');

/**
 * @function read
 * @description Find one config data
 * @param {*} callback 
 */
const read = (callback) => {
    db.get().collection(tablename)
        .findOne((err, result) => {
            return callback(err, result);
        });

}
const readOne = (condition, callback) => {
    db.get().collection(tablename)
        .findOne(condition, (err, result) => {
            return callback(err, result);
        });

}


module.exports = {
    read,
    readOne
};
