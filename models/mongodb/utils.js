'use strict'

var db = require('./');

function Select(tablename, data, callback) {
    db.get().collection(tablename).find(data).toArray((err, result) => {
        return callback(err, result);
    });
}

module.exports = { Select };
