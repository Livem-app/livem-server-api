'use strict'

const tablename = 'adminPushNotifications';
const db = require('../mongodb')
const logger = require('winston');
const moment = require('moment');//date-time
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId



const post = (data, callback) => {
    db.get().collection(tablename)
        .insert(data, ((err, result) => {
            return callback(err, result);
        }));

}
module.exports = {
    post
};

