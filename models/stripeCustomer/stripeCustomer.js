'use strict'

const tablename = 'stripeCustomer';

const db = require('../../models/mongodb')
const logger = require('winston');

const ObjectID = require('mongodb').ObjectID;

/**
 * get customer for stripe
 * @param {*} userId 
 * @param {*} mode test/live
 */
const getCustomer = (userId, mode) => {
    return new Promise((resolve, reject) => {
        db.get().collection(tablename)
            .findOne({ 'user': new ObjectID(userId), 'mode': mode }, (function (err, result) {
                err ? reject(err) : resolve(result);
            }));
    });
};

/**
 * create connect account for stripe
 * @param {*} insObj 
 */
const createCustomer = (insObj) => {
    return new Promise((resolve, reject) => {
        db.get().collection(tablename)
            .insert(insObj, (function (err, result) {
                err ? reject(err) : resolve(result);
            }));
    });
};

module.exports = {
    getCustomer,
    createCustomer
};
