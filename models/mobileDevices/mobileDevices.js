'use strict'

const tablename = 'mobileDevices';
const db = require('../mongodb')
const logger = require('winston');
const moment = require('moment');//date-time
var ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId
const Timestamp = require('mongodb').Timestamp;

function updateByUserId(id, callback) {
    db.get().collection(tablename)
        .update(
        { userId: new ObjectID(id) },
        {
            $set: {
                loggedIn: false
            }
        },
        { multi: true }, ((err, result) => {
            return callback(err, result);
        }));

}

function updateByDeviceId(data, callback) {
    logger.info("updateByDeviceId", data);
    db.get().collection(tablename)
        .findOneAndUpdate(
        { deviceId: data.deviceId },
        {
            $set: {
                userId: new ObjectID(data.userId),
                userType: data.userType, //1 - slave ,2 - master
                deviceId: data.deviceId,
                deviceType: parseInt(data.deviceType),
                appversion: data.appVersion || '',
                deviceOsVersion: data.deviceOsVersion || '',
                devMake: data.deviceMake || '',
                batteryPercentage: data.batteryPercentage || '',
                locationHeading: data.locationHeading || '',
                devModel: data.deviceModel || '',
                deviceTime: new Date(data.deviceTime) || '',
                serverTime: moment().unix(),
                lastLogin: moment().unix(),
                lastActive: moment().unix(),
                loggedIn: true,
                lastLoginTs: new Timestamp(),
                lastLoginDate: new Date(),
                deviceTypeSt: parseInt(data.deviceType) == 1 ? 'IOS' : 'Android'

            }
        },
        { upsert: true }, ((err, result) => {
            return callback(err, result);
        }));

}
module.exports = { updateByUserId, updateByDeviceId };
