'use strict'

const tablename = 'operators';
const db = require('../mongodb')
const logger = require('winston');
const moment = require('moment');//date-time
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId

/**
 * @function postSignUp 
 * @description update data by email
 * @param {*} data 
 * @param {*} callback 
 */
const postSignUp = (data, callback) => {
    db.get().collection(tablename)
        .findOneAndUpdate({ email: data.email }, data, { upsert: true }, ((err, result) => {
            return callback(err, result);
        }));

}
/**
 * @function read
 * @description Read Provider by condition
 * @param {*} data 
 * @param {*} callback 
 */
const read = (data, callback) => {
    db.get().collection(tablename)
        .findOne(data, ((err, result) => {
            return callback(err, result);
        }));

}

/**
 * @function findOneAndUpdate
 * @description update data by id
 * @param {*} userId -providerId
 * @param {*} data 
 * @param {*} callback 
 */
const findOneAndUpdate = (userId, data, callback) => {
    db.get().collection(tablename)
        .findOneAndUpdate(
        { _id: new ObjectID(userId) },
        data,
        ((err, result) => {
            return callback(err, result);
        }));
}

/**
 * @function readByAggregate
 * @description Find data by lat-long
 * @param {*} lat 
 * @param {*} long 
 * @param {*} query 
 * @param {*} callback 
 */
const readByAggregate = (lat, long, query, callback) => {
    let condition = [
        {
            $geoNear: {
                near: {
                    longitude: long,
                    latitude: lat
                },
                spherical: true,
                distanceField: "distance",
                maxDistance: 50000 / 6378137,
                distanceMultiplier: 6378.1,
                query: query
            }
        },
        { $sort: { _id: 1 } }
    ];
    db.get().collection(tablename)
        .aggregate(condition, (function (err, result) { // aggregate method
            return callback(err, result);
        }));
}

const readAllByLimit = (data, callback) => {
    db.get().collection(tablename)
        .find(data.q).sort(data.sort || {}).limit(data.limit).skip(data.skip).toArray((err, result) => {
            return callback(err, result);
        });
}

const count = (data, callback) => {
    db.get().collection(tablename)
        .count(data, ((err, result) => {
            return callback(err, result);
        }));
}
const readAll = (data, callback) => {
    db.get().collection(tablename)
        .find(data).toArray((err, result) => {
            return callback(err, result);
        });
}

module.exports = {
    count,
    read,
    readAll,
    readByAggregate,
    postSignUp,
    findOneAndUpdate,
    readAllByLimit
};
