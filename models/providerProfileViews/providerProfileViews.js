'use strict'

const tablename = 'providerProfileViews';
const db = require('../mongodb')
const logger = require('winston');
const moment = require('moment');//date-time
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId


const updateByProviderId = (data, callback) => {
    db.get().collection(tablename)
        .findOneAndUpdate(
        { proId: new ObjectID(data.providerId) },
        {
            $set: { proId: new ObjectID(data.providerId) },
            $push: {
                views: {
                    viewedBy: new ObjectID(data.customerId),
                    viewedOn: moment().unix(),
                    lat: data.lat ? data.lat : '',
                    long: data.long ? data.long : ''
                }
            }
        },
        { upsert: true },
        ((err, result) => {
            return callback(err, result);
        }));
}

const read = (condition, callback) => {
    db.get().collection(tablename)
        .aggregate(condition, ((err, result) => { // aggregate method
            return callback(err, result);
        }));

}
module.exports = {
    updateByProviderId,
    read
};
