'use strict'

const tablename = 'payoutFeeds';

const db = require('../../models/mongodb')
const logger = require('winston');

const ObjectID = require('mongodb').ObjectID;

/**
 * insert Stripe Event Logs in DB
 * @param {*} insObj
 */
const insert = (insObj) => {
    return new Promise((resolve, reject) => {
        db.get().collection(tablename)
            .insert(insObj, (function (err, result) {
                err ? reject(err) : resolve(result);
            }));
    });
};

module.exports = {
    insert
};
