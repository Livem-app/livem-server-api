'use strict'

const db = require('../mongodb')
const logger = require('winston');
const moment = require('moment');//date-time
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId

const tablename = 'unassignedBookings';

const post = (data, callback) => {
    db.get().collection(tablename)
        .insert(data, ((err, result) => {
            return callback(err, result);
        }));

}
const read = (data, callback) => {
    db.get().collection(tablename)
        .findOne(data, ((err, result) => {
            return callback(err, result);
        }));

}
const readAll = (data, callback) => {
    db.get().collection(tablename)
        .find(data).sort({ bookingId: -1 }).toArray((err, result) => {
            return callback(null, result);
        });
}
const findOneAndUpdate = (obj, callback) => {
    db.get().collection(tablename)
        .findOneAndUpdate(
        obj.query,
        obj.data,
        ((err, result) => {
            return callback(err, result);
        }));
}
const findUpdate = (obj, callback) => {
    db.get().collection(tablename)
        .findOneAndUpdate(
        obj.query,
        obj.data,
        ((err, result) => {
            return callback(err, result);
        }));
}

const findOneDelete = (data, callback) => {
    db.get().collection(tablename)
        .findOneAndDelete(data, ((err, result) => {
            return callback(err, result);
        }));
}
module.exports = {
    post,
    read,
    readAll,
    findUpdate,
    findOneDelete,
    findOneAndUpdate
};

