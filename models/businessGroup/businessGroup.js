'use strict'

const db = require('../mongodb')
const logger = require('winston');
const moment = require('moment');//date-time
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId

const tablename = 'bussinessGroup';


/**
 * @function readAll
 * @description read All category
 * @param {*} callback 
 */
const readAll = (callback) => {
    db.get().collection(tablename)
        .find({}).toArray((err, result) => {
            return callback(null, result);
        });
}
/**
 * @function read
 * @description read only one category
 * @param {*} data 
 * @param {*} callback 
 */
const read = (data, callback) => {
    db.get().collection(tablename)
        .findOne(data, ((err, result) => {
            return callback(err, result);
        }));

}
module.exports = {
    read,
    readAll
};

