'use strict'

const tablename = 'documentTypes';
const db = require('../mongodb')
const logger = require('winston');
const moment = require('moment');//date-time
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId

const getByCategoryId = (catId, callback) => {
    db.get().collection(tablename)
        .find({ Service_category_id: catId }).toArray((err, result) => {
            return callback(null, result);
        });
}
module.exports = { getByCategoryId };

