'use strict'

const tablename = 'schedule';
const db = require('../mongodb')
const logger = require('winston');
const moment = require('moment');//date-time
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId

function postSchedule(data, callback) {
    db.get().collection(tablename)
        .insert([data], ((err, result) => {
            return callback(err, result);
        }));

}
function getByProviderId(id, callback) {
    db.get().collection(tablename)
        .find({ providerId: new ObjectID(id) }).toArray((err, result) => {
            return callback(err, result);
        });

}
function checkAvailableSlot(providerId, strtDateTime, plus, callback) {
    let query = {
        providerId: new ObjectID(providerId),
        'start': { '$lte': strtDateTime },
        'end': { '$gte': strtDateTime },
        'startTimeInt': { '$lte': plus },
        'endTimeInt': { '$gte': plus }
    } 
    db.get().collection(tablename)
        .find(query).toArray((err, result) => { 
            return callback(err, result);
        });

}
function checkAvailableSchedule(providerId, strtDateTime, plus, end, callback) {
    let query = {
        providerId: new ObjectID(providerId),
        'startDate': { '$lte': strtDateTime },
        'endDate': { '$gte': strtDateTime },
        $or: [
            { startTimeInt: plus },
            { endTimeInt: end },
            { $and: [{ startTimeInt: { '$gt': plus } }, { startTimeInt: { '$lt': end } }] },
            { $and: [{ startTimeInt: { '$lt': plus } }, { endTimeInt: { '$gt': plus } }] }
        ]
    } 
    db.get().collection(tablename)
        .find(query).toArray((err, result) => { 
            return callback(err, result);
        });

}
function deleteById(id, callback) {
    db.get().collection(tablename)
        .remove(
        { _id: new ObjectID(id) },
        ((err, result) => {
            return callback(err, result);
        }));

}

const readByAggregate = (lat, long, query, callback) => {
    let condition = [
        {
            $geoNear: {
                near: {
                    longitude: long,
                    latitude: lat
                },
                spherical: true,
                distanceField: "distance",
                maxDistance: 50000 / 6378137,
                distanceMultiplier: 6378.1,
                query: query
            }
        },
        { $sort: { _id: 1 } }
    ];
    db.get().collection(tablename)
        .aggregate(condition, (function (err, result) { // aggregate method
            return callback(err, result);
        }));
}


module.exports = {
    postSchedule,
    getByProviderId,
    checkAvailableSlot,
    deleteById,
    readByAggregate,
    checkAvailableSchedule
};

