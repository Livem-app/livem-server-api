'use strict'

const tablename = 'services';
const db = require('../mongodb')
const logger = require('winston');
const moment = require('moment');//date-time
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId

const getByCatId = (catId, callback) => {
    db.get().collection(tablename)
        .find({ cat_id: (catId).toString() }).toArray((err, result) => {
            return callback(err, result);
        });
}
const read = (data, callback) => {
    db.get().collection(tablename)
        .findOne(data, ((err, result) => {
            return callback(err, result);
        }));

}
module.exports = {
    read,
    getByCatId
};

