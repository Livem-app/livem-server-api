'use strict'

const tablename = 'payroll';
const db = require('../mongodb')
const logger = require('winston');
const moment = require('moment');//date-time
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId

const findUpdate = (obj, callback) => {
    db.get().collection(tablename)
        .findOneAndUpdate(
        obj.query,
        obj.data,
        { upsert: true },
        ((err, result) => {
            return callback(err, result);
        }));
}

const aggregate = (condition, callback) => {
    db.get().collection(tablename)
        .aggregate(condition, ((err, result) => { // aggregate method
            return callback(err, result);
        }));
}

const readAll = (condition, callback) => {
    db.get().collection(tablename)
        .find(condition).toArray((err, result) => {
            return callback(err, result);
        });
}

const read = (data, callback) => {
    db.get().collection(tablename)
        .findOne(data, ((err, result) => {
            return callback(err, result);
        }));

}
const count = (data, callback) => {
    db.get().collection(tablename)
        .count(data, ((err, result) => {
            return callback(err, result);
        }));
}
const readPayroll = (queryObj, callback) => {
    db.get().collection(tablename)
        .find(queryObj.q || {}, queryObj.p || {})
        .sort(queryObj.s || {})
        .skip(queryObj.skip || 0)
        .limit(queryObj.limit || 0)
        .toArray((err, docs) => {
            return callback(err, docs);
        });
}
module.exports = {
    findUpdate,
    aggregate,
    readAll,
    read,
    count,
    readPayroll
};
