'use strict'

const logger = require('winston');
const db = require('../mongodb')
let tablename = 'emailLog';//collection name

function Insert(data, callback) {
    db.get().collection(tablename).insert([data], (err, result) => {
        return callback(err, result);
    });
}//insert email log in db

function updateByMsgId(queryObj, callback) {
    db.get().collection(tablename)
        .findOneAndUpdate(
        { msgId: queryObj.msgId },
        {
            $set: {
                status: queryObj.event
            }
        }, ((err, result) => {
            return callback(err, result);
        }));

}//update mail status by msgid

const readAll = (con, callback) => {
    db.get().collection(tablename)
        .find(con).sort({ _id: -1 }).toArray((err, result) => {
            return callback(null, result);
        });
}

module.exports = {
    readAll,
    Insert,
    updateByMsgId

};
