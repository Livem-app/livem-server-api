'use strict'

const tablename = 'providerLocationLog';
const db = require('../mongodb')
const logger = require('winston');
const moment = require('moment');//date-time
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId



function updateByBookingId(bookingId, dataObj, callback) {
    db.get().collection(tablename)
        .findOneAndUpdate(
        { bookingId: bookingId },
        dataObj,
        { upsert: true },
        ((err, result) => {
            return callback(err, result);
        }));
}


module.exports = { updateByBookingId };
