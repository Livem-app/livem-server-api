'use strict'

const tablename = 'dispatcher';
const db = require('../mongodb')
const logger = require('winston');
const moment = require('moment');//date-time
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId

const read = (data, callback) => {
    db.get().collection(tablename)
        .findOne(data, ((err, result) => {
            return callback(err, result);
        }));

}
const UpdateById = (userId, data, callback) => {
    db.get().collection(tablename)
        .findOneAndUpdate(
        { _id: new ObjectID(userId) },
        data,
        ((err, result) => {
            return callback(err, result);
        }));
}

const findOneAndUpdate = (obj, callback) => {
    db.get().collection(tablename)
        .findOneAndUpdate(
        obj.query,
        obj.data,
        ((err, result) => {
            return callback(err, result);
        }));
}

module.exports = {
    read,
    UpdateById,
    findOneAndUpdate
};
