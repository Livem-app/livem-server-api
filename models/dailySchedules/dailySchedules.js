'use strict'
const db = require('../mongodb')
const moment = require('moment');
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;

const tablename = 'dailySchedules';

/**
 * @function addDailySchedule
 * @description add schedule for start date to end date each day
 * @param {*} req 
 * @param {*} item 
 * @param {*} scheduleId 
 * @param {*} startD 
 * @param {*} endD 
 * @param {*} callback 
 */
const addDailySchedule = (req, item, scheduleId, startD, endD, locationLog, callback) => {
    db.get().collection(tablename)
        .findOneAndUpdate(
            { date: moment(item).startOf('day').unix() },
            {
                $set: { date: moment(item).startOf('day').unix() },
                $push: {
                    schedule: {
                        providerId: new ObjectID(req.providerId),
                        scheduleId: scheduleId,
                        location: locationLog,
                        address: (req.payload.addresssId).toString(),
                        startTime: startD,
                        endTime: endD,
                        booked: [],
                    }
                }
            },
            { upsert: true },
            ((err, result) => {
                return callback(err, result);
            }));
}

/**
 * @function getSchduleByMonth
 * @description get schedule by start date to end date for perticluer providerId
 * @param {*} proid 
 * @param {*} start 
 * @param {*} end 
 * @param {*} callback  
 */
const getSchduleByMonth = (proid, start, end, callback) => {
    db.get().collection(tablename)
        .find({
            "schedule.providerId": new ObjectID(proid),
            'date': {
                '$gte': start
            },
            'date': {
                '$lte': end
            }
        }).toArray((err, result) => {
            return callback(err, result);
        });

}

/**
 * @function deleteSchedule
 * @description delete schedule by scchedule id.
 * @param {*} id
 * @param {*} callback 
 */
const deleteSchedule = (id, callback) => {
    db.get().collection(tablename)
        .update(
            {
                'schedule':
                    {
                        '$elemMatch':
                            {
                                'scheduleId': new ObjectID(id)
                            }
                    }
            },
            {
                $pull: {
                    schedule: {
                        'scheduleId': new ObjectID(id)
                    }
                }
            },
            { multi: true },
            ((err, result) => {
                return callback(err, result);
            }));

}

const cancelSchedule = (id, callback) => {
    db.get().collection(tablename)
        .update(
            {
                'schedule':
                    {
                        '$elemMatch':
                            {
                                'booked.bookingId': id
                            }
                    }
            },
            {
                $pull: {
                    schedule: {
                        'booked.bookingId': id
                    }
                }
            },
            ((err, result) => {
                return callback(err, result);
            }));

}

const readByAggregate = (condition, callback) => {
    db.get().collection(tablename)
        .aggregate(condition, ((err, result) => { // aggregate method
            return callback(err, result);
        }));
}
const addDailyScheduleInBook = (dailySchedulesId, schedulArr, callback) => {
    db.get().collection(tablename)
        .findOneAndUpdate(
            { _id: new ObjectID(dailySchedulesId) },
            {

                $push: {
                    schedule: schedulArr
                }
            },
            ((err, result) => {
                return callback(err, result);
            }));
}
const updateSchedule = (dailySchedulesId, id, data, callback) => {
    db.get().collection(tablename)
        .findOneAndUpdate(
            {
                _id: new ObjectID(dailySchedulesId),
                'schedule.scheduleId': new ObjectID(id)

            },
            {
                $addToSet: {
                    'schedule.$.booked': data

                }
            },
            ((err, result) => {
                return callback(err, result);
            }));

}
const readByBookingId = (bookingId, callback) => {
    db.get().collection(tablename)
        .findOne({
            "schedule.booked.bookingId": bookingId
        }, ((err, result) => {
            return callback(err, result);
        }));

}
const findUpdate = (obj, callback) => {
    db.get().collection(tablename)
        .findOneAndUpdate(
            obj.query,
            obj.data,
            ((err, result) => {
                return callback(err, result);
            }));
}


module.exports = {
    readByBookingId,
    addDailyScheduleInBook,
    cancelSchedule,
    addDailySchedule,
    getSchduleByMonth,
    deleteSchedule,
    readByAggregate,
    updateSchedule,
    findUpdate
};
