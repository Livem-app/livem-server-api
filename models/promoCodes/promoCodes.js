'use strict'
const joi = require('joi')
const db = require('../mongodb');
const moment = require('moment')
const tableName = 'promoCodes'
const ObjectID = require('mongodb').ObjectID;

/** 
 * @function
 * @name insert a new offer 
 * @param {object} params - data coming from controller
 */
const postCouponCode = (offersData, callback) => {
    db.get().collection(tableName)
        .insert(offersData,
            (err, result) => {
                return callback(err, result);
            });
}

const validateCoupon = (requestData, callback) => {
    var citiIds = requestData.cityId;
    var zoneIds = requestData.cityId;
    var paymentMethod = requestData.paymentMethod;
    var vechileType = requestData.vehicleType;


    db.get().collection(tableName).aggregate([{
        $match: {
            "cities.cityId": {
                $in: [citiIds]
            },
            "category.catId": {
                $in: [requestData.categoryId]
            },
            // "zones":{
            //         $in: [citiIds]
            // },
            // "paymentMethod": { $in: [paymentMethod, 3] },
            "status": 2,
            // "promoType": "couponCode",
            "vehicleType": {
                $in: [vechileType, 4]
            },
            "code": requestData.couponCode
        }
    }, {
        $project: {
            ab: {
                $lt: ["$globalClaimCount", "$globalUsageLimit"],

            },
            _id: 1,
            title: 1,
            code: 1,
            adminLiability: 1,
            storeLiability: 1,
            promoType: 1,
            status: 1,
            cities: 1,
            zones: 1,
            minimumPurchaseValue: 1,
            applicableOn: 1,
            rewardType: 1,
            paymentMethod: 1,
            discount: 1,
            startTime: 1,
            endTime: 1,
            globalUsageLimit: 1,
            perUserLimit: 1,
            globalClaimCount: 1,
            vehicleType: 1,
            created: 1,
            users: 1,
        }
    }, {
        $match: {
            ab: {
                $eq: true
            }
        }
    }]).toArray(
        (err, result) => {
            console.log(err);
            console.log(result);
            return callback(err, result);
        });
}

const getAllCouponCodeByStatus = (requestData, callback) => {

    var condition = {
        'status': requestData.status

    }
    if (typeof (requestData.sSearch) !== "undefined" && requestData.sSearch != '') {

        var regexValue = new RegExp("^" + requestData.sSearch, "i")

        condition['$or'] = [{
            'title': regexValue
        }, {
            'code': regexValue
        }, {
            'cities.cityName': regexValue
        },];
    }

    if (typeof (requestData.cityId) !== "undefined" && requestData.cityId != '') {
        condition['cities.cityId'] = requestData.cityId;
    }
    if (typeof (requestData.dateTime) !== "undefined" && requestData.dateTime !== '') {
        var dateTime = requestData.dateTime;
        var dateTimeString = dateTime.split("-", 2);
        var startDate = dateTimeString[0];
        var endDate = dateTimeString[1];
        var startDateISO = new Date(startDate).toISOString();
        var endDateISO = new Date(endDate + ' 23:59:50').toISOString();
        console.log(startDate);
        console.log(endDate);
        console.log(startDateISO);
        console.log(endDateISO);
        condition['$or'] = [{
            'startTime': {
                $gte: startDateISO,
                $lte: endDateISO

            }
        },
        {
            'endTime': {
                $gte: startDateISO,
                $lte: endDateISO
            }
        }
        ];


    }
    console.log(condition);
    db.get().collection(tableName).find(condition).skip(requestData.offset).limit(requestData.limit)
        .toArray((err, result) => {
            console.log(result);
            return callback(err, result);
        });
}

const updateStatus = (params, callback) => {
    db.get().collection(tableName).findOneAndUpdate({
        "_id": {
            '$in': params.couponMongoIds
        }
    }, {
            $set: {
                "status": params.status
            }
        }, {
            returnOriginal: false
        },


        (err, result) => {
            return callback(err, result);
        });
}


// update promocode
const updatePromoCode = (params, callback) => {
    console.log("---------------------------------", params.startTime);
    db.get().collection(tableName).findOneAndUpdate({
        "_id": params.promoId
    }, {
            $set: {
                // "status": params.status
                "title": params.title,
                "code": params.code,
                "storeLiability": parseInt(params.storeLiability),
                "adminLiability": parseInt(params.adminLiability),
                "status": params.status,
                "statusString": 'active',
                "promoType": "couponCode",
                "cities": params.cities,
                "category": params.category,
                // "cityNames": params.cityNames,
                "zones": params.zones,
                "paymentMethod": params.paymentMethod,
                "paymentMethodString": params.paymentMethodString,
                "minimumPurchaseValue": parseInt(params.minimumPurchaseValue),
                "discount": params.discount,
                "startTime": params.startTime,
                "endTime": params.endTime,
                "globalUsageLimit": params.globalUsageLimit,
                "perUserLimit": params.perUserLimit, 
                "vehicleType": params.vehicleType,

                "created": params.formatted,
                "createdIso": new Date(),
                "applicableOn": params.applicableOn,
                "applicableOnString": params.applicableOnString,
                "termsAndConditions": params.termsAndConditions,
                "description": params.description,
                "howItWorks": params.howItWorks
            }
        }, {
            returnOriginal: false
        },


        (err, result) => {
            if (err)
                console.log(err)
            return callback(err, result);
        });
}



const increaseClaimCount = (userData, callback) => {
    db.get().collection(tableName).update({
        "_id": new ObjectID(userData.promoId)

    }, {
            $inc: {
                "globalClaimCount": 1
            }
        },


        (err, result) => {
            return callback(err, result);
        });
}

const getAllCouponCodeByCityId = (cityId, callback) => {

    db.get().collection(tableName).aggregate([{
        $match: {
            'status': 2,
            'promoType': 'couponCode',
            'cities.cityId': {
                $in: [cityId]
            }
        }
    }, {
        $project: {
            ab: {
                $lt: ["$globalClaimCount", "$globalUsageLimit"],

            },
            title: 1,
            code: 1,
            startTime: 1,
            endTime: 1,
            discount: 1,
            termsAndConditions: 1,
            description: 1,
            howItWorks: 1,
            minimumPurchaseValue: 1
        }
    }, {
        $match: {
            ab: {
                $eq: true
            }
        }
    }],

        (err, result) => {
            return callback(err, result);
        });
}

const getCountByStatus = (status, callBack) => {
    db.get().collection(tableName).count({
        'status': status, 
    },
        (err, result) => {
            return callBack(err, result);
        });
}
const getPromoCodeById = (promoId, callBack) => {
    db.get().collection(tableName).find({
        '_id': new ObjectID(promoId)
    })
        .toArray((err, result) => {
            return callBack(err, result);
        });
}

module.exports = {
    postCouponCode,
    validateCoupon,
    getAllCouponCodeByStatus,
    updateStatus,
    updatePromoCode,
    increaseClaimCount,
    getAllCouponCodeByCityId,
    getCountByStatus,
    getPromoCodeById
}