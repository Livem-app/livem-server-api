'use strict'

const tablename = 'documents';
const db = require('../mongodb')
const logger = require('winston');
const moment = require('moment');//date-time
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId

function post(data, callback) {
    db.get().collection(tablename)
        .insert(data, ((err, result) => {
            return callback(err, result);
        }));

}
const deleteById = (id, callback) => {
    db.get().collection(tablename)
        .remove({ master_id: new ObjectID(id) }, ((err, result) => {
            return callback(err, result);
        }));

}


module.exports = {
    post,
    deleteById
};
