'use strict'

const tablename = 'providerOnlineLogs';
const db = require('../mongodb')
const logger = require('winston');
const moment = require('moment');//date-time
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId


function updateByProviderId(data, callback) {
    db.get().collection(tablename)
        .findOneAndUpdate(
        { _id: new ObjectID(data.providerId) },
        {
            $push: {
                logs: {
                    status: data.status,
                    timeStamp: moment().unix(),
                    latitude: parseFloat(data.latitude),
                    longitude: parseFloat(data.longitude)
                }
            }
        },
        { upsert: true },
        ((err, result) => {
            return callback(err, result);
        }));
}


module.exports = { updateByProviderId };
