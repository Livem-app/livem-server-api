'use strict'

const tablename = 'payrollWeekly';
const db = require('../mongodb')
const logger = require('winston');
const moment = require('moment');//date-time
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId

const findUpdate = (obj, callback) => {
    db.get().collection(tablename)
        .findOneAndUpdate(
        obj.query,
        obj.data,
        { upsert: true },
        ((err, result) => {
            return callback(err, result);
        }));
}

const aggregate = (condition, callback) => {
    db.get().collection(tablename)
        .aggregate(condition, ((err, result) => { // aggregate method
            return callback(err, result);
        }));
}

const readAll = (condition, callback) => {
    db.get().collection(tablename)
        .find(condition).toArray((err, result) => {
            return callback(err, result);
        });
}

module.exports = {
    findUpdate,
    aggregate,
    readAll
};
