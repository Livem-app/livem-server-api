'use strict'
const joi = require('joi')
const db = require('../mongodb');
const moment = require('moment')
const tableName = 'cities'
const ObjectID = require('mongodb').ObjectID;
const logger = require('winston');



const cityDetailsByCityId = (cityIds, callBack) => {
    logger.error("checking error details  ------------------------------- ");
    logger.error(typeof (cityIds))


    db.get().collection(tableName)
        .find({
            "_id": { $in: cityIds }
        }).toArray((err, result) => {
            logger.error(err) 
            return callBack(err, result);
        });
}

const getAllCities = (params, callback) => {
    db.get().collection(tableName).find({})
        .toArray((err, result) => {
            return callback(err, result);
        });
}



module.exports = {
    cityDetailsByCityId,
    getAllCities
}