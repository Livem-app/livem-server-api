'use strict';
const logger = require('winston');
const redis = require("redis");
const provider = require('../../web/commonModels/provider');
const booking = require('../../web/commonModels/booking');
const request = require('request');

const client = redis.createClient({ host: '139.59.130.17', 'port': '6379', });
const subscriber = redis.createClient({ host: '139.59.130.17', 'port': '6379', });
client.auth('DMEdvUwXEmraNna4HaYxcw8kNYCuCa8JRFiTN7odkwGAoUZh2ZfWssWFsZC6BDus');
subscriber.auth('DMEdvUwXEmraNna4HaYxcw8kNYCuCa8JRFiTN7odkwGAoUZh2ZfWssWFsZC6BDus');
let is_first = true;
logger.info('Redis file loaded');


client.on('connect', function () {
    // logger.info('Redis connected');

});

const setex = (channel, expTime, data) => {
    client.setex(channel, expTime, data, (err, result) => {
    });
}
const delteSet = (channel) => {
    client.del(channel);

}

// subscriber.psubscribe('__key*__:*')
// subscriber.on("pmessage", function (pattern, channel, message) {
//     switch (channel) {
//         case '__keyevent@0__:del':
//             break;
//         case '__keyevent@0__:expired':

//             logger.info("logger", message);
//             var data = message.split("_");
//             if (data[0] == 'bid') {
//                 logger.silly("Booking Expired - Booking Id :", data[1]);
//                 booking.expired(data[1]);
//                 break;
//             }
//             if (data[0] == 'presence') {
//                 logger.silly("Provider TimeOut - ProviderId :", data[1]);
//                 provider.timeOut(data[1]);
//                 break;
//             }

//     }
// });
module.exports = {
    setex,
    delteSet,
    subscriber
}

