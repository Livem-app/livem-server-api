
'use strict';

const redis = require('./redis');
const request = require('superagent');

const subscriber = redis.subscriber;

subscriber.psubscribe('__key*__:*')
subscriber.on("pmessage", function (pattern, channel, message) {
    if (channel == '__keyevent@0__:expired') {
        request.post('https://api-test.livem.today/webhooks/redisEvent')
            .send({ pattern: pattern, channel: channel, message: message })
            .end(function (err, res) {
            });
    }
});
