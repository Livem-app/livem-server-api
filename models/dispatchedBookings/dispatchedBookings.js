'use strict'

const db = require('../mongodb')
const logger = require('winston');
const moment = require('moment');//date-time
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId

const tablename = 'dispatchedBookings';

const post = (data, callback) => {
    db.get().collection(tablename)
        .insert(data, ((err, result) => {
            return callback(err, result);
        }));

}

const readAll = (userType, callback) => {
    db.get().collection(tablename)
        .find({ usertype: userType }).toArray((err, result) => {
            return callback(null, result);
        });
}

const findOneAndUpdate = (obje, callback) => {
    db.get().collection(tablename)
        .findOneAndUpdate(obje.query, obje.data,
        ((err, result) => {
            return callback(err, result);
        }));
}

const readAllBooking = (condition, callback) => {
    db.get().collection(tablename)
        .find(condition).sort({ _id: -1 }).toArray((err, result) => {
            return callback(null, result);
        });
}

module.exports = {
    post,
    readAll,
    findOneAndUpdate,
    readAllBooking
};

