'use strict'

const logger = require('winston');

let db = require('../mongodb')


let tablename = 'smsLog';


function Insert(data, callback) {
    db.get().collection(tablename).insert([data], (err, result) => {
        return callback(err, result);
    });
}
function updateByMsgId(queryObj, callback) {
    db.get().collection(tablename).findOneAndUpdate(queryObj.query, queryObj.data, ((err, result) => {
        return callback(err, result);
    }));
}
const readAll = (con, callback) => {
    db.get().collection(tablename)
        .find(con).sort({ _id: -1 }).toArray((err, result) => {
            return callback(null, result);
        });
}
module.exports = {
    Insert,
    readAll,
    updateByMsgId
};
