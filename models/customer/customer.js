'use strict'

const tablename = 'slaves';
const db = require('../mongodb')
const logger = require('winston');
const moment = require('moment');//date-time
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId

const postSignUp = (query, data, callback) => {
    db.get().collection(tablename)
        .findOneAndUpdate(query, data, { upsert: true }, ((err, result) => {
            return callback(err, result);
        }));

}

const updateByUserId = (data, callback) => {
    db.get().collection(tablename)
        .findOneAndUpdate(
        { _id: new ObjectID(data.userId) },
        {
            $set: {
                status: data.status,
                loggedIn: true,
                fcmTopic: data.fcmTopic,
                lastActive: moment().unix(),
                mobileDevices: {
                    deviceId: data.deviceId,
                    deviceType: parseInt(data.deviceType) || '',
                    deviceTypeText: parseInt(data.deviceType) == 1 ? 'IOS' : "Android",
                    lastLogin: moment().unix(),
                    currentlyActive: true,
                    batteryPercentage: data.batteryPercentage || '',
                    appversion: data.appVersion || '',
                }
            }
        },
        { upsert: true }, ((err, result) => {
            return callback(err, result);
        }));
}

const updateAddress = (userId, addressId, callback) => {
    db.get().collection(tablename)
        .findOneAndUpdate(
        { _id: new ObjectID(userId) },
        {
            $push: {
                savedAddresses: {
                    id: new ObjectID(addressId)
                }
            }
        },
        ((err, result) => {
            return callback(err, result);
        }));
}

const deleteAddress = (userId, addressId, callback) => {
    db.get().collection(tablename)
        .findOneAndUpdate(
        {
            _id: new ObjectID(userId),
            'savedAddresses':
                {
                    '$elemMatch': {
                        'id': new ObjectID(addressId)
                    }
                }
        },
        {
            $pull: {
                savedAddresses: {
                    id: new ObjectID(addressId)
                }
            }
        },
        ((err, result) => {
            return callback(err, result);
        }));
}

const readReviewAndRating = (userId, start, callback) => {
    db.get().collection(tablename)
        .findOne(
        {
            _id: new ObjectID(userId),
        },
        {
            averageRating: 1, reviewCount: 1, about: 1, reviews: { $slice: [start, 5] }
        },
        ((err, result) => {
            return callback(err, result);
        }));
}
/**
 * @function read
 * @param {object} data 
 * @param {*} callback 
 */
const read = (data, callback) => {
    db.get().collection(tablename)
        .findOne(data, ((err, result) => {
            return callback(err, result);
        }));

}

const findOneAndUpdate = (userId, data, callback) => {
    db.get().collection(tablename)
        .findOneAndUpdate(
        { _id: new ObjectID(userId) },
        data,
        ((err, result) => {
            return callback(err, result);
        }));
}

const updateVerifyPhone = (userId, callback) => {
    db.get().collection(tablename)
        .findOneAndUpdate(
        { _id: new ObjectID(userId) },
        {
            $set: { status: 3, 'loginVerifiredBy.phone': 1 }
        },
        ((err, result) => {
            return callback(err, result);
        }));

}
const readByAggregate = (condition, callback) => {
    db.get().collection(tablename)
        .aggregate(condition, ((err, result) => { // aggregate method
            return callback(err, result);
        }));
}
const deleteById = (id, callback) => {
    db.get().collection(tablename)
        .remove({ _id: new ObjectID(id) }, ((err, result) => {
            return callback(err, result);
        }));

}
const readAll = (data, callback) => {
    db.get().collection(tablename)
        .find(data).toArray((err, result) => {
            return callback(err, result);
        });
}
//promocode

const getUserDetails = (userId, callBack) => {
    db.get().collection(tablename)
        .find({
            "_id": new ObjectID(userId),
        }).toArray((err, result) => {
            return callBack(err, result);
        });
}

const addReferralCampaingsToUser = (data, callBack) => {
    db.get().collection(tablename).update({
        "_id": new ObjectID(data.userId)
    }, {
            $set: {
                "referralCode": data.referralCode
            }
        },
        (err, result) => {
            return callBack(err, result);
        });
}

const updateUserData = (data, callBack) => {
    db.get().collection(tablename).update({
        "_id": new ObjectID(data.userId)
    },
        {
            $set: {
                "referralDiscountStatus": data.referralDiscountStatus,
                "campaignId": data.campaignId,
                "referralData": data.referrerData,
                "referralCode": data.referralCode,
                // "discountData": data.discountData
            }
        })
}

const updateUserReferralDiscountStatus = (data, callBack) => {
    db.get().collection(tablename).update({
        "_id": new ObjectID(data.userId)
    },
        {
            $set: {
                "referralDiscountStatus": data.status

            }
        })
}
module.exports = {
    postSignUp,
    updateByUserId,
    updateAddress,
    deleteAddress,
    readReviewAndRating,
    read,
    findOneAndUpdate,
    updateVerifyPhone,
    readByAggregate,
    deleteById,
    readAll,
    getUserDetails,
    addReferralCampaingsToUser,
    updateUserData,
    updateUserReferralDiscountStatus
};
