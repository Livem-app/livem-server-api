'use strict'

const tablename = 'provider';
const db = require('../mongodb')
const logger = require('winston');
const moment = require('moment');//date-time
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId

/**
 * @function postSignUp 
 * @description update data by email
 * @param {*} data 
 * @param {*} callback 
 */
const postSignUp = (data, callback) => {
    db.get().collection(tablename)
        .findOneAndUpdate({ email: data.email }, data, { upsert: true }, ((err, result) => {
            return callback(err, result);
        }));

}
/**
 * @function read
 * @description Read Provider by condition
 * @param {*} data 
 * @param {*} callback 
 */
const read = (data, callback) => {
    db.get().collection(tablename)
        .findOne(data, ((err, result) => {
            return callback(err, result);
        }));

}
/**
 * @function getProviderById
 * @description Find data by providerId
 * @param {*} id provider id 
 * @param {*} callback 
 */
const getProviderById = (id, callback) => {
    db.get().collection(tablename)
        .findOne({ _id: new ObjectID(id) }, ((err, result) => {
            return callback(err, result);
        }));

}
/**
 * @function updateByProviderId
 * @description Find data by providerId and update data
 * @param {*} data 
 * @param {*} callback 
 */
const updateByProviderId = (data, callback) => {
    db.get().collection(tablename)
        .findOneAndUpdate(
            { _id: new ObjectID(data.userId) },
            {
                $set: {
                    status: data.status,
                    loggedIn: data.loggedIn,
                    fcmTopic: data.fcmTopic,
                    lastActive: moment().unix(),
                    mobileDevices: {
                        deviceId: data.deviceId,
                        deviceType: parseInt(data.deviceType) || '',
                        lastLogin: moment().unix(),
                        currentlyActive: true,
                        batteryPercentage: data.batteryPercentage || '',
                        appversion: data.appVersion || '',
                        deviceTypeText: parseInt(data.deviceType) == 1 ? 'IOS' : "Android"
                    }
                }
            },
            { upsert: true }, ((err, result) => {
                return callback(err, result);
            }));
}

/**
 * @function updateVerifyPhone
 * @description Update verify phone number by id
 * @param {*} userId 
 * @param {*} callback 
 */
const updateVerifyPhone = (userId, callback) => {
    db.get().collection(tablename)
        .findOneAndUpdate(
            { _id: new ObjectID(userId) },
            {
                $set: { status: 1, 'loginVerifiredBy.phone': 1 }
            },
            ((err, result) => {
                return callback(err, result);
            }));

}
/**
 * @function updateProfile
 * @description provider profile data update by id
 * @param {*} userId providerId
 * @param {*} data 
 * @param {*} callback 
 */
const updateProfile = (userId, data, callback) => {
    db.get().collection(tablename)
        .findOneAndUpdate(
            { _id: new ObjectID(userId) },
            {
                $set: data
            },
            ((err, result) => {
                return callback(err, result);
            }));

}
/**
 * @function updateAddress
 * @description update saved address
 * @param {*} userId 
 * @param {*} addressId 
 * @param {*} callback 
 */
const updateAddress = (userId, addressId, callback) => {
    db.get().collection(tablename)
        .findOneAndUpdate(
            { _id: new ObjectID(userId) },
            {
                $push: {
                    savedAddresses: {
                        id: new ObjectID(addressId)
                    }
                }
            },
            ((err, result) => {
                return callback(err, result);
            }));
}
/**
 * @function deleteAddress
 * @description delete address id in savedAddresses
 * @param {*} userId 
 * @param {*} addressId 
 * @param {*} callback 
 */
const deleteAddress = (userId, addressId, callback) => {
    db.get().collection(tablename)
        .findOneAndUpdate(
            {
                _id: new ObjectID(userId),
                'savedAddresses':
                    {
                        '$elemMatch': {
                            'id': new ObjectID(addressId)
                        }
                    }
            },
            {
                $pull: {
                    savedAddresses: {
                        id: new ObjectID(addressId)
                    }
                }
            },
            ((err, result) => {
                return callback(err, result);
            }));
}
/**
 * @function readReviewAndRating
 * @description update review and rating by id
 * @param {*} userId 
 * @param {*} start 
 * @param {*} callback 
 */
const readReviewAndRating = (userId, start, callback) => {
    db.get().collection(tablename)
        .findOne(
            {
                _id: new ObjectID(userId),
            },
            {
                averageRating: 1, reviewCount: 1, reviews: { $slice: [start, 5] }
            },
            ((err, result) => {
                return callback(err, result);
            }));
}
/**
 * @function findOneAndUpdate
 * @description update data by id
 * @param {*} userId -providerId
 * @param {*} data 
 * @param {*} callback 
 */
const findOneAndUpdate = (userId, data, callback) => {
    db.get().collection(tablename)
        .findOneAndUpdate(
            { _id: new ObjectID(userId) },
            data,
            ((err, result) => {
                return callback(err, result);
            }));
}
/**
 * @function readBylatLong 
 * @description Find data by lat-long
 * @param {*} lat 
 * @param {*} long 
 * @param {*} callback 
 */
const readBylatLong = (lat, long, cityId, callback) => {
    let condition = [
        {
            $geoNear: {
                near: {
                    longitude: long,
                    latitude: lat
                },
                spherical: true,
                distanceField: "distance",
                maxDistance: 50000 / 6378137,
                distanceMultiplier: 6378137,
                query: { cityId: cityId, profileStatus: 1, loggedIn: true, status: { '$in': [3, 4, 7] } }
            }
        },
        { $sort: { _id: 1 } }
    ];
    db.get().collection(tablename)
        .aggregate(condition, (function (err, result) { // aggregate method
            return callback(err, result);
        }));
}
/**
 * @function readByAggregate
 * @description Find data by lat-long
 * @param {*} lat 
 * @param {*} long 
 * @param {*} query 
 * @param {*} callback 
 */
const readByAggregate = (lat, long, query, callback) => {
    let condition = [
        {
            $geoNear: {
                near: {
                    longitude: long,
                    latitude: lat
                },
                spherical: true,
                distanceField: "distance",
                maxDistance: 50000 / 6378137,
                distanceMultiplier: 6378137,
                query: query
            }
        },
        { $sort: { _id: 1 } }
    ];
    db.get().collection(tablename)
        .aggregate(condition, (function (err, result) { // aggregate method
            return callback(err, result);
        }));
}

const readAllByLimit = (data, callback) => {
    db.get().collection(tablename)
        .find(data.q).sort(data.sort || {}).limit(data.limit).skip(data.skip).toArray((err, result) => {
            return callback(err, result);
        });
}

const count = (data, callback) => {
    db.get().collection(tablename)
        .count(data, ((err, result) => {
            return callback(err, result);
        }));
}
const readAll = (data, callback) => {
    db.get().collection(tablename)
        .find(data).toArray((err, result) => {
            return callback(err, result);
        });
}
const readByAggregateMethod = (condition, callback) => {
    db.get().collection(tablename)
        .aggregate(condition, ((err, result) => { // aggregate method
            return callback(err, result);
        }));
}
const deleteProviderById = (id, callback) => {
    db.get().collection(tablename)
        .remove({ _id: new ObjectID(id) }, ((err, result) => {
            return callback(err, result);
        }));

}
const updateOne = (obj, callback) => {
    db.get().collection(tablename)
        .updateOne(
            obj.query,
            obj.data,
            ((err, result) => {
                return callback(err, result);
            }));
}
module.exports = {
    count,
    read,
    readAll,
    readBylatLong,
    readByAggregate,
    getProviderById,
    deleteAddress,
    updateByProviderId,
    postSignUp,
    updateVerifyPhone,
    updateProfile,
    updateAddress,
    readReviewAndRating,
    findOneAndUpdate,
    readAllByLimit,
    readByAggregateMethod,
    deleteProviderById,
    updateOne
};
