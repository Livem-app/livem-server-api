'use strict'

const db = require('../mongodb')
const logger = require('winston'); 
const moment = require('moment');//date-time
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId

const tablename = 'cancellationReasons';

function readAll(userType, callback) {
    db.get().collection(tablename)
        .find({ "res_for": userType == 1 ? "Passenger" : "Driver" }).toArray((err, result) => {
            return callback(null, result);
        });
}

const read = (data, callback) => {
    db.get().collection(tablename)
        .findOne(data, ((err, result) => {
            return callback(err, result);
        }));

}
module.exports = {
    readAll,
    read
};

