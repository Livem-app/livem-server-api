var config = process.env
const bookingStatus_notifyi = function (successCode, driver, language) {
    var return_;
    switch (successCode) {
        case 1:
            return_ = 'Thank you! We will inform you as soon as we find the best possible driver for your load.';
            break;
        case 2:
            return_ = 'Driver ' + driver.name + ' has accepted your load.';
            break;
        case 3:
            return_ = 'Driver has cancelled this delivery with the reason ' + driver.reason + '.';
            break;
        case 4:
            return_ = 'Your booking is cancelled, hope you book soon on ' + config.appName + ' again.';
            break;

        case 6:
            return_ = 'Driver ' + driver.name + ' is on his way to pickup your load , please be ready.';
            break;
        case 7:
            return_ = 'Driver ' + driver.name + ' has arrived at your address , please hand over the load to him';
            break;
        // case 8:
        //     return_ = 'Vehicle has been loaded and our driver is on his way to drop your load.';
        //     break;
        // case 9:
        //     return_ = 'Driver has Reached to Drop Location.';
        //     break;
        case 8:
            return_ = 'Driver ' + driver.name + ' is on his way to pickup your load , please be ready.';
            break;
        case 9:
            return_ = 'Driver has cancelled this delivery with the reason ' + driver.reason + '.';
            break;
        case 16:
            return_ = 'The load has been delivered.';
            break;

        case 10:
            return_ = 'Please check the receipt.';
            break;

        case 11:
            return_ = 'Sorry , we cannot take up your delivery request at this moment , all our drivers are busy. Please try again after sometime.';
            break;

        case 12:
            return_ = {
                title: "You got New Booking",
                body: "Customer Has requested shipment",
                sound: "default"
            };
            break;

        case 13:
            return_ = 'Customer has cancelled Booking.';
            break;


        case 14:
            return_ = 'You Got New Booking.';
            break;

        case 15:
            return_ = 'Verification code from ' + config.appName;
            break;

            case 111:
            return_ = 'Your account is logged in with some other device.';
            break;
        default:
            return_ = 'Unassigned.';

    }
}

const bookingStatus = function (successCode, language) {
    var return_;
    switch (successCode) {
        case 1:
            return_ = 'New Order';
            break;
        case 2:
            return_ = 'Cancelled by manager';
            break;
        case 3:
            return_ = 'Rejected by manager';
            break;
        case 4:
            return_ = 'Accepted by manager.';
            break;
        case 5:
            return_ = 'Order ready.';
            break;

        case 6:
            return_ = 'Order picked.';
            break;
        case 7:
            return_ = 'Order completed.';
            break;
        case 8:
            return_ = 'Accepted by driver.';
            break;
        case 9:
            return_ = 'Rejected by driver.';
            break;
        case 10:
            return_ = 'Driver on the way.';
            break;
        case 11:
            return_ = 'Driver arrived.';
            break;
        case 12:
            return_ = 'Driver journey Started'
            break;
        case 13:
            return_ = 'Driver reached at location.';
            break;
        case 14:
            return_ = 'Order delivered.';
            break;
        case 15:
            return_ = 'Order completed.';
            break;
        case 16:
            return_ = 'Cancelled by customer.';
            break;
        case 17:
            return_ = 'Cancelled by driver.';
            break;


        default:
            return_ = 'Unassigned.';

    }
    return return_;

}

module.exports = { bookingStatus_notifyi, bookingStatus }